<?php

namespace backend\controllers;

use backend\models\SignupForm;
use common\modules\user\models\BaseUser;
use Yii;
use common\models\User;
use backend\models\search\UserSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UsersController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createUser',
        'view'   => 'viewUser',
        'update' => 'updateUser',
        'index'  => 'indexUser',
        'delete' => 'deleteUser',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = User::className();
            $this->searchModel = UserSearch::className();

            $this->scenarios = array_replace($this->scenarios, [
                'update' => User::SCENARIO_UPDATE,
            ]);

            return true;
        }

        return false;
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (false === \Yii::$app->user->can($this->permissions['create'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на создание');
        }
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {

//            print_r("<pre>");
//            print_r(Yii::$app->request->post()); die;

            if($user = $model->signup()) {
                \Yii::$app->session->setFlash('success', 'Объект успешно создан и сохранен!');
                return $this->redirect(['view', 'id' => $user->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SignupForm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can($this->permissions['update'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на изменение');
        }

//        $model = $this->findModel($id);
        $model = SignupForm::getOne($id);

//        print_r("<pre>");
//        print_r($model);die;

        if ($model->load(Yii::$app->request->post()) && $model->update($id)) {

            if($model->save()) {
                \Yii::$app->session->setFlash('info', 'Изменения приняты!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
