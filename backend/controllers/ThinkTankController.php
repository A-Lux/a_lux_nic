<?php

namespace backend\controllers;

use Yii;
use common\models\ThinkTank;
use backend\models\search\ThinkTankSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;
use yii\web\UploadedFile;

/**
 * ThinkTankController implements the CRUD actions for ThinkTank model.
 */
class ThinkTankController extends BackendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'        => ['POST'],
                    'delete-image'  => ['POST', 'GET'],
                ],
            ],
        ];
    }

    /**
     * Lists all ThinkTank models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ThinkTankSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ThinkTank model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ThinkTank model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ThinkTank();

        if ($model->load(Yii::$app->request->post())) {
            $this->createImage($model);
            $this->createBannerImage($model);
            $model->slug = $this->generateCyrillicToLatin(strip_tags($model->title));

            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ThinkTank model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImage = $model->image;
        $oldBannerImage = $model->banner_image;

        if ($model->load(Yii::$app->request->post())) {
            $this->updateImage($model, $oldImage);
            $this->updateBannerImage($model, $oldBannerImage);
            $model->slug = $this->generateCyrillicToLatin(strip_tags($model->title));

            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ThinkTank model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes image an existing ThinkTank model.
     * If deletion is successful, the browser will be redirected to the 'form' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
    */
    public function actionDeleteImage($id)
    {
        $model = $this->findModel($id);

        if(!$this->deleteImage($model)){
            return false;
        }

        if($model->save(false)){
            return true;
        }
    }

    /**
     * Deletes image an existing ThinkTank model.
     * If deletion is successful, the browser will be redirected to the 'form' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeleteBannerImage($id)
    {
        $model = $this->findModel($id);

        if(!$this->deleteBannerImage($model)){
            return false;
        }

        if($model->save(false)){
            return true;
        }
    }


    /**
     * Finds the ThinkTank model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = ThinkTank::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
        throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }

    protected function createBannerImage($model)
    {
        $image = UploadedFile::getInstance($model, 'banner_image');
        $hash   = \Yii::$app->security->generateRandomString(10);
        if ($image !== null) {
            $time = time();
            $image->saveAs($model->path() . $time . '_' . $hash . '.' . $image->extension);
            $model->banner_image = $time . '_' . $hash . '.' . $image->extension;
        }
    }

    protected function updateBannerImage($model, $oldBannerImage)
    {
        $image = UploadedFile::getInstance($model, 'banner_image');
        $hash   = \Yii::$app->security->generateRandomString(10);

        if ($image == null) {
            $model->banner_image = $oldBannerImage;
        } else {
            $time = time();
            $image->saveAs($model->path() . $time . '_' . $hash . '.' . $image->extension);
            $model->banner_image = $time . '_' . $hash . '.' . $image->extension;

            if (!empty($oldBannerImage) && file_exists($model->path() . $oldBannerImage)) {
                unlink($model->path() . $oldBannerImage);
            }
        }
    }

    protected function deleteBannerImage($model)
    {
        $oldImage = $model->banner_image;

        if (file_exists($model->path() . $oldImage)) {
            $model->banner_image   = null;
            unlink($model->path() . $oldImage);
            return true;
        }
        return false;
    }
}
