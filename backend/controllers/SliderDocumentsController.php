<?php

namespace backend\controllers;

use common\models\i18n;
use Yii;
use common\models\SliderDocuments;
use backend\models\search\SliderDocumentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;
use yii\web\UploadedFile;

/**
 * SliderDocumentsController implements the CRUD actions for SliderDocuments model.
 */
class SliderDocumentsController extends BackendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SliderDocuments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SliderDocumentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SliderDocuments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SliderDocuments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SliderDocuments();

        if ($model->load(Yii::$app->request->post())) {
            $this->createFile($model);
            $this->createFileLanguage($model);

            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SliderDocuments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldFile = $model->file;
        $oldFileLang    = [];


        foreach (i18n::locales() as $locale){
            if(Yii::$app->language <> $locale){
                $property = 'file_' . $locale;
                $oldFileLang[$locale] = $model->{$property};

            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $this->updateFile($model, $oldFile);
            $this->fileLanguage($model, $oldFileLang);

            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SliderDocuments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SliderDocuments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = SliderDocuments::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
        throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }

    protected function createFile($model)
    {
        $files      = UploadedFile::getInstances($model, 'file');
        $path       = $model->pathFile();
        $jsonFiles  = [];
        $time       = time();

        foreach ($files as $key => $file){
            $hash   = \Yii::$app->security->generateRandomString(10);
            $file->saveAs($path . $time . '_' . $hash . '.' . $file->extension);
            $jsonFiles[]   = [
                'name'      => $file->baseName,
                'file'      => $time . '_' . $hash . '.' . $file->extension,
                'extension' => $file->extension,
            ];
        }

        $model->file    = json_encode($jsonFiles, JSON_UNESCAPED_UNICODE);

        return $model->file;
    }

    protected function createFileLanguage($model)
    {
        foreach (i18n::locales() as $locale){
            if (\Yii::$app->language <> $locale){
                $files      = UploadedFile::getInstances($model, 'file_' . $locale);
                $path       = $model->pathFile();
                $jsonFiles  = [];
                $time       = time();

                foreach ($files as $key => $file){
                    $hash   = \Yii::$app->security->generateRandomString(10);
                    $file->saveAs($path . $time . '_' . $hash . '.' . $file->extension);
                    $jsonFiles[]   = [
                        'name'      => $file->baseName,
                        'file'      => $time . '_' . $hash . '.' . $file->extension,
                        'extension' => $file->extension,
                    ];
                }

                $property = 'file_' . $locale;
                $model->{$property} = json_encode($jsonFiles, JSON_UNESCAPED_UNICODE);
            }
        }
    }

    protected function updateFile($model, $oldFile)
    {
        $files = UploadedFile::getInstances($model, 'file');

        if(empty($files)){
            $model->file    = $oldFile;
        }else{
            $path           = $model->pathFile();
            $jsonFiles      = [];
            $time           = time();
            foreach ($files as $key => $file){
                $hash   = \Yii::$app->security->generateRandomString(10);
                $file->saveAs($path . $time . '_' . $hash . '.' . $file->extension);
                $jsonFiles[]   = [
                    'name'      => $file->baseName,
                    'file'      => $time . '_' . $hash . '.' . $file->extension,
                    'extension' => $file->extension,
                ];
            }

            $oldFiles   = json_decode($oldFile, true);

            foreach ($oldFiles as $old){
                if(!empty($old['file'])){
                    if(file_exists($path . $old['file'])) {
                        unlink($path . $old['file']);
                    }
                }
            }
            $model->file    = json_encode($jsonFiles, JSON_UNESCAPED_UNICODE);
        }

        return $model->file;

    }

    protected function updateFileLanguage($model, $oldFile, $locale)
    {
        $files      = UploadedFile::getInstances($model, 'file_' . $locale);
        $property   = 'file_' . $locale;

        if(empty($files)){
            $model->{$property} = $oldFile;
        }else{
            $path       = $model->pathFile();
            $jsonFiles  = [];
            $time       = time();

            foreach ($files as $key => $file){
                $hash   = \Yii::$app->security->generateRandomString(10);
                $file->saveAs($path . $time . '_' . $hash . '.' . $file->extension);
                $jsonFiles[]   = [
                    'name'      => $file->baseName,
                    'file'      => $time . '_' . $hash . '.' . $file->extension,
                    'extension' => $file->extension,
                ];
            }

            $oldFiles   = json_decode($oldFile, true);

            foreach ($oldFiles as $old){
                if(!empty($old['file'])){
                    if(file_exists($path . $old['file'])) {
                        unlink($path . $old['file']);
                    }
                }
            }

            $model->{$property} = json_encode($jsonFiles, JSON_UNESCAPED_UNICODE);
        }
    }

    protected function fileLanguage($model, $oldFileLang)
    {
        foreach (i18n::locales() as $locale){
            if(\Yii::$app->language <> $locale){
                $this->updateFileLanguage($model, $oldFileLang[$locale], $locale);
            }
        }
    }
}
