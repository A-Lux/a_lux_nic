<?php

namespace backend\controllers;

use common\models\i18n;
use Yii;
use common\models\DocumentsConfidential;
use backend\models\search\DocumentsConfidentialSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;
use yii\web\UploadedFile;

/**
 * DocumentsConfidentialController implements the CRUD actions for DocumentsConfidential model.
 */
class DocumentsConfidentialController extends BackendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DocumentsConfidential models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DocumentsConfidentialSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DocumentsConfidential model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DocumentsConfidential model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DocumentsConfidential();

        if ($model->load(Yii::$app->request->post())) {
            $this->createFile($model);
            $this->createFileLanguage($model);

            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DocumentsConfidential model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldFile = $model->file;
        $oldFileLang    = [];
        foreach (i18n::locales() as $locale){
            if(Yii::$app->language <> $locale){
                $property = 'file_' . $locale;
                $oldFileLang[$locale] = $model->{$property};

            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $this->updateFile($model, $oldFile);
            $this->fileLanguage($model, $oldFileLang);

            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DocumentsConfidential model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes image an existing DocumentsConfidential model.
     * If deletion is successful, the browser will be redirected to the 'form' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
    */
    public function actionDeleteImage($id)
    {
        $model = $this->findModel($id);

        $this->deleteImage($model);

        if($model->save(false)){
        return $this->redirect(['update?id='.$id]);
        }
    }

    /**
     * Finds the DocumentsConfidential model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = DocumentsConfidential::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
        throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }

    protected function createFile($model)
    {
        $file = UploadedFile::getInstance($model, 'file');
        $hash   = \Yii::$app->security->generateRandomString(10);

        if($file != null){
            $time = time();
            $file->saveAs($model->pathFile() . $time . '_' . $hash . '.' . $file->extension);
            $model->file = $time . '_' . $hash . '.' . $file->extension;
            $model->extension = $file->extension;
        }
    }

    protected function updateFile($model, $oldFile)
    {
        $file = UploadedFile::getInstance($model, 'file');
        $hash   = \Yii::$app->security->generateRandomString(10);

        if($file == null){
            $model->file = $oldFile;
        }else{
            $time = time();
            $file->saveAs($model->pathFile() . $time . '_' . $hash . '.' . $file->extension);
            $model->file        = $time . '_' . $hash . '.' . $file->extension;
            $model->extension   = $file->extension;
            if(!($oldFile == null)){
                if(file_exists($model->pathFile() . $oldFile)) {
                    unlink($model->pathFile() . $oldFile);
                }
            }
        }
    }
}
