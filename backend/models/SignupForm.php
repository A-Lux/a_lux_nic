<?php
namespace backend\models;

use common\modules\user\models\BaseUser;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    const ROLE_GUEST     = 0;
    const ROLE_USER      = 1;
    const ROLE_OPERATOR  = 4;
    const ROLE_MANAGER   = 8;
    const ROLE_ADMIN     = 16;

    /**
     * User confirm your account
     */
    const STATUS_ACTIVE = 1;
    /**
     * User has been deleted
     */
    const STATUS_DELETED = -1;

    public $email;
    public $password;
    public $role;
    public $status;

    /**
     * Labels for fields
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email'           => 'Электронная почта',
            'password'        => 'Пароль',
            'role'            => 'Роль',
            'status'          => 'Статус',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User',
                'message' => Yii::t('main', 'Этот адрес электронной почты уже занят.')],

            ['password', 'required'],
            ['password', 'string', 'min' => 8],

            ['status', 'default', 'value' => BaseUser::STATUS_READY],
            ['status', 'in', 'range' => [BaseUser::STATUS_ACTIVE, BaseUser::STATUS_READY, BaseUser::STATUS_DELETED]],

            [
                ['role'], 'in',
                'range' => !\Yii::$app->user->isGuest && \Yii::$app->user->identity->role === BaseUser::ROLE_DEVELOPER
                    ? [BaseUser::ROLE_USER, BaseUser::ROLE_OPERATOR, BaseUser::ROLE_MANAGER, BaseUser::ROLE_ADMIN, BaseUser::ROLE_DEVELOPER]
                    : [BaseUser::ROLE_USER, BaseUser::ROLE_OPERATOR, BaseUser::ROLE_MANAGER, BaseUser::ROLE_ADMIN],
            ],
            [['role'], 'default', 'value' => BaseUser::ROLE_USER],

            [['status', 'role'], 'required'],
        ];
    }

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_ACTIVE  => 'active',
            self::STATUS_DELETED => 'deleted',
        ];
    }

    /**
     * @return array
     */
    public static function statusDescription()
    {
        return [
            self::STATUS_ACTIVE     => 'Активный',
            self::STATUS_DELETED    => 'Удаленный',
        ];
    }

    /**
     * @return array
     */
    public static function roles()
    {
        return [
            self::ROLE_GUEST        => 'guest',
            self::ROLE_USER         => 'user',
            self::ROLE_OPERATOR     => 'operator',
            self::ROLE_MANAGER      => 'manager',
            self::ROLE_ADMIN        => 'admin',
        ];
    }

    /**
     * @return array
     */
    public static function roleDescription()
    {
        return [
            self::ROLE_GUEST        => 'Гость',
            self::ROLE_USER         => 'Пользователь',
            self::ROLE_OPERATOR     => 'Оператор',
            self::ROLE_MANAGER      => 'Менеджер',
            self::ROLE_ADMIN        => 'Администратор',
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool|\common\modules\user\models\User
     */
    public function signup()
    {
        if ($this->validate()) {

            $user           = new User();
            $user->email    = $this->email;
            $user->role     = $this->role;
            $user->status   = $this->status;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateToken();
            $user->save();

            return $user;
        }

        return false;
    }

    public static function getOne($id)
    {
        return User::findOne(['id' => $id]);
    }

    /**
     * Signs user up.
     *
     * @param integer
     * @return bool|\common\modules\user\models\User
     */
    public function update($id)
    {
        if ($this->validate()) {
            $user           = self::getOne($id);
            $user->email    = $this->email;
            $user->role     = $this->role;
            $user->status   = $this->status;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateToken();
            $user->save();

            return $user;
        }

        return false;
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'NSCOM robot'])
            ->setTo($this->email)
            ->setSubject('Вы зарегистрировались на сайте NSCOM')
            ->send();
    }
}
