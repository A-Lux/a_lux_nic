<?php
namespace backend\models;

use common\modules\user\models\BaseUser;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * User form
 */
class UserForm extends Model
{
    public $email;
    public $password;
    public $role;
    public $status;

    /**
     * Labels for fields
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email'           => 'Электронная почта',
            'password'        => 'Пароль',
            'role'            => 'Роль',
            'status'          => 'Статус',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User',
                'message' => Yii::t('main', 'Этот адрес электронной почты уже занят.')],

            ['password', 'required'],
            ['password', 'string', 'min' => 8],

            ['status', 'default', 'value' => BaseUser::STATUS_READY],
            ['status', 'in', 'range' => [BaseUser::STATUS_ACTIVE, BaseUser::STATUS_READY, BaseUser::STATUS_DELETED]],

            [
                ['role'], 'in',
                'range' => !\Yii::$app->user->isGuest && \Yii::$app->user->identity->role === BaseUser::ROLE_DEVELOPER
                    ? [BaseUser::ROLE_USER, BaseUser::ROLE_OPERATOR, BaseUser::ROLE_MANAGER, BaseUser::ROLE_ADMIN, BaseUser::ROLE_DEVELOPER]
                    : [BaseUser::ROLE_USER, BaseUser::ROLE_OPERATOR, BaseUser::ROLE_MANAGER, BaseUser::ROLE_ADMIN],
            ],
            [['role'], 'default', 'value' => BaseUser::ROLE_USER],

            [['status', 'role'], 'required'],
        ];
    }

    public static function getOne($id)
    {
        return User::findOne(['id' => $id]);
    }

    /**
     * Signs user up.
     *
     * @param integer
     * @return bool|\common\modules\user\models\User
     */
    public function update($id)
    {
        if ($this->validate()) {
            $user           = self::getOne($id);
            $user->email    = $this->email;
            $user->role     = $this->role;
            $user->status   = $this->status;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateToken();
            $user->save();

            return $user;
        }

        return false;
    }
}
