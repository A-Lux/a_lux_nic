<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        "/backend/web/private/bower_components/select2/dist/css/select2.min.css",
    ];
    public $js = [
        "/backend/web/private/bower_components/select2/dist/js/select2.full.min.js",
        "/backend/web/js/app.js"
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'backend\assets\CDN_CodeSeven_Toastr_Asset',
    ];
}
