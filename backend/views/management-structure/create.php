<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ManagementStructure */

$this->title = 'Создание Структура управления';
$this->params['breadcrumbs'][] = ['label' => 'Структура управления', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="management-structure-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
