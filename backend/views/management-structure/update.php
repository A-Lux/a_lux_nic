<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ManagementStructure */

$this->title = 'Редактировать Структура управления: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Структура управления', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="management-structure-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
