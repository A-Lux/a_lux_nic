<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RequestVacancy */

$this->title = 'Редактировать : ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заявки на вакансию', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="request-vacancy-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
