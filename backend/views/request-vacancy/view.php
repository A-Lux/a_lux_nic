<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use common\models\RequestVacancy;

/* @var $this yii\web\View */
/* @var $model common\models\RequestVacancy */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заявки на вакансию', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="request-vacancy-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php $files = $model->getFile(); ?>
    <?php
    if($files != null)
        $file = ['attribute'=>'file','value' => Html::a($model->file,$files,['target'=>'_blank']),'format' => 'raw' ];
    else
        $file =  ['attribute'=>'file','value' => '']?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'status',
                'filter' => RequestVacancy::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(RequestVacancy::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'username',
            'phone',
//            'email:email',
            $file,
            'created_at',
        ],
    ]) ?>

</div>
