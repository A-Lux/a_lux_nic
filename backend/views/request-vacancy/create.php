<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RequestVacancy */

$this->title = 'Создание Заявки на вакансию';
$this->params['breadcrumbs'][] = ['label' => 'Заявки на вакансию', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-vacancy-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
