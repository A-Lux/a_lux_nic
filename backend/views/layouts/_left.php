<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->email; ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'По сайту', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Главное',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Меню', 'icon' => 'bookmark-o', 'url' => ['/menu']],
                            ['label' => 'Подменю', 'icon' => 'bookmark-o', 'url' => ['/submenu']],
                            ['label' => 'Преимущество на главном', 'icon' => 'bookmark-o', 'url' => ['/main-advantages']],
                        ],
                    ],
                    [
                        'label' => 'Раздел закупок',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Закупки', 'icon' => 'ship', 'url' => ['/procurement']],
                            ['label' => 'Файлы закупок', 'icon' => 'ship', 'url' => ['/procurement-files']],
                            ['label' => 'Документы', 'icon' => 'ship', 'url' => ['/procurement-documents']],
                            ['label' => 'Пользователи', 'icon' => 'ship', 'url' => ['/procurement-user']],
                        ],
                    ],
                    [
                        'label' => 'О нас',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Слайдеры', 'icon' => 'ship', 'url' => ['/sliders']],
                            ['label' => 'Документы в слайдерах', 'icon' => 'ship', 'url' => ['/slider-documents']],
                            ['label' => 'Структура управления', 'icon' => 'ship', 'url' => ['/management-structure']],
                            ['label' => 'Наша команда', 'icon' => 'ship', 'url' => ['/our-team']],
                            ['label' => 'Преимущества команды', 'icon' => 'ship', 'url' => ['/team-advantages']],
                            ['label' => 'Команда', 'icon' => 'ship', 'url' => ['/team']],
                            ['label' => 'Членство в организациях', 'icon' => 'ship', 'url' => ['/membership']],
                            ['label' => 'Путь развития', 'icon' => 'ship', 'url' => ['/about-path']],
                        ],
                    ],
                    [
                        'label' => 'Инвестиции',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Инвестиционный портфель', 'icon' => 'ship', 'url' => ['/investment-portfolio']],
                            ['label' => 'Подход «core-satellite»', 'icon' => 'ship', 'url' => ['/core-satellite']],
                            ['label' => 'Альтернативные инвестиции', 'icon' => 'ship', 'url' => ['/classes-investments']],
                            ['label' => 'Значение инвестиции', 'icon' => 'ship', 'url' => ['/values-investments']],
                            ['label' => 'Риск-менеджмент', 'icon' => 'ship', 'url' => ['/risk']],
                            ['label' => 'Система управления рисками', 'icon' => 'ship', 'url' => ['/risk-system']],
                        ],
                    ],
                    [
                        'label' => 'Информационный центр',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Новости/Публикации', 'icon' => 'ship', 'url' => ['/think-tank']],
                            ['label' => 'Think Tank', 'icon' => 'ship', 'url' => ['/think-tank-content']],
                        ],
                    ],
                    [
                        'label' => 'Карьера',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Карьера', 'icon' => 'bookmark-o', 'url' => ['/careers']],
                            ['label' => 'Заявки на вакансию', 'icon' => 'ship', 'url' => ['/request-vacancy']],
                        ],
                    ],
                ],
            ]
        ) ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Основное', 'options' => ['class' => 'header']],
                    ['label' => 'Переводы', 'icon' => 'language', 'url' => ['/source-message/']],
                    ['label' => 'Пользователи', 'icon' => 'user-circle-o', 'url' => ['/users']],
                    ['label' => 'Язык', 'icon' => 'language', 'url' => ['/language']],
                    ['label' => 'Баннер', 'icon' => 'columns', 'url' => ['/banner']],
                    ['label' => 'Логотип', 'icon' => 'image', 'url' => ['/logo']],
                    ['label' => 'Контакты', 'icon' => 'map', 'url' => ['/contact']],
                    ['label' => 'Конфиденциальные документы', 'icon' => 'bookmark-o', 'url' => ['/documents-confidential']],
//                    ['label' => 'Информация о клиентах', 'icon' => 'user-circle-o', 'url' => ['/client']],
//                    ['label' => 'Документы', 'icon' => 'image', 'url' => ['/documents']],
//                    ['label' => 'Бонусы', 'icon' => 'ship', 'url' => ['/bonus-system']],
//                    ['label' => 'Элекронная почта', 'icon' => 'ship', 'url' => ['/email-for-request']],

                ],
            ]
        ) ?>

    </section>

</aside>
