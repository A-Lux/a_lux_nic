<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProcurementDocuments */

$this->title = 'Редактировать : ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Документы закупок', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="procurement-documents-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
