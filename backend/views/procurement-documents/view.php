<?php

use yii\helpers\Html;
use common\widgets\MultilingualDetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ProcurementDocuments */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Документы закупок', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="procurement-documents-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php $files = $model->getFile(); ?>
    <?php
    if($files != null)
        $file = ['attribute'=>'file','value' => Html::a($model->file,$files,['target'=>'_blank']),'format' => 'raw' ];
    else
        $file =  ['attribute'=>'file','value' => '']?>

    <?= MultilingualDetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'procurement_id',
                'value' => function ($model) {
                    return
                        Html::a($model->procurement->name, ['view', 'id' => $model->procurement->id]);
                },
                'format' => 'raw',
            ],
            'name',
            $file,
            'date',
        ],
    ]) ?>

</div>
