<?php

use yii\helpers\Html;
use common\widgets\MultilingualDetailView;
use common\models\Banner;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Banner */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Баннер', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="banner-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= MultilingualDetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'menu_id',
                'value' => function ($model) {
                    return
                        Html::a($model->menu->title, ['/menu/view', 'id' => $model->menu->id]);
                },
                'format' => 'raw',
            ],
            'title',
//            [
//                'format' => 'html',
//                'attribute' => 'image',
//                'value' => function($model){
//                    return Html::img($model->getImage(), ['width'=>100]);
//                }
//            ],
//            'video',
//            [
//                'attribute' => 'status',
//                'filter' => Banner::statusDescription(),
//                'value' => function ($model) {
//                    return ArrayHelper::getValue(Banner::statusDescription(), $model->status);
//                },
//                'format' => 'raw',
//            ],
            'created_at',
        ],
    ]) ?>

</div>
