<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Banner;
use yii\helpers\ArrayHelper;
use common\models\Menu;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\BannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Баннер';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'menu_id',
                'filter'    => Menu::getList(),
                'value' => function ($model) {
                    return
                        Html::a($model->menu->title, ['/menu/view', 'id' => $model->menu->id]);
                },
                'format' => 'raw',
            ],
            [
                'format' => 'raw',
                'attribute' => 'title',
                'value' => function($model){
                    return $model->title;
                }
            ],
//            [
//                'format' => 'html',
//                'attribute' => 'image',
//                'value' => function($model){
//                    return Html::img($model->getImage(), ['width'=>100]);
//                }
//            ],
//            [
//                'attribute' => 'status',
//                'filter' => Banner::statusDescription(),
//                'value' => function ($model) {
//                    return ArrayHelper::getValue(Banner::statusDescription(), $model->status);
//                },
//                'format' => 'raw',
//            ],
            //'status',
            //'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
