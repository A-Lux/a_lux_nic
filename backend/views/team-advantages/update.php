<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TeamAdvantages */

$this->title = 'Редактировать Team Advantages: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Team Advantages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="team-advantages-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
