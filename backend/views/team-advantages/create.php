<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TeamAdvantages */

$this->title = 'Создание Team Advantages';
$this->params['breadcrumbs'][] = ['label' => 'Team Advantages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-advantages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
