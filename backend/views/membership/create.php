<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Membership */

$this->title = 'Создание Членство в организациях';
$this->params['breadcrumbs'][] = ['label' => 'Членство в организациях', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="membership-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
