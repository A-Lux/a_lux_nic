<?php

use yii\helpers\Html;
use common\widgets\MultilingualDetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SliderDocuments */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Документы в слайдерах', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="slider-documents-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= MultilingualDetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'slider_id',
                'value' => function ($model) {
                    return
                        Html::a($model->slider->title, ['/sliders/view', 'id' => $model->slider->id]);
                },
                'format' => 'raw',
            ],
            'name',
            'file',
            'date',
            'create_at',
        ],
    ]) ?>

</div>
