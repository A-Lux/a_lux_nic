<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SliderDocuments */

$this->title = 'Создание Документы в слайдерах';
$this->params['breadcrumbs'][] = ['label' => 'Документы в слайдерах', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-documents-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
