<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SliderDocuments */

$this->title = 'Редактировать Документы в слайдерах: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Документы в слайдерах', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="slider-documents-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
