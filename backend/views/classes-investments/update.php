<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ClassesInvestments */

$this->title = 'Редактировать : ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Альтернативные инвестиции', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="classes-investments-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
