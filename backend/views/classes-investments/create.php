<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ClassesInvestments */

$this->title = 'Создание Альтернативные инвестиции';
$this->params['breadcrumbs'][] = ['label' => 'Альтернативные инвестиции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classes-investments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
