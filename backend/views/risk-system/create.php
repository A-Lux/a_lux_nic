<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RiskSystem */

$this->title = 'Создание Система управления рисками';
$this->params['breadcrumbs'][] = ['label' => 'Система управления рисками', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="risk-system-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
