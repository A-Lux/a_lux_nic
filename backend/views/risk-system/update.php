<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RiskSystem */

$this->title = 'Редактировать Система управления рисками: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Система управления рисками', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="risk-system-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
