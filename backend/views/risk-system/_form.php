<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model common\models\RiskSystem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="risk-system-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Общие</a>
            </li>
            <? foreach (\common\models\i18n::locales() as $locale) : ?>
                <? if (Yii::$app->language <> $locale) : ?>
                    <li class="nav-item">
                        <a id="<?= $locale?>-tab" data-toggle="tab" href="#lang-<?= $locale?>" role="tab" aria-controls="<?= $locale?>" aria-selected="false" class="nav-link"><?= $locale?></a>
                    </li>
                <? endif; ?>
            <? endforeach; ?>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <? foreach (\common\models\i18n::locales() as $locale) : ?>
                <? if (Yii::$app->language <> $locale) : ?>
                    <div id="lang-<?= $locale?>" role="tabpanel" aria-labelledby="<?= $locale?>-tab" class="tab-pane fade">

                        <?= $form->field($model, 'name_' . $locale)->textInput(['maxlength' => true]) ?>

                    </div>
                <? endif; ?>
            <? endforeach; ?>

            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'image')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                        'showUpload'            => false ,
                        'initialPreview'        => $model->isNewRecord ? '' : $model->getImage(),
                        'initialPreviewAsData'  => true,
                        'initialCaption'        => $model->isNewRecord ? '': $model->image,
                        'showRemove'            => true ,
                        'deleteUrl'             => \yii\helpers\Url::to(['/risk-system/delete-image', 'id'=> $model->id]),
                    ] ,'options' => ['accept' => 'image/*'],
                ]); ?>

            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
