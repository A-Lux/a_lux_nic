<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ThinkTank */

$this->title = 'Редактировать Информационный центр: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Информационный центр', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="think-tank-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
