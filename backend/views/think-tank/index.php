<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\ThinkTank;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ThinkTankSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Информационный центр';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="think-tank-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'type',
                'filter' => ThinkTank::typeDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(ThinkTank::typeDescription(), $model->type);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status',
                'filter' => ThinkTank::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(ThinkTank::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'title',
//            'slug:ntext',
            //'description:ntext',
            //'content:ntext',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($model){
                    return Html::img($model->getImage(), ['width'=>100]);
                }
            ],
            //'metaName',
            //'metaDesc:ntext',
            //'metaKey:ntext',
            //'date',
            //'sort',
            //'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
