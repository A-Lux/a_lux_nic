<?php

use yii\helpers\Html;
use common\widgets\MultilingualDetailView;
use yii\helpers\ArrayHelper;
use common\models\ThinkTank;

/* @var $this yii\web\View */
/* @var $model common\models\ThinkTank */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Информационный центр', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="think-tank-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= MultilingualDetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'type',
                'filter' => ThinkTank::typeDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(ThinkTank::typeDescription(), $model->type);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status',
                'filter' => ThinkTank::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(ThinkTank::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'title',
            'slug:ntext',
            'description:ntext',
            'content:ntext',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($model){
                    return Html::img($model->getImage(), ['width'=>100]);
                }
            ],
            [
                'format' => 'html',
                'attribute' => 'banner_image',
                'value' => function($model){
                    return Html::img($model->getBannerImage(), ['width'=>100]);
                }
            ],
            'metaName',
            'metaDesc:ntext',
            'metaKey:ntext',
            'date',
            'created_at',
        ],
    ]) ?>

</div>
