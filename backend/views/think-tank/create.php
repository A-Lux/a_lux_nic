<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ThinkTank */

$this->title = 'Создание Информационный центр';
$this->params['breadcrumbs'][] = ['label' => 'Информационный центр', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="think-tank-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
