<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Sliders */

$this->title = 'Создание Слайдеры';
$this->params['breadcrumbs'][] = ['label' => 'Слайдеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sliders-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
