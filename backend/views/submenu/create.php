<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Submenu */

$this->title = 'Создание Submenu';
$this->params['breadcrumbs'][] = ['label' => 'Submenus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="submenu-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
