<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProcurementUser */

$this->title = 'Редактировать Procurement User: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Procurement Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="procurement-user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
