<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProcurementUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Procurement Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="procurement-user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать Procurement User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'legal_name',
            'bin',
            'postcode',
            'city',
            //'address',
            //'username',
            //'position',
            //'id_number',
            //'who_issued',
            //'date_issue',
            //'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
