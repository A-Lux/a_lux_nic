<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProcurementUser */

$this->title = 'Создание Procurement User';
$this->params['breadcrumbs'][] = ['label' => 'Procurement Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="procurement-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
