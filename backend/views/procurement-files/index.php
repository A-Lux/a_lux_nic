<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProcurementFilesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Файлы закупок';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="procurement-files-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'procurement_id',
                'value' => function ($model) {
                    return
                        Html::a($model->procurement->name, ['view', 'id' => $model->procurement->id]);
                },
                'format' => 'raw',
            ],
            'name',
//            'file',
//            'date',
            'create_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
