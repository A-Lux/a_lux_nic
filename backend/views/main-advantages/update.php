<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MainAdvantages */

$this->title = 'Редактировать Преимущество на главном: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Преимущество на главном', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="main-advantages-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
