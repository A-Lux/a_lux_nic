<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MainAdvantages */

$this->title = 'Создание Преимущество на главном';
$this->params['breadcrumbs'][] = ['label' => 'Преимущество на главном', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-advantages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
