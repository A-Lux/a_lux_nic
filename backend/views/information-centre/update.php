<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\InformationCentre */

$this->title = 'Редактировать Information Centre: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Information Centres', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="information-centre-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
