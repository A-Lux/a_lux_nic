<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\InformationCentre */

$this->title = 'Создание Information Centre';
$this->params['breadcrumbs'][] = ['label' => 'Information Centres', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="information-centre-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
