<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CoreSatellite */

$this->title = 'Редактировать Подход «core-satellite»: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Подход «core-satellite»', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="core-satellite-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
