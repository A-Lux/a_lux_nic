<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CoreSatellite */

$this->title = 'Создание Подход «core-satellite»';
$this->params['breadcrumbs'][] = ['label' => 'Подход «core-satellite»', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-satellite-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
