<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ThinkTankContent */

$this->title = 'Создание Think Tank Content';
$this->params['breadcrumbs'][] = ['label' => 'Think Tank Contents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="think-tank-content-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
