<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ThinkTankContent */

$this->title = 'Редактировать Think Tank Content: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Think Tank Contents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="think-tank-content-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
