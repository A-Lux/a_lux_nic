<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\InvestmentPortfolio */

$this->title = 'Создание Инвестиционный портфель';
$this->params['breadcrumbs'][] = ['label' => 'Инвестиционный портфель', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investment-portfolio-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
