<?php

use yii\helpers\Html;
use common\widgets\MultilingualDetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ValuesInvestments */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Значение инвестиции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="values-investments-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= MultilingualDetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'classes_id',
                'value' => function ($model) {
                    return
                        Html::a($model->classes->title, ['/classes-investments/view', 'id' => $model->classes->id]);
                },
                'format' => 'raw',
            ],
            'name',
            'content:ntext',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function ($model) {
                    return Html::img($model->getImage(), ['width' => 100]);
                }
            ],
            'created_at',
        ],
    ]) ?>

</div>