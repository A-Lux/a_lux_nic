<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ValuesInvestments */

$this->title = 'Создание Значение инвестиции';
$this->params['breadcrumbs'][] = ['label' => 'Значение инвестиции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="values-investments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
