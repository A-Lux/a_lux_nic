<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use common\models\ClassesInvestments;

/* @var $this yii\web\View */
/* @var $model common\models\ValuesInvestments */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="values-investments-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Общие</a>
            </li>
            <? foreach (\common\models\i18n::locales() as $locale) : ?>
                <? if (Yii::$app->language <> $locale) : ?>
                    <li class="nav-item">
                        <a id="<?= $locale ?>-tab" data-toggle="tab" href="#lang-<?= $locale ?>" role="tab" aria-controls="<?= $locale ?>" aria-selected="false" class="nav-link"><?= $locale ?></a>
                    </li>
                <? endif; ?>
            <? endforeach; ?>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <? foreach (\common\models\i18n::locales() as $locale) : ?>
                <? if (Yii::$app->language <> $locale) : ?>
                    <div id="lang-<?= $locale ?>" role="tabpanel" aria-labelledby="<?= $locale ?>-tab" class="tab-pane fade">

                        <?= $form->field($model, 'name_' . $locale)->textInput(['maxlength' => true]) ?>
                        <? if (!$model->isNewRecord) : ?>
                            <admin-component :classs="'content-<?= $locale ?>'" :array='<?= ($model->{'content_' . $locale}) ?>'></admin-component>
                        <? else : ?>
                            <admin-component :classs="'content-<?= $locale ?>'" :array="[]"></admin-component>
                        <? endif; ?>

                        <?= $form->field($model, 'content_' . $locale)->textInput(['type' => "hidden", 'class' => 'content_' . $locale])->label(false) ?>
                    </div>
                <? endif; ?>
            <? endforeach; ?>

            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <?= $form->field($model, 'classes_id')->dropDownList(ClassesInvestments::getList()) ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <? if (!$model->isNewRecord) : ?>
                    <admin-component :classs="'content-ru'" :array='<?= $model->content ?>'></admin-component>
                <? else : ?>
                    <admin-component :classs="'content-ru'" :array='[]'></admin-component>
                <? endif; ?>

                <?= $form->field($model, 'content')->textInput(['type' => 'hidden', 'class' => "content_ru"])->label(false) ?>

                <?= $form->field($model, 'image')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                        'showUpload'            => false,
                        'initialPreview'        => $model->isNewRecord ? '' : $model->getImage(),
                        'initialPreviewAsData'  => true,
                        'initialCaption'        => $model->isNewRecord ? '' : $model->image,
                        'showRemove'            => true,
                        'deleteUrl'             => \yii\helpers\Url::to(['/values-investments/delete-image', 'id' => $model->id]),
                    ], 'options' => ['accept' => 'image/*'],
                ]); ?>

            </div>
        </div>
    </div>

    <div class=" form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
<script>
    document.querySelector(' [type="submit" ]').addEventListener('click', function(e) {
        e.preventDefault();
        let contentArray = [];
        <? foreach (\common\models\i18n::locales() as $locale) : ?>
            <? if (Yii::$app->language <> $locale) : ?>
                let content<?= $locale ?> = [];
                document.querySelectorAll('.content-' + '<?= $locale ?>').forEach(input => {
                    content<?= $locale ?>.push(input.value);
                    console.log(content<?= $locale ?>);
                });
                document.querySelector('.content_' + '<?= $locale ?>').value = JSON.stringify(content<?= $locale ?>);
            <? endif; ?>
        <? endforeach; ?>
        document.querySelectorAll('.content-ru').forEach(input => {
            contentArray.push(input.value);
        });
        document.querySelector('.content_ru').value = JSON.stringify(contentArray);
        document.getElementById('w0').submit();
    });
</script>
<style>
    .fc-0 {
        display: flex;
        flex-wrap: nowrap;
    }
</style>