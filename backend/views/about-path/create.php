<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AboutPath */

$this->title = 'Создание Путь развития';
$this->params['breadcrumbs'][] = ['label' => 'Путь развития', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-path-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
