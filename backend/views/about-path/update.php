<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AboutPath */

$this->title = 'Редактировать Путь развития: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Путь развития', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="about-path-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
