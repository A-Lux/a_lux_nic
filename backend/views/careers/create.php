<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Careers */

$this->title = 'Создание Карьера';
$this->params['breadcrumbs'][] = ['label' => 'Карьера', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="careers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
