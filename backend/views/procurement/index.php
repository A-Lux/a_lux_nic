<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Procurement;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProcurementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Закупки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="procurement-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'parent_id',
                'value' => function ($model) {
                    return
                        $model->parent
                            ? Html::a($model->parent->name, ['view', 'id' => $model->parent->id])
                            : '';
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status',
                'filter' => Procurement::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Procurement::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'name',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($model){
                    return Html::img($model->getImage(), ['width'=>100]);
                }
            ],
//            'description:ntext',
            //'content:ntext',
            //'file',
            'create_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
