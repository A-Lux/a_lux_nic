<?php

use yii\helpers\Html;
use common\widgets\MultilingualDetailView;
use yii\helpers\ArrayHelper;
use common\models\Procurement;

/* @var $this yii\web\View */
/* @var $model common\models\Procurement */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Закупки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="procurement-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php $files = $model->getFile(); ?>
    <?php
    if($files != null)
        $file = ['attribute'=>'file','value' => Html::a($model->file,$files,['target'=>'_blank']),'format' => 'raw' ];
    else
        $file =  ['attribute'=>'file','value' => '']?>

    <?= MultilingualDetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date',
            [
                'attribute' => 'parent_id',
                'value' => function ($model) {
                    return
                        Html::a($model->parent->name, ['view', 'id' => $model->parent->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return ArrayHelper::getValue(Procurement::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'name',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($model){
                    return Html::img($model->getImage(), ['width'=>100]);
                }
            ],
            'description:ntext',
            'content:ntext',
            'create_at',
        ],
    ]) ?>

</div>
