<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Procurement;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Procurement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="procurement-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Общие</a>
            </li>
            <? foreach (\common\models\i18n::locales() as $locale) : ?>
                <? if (Yii::$app->language <> $locale) : ?>
                    <li class="nav-item">
                        <a id="<?= $locale?>-tab" data-toggle="tab" href="#lang-<?= $locale?>" role="tab" aria-controls="<?= $locale?>" aria-selected="false" class="nav-link"><?= $locale?></a>
                    </li>
                <? endif; ?>
            <? endforeach; ?>

            <li class="nav-item">
                <a id="content-tab" data-toggle="tab" href="#content" role="tab" aria-controls="content" aria-selected="false" class="nav-link">Контент</a>
            </li>
            <? foreach (\common\models\i18n::locales() as $locale) : ?>
                <? if (Yii::$app->language <> $locale) : ?>
                    <li class="nav-item">
                        <a id="content-<?= $locale?>-tab" data-toggle="tab" href="#lang-<?= $locale?>-content" role="tab" aria-controls="<?= $locale?>" aria-selected="false" class="nav-link">Контент (<?= $locale?>)</a>
                    </li>
                <? endif; ?>
            <? endforeach; ?>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <? foreach (\common\models\i18n::locales() as $locale) : ?>
                <? if (Yii::$app->language <> $locale) : ?>
                    <div id="lang-<?= $locale?>" role="tabpanel" aria-labelledby="<?= $locale?>-tab" class="tab-pane fade">

                        <?= $form->field($model, 'name_' . $locale)->textInput(['maxlength' => true]) ?>

                    </div>
                <? endif; ?>
            <? endforeach; ?>

            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade active in">

                <?= $form->field($model, 'parent_id')->dropDownList(Procurement::listParent(), [
                    'prompt' => ' '
                ]) ?>

                <?= $form->field($model, 'status')->dropDownList(Procurement::statusDescription()) ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <label class = "control-label"> Дата создания</label>
                <?php
                echo DatePicker::widget([
                    'name'=>'Procurement[date]',
                    'value' => $model->date,
                    'options' => ['placeholder' => 'Выберите дату...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]);
                ?> <br>

                <?= $form->field($model, 'image')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                        'showUpload'            => false ,
                        'initialPreview'        => $model->isNewRecord ? '' : $model->getImage(),
                        'initialPreviewAsData'  => true,
                        'initialCaption'        => $model->isNewRecord ? '': $model->image,
                        'showRemove'            => true ,
                        'deleteUrl'             => \yii\helpers\Url::to(['/procurement/delete-image', 'id'=> $model->id]),
                    ] ,'options' => ['accept' => 'image/*'],
                ]); ?>

            </div>

            <? foreach (\common\models\i18n::locales() as $locale) : ?>
                <? if (Yii::$app->language <> $locale) : ?>
                    <div id="lang-<?= $locale?>-content" role="tabpanel" aria-labelledby="content-<?= $locale?>-tab" class="tab-pane fade">

                        <?= $form->field($model, 'description_' . $locale)->textarea(['rows' => 6]) ?>

                        <?= $form->field($model, 'content_' . $locale)->widget(CKEditor::className(), [
                            'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                                'preset' => 'full',    // basic, standard, full
                                'inline' => false,      //по умолчанию false
                            ])
                        ]); ?>

                    </div>
                <? endif; ?>
            <? endforeach; ?>

            <div id="content" role="tabpanel" aria-labelledby="content-tab" class="tab-pane fade">

                <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full',    // basic, standard, full
                        'inline' => false,      //по умолчанию false
                    ])
                ]); ?>

<!--                --><?//= $form->field($model, 'file')->widget(FileInput::classname(), [
//                    'pluginOptions' => [
//                        'showUpload'            => false ,
//                        'initialCaption'        => $model->isNewRecord ? '': $model->file,
//                    ] ,'options' => ['accept' => 'file/*'],
//                ]); ?>

            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
