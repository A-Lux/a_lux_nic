import Vue from "vue";
import Vuex from "vuex";
import appService from "./service/appService.js";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    value: "",
  },
  mutations: {
    SET_VALUES(state, value) {
      state.value = value;
    },
  },
  actions: {
    setValues({ commit }, value) {
      commit(SET_VALUES, value);
    },
  },
  getters: {},
});
