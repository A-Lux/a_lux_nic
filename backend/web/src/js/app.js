import Vue from "vue";
import store from "./store.js";

// components
import Admin from "./components/Admin.vue";
import BaseInput from "./components/base/BaseInput.vue";

Vue.component("admin-component", Admin);
Vue.component("BaseInput", BaseInput);
const app = new Vue({
  el: "#myTabContent",
  store,
});
