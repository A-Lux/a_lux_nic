import axios from "axios";

const apiClient = axios.create({
  baseURL: window.location.origin,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

export default {
  getInputs() {},
  postInputs() {},
};
