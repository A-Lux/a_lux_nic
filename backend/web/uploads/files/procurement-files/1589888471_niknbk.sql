-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 15 2020 г., 10:24
-- Версия сервера: 5.7.20
-- Версия PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `niknbk`
--

-- --------------------------------------------------------

--
-- Структура таблицы `about_path`
--

CREATE TABLE `about_path` (
  `id` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `sort` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `about_path`
--

INSERT INTO `about_path` (`id`, `title`, `content`, `sort`, `created_at`) VALUES
(1, '2012', '<p>Основание</p>\r\n', NULL, '2020-05-15 06:57:00'),
(2, '2013', '<p>Выбор стратегических партнеров по мандатам частного капитала и хедж-фондов</p>\r\n', NULL, '2020-05-15 06:57:00'),
(3, '2014', '<p>Вступление в IFSWF</p>\r\n', NULL, '2020-05-15 06:57:00'),
(4, '2015', '<p>Первые инвистиции в хедж-фонды и фонды частного капитала</p>\r\n', NULL, '2020-05-15 06:57:00'),
(5, '2016', '<p>Запуск программы инвестирования в недвижимость</p>\r\n', NULL, '2020-05-15 06:58:00'),
(6, '2017', '<p>Начало прямого инвестирования в хедж-фонды</p>\r\n', NULL, '2020-05-15 06:58:00'),
(7, '2018', '<p>Разработка подхода Reference Portfolio</p>\r\n', NULL, '2020-05-15 06:58:00'),
(8, '2019', '<p>Переход к подходу Reference Portfolio</p>\r\n', NULL, '2020-05-15 06:59:00');

-- --------------------------------------------------------

--
-- Структура таблицы `about_path_translation`
--

CREATE TABLE `about_path_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `about_path_translation`
--

INSERT INTO `about_path_translation` (`id`, `language`, `title`, `content`) VALUES
(1, 'en', '', ''),
(1, 'kk', '', ''),
(1, 'ru', '2012', '<p>Основание</p>\r\n'),
(2, 'en', '', ''),
(2, 'kk', '', ''),
(2, 'ru', '2013', '<p>Выбор стратегических партнеров по мандатам частного капитала и хедж-фондов</p>\r\n'),
(3, 'en', '', ''),
(3, 'kk', '', ''),
(3, 'ru', '2014', '<p>Вступление в IFSWF</p>\r\n'),
(4, 'en', '', ''),
(4, 'kk', '', ''),
(4, 'ru', '2015', '<p>Первые инвистиции в хедж-фонды и фонды частного капитала</p>\r\n'),
(5, 'en', '', ''),
(5, 'kk', '', ''),
(5, 'ru', '2016', '<p>Запуск программы инвестирования в недвижимость</p>\r\n'),
(6, 'en', '', ''),
(6, 'kk', '', ''),
(6, 'ru', '2017', '<p>Начало прямого инвестирования в хедж-фонды</p>\r\n'),
(7, 'en', '', ''),
(7, 'kk', '', ''),
(7, 'ru', '2018', '<p>Разработка подхода Reference Portfolio</p>\r\n'),
(8, 'en', '', ''),
(8, 'kk', '', ''),
(8, 'ru', '2019', '<p>Переход к подходу Reference Portfolio</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `banner`
--

INSERT INTO `banner` (`id`, `menu_id`, `title`, `image`, `video`, `status`, `created_at`) VALUES
(1, 1, '<div class=\"tagline-top\">инвестиции во благо будущих поколений</div>\r\n\r\n<div class=\"span-fix\">и благосостояние казахстана</div>\r\n', '', '1589523931_vid.mp4', 2, '2020-05-15 06:25:00');

-- --------------------------------------------------------

--
-- Структура таблицы `banner_translation`
--

CREATE TABLE `banner_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `banner_translation`
--

INSERT INTO `banner_translation` (`id`, `language`, `title`) VALUES
(1, 'en', ''),
(1, 'kk', ''),
(1, 'ru', '<div class=\"tagline-top\">инвестиции во благо будущих поколений</div>\r\n\r\n<div class=\"span-fix\">и благосостояние казахстана</div>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `careers`
--

CREATE TABLE `careers` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `careers`
--

INSERT INTO `careers` (`id`, `title`, `content`, `image`, `created_at`) VALUES
(1, 'Человеческий ресурс как основополагающая ценность нашей динамично развивающейся Корпорации', '<p>Сотрудники Корпорации &ndash; это команда профессионалов, реализующая стратегическую цель Корпорации &ndash; быть высокоэффективным управляющим суверенными активами.</p>\r\n\r\n<p>Корпорация нацелена на инвестирование в человеческие ресурсы для развития и поддержки высококвалифицированных сотрудников, которые обеспечивают достижение результатов должным образом.</p>\r\n\r\n<p>Приоритетными целями Корпорации являются создание оптимальных условий труда и стимулирование к постоянному повышению профессиональных навыков, а также обеспечение социальной защищенности всех сотрудников.</p>\r\n', '1589527259_carrer-img-1.png', '2020-05-15 07:20:00'),
(2, 'Твоя карьера в АО «Национальная инвестиционная корпорация Национального Банка Казахстана»', '<p>Корпорация приветствует кандидатов, обладающих необходимыми навыками и компетенциями, готовых присоединиться к команде экспертов и стремящихся к достижению поставленных целей, чьи жизненные принципы и взгляды соответствуют корпоративной культуре Корпорации.</p>\r\n\r\n<p>В рамках подбора персонала Корпорация использует международные практики, направленные на поиск соответствующих кандидатов и укрепление имиджа компании как надежного работодателя.</p>\r\n', '1589527330_carrer-img-2.png', '2020-05-15 07:22:00');

-- --------------------------------------------------------

--
-- Структура таблицы `careers_translation`
--

CREATE TABLE `careers_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `careers_translation`
--

INSERT INTO `careers_translation` (`id`, `language`, `title`, `content`) VALUES
(1, 'en', '', ''),
(1, 'kk', '', ''),
(1, 'ru', 'Человеческий ресурс как основополагающая ценность нашей динамично развивающейся Корпорации', '<p>Сотрудники Корпорации &ndash; это команда профессионалов, реализующая стратегическую цель Корпорации &ndash; быть высокоэффективным управляющим суверенными активами.</p>\r\n\r\n<p>Корпорация нацелена на инвестирование в человеческие ресурсы для развития и поддержки высококвалифицированных сотрудников, которые обеспечивают достижение результатов должным образом.</p>\r\n\r\n<p>Приоритетными целями Корпорации являются создание оптимальных условий труда и стимулирование к постоянному повышению профессиональных навыков, а также обеспечение социальной защищенности всех сотрудников.</p>\r\n'),
(2, 'en', '', ''),
(2, 'kk', '', ''),
(2, 'ru', 'Твоя карьера в АО «Национальная инвестиционная корпорация Национального Банка Казахстана»', '<p>Корпорация приветствует кандидатов, обладающих необходимыми навыками и компетенциями, готовых присоединиться к команде экспертов и стремящихся к достижению поставленных целей, чьи жизненные принципы и взгляды соответствуют корпоративной культуре Корпорации.</p>\r\n\r\n<p>В рамках подбора персонала Корпорация использует международные практики, направленные на поиск соответствующих кандидатов и укрепление имиджа компании как надежного работодателя.</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `classes_investments`
--

CREATE TABLE `classes_investments` (
  `id` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `classes_investments`
--

INSERT INTO `classes_investments` (`id`, `title`, `content`, `created_at`) VALUES
(1, 'ЧАСТНЫЙ КАПИТАЛ', '<p>Инвестиции в частный капитал, направленные на создание диверсифицированного портфеля с точки зрения стратегии, отрасли и географии. Инвестиции в частный капитал относятся к динамичной категории активов.</p>\r\n', NULL),
(2, 'НЕДВИЖИМОСТЬ', '<p>Инвестиции в фонды, основной деятельностью которых является инвестирование в объекты недвижимости, охватывающие различные категории недвижимости, стратегии и географии. Инвестиции в фонды недвижимости относятся к категории активов-диверсификаторов.</p>\r\n', NULL),
(3, 'ХЕДЖ-ФОНДЫ', '<p>Инвестиционные фонды, созданные для инвестирования в соответствии с определенной стратегией и в различные типы финансовых инструментов. Инвестиции в хедж-фонды относятся к категории медиаторов риска.</p>\r\n', NULL),
(4, 'ИНФРАСТРУКТУРА', 'Инвестиции в фонды, основным направлением инвестирование которых являются объекты инфраструктуры.\r\n            Инвестиции в инфраструктуру относятся к категории активов-диверсификаторов', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `classes_investments_translation`
--

CREATE TABLE `classes_investments_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `classes_investments_translation`
--

INSERT INTO `classes_investments_translation` (`id`, `language`, `title`, `content`) VALUES
(1, 'en', '', ''),
(1, 'kk', '', ''),
(1, 'ru', 'ЧАСТНЫЙ КАПИТАЛ', '<p>Инвестиции в частный капитал, направленные на создание диверсифицированного портфеля с точки зрения стратегии, отрасли и географии. Инвестиции в частный капитал относятся к динамичной категории активов.</p>\r\n'),
(2, 'en', '', ''),
(2, 'kk', '', ''),
(2, 'ru', 'НЕДВИЖИМОСТЬ', '<p>Инвестиции в фонды, основной деятельностью которых является инвестирование в объекты недвижимости, охватывающие различные категории недвижимости, стратегии и географии. Инвестиции в фонды недвижимости относятся к категории активов-диверсификаторов.</p>\r\n'),
(3, 'en', '', ''),
(3, 'kk', '', ''),
(3, 'ru', 'ХЕДЖ-ФОНДЫ', '<p>Инвестиционные фонды, созданные для инвестирования в соответствии с определенной стратегией и в различные типы финансовых инструментов. Инвестиции в хедж-фонды относятся к категории медиаторов риска.</p>\r\n'),
(4, 'en', '', ''),
(4, 'kk', '', ''),
(4, 'ru', 'ИНФРАСТРУКТУРА', 'Инвестиции в фонды, основным направлением инвестирование которых являются объекты инфраструктуры.\r\n            Инвестиции в инфраструктуру относятся к категории активов-диверсификаторов');

-- --------------------------------------------------------

--
-- Структура таблицы `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `contact`
--

INSERT INTO `contact` (`id`, `address`, `phone`, `email`, `website`) VALUES
(1, 'Казахстан, г. Алматы,<br>\r\n                                        А25D6H8, пр. Достык 136', '+7 (727) 244 94 04', 'office@nicnbk.kz', 'www.nicnbk.kz');

-- --------------------------------------------------------

--
-- Структура таблицы `contact_translation`
--

CREATE TABLE `contact_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `contact_translation`
--

INSERT INTO `contact_translation` (`id`, `language`, `address`) VALUES
(1, 'en', ''),
(1, 'kk', ''),
(1, 'ru', 'Казахстан, г. Алматы,<br>\r\n                                        А25D6H8, пр. Достык 136');

-- --------------------------------------------------------

--
-- Структура таблицы `core_satellite`
--

CREATE TABLE `core_satellite` (
  `id` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `core_satellite`
--

INSERT INTO `core_satellite` (`id`, `title`, `content`, `created_at`) VALUES
(1, 'Основа портфеля', '<p>прямые инвестиции в крупные глобальные фонды под непосредственным управлением Корпорации</p>\r\n', '2020-05-15 07:17:00'),
(2, 'Доверительное управление', '<p>инвестиции в специализированные фонды и стратегии под управлением стратегического партнера</p>\r\n', '2020-05-15 07:17:00');

-- --------------------------------------------------------

--
-- Структура таблицы `core_satellite_translation`
--

CREATE TABLE `core_satellite_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `core_satellite_translation`
--

INSERT INTO `core_satellite_translation` (`id`, `language`, `title`, `content`) VALUES
(1, 'en', '', ''),
(1, 'kk', '', ''),
(1, 'ru', 'Основа портфеля', '<p>прямые инвестиции в крупные глобальные фонды под непосредственным управлением Корпорации</p>\r\n'),
(2, 'en', '', ''),
(2, 'kk', '', ''),
(2, 'ru', 'Доверительное управление', '<p>инвестиции в специализированные фонды и стратегии под управлением стратегического партнера</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `documents_confidential`
--

CREATE TABLE `documents_confidential` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `documents_confidential_translation`
--

CREATE TABLE `documents_confidential_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `investment_portfolio`
--

CREATE TABLE `investment_portfolio` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `investment_portfolio`
--

INSERT INTO `investment_portfolio` (`id`, `title`, `content`, `created_at`) VALUES
(1, 'Инвестиционный портфель', '<p>Корпорация была создана в 2012 году, с целью повышения эффективности управления и диверсификации международных резервов Республики Казахстан. Основным направлением деятельности Корпорации является активное инвестирование в альтернативные классы активов: частный капитал, недвижимость, инфраструктуру и хедж-фонды.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>При управлении портфелем Корпорация руководствуется Инвестиционной стратегий. Стратегия определяет горизонт инвестирования, риск аппетит, целевые параметры доходности и инвестиционные ограничения. Согласно стратегии, определением целевой доходности и риск-аппетита для Корпорации служит Reference Portfolio &mdash; низко-затратный и пассивно управляемый портфель, состоящий на 80% из глобальных акций и на 20% из глобальных облигаций.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Цель управления портфелем Корпорации заключается в обеспечении в долгосрочном горизонте доходности, превышающей доходность Reference portfolio, при равном или более низком уровне риска.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<div class=\"text-left-content\">\r\n<p>Целевой портфель Корпорации диверсифицируется по трем категориям активов:</p>\r\n&nbsp;\r\n\r\n<ul>\r\n	<li>Высокодоходные рисковые активы: 30-40%</li>\r\n	<li>Диверсификаторы: 25-40%</li>\r\n	<li>Медиаторы риска: 20-35%</li>\r\n</ul>\r\n</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Корпорация производит инвестиции в альтернативные активы преимущественно путем инвестирования в фонды, методом &laquo;Core-Satellite&raquo; . В качестве ядра (Core) выступают инвестиции в крупные диверсифицированные фонды. Спутниками (Satellite) служат специализированные фонды меньших размеров, призванные увеличивать доходность или снижать риски</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2020-05-15 07:15:00');

-- --------------------------------------------------------

--
-- Структура таблицы `investment_portfolio_translation`
--

CREATE TABLE `investment_portfolio_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `investment_portfolio_translation`
--

INSERT INTO `investment_portfolio_translation` (`id`, `language`, `title`, `content`) VALUES
(1, 'en', '', ''),
(1, 'kk', '', ''),
(1, 'ru', 'Инвестиционный портфель', '<p>Корпорация была создана в 2012 году, с целью повышения эффективности управления и диверсификации международных резервов Республики Казахстан. Основным направлением деятельности Корпорации является активное инвестирование в альтернативные классы активов: частный капитал, недвижимость, инфраструктуру и хедж-фонды.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>При управлении портфелем Корпорация руководствуется Инвестиционной стратегий. Стратегия определяет горизонт инвестирования, риск аппетит, целевые параметры доходности и инвестиционные ограничения. Согласно стратегии, определением целевой доходности и риск-аппетита для Корпорации служит Reference Portfolio &mdash; низко-затратный и пассивно управляемый портфель, состоящий на 80% из глобальных акций и на 20% из глобальных облигаций.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Цель управления портфелем Корпорации заключается в обеспечении в долгосрочном горизонте доходности, превышающей доходность Reference portfolio, при равном или более низком уровне риска.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<div class=\"text-left-content\">\r\n<p>Целевой портфель Корпорации диверсифицируется по трем категориям активов:</p>\r\n&nbsp;\r\n\r\n<ul>\r\n	<li>Высокодоходные рисковые активы: 30-40%</li>\r\n	<li>Диверсификаторы: 25-40%</li>\r\n	<li>Медиаторы риска: 20-35%</li>\r\n</ul>\r\n</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Корпорация производит инвестиции в альтернативные активы преимущественно путем инвестирования в фонды, методом &laquo;Core-Satellite&raquo; . В качестве ядра (Core) выступают инвестиции в крупные диверсифицированные фонды. Спутниками (Satellite) служат специализированные фонды меньших размеров, призванные увеличивать доходность или снижать риски</p>\r\n\r\n<p>&nbsp;</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(128) NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `language`
--

INSERT INTO `language` (`id`, `name`, `code`, `image`) VALUES
(1, 'казахский', 'kk', '1580468762_kz_flag.png'),
(2, 'русский', 'ru', '1580468811_ru_flag.png'),
(3, 'english', 'en', '1580468824_uk_flag.png');

-- --------------------------------------------------------

--
-- Структура таблицы `logo`
--

CREATE TABLE `logo` (
  `id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `logo`
--

INSERT INTO `logo` (`id`, `position`, `image`) VALUES
(1, 1, '1589516537_logo-company.png'),
(2, 2, '1589516616_logo-company.png');

-- --------------------------------------------------------

--
-- Структура таблицы `main_advantages`
--

CREATE TABLE `main_advantages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `main_advantages`
--

INSERT INTO `main_advantages` (`id`, `title`, `content`, `image`, `sort`, `created_at`) VALUES
(1, 'Миссия', '<p>Миссией Корпорации является диверсификация и повышение долгосрочной доходности суверенных активов Республики Казахстан путем инвестирования в альтернативные инструменты</p>\r\n', '1589517161_mission2.jpg', 1, '2020-05-15 04:32:00'),
(2, 'Видение', '<p>Видение Корпорации &ndash; высокоэффективный управляющий суверенными активами</p>\r\n', '1589517197_vision2.jpg', 2, '2020-05-15 04:33:00'),
(3, 'Цель', '<p>Целью Корпорации является достижение долгосрочной доходности выше уровня доходности Референс портфеля и стратегические инвестиции в инновации</p>\r\n', '1589517258_target1.jpg', 3, '2020-05-15 04:34:00');

-- --------------------------------------------------------

--
-- Структура таблицы `main_advantages_translation`
--

CREATE TABLE `main_advantages_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `main_advantages_translation`
--

INSERT INTO `main_advantages_translation` (`id`, `language`, `title`, `content`) VALUES
(1, 'en', '', ''),
(1, 'kk', '', ''),
(1, 'ru', 'Миссия', '<p>Миссией Корпорации является диверсификация и повышение долгосрочной доходности суверенных активов Республики Казахстан путем инвестирования в альтернативные инструменты</p>\r\n'),
(2, 'en', '', ''),
(2, 'kk', '', ''),
(2, 'ru', 'Видение', '<p>Видение Корпорации &ndash; высокоэффективный управляющий суверенными активами</p>\r\n'),
(3, 'en', '', ''),
(3, 'kk', '', ''),
(3, 'ru', 'Цель', '<p>Целью Корпорации является достижение долгосрочной доходности выше уровня доходности Референс портфеля и стратегические инвестиции в инновации</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `management_structure`
--

CREATE TABLE `management_structure` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `sort` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `management_structure`
--

INSERT INTO `management_structure` (`id`, `name`, `content`, `sort`, `created_at`) VALUES
(1, 'СОВЕТ ДИРЕКТОРОВ', '<p>Обеспечивает реализацию интересов и защиту прав Единственного акционера Корпорации</p>\r\n', 1, '2020-05-15 06:27:00'),
(2, 'Правление', '<p>Осуществляет руководство текущей деятельностью Корпорации</p>\r\n', 2, '2020-05-15 06:27:00'),
(3, 'Инвестиционный комитет', '<p>Осуществляет принятие инвестиционных решений для достижения максимальной доходности портфеля</p>\r\n', 3, '2020-05-15 06:28:00');

-- --------------------------------------------------------

--
-- Структура таблицы `management_structure_translation`
--

CREATE TABLE `management_structure_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `management_structure_translation`
--

INSERT INTO `management_structure_translation` (`id`, `language`, `name`, `content`) VALUES
(1, 'en', '', ''),
(1, 'kk', '', ''),
(1, 'ru', 'СОВЕТ ДИРЕКТОРОВ', '<p>Обеспечивает реализацию интересов и защиту прав Единственного акционера Корпорации</p>\r\n'),
(2, 'en', '', ''),
(2, 'kk', '', ''),
(2, 'ru', 'Правление', '<p>Осуществляет руководство текущей деятельностью Корпорации</p>\r\n'),
(3, 'en', '', ''),
(3, 'kk', '', ''),
(3, 'ru', 'Инвестиционный комитет', '<p>Осуществляет принятие инвестиционных решений для достижения максимальной доходности портфеля</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `membership`
--

CREATE TABLE `membership` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `membership`
--

INSERT INTO `membership` (`id`, `title`, `content`, `image`, `link`, `sort`, `created_at`) VALUES
(1, '2015 год', '<p>Международный форум фондов суверенного благосостояния является добровольной организацией способствующей совместной работе и укреплению сообщества посредством диалога, исследований и взаимодействия</p>\r\n', '1589525438_logo-about.png', 'www.ifswf.org/santiago-principles', 1, '2020-05-15 06:50:00'),
(2, '2016 год', '<p>Международная платформа для суверенных и пенсионных фондов для улучшения взаимодействия между инвесторами и другими финансовыми институтами в целях осуществления со-инвестиций</p>\r\n', '1589525472_bottom-31.png', 'www.crosapf.org', 2, '2020-05-15 06:51:00'),
(3, '2017 год', '<p>Международная ассоциация институциональных инвесторов в частный капитал, включающая порядка 450 членов с более чем $2 трлн. под управлением</p>\r\n', '1589525511_ilpa-logo.svg', 'ilpa.org', 3, '2020-05-15 06:51:00');

-- --------------------------------------------------------

--
-- Структура таблицы `membership_translation`
--

CREATE TABLE `membership_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `membership_translation`
--

INSERT INTO `membership_translation` (`id`, `language`, `title`, `content`) VALUES
(1, 'en', '', ''),
(1, 'kk', '', ''),
(1, 'ru', '2015 год', '<p>Международный форум фондов суверенного благосостояния является добровольной организацией способствующей совместной работе и укреплению сообщества посредством диалога, исследований и взаимодействия</p>\r\n'),
(2, 'en', '', ''),
(2, 'kk', '', ''),
(2, 'ru', '2016 год', '<p>Международная платформа для суверенных и пенсионных фондов для улучшения взаимодействия между инвесторами и другими финансовыми институтами в целях осуществления со-инвестиций</p>\r\n'),
(3, 'en', '', ''),
(3, 'kk', '', ''),
(3, 'ru', '2017 год', '<p>Международная ассоциация институциональных инвесторов в частный капитал, включающая порядка 450 членов с более чем $2 трлн. под управлением</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `key` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `url` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `metaName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaDesc` text COLLATE utf8_unicode_ci,
  `metaKey` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`id`, `status`, `key`, `title`, `content`, `url`, `sort`, `metaName`, `metaDesc`, `metaKey`) VALUES
(1, 0, 'main', 'Главная', '', '/', 1, '', '', ''),
(2, 1, 'about', 'О нас', 'Корпорация является институциональным инвестором международного уровня и высокоэффективным управляющим суверенными активами', '/about', 2, '', '', ''),
(3, 1, 'investments', 'Инвестиции', 'Инвестиции в альтернативные инструменты способствуют повышению долгосрочной доходности и диверсификации портфеля при приемлемом уровне риска', '/investments', 3, '', '', ''),
(4, 1, 'information-centre', 'Информационный центр', 'Новости о Корпорации и экспертное мнение о рынках, трендах и последних изменениях в индустрии инвестиций.', '/information-centre', 4, '', '', ''),
(5, 1, 'career', 'Карьера', 'Присоединяйся к команде профессионалов с уникальным опытом в сфере инвестиций', '/career', 5, '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_translation`
--

CREATE TABLE `menu_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `metaName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaDesc` text COLLATE utf8_unicode_ci,
  `metaKey` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `menu_translation`
--

INSERT INTO `menu_translation` (`id`, `language`, `title`, `content`, `metaName`, `metaDesc`, `metaKey`) VALUES
(1, 'en', '', '', '', '', ''),
(1, 'kk', '', '', '', '', ''),
(1, 'ru', 'Главная', '', '', '', ''),
(2, 'en', '', '', '', '', ''),
(2, 'kk', '', '', '', '', ''),
(2, 'ru', 'О нас', 'Корпорация является институциональным инвестором международного уровня и высокоэффективным управляющим суверенными активами', '', '', ''),
(3, 'en', '', '', '', '', ''),
(3, 'kk', '', '', '', '', ''),
(3, 'ru', 'Инвестиции', 'Инвестиции в альтернативные инструменты способствуют повышению долгосрочной доходности и диверсификации портфеля при приемлемом уровне риска', '', '', ''),
(4, 'en', '', '', '', '', ''),
(4, 'kk', '', '', '', '', ''),
(4, 'ru', 'Информационный центр', 'Новости о Корпорации и экспертное мнение о рынках, трендах и последних изменениях в индустрии инвестиций.', '', '', ''),
(5, 'en', '', '', '', '', ''),
(5, 'kk', '', '', '', '', ''),
(5, 'ru', 'Карьера', 'Присоединяйся к команде профессионалов с уникальным опытом в сфере инвестиций', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `translation` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1580184834),
('m130524_201442_init', 1580184836),
('m200128_040548_create_language_table', 1586515343),
('m200128_041002_create_i18n_init_table', 1586515344),
('m200410_091728_create_menu_table', 1586516416),
('m200410_091742_create_submenu_table', 1586516418),
('m200410_093238_create_classes_investments', 1589353120),
('m200414_061857_create_procurement_user_table', 1586846065),
('m200414_072517_create_procurement_table', 1587708595),
('m200414_072915_create_procurement_documents_table', 1587708597),
('m200417_051810_create_documents_confidential_table', 1587708599),
('m200424_064131_add_image_column_procurement_table', 1587710586),
('m200424_083141_add_columns_procurement_user_table', 1587717161),
('m200513_040409_craete_banner_table', 1589353122),
('m200513_040448_create_about_path_table', 1589353123),
('m200513_040519_create_values_investments_table', 1589354102),
('m200513_040543_create_management_structure_table', 1589353127),
('m200513_040759_create_team_table', 1589353129),
('m200513_040828_create_membership_table', 1589353130),
('m200513_040847_create_core_satellite_table', 1589353132),
('m200513_040913_create_risk_table', 1589353133),
('m200513_040929_create_risk_system_table', 1589353135),
('m200513_040957_create_think_tank_table', 1589353136),
('m200513_065704_create_main_advantages_table', 1589353138),
('m200515_034814_create_logo_table', 1589514943),
('m200515_034834_create_contact_table', 1589514945),
('m200515_034904_create_careers_table', 1589514946),
('m200515_034942_create_investment_portfolio_table', 1589514960);

-- --------------------------------------------------------

--
-- Структура таблицы `procurement`
--

CREATE TABLE `procurement` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `procurement`
--

INSERT INTO `procurement` (`id`, `parent_id`, `status`, `name`, `description`, `content`, `file`, `create_at`, `image`) VALUES
(1, NULL, 1, 'Нормативно-правовые акты', '', '', '', '2020-04-24 06:33:00', '1587711396_law.svg'),
(2, NULL, 1, 'План закупок', '', '', '', '2020-04-24 06:34:00', '1587711421_about-plan.svg'),
(3, NULL, 1, 'Тендер', '', '', '', '2020-04-24 06:34:00', '1587711446_tender.svg'),
(4, NULL, 1, 'Запрос ценовых предложений', '', '', '', '2020-04-24 06:34:00', '1587711463_about-price.svg'),
(5, 3, 1, 'Закупки услуг аудита годовой финансовой отчетности АО «НИКНБК» за 2018 год', 'Период предоставления заявок: 4 июня 2018 г., 9:00 — 25 июня 2018 г., 10:00', '<p>АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;, находящееся по адресу: Республика Казахстан, 050051, г. Алматы, Медеуский район, пр. Достык 136, интернет-ресурс: www.nicnbk.kz, электронный адрес: kashaganova@nicnbk.kz , в соответствии с Правилами приобретения товаров, работ и услуг Национальным Банком Республики Казахстан, его ведомствами, организациями, входящими в его структуру, и юридическими лицами, пятьдесят и более процентов голосующих акций (долей участия в уставном капитале) которых принадлежат Национальному Банку Республики Казахстан или находятся в его доверительном управлении, и аффилиированными с ними юридическими лицами, утвержденных постановлением Правления Национального Банка Республики Казахстан 19 декабря 2015 года № 237, объявляет о проведении конкурса по закупкам услуг аудита годовой финансовой отчетности АО &laquo;Национальная инвестиционная корпорация Национального Банка казахстана&raquo; за 2018 год.</p>\r\n\r\n<p>Способ получения конкурсной документации: копия конкурсной документации доступна для получения в электронном виде на интернет-ресурсе заказчика www.nicnbk.kz (раздел: Закупки) либо конкурсную документацию можно получить по адресу: г. Алматы, пр. Достык 136, 5-этаж, тел.+7 (727) 244 94 16, в срок до 10 часов 00 минут &laquo;25&raquo; июня 2018 года включительно Услуги оказываются в соответствии с условиями договора, полный перечень закупаемых услуг, их количество (объем) и подробная спецификация указаны в конкурсной документации.</p>\r\n\r\n<p>Место оказания услуг: г. Алматы, пр. Достык 136, БЦ &laquo;Пионер&raquo;, 4 и 5 этажи.</p>\r\n\r\n<p>Требуемый срок оказания услуг: в соответствии условиями договора.</p>\r\n\r\n<p>Заявки на участие в конкурсе, запечатанные в конверты, предоставляются (направляются) потенциальными поставщиками в Управление по административным вопросам и коммуникациям АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo; по адресу: Республика Казахстан, 050051, г. Алматы, Медеуский район, пр. Достык 136, 5-этаж, с 09 часов 00 минут до 18 часов 30 минут, тел. +7 (727) 244 94 16, + 7 (727) 244 94 04.Окончательный срок представления заявок на участие в конкурсе: до 10 часов 00 минут &laquo;25&raquo; июня 2018 года.Конверты с заявками на участие в конкурсе будут вскрываться в 10 часов 30 минут &laquo;25&raquo; июня 2018 года, по адресу: 050051, г. Алматы, Медеуский район, пр. Достык 136, 5-этаж.Дополнительную информацию и справку можно получить по телефонам: +7 (727) 244 94 16, + 7 (727) 244 94 04.Уполномоченный представитель организатора закупок, секретарь конкурсной комиссии: Кашаганова А.Н., Менеджер по закупкам Управления по административным вопросам и коммуникациям АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;, тел.: + 7(727) 244 94 16.</p>\r\n', '1587711151_HPLJP1000_P1500_Series.log', '2020-04-24 06:36:00', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `procurement_documents`
--

CREATE TABLE `procurement_documents` (
  `id` int(11) NOT NULL,
  `procurement_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `procurement_documents`
--

INSERT INTO `procurement_documents` (`id`, `procurement_id`, `name`, `file`, `date`, `create_at`) VALUES
(1, 5, 'Протокол вскрытия', '1587710258_HPLJP1000_P1500_Series.log', '2020-04-23', '2020-04-24 06:37:00'),
(2, 5, 'Протокол о предварительном допуске к участию в конкурсе', '1587710283_HPLJP1000_P1500_Series.log', '2020-04-07', '2020-04-24 06:38:00'),
(3, 5, 'приложение к протоколу предварит допуска', '1587710300_1020.log', '2020-04-16', '2020-04-24 06:38:00'),
(4, 5, 'приложение 1 к протоколу Допуска', '1587710317_HPLJP1000_P1500_Series.log', '2020-04-15', '2020-04-24 06:38:00');

-- --------------------------------------------------------

--
-- Структура таблицы `procurement_documents_translation`
--

CREATE TABLE `procurement_documents_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `procurement_documents_translation`
--

INSERT INTO `procurement_documents_translation` (`id`, `language`, `name`, `file`) VALUES
(1, 'en', '', NULL),
(1, 'kk', '', NULL),
(1, 'ru', 'Протокол вскрытия', '1587710258_HPLJP1000_P1500_Series.log'),
(2, 'en', '', NULL),
(2, 'kk', '', NULL),
(2, 'ru', 'Протокол о предварительном допуске к участию в конкурсе', '1587710283_HPLJP1000_P1500_Series.log'),
(3, 'en', '', NULL),
(3, 'kk', '', NULL),
(3, 'ru', 'приложение к протоколу предварит допуска', '1587710300_1020.log'),
(4, 'en', '', NULL),
(4, 'kk', '', NULL),
(4, 'ru', 'приложение 1 к протоколу Допуска', '1587710317_HPLJP1000_P1500_Series.log');

-- --------------------------------------------------------

--
-- Структура таблицы `procurement_translation`
--

CREATE TABLE `procurement_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `procurement_translation`
--

INSERT INTO `procurement_translation` (`id`, `language`, `name`, `description`, `content`, `file`) VALUES
(1, 'en', '', '', '', NULL),
(1, 'kk', '', '', '', NULL),
(1, 'ru', 'Нормативно-правовые акты', '', '', ''),
(2, 'en', '', '', '', NULL),
(2, 'kk', '', '', '', NULL),
(2, 'ru', 'План закупок', '', '', ''),
(3, 'en', '', '', '', NULL),
(3, 'kk', '', '', '', NULL),
(3, 'ru', 'Тендер', '', '', ''),
(4, 'en', '', '', '', NULL),
(4, 'kk', '', '', '', NULL),
(4, 'ru', 'Запрос ценовых предложений', '', '', ''),
(5, 'en', '', '', '', NULL),
(5, 'kk', '', '', '', NULL),
(5, 'ru', 'Закупки услуг аудита годовой финансовой отчетности АО «НИКНБК» за 2018 год', 'Период предоставления заявок: 4 июня 2018 г., 9:00 — 25 июня 2018 г., 10:00', '<p>АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;, находящееся по адресу: Республика Казахстан, 050051, г. Алматы, Медеуский район, пр. Достык 136, интернет-ресурс: www.nicnbk.kz, электронный адрес: kashaganova@nicnbk.kz , в соответствии с Правилами приобретения товаров, работ и услуг Национальным Банком Республики Казахстан, его ведомствами, организациями, входящими в его структуру, и юридическими лицами, пятьдесят и более процентов голосующих акций (долей участия в уставном капитале) которых принадлежат Национальному Банку Республики Казахстан или находятся в его доверительном управлении, и аффилиированными с ними юридическими лицами, утвержденных постановлением Правления Национального Банка Республики Казахстан 19 декабря 2015 года № 237, объявляет о проведении конкурса по закупкам услуг аудита годовой финансовой отчетности АО &laquo;Национальная инвестиционная корпорация Национального Банка казахстана&raquo; за 2018 год.</p>\r\n\r\n<p>Способ получения конкурсной документации: копия конкурсной документации доступна для получения в электронном виде на интернет-ресурсе заказчика www.nicnbk.kz (раздел: Закупки) либо конкурсную документацию можно получить по адресу: г. Алматы, пр. Достык 136, 5-этаж, тел.+7 (727) 244 94 16, в срок до 10 часов 00 минут &laquo;25&raquo; июня 2018 года включительно Услуги оказываются в соответствии с условиями договора, полный перечень закупаемых услуг, их количество (объем) и подробная спецификация указаны в конкурсной документации.</p>\r\n\r\n<p>Место оказания услуг: г. Алматы, пр. Достык 136, БЦ &laquo;Пионер&raquo;, 4 и 5 этажи.</p>\r\n\r\n<p>Требуемый срок оказания услуг: в соответствии условиями договора.</p>\r\n\r\n<p>Заявки на участие в конкурсе, запечатанные в конверты, предоставляются (направляются) потенциальными поставщиками в Управление по административным вопросам и коммуникациям АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo; по адресу: Республика Казахстан, 050051, г. Алматы, Медеуский район, пр. Достык 136, 5-этаж, с 09 часов 00 минут до 18 часов 30 минут, тел. +7 (727) 244 94 16, + 7 (727) 244 94 04.Окончательный срок представления заявок на участие в конкурсе: до 10 часов 00 минут &laquo;25&raquo; июня 2018 года.Конверты с заявками на участие в конкурсе будут вскрываться в 10 часов 30 минут &laquo;25&raquo; июня 2018 года, по адресу: 050051, г. Алматы, Медеуский район, пр. Достык 136, 5-этаж.Дополнительную информацию и справку можно получить по телефонам: +7 (727) 244 94 16, + 7 (727) 244 94 04.Уполномоченный представитель организатора закупок, секретарь конкурсной комиссии: Кашаганова А.Н., Менеджер по закупкам Управления по административным вопросам и коммуникациям АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;, тел.: + 7(727) 244 94 16.</p>\r\n', '1587711151_HPLJP1000_P1500_Series.log');

-- --------------------------------------------------------

--
-- Структура таблицы `procurement_user`
--

CREATE TABLE `procurement_user` (
  `id` int(11) NOT NULL,
  `legal_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `who_issued` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_issue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `procurement_user`
--

INSERT INTO `procurement_user` (`id`, `legal_name`, `bin`, `postcode`, `city`, `address`, `username`, `position`, `id_number`, `who_issued`, `date_issue`, `created_at`, `phone`, `email`) VALUES
(1, 'aaa', 'aaaa', NULL, 'aaaa', 'aaa', 'aaa', 'aaa', 'aaa', 'aaaa', 'aaaaa', NULL, '+7(213) 123-1231', 'aaaaa@mail.cs'),
(2, 'asdasd', 'asdasdasd', 'asdasdasd', 'asdasd', 'asdasdas', 'dasdasd', 'asdasdasd', 'asdasd', 'asdasdasd', 'asdasd', NULL, '+7(213) 123-123_', 'sadasdasd@asdasd.c'),
(3, 'asdasd', 'asdasd', '', 'asdasd', '', 'adasdasdad', 'asdasd', 'asdasd', 'asdasd', 'asdasd', NULL, '+7(213) 123-____', ''),
(4, 'adasd', 'asdadasd', 'asdasd', 'asdasd', 'asdasdasd', 'asdasd', 'adasd', 'asdasd', 'asdasd', 'asdasd', NULL, '+7(123) 123-1231', 'sadasd@nail.c'),
(5, 'adasd', 'asdasd', 'dasdasd', 'asdasd', '', 'asdasd', 'asdasd', 'asdsad', 'asdasd', 'asdas', NULL, '+7(312) 312-3123', 'sadasdasd@Asdac.c'),
(6, 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'dasd', 'asdasd', 'asdasd', 'asdasd', 'asdasda', 'asdasdasd', NULL, '+7(231) 231-2313', 'asdasdasd@aa.cas'),
(7, 'asdasdsd', 'asdasdasd', 'asdasd', 'asdasdasd', 'asd', 'asdasd', 'asdasd', 'asdasdad', 'asdasd', 'asdasd', NULL, '+7(231) 231-2313', 'asdasdasd@aa.cas'),
(8, 'asdasdasd', 'asdasd', 'asdasd', 'asdasdas', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'adasd', NULL, '+7(231) 231-2313', 'asdasdasd@aa.cas'),
(9, 'asdasd', 'asdasd', 'sdasdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasasd', NULL, '+7(231) 231-2313', 'asdasdasd@aa.cas'),
(10, 'фывфыв', '123123123', '123123123', '123123123', '123123123', '123123', '123123', '123123123', '123123123', '12/31/2312', NULL, '+7(231) 231-2313', 'asdasdasd@aa.cas');

-- --------------------------------------------------------

--
-- Структура таблицы `risk`
--

CREATE TABLE `risk` (
  `id` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `sort` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `risk`
--

INSERT INTO `risk` (`id`, `content`, `sort`, `created_at`) VALUES
(1, '<p>Риск-менеджмент в Корпорации выстроен на основе опыта, постоянного обучения и технологического развития</p>\r\n', 1, '2020-05-15 07:17:00'),
(2, '<p>Риск-менеджмент фокусируется на предупреждении, реагировании, эффективном информировании и снижении рисков</p>\r\n', 2, '2020-05-15 07:18:00');

-- --------------------------------------------------------

--
-- Структура таблицы `risk_system`
--

CREATE TABLE `risk_system` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `risk_system`
--

INSERT INTO `risk_system` (`id`, `name`, `image`, `sort`, `created_at`) VALUES
(1, 'Операционные', '', 1, '2020-05-15 07:18:00'),
(2, 'Инвестиционные', '', 2, '2020-05-15 07:18:00'),
(3, 'Идентификация рисков', '', 3, '2020-05-15 07:18:00'),
(4, 'Анализ и Оценка', '', 4, '2020-05-15 07:19:00'),
(5, 'Определение мероприятий', '', 5, '2020-05-15 07:19:00'),
(6, 'Принятие решений', '', 6, '2020-05-15 07:19:00'),
(7, 'Контроль', '', 7, '2020-05-15 07:19:00');

-- --------------------------------------------------------

--
-- Структура таблицы `risk_system_translation`
--

CREATE TABLE `risk_system_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `risk_system_translation`
--

INSERT INTO `risk_system_translation` (`id`, `language`, `name`) VALUES
(1, 'en', ''),
(1, 'kk', ''),
(1, 'ru', 'Операционные'),
(2, 'en', ''),
(2, 'kk', ''),
(2, 'ru', 'Инвестиционные'),
(3, 'en', ''),
(3, 'kk', ''),
(3, 'ru', 'Идентификация рисков'),
(4, 'en', ''),
(4, 'kk', ''),
(4, 'ru', 'Анализ и Оценка'),
(5, 'en', ''),
(5, 'kk', ''),
(5, 'ru', 'Определение мероприятий'),
(6, 'en', ''),
(6, 'kk', ''),
(6, 'ru', 'Принятие решений'),
(7, 'en', ''),
(7, 'kk', ''),
(7, 'ru', 'Контроль');

-- --------------------------------------------------------

--
-- Структура таблицы `risk_translation`
--

CREATE TABLE `risk_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `risk_translation`
--

INSERT INTO `risk_translation` (`id`, `language`, `content`) VALUES
(1, 'en', ''),
(1, 'kk', ''),
(1, 'ru', '<p>Риск-менеджмент в Корпорации выстроен на основе опыта, постоянного обучения и технологического развития</p>\r\n'),
(2, 'en', ''),
(2, 'kk', ''),
(2, 'ru', '<p>Риск-менеджмент фокусируется на предупреждении, реагировании, эффективном информировании и снижении рисков</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `source_message`
--

CREATE TABLE `source_message` (
  `id` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `submenu`
--

CREATE TABLE `submenu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `url` varchar(128) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `submenu`
--

INSERT INTO `submenu` (`id`, `menu_id`, `title`, `content`, `url`) VALUES
(1, 2, 'Корпоративное управление', '', 'corporation'),
(2, 2, 'Наша команда', '', 'comanda'),
(3, 2, 'Членство в международных организациях', '', 'inter'),
(4, 3, 'Инвестиционный портфель', '', 'portfel'),
(5, 3, 'Альтернативные инвестиции', '', 'alternative'),
(6, 3, 'Риск-менеджмент', '', 'risk'),
(7, 4, 'Новости о Корпорации', '', '#'),
(8, 4, 'Публикации', '', '#');

-- --------------------------------------------------------

--
-- Структура таблицы `submenu_translation`
--

CREATE TABLE `submenu_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `submenu_translation`
--

INSERT INTO `submenu_translation` (`id`, `language`, `title`, `content`) VALUES
(1, 'ru', 'Корпоративное управление', ''),
(2, 'ru', 'Наша команда', ''),
(3, 'ru', 'Членство в международных организациях', ''),
(4, 'ru', 'Инвестиционный портфель', ''),
(5, 'ru', 'Альтернативные инвестиции', ''),
(6, 'ru', 'Риск-менеджмент', ''),
(7, 'ru', 'Новости о Корпорации', ''),
(8, 'ru', 'Публикации', '');

-- --------------------------------------------------------

--
-- Структура таблицы `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `management_id` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `team`
--

INSERT INTO `team` (`id`, `management_id`, `username`, `position`, `content`, `image`, `status`, `created_at`) VALUES
(1, 1, 'Молдабекова Алия Мейрбековна', 'Председатель Совета директоров', '<p>С февраля 2020 года является Председателем Совета директоров АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>С 2015 года по декабрь 2019 года занимала должность Директора Департамента монетарных операций в Национальном Банке Республики Казахстан.</p>\r\n\r\n<p>C декабря 2019 года назначена на должность Заместителя Председателя Национального Банка Республики Казахстан.</p>\r\n\r\n<p>Получила степень Бакалавра и Магистра прикладной математики в Казахском государственном университете, также степень Бакалавра финансов в Евразийском национальном университете им. Л.Н. Гумилева. Является кандидатом физико-математических наук.</p>\r\n', '1589524693_1.jpg', 1, '2020-05-15 06:38:00'),
(2, 1, 'Таджияков Галымжан Бисенгалиевич', 'Член Совета директоров', '<p>C 15 мая 2019 года избран Председателем Правления и членом Совета директоров АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>До избрания, Галымжан Таджияков занимал пост заместителя Председателя Правления холдинга &laquo;Байтерек&raquo;, где занимался вопросами управления активами, корпоративных финансов, стратегии и корпоративного развития, анализа и исследований. Галымжан Таджияков обладает более чем 15-летним управленческим стажем работы в банковской сфере и корпоративном управлении, в том числе он работал на различных руководящих должностях АО &laquo;Ситибанк Казахстан&raquo;, АО &laquo;HSBC Банк Казахстан&raquo;, АО &laquo;ABN AMRO Банк Казахстан&raquo;.</p>\r\n\r\n<p>Окончил Казахскую государственную академию управления, бизнес-школу Университета Дарема (Великобритания).</p>\r\n', '1589524791_pravlenie1.jpg', 1, '2020-05-15 06:39:00'),
(3, 1, 'Турсунханов Нуржан Аскарович', 'Член Совета директоров', '<p>Окончил Казахскую государственную академию управления, бизнес-школу Университета Дарема (Великобритания).</p>\r\n\r\n<p>Турсунханов Н.А. начал свою карьеру в Национальном Банке Республики Казахстан в 2004 году. В 2009 году был назначен на должность Заместителя Начальника управления, где отвечал за управление золотовалютными активами и активами Национального фонда. В 2015 году Турсунханов Н.А. был назначен на должность Заместителя Директора Департамента монетарных операций Национального Банка Республики Казахстан. С декабря 2019 года Нуржан Аскарович занимает должность Директора Департамента монетарных операций Национального Банка Республики Казахстан.</p>\r\n\r\n<p>С 2012 по 2015 год Нуржан Аскарович занимал должность Заместителя Председателя Правления в АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>Получил степень бакалавра экономики в университете Nottingham (Великобритания).</p>\r\n', '1589524821_5.jpg', 1, '2020-05-15 06:40:00'),
(4, 1, 'Жорж Сударскис', 'Член Совета директоров', '<p>С июня 2018 года является членом Совета директоров &ndash; независимым директором АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>Основатель и Управляющий партнер Sudarskis &amp; Partners &ndash; компании, предоставляющей консалтинговые услуги фондам благосостояния, государственным организациям и др. В 1998 &ndash; 2009 г.г. занимал должность главного инвестиционного директора (Chief Investment Officer of Private Equity) Abu Dhabi Investment Authority &ndash; суверенного фонда благосостояния, принадлежащего эмирату Абу-Даби. В 1975-1995 г.г. занимал различные должности в Arthur Andersen &amp; Co, Morgan Guaranty Trust Co. of NY, FIDAL, INF Bank France, Clinvest (Credit Lyonnais).</p>\r\n\r\n<p>Получил степень Бакалавра в бизнес-школе Ecole Superieure de Commerce de Paris, а также степень MBA в университете McGill.</p>\r\n', '1589524863_3.jpg', 1, '2020-05-15 06:41:00'),
(5, 1, 'Марк Кашер', 'Член Совета директоров', '<p>С июня 2018 года является членом Совета директоров &ndash; независимым директором АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>С 2013 года является Председателем Комитета по аудиту и вознаграждениям, Членом Совета директоров Luxoft, глобальной аутсорсинговой компании по разработке программного обеспечения.</p>\r\n\r\n<p>В 2016 &ndash; 2017 г.г. занимал должность Управляющего директора Highland Capital, региональной структуры частного капитала для инвестиций в Центральноазиатский рынок. В 2015-2016 г.г. занимал должность Члена Совета директоров BI Group, казахстанской строительной компании. В 2014-2015 г.г. занимал должность Директора частного капитала в Verno Investment Management Limited. В 1997-2015 г.г. занимал должность Управляющего директора Pinebridge Investments, также занимал должности Председателя правления Pinebridge Investments Russia, Ltd. и Инвестиционного директора AIG Silk Road Capital Management. В 1994-1995 г.г. был консультантом United States Agency for International Development (USAID) по проектам в России, Украине, Узбекистане и Кыргызстане.</p>\r\n\r\n<p>Получил степень Бакалавра в университете Tufts, а также степень MBA в университете Georgetown.</p>\r\n', '1589524890_4.jpg', 1, '2020-05-15 06:41:00'),
(6, 2, 'Таджияков Галымжан Бисенгалиевич', 'Председатель правления', '<p>C 15 мая 2019 года избран Председателем Правления и членом Совета директоров АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>До избрания, Галымжан Таджияков занимал пост заместителя Председателя Правления холдинга &laquo;Байтерек&raquo;, где занимался вопросами управления активами, корпоративных финансов, стратегии и корпоративного развития, анализа и исследований. Галымжан Таджияков обладает более чем 15-летним управленческим стажем работы в банковской сфере и корпоративном управлении, в том числе он работал на различных руководящих должностях АО &laquo;Ситибанк Казахстан&raquo;, АО &laquo;HSBC Банк Казахстан&raquo;, АО &laquo;ABN AMRO Банк Казахстан&raquo;.</p>\r\n\r\n<p>Окончил Казахскую государственную академию управления, бизнес-школу Университета Дарема (Великобритания).</p>\r\n', '1589524926_pravlenie1.jpg', 1, '2020-05-15 06:42:00'),
(7, 2, 'Рысбеков Серикжан Данасултанович', 'Заместитель Председателя Правления', '<p>C февраля 2016 года занимает должность Заместителя Председателя Правления, входит в состав Правления АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>До начала своей трудовой деятельности в Корпорации, Рысбеков С.Д. работал в Департаменте монетарных операций Национального Банка Республики Казахстан, где в рамках управления международными резервами РК занимался вопросами разработки инвестиционной стратегии, выбором внешних инвестиционных управляющих, анализом портфелей финансовых инструментов.</p>\r\n\r\n<p>В октябре 2012 года был назначен на должность директора Департамента анализа, рисков и стратегий Корпорации. С мая 2015 года по февраль 2016 года работал директором Департамента анализа и отчетности по внешним операциям Корпорации.</p>\r\n\r\n<p>Получил степень Бакалавра экономики в Казахском национальном университете имени аль-Фараби, степень MBA в Казахском экономическом университете. Имеет международный профессиональный сертификат Chartered Financial Analyst (CFA).</p>\r\n', '1589524956_pravlenie3.jpg', 1, '2020-05-15 06:42:00'),
(8, 2, 'Азимбаев Назым Аспендыевич', 'Заместитель Председателя Правления', '<p>C сентября 2019 года занимает должность Заместителя Председателя Правления, входит в состав Правления АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>С 2016 года занимал должность Управляющего директора Департамента альтернативных инвестиций Корпорации.</p>\r\n\r\n<p>Раннее занимал должность Консультанта Председателя Правления АО &quot;Казахстанская фондовая биржа&quot; (KASE).</p>\r\n\r\n<p>Получил степень Бакалавра и Магистра прикладной математики и является аспирантом по специальности Теория вероятности и математическая статистика в Московском государственном университете им. Ломоносова. Выпускник профессиональной программы риск менеджмента Massachusetts Institute of Technology.</p>\r\n', '1589524988_pravlenie4.jpg', 1, '2020-05-15 06:43:00'),
(9, 2, 'Кумарбеков Амир Агбаевич', 'Управляющий Директор', '<p>С августа 2019 года занимает должность Управляющего директора, входит в состав Правления АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>Имеет более 5 лет опыта работы в сфере бухгалтерского учета и аудита, бюджетного планирования и экономического анализа, включая опыт работы в аудиторской компании &laquo;Эрнст энд Янг&raquo; в Казахстане, также занимал различные позиции в АО &laquo;НК &laquo;КазМунайГаз&raquo;.</p>\r\n\r\n<p>Выпускник Университета КИМЭП по специальности делового администрирования и бухгалтерского учета.</p>\r\n', '1589525018_pravlenie2.jpg', 1, '2020-05-15 06:43:00'),
(10, 3, 'Мешiтбай Жамиля Есанғалиқызы', 'Стратегия', '<p>Мешiтбай Жамиля Есанғалиқызы,<br />\r\nCFA, CIPM, FRM</p>\r\n', '1589525213_comand8.jpg', 1, '2020-05-15 06:46:00'),
(11, 3, 'Бектемисов Галым Сабыржанович', 'Фонды частного рынка', '<p>Бектемисов Галым Сабыржанович,<br />\r\nCFA</p>\r\n', '1589525253_comand1.jpg', 1, '2020-05-15 06:47:00'),
(12, 3, 'Сейдали Алдияр Булатұлы', 'Хедж-фонды', '<p>Сейдали Алдияр Булатұлы,<br />\r\nCFA</p>\r\n', '1589525281_comand2.jpg', 1, '2020-05-15 06:48:00'),
(13, 3, 'Саенко Александр Игоревич', 'Риск-менеджмент', '<p>Саенко Александр Игоревич,<br />\r\nCFA, CIPM</p>\r\n', '1589525306_comand12.jpg', 1, '2020-05-15 06:48:00'),
(14, 1, 'Карашукеев Ернар Шыракбаевич', 'Казначейство', '<p>Карашукеев Ернар Шыракбаевич</p>\r\n', '1589525330_comand7.jpg', 1, '2020-05-15 06:48:00'),
(15, 1, 'Омаров Тимур Муратович', 'Портфельный анализ и отчетность', '<p>Омаров Тимур Муратович</p>\r\n', '1589525366_team1.jpg', 1, '2020-05-15 06:49:00');

-- --------------------------------------------------------

--
-- Структура таблицы `team_translation`
--

CREATE TABLE `team_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `team_translation`
--

INSERT INTO `team_translation` (`id`, `language`, `username`, `position`, `content`) VALUES
(1, 'en', '', '', ''),
(1, 'kk', '', '', ''),
(1, 'ru', 'Молдабекова Алия Мейрбековна', 'Председатель Совета директоров', '<p>С февраля 2020 года является Председателем Совета директоров АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>С 2015 года по декабрь 2019 года занимала должность Директора Департамента монетарных операций в Национальном Банке Республики Казахстан.</p>\r\n\r\n<p>C декабря 2019 года назначена на должность Заместителя Председателя Национального Банка Республики Казахстан.</p>\r\n\r\n<p>Получила степень Бакалавра и Магистра прикладной математики в Казахском государственном университете, также степень Бакалавра финансов в Евразийском национальном университете им. Л.Н. Гумилева. Является кандидатом физико-математических наук.</p>\r\n'),
(2, 'en', '', '', ''),
(2, 'kk', '', '', ''),
(2, 'ru', 'Таджияков Галымжан Бисенгалиевич', 'Член Совета директоров', '<p>C 15 мая 2019 года избран Председателем Правления и членом Совета директоров АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>До избрания, Галымжан Таджияков занимал пост заместителя Председателя Правления холдинга &laquo;Байтерек&raquo;, где занимался вопросами управления активами, корпоративных финансов, стратегии и корпоративного развития, анализа и исследований. Галымжан Таджияков обладает более чем 15-летним управленческим стажем работы в банковской сфере и корпоративном управлении, в том числе он работал на различных руководящих должностях АО &laquo;Ситибанк Казахстан&raquo;, АО &laquo;HSBC Банк Казахстан&raquo;, АО &laquo;ABN AMRO Банк Казахстан&raquo;.</p>\r\n\r\n<p>Окончил Казахскую государственную академию управления, бизнес-школу Университета Дарема (Великобритания).</p>\r\n'),
(3, 'en', '', '', ''),
(3, 'kk', '', '', ''),
(3, 'ru', 'Турсунханов Нуржан Аскарович', 'Член Совета директоров', '<p>Окончил Казахскую государственную академию управления, бизнес-школу Университета Дарема (Великобритания).</p>\r\n\r\n<p>Турсунханов Н.А. начал свою карьеру в Национальном Банке Республики Казахстан в 2004 году. В 2009 году был назначен на должность Заместителя Начальника управления, где отвечал за управление золотовалютными активами и активами Национального фонда. В 2015 году Турсунханов Н.А. был назначен на должность Заместителя Директора Департамента монетарных операций Национального Банка Республики Казахстан. С декабря 2019 года Нуржан Аскарович занимает должность Директора Департамента монетарных операций Национального Банка Республики Казахстан.</p>\r\n\r\n<p>С 2012 по 2015 год Нуржан Аскарович занимал должность Заместителя Председателя Правления в АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>Получил степень бакалавра экономики в университете Nottingham (Великобритания).</p>\r\n'),
(4, 'en', '', '', ''),
(4, 'kk', '', '', ''),
(4, 'ru', 'Жорж Сударскис', 'Член Совета директоров', '<p>С июня 2018 года является членом Совета директоров &ndash; независимым директором АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>Основатель и Управляющий партнер Sudarskis &amp; Partners &ndash; компании, предоставляющей консалтинговые услуги фондам благосостояния, государственным организациям и др. В 1998 &ndash; 2009 г.г. занимал должность главного инвестиционного директора (Chief Investment Officer of Private Equity) Abu Dhabi Investment Authority &ndash; суверенного фонда благосостояния, принадлежащего эмирату Абу-Даби. В 1975-1995 г.г. занимал различные должности в Arthur Andersen &amp; Co, Morgan Guaranty Trust Co. of NY, FIDAL, INF Bank France, Clinvest (Credit Lyonnais).</p>\r\n\r\n<p>Получил степень Бакалавра в бизнес-школе Ecole Superieure de Commerce de Paris, а также степень MBA в университете McGill.</p>\r\n'),
(5, 'en', '', '', ''),
(5, 'kk', '', '', ''),
(5, 'ru', 'Марк Кашер', 'Член Совета директоров', '<p>С июня 2018 года является членом Совета директоров &ndash; независимым директором АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>С 2013 года является Председателем Комитета по аудиту и вознаграждениям, Членом Совета директоров Luxoft, глобальной аутсорсинговой компании по разработке программного обеспечения.</p>\r\n\r\n<p>В 2016 &ndash; 2017 г.г. занимал должность Управляющего директора Highland Capital, региональной структуры частного капитала для инвестиций в Центральноазиатский рынок. В 2015-2016 г.г. занимал должность Члена Совета директоров BI Group, казахстанской строительной компании. В 2014-2015 г.г. занимал должность Директора частного капитала в Verno Investment Management Limited. В 1997-2015 г.г. занимал должность Управляющего директора Pinebridge Investments, также занимал должности Председателя правления Pinebridge Investments Russia, Ltd. и Инвестиционного директора AIG Silk Road Capital Management. В 1994-1995 г.г. был консультантом United States Agency for International Development (USAID) по проектам в России, Украине, Узбекистане и Кыргызстане.</p>\r\n\r\n<p>Получил степень Бакалавра в университете Tufts, а также степень MBA в университете Georgetown.</p>\r\n'),
(6, 'en', '', '', ''),
(6, 'kk', '', '', ''),
(6, 'ru', 'Таджияков Галымжан Бисенгалиевич', 'Председатель правления', '<p>C 15 мая 2019 года избран Председателем Правления и членом Совета директоров АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>До избрания, Галымжан Таджияков занимал пост заместителя Председателя Правления холдинга &laquo;Байтерек&raquo;, где занимался вопросами управления активами, корпоративных финансов, стратегии и корпоративного развития, анализа и исследований. Галымжан Таджияков обладает более чем 15-летним управленческим стажем работы в банковской сфере и корпоративном управлении, в том числе он работал на различных руководящих должностях АО &laquo;Ситибанк Казахстан&raquo;, АО &laquo;HSBC Банк Казахстан&raquo;, АО &laquo;ABN AMRO Банк Казахстан&raquo;.</p>\r\n\r\n<p>Окончил Казахскую государственную академию управления, бизнес-школу Университета Дарема (Великобритания).</p>\r\n'),
(7, 'en', '', '', ''),
(7, 'kk', '', '', ''),
(7, 'ru', 'Рысбеков Серикжан Данасултанович', 'Заместитель Председателя Правления', '<p>C февраля 2016 года занимает должность Заместителя Председателя Правления, входит в состав Правления АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>До начала своей трудовой деятельности в Корпорации, Рысбеков С.Д. работал в Департаменте монетарных операций Национального Банка Республики Казахстан, где в рамках управления международными резервами РК занимался вопросами разработки инвестиционной стратегии, выбором внешних инвестиционных управляющих, анализом портфелей финансовых инструментов.</p>\r\n\r\n<p>В октябре 2012 года был назначен на должность директора Департамента анализа, рисков и стратегий Корпорации. С мая 2015 года по февраль 2016 года работал директором Департамента анализа и отчетности по внешним операциям Корпорации.</p>\r\n\r\n<p>Получил степень Бакалавра экономики в Казахском национальном университете имени аль-Фараби, степень MBA в Казахском экономическом университете. Имеет международный профессиональный сертификат Chartered Financial Analyst (CFA).</p>\r\n'),
(8, 'en', '', '', ''),
(8, 'kk', '', '', ''),
(8, 'ru', 'Азимбаев Назым Аспендыевич', 'Заместитель Председателя Правления', '<p>C сентября 2019 года занимает должность Заместителя Председателя Правления, входит в состав Правления АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>С 2016 года занимал должность Управляющего директора Департамента альтернативных инвестиций Корпорации.</p>\r\n\r\n<p>Раннее занимал должность Консультанта Председателя Правления АО &quot;Казахстанская фондовая биржа&quot; (KASE).</p>\r\n\r\n<p>Получил степень Бакалавра и Магистра прикладной математики и является аспирантом по специальности Теория вероятности и математическая статистика в Московском государственном университете им. Ломоносова. Выпускник профессиональной программы риск менеджмента Massachusetts Institute of Technology.</p>\r\n'),
(9, 'en', '', '', ''),
(9, 'kk', '', '', ''),
(9, 'ru', 'Кумарбеков Амир Агбаевич', 'Управляющий Директор', '<p>С августа 2019 года занимает должность Управляющего директора, входит в состав Правления АО &laquo;Национальная инвестиционная корпорация Национального Банка Казахстана&raquo;.</p>\r\n\r\n<p>Имеет более 5 лет опыта работы в сфере бухгалтерского учета и аудита, бюджетного планирования и экономического анализа, включая опыт работы в аудиторской компании &laquo;Эрнст энд Янг&raquo; в Казахстане, также занимал различные позиции в АО &laquo;НК &laquo;КазМунайГаз&raquo;.</p>\r\n\r\n<p>Выпускник Университета КИМЭП по специальности делового администрирования и бухгалтерского учета.</p>\r\n'),
(10, 'en', '', '', ''),
(10, 'kk', '', '', ''),
(10, 'ru', 'Мешiтбай Жамиля Есанғалиқызы', 'Стратегия', '<p>Мешiтбай Жамиля Есанғалиқызы,<br />\r\nCFA, CIPM, FRM</p>\r\n'),
(11, 'en', '', '', ''),
(11, 'kk', '', '', ''),
(11, 'ru', 'Бектемисов Галым Сабыржанович', 'Фонды частного рынка', '<p>Бектемисов Галым Сабыржанович,<br />\r\nCFA</p>\r\n'),
(12, 'en', '', '', ''),
(12, 'kk', '', '', ''),
(12, 'ru', 'Сейдали Алдияр Булатұлы', 'Хедж-фонды', '<p>Сейдали Алдияр Булатұлы,<br />\r\nCFA</p>\r\n'),
(13, 'en', '', '', ''),
(13, 'kk', '', '', ''),
(13, 'ru', 'Саенко Александр Игоревич', 'Риск-менеджмент', '<p>Саенко Александр Игоревич,<br />\r\nCFA, CIPM</p>\r\n'),
(14, 'en', '', '', ''),
(14, 'kk', '', '', ''),
(14, 'ru', 'Карашукеев Ернар Шыракбаевич', 'Казначейство', '<p>Карашукеев Ернар Шыракбаевич</p>\r\n'),
(15, 'en', '', '', ''),
(15, 'kk', '', '', ''),
(15, 'ru', 'Омаров Тимур Муратович', 'Портфельный анализ и отчетность', '<p>Омаров Тимур Муратович</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `think_tank`
--

CREATE TABLE `think_tank` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaDesc` text COLLATE utf8_unicode_ci,
  `metaKey` text COLLATE utf8_unicode_ci,
  `date` date NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `think_tank_translation`
--

CREATE TABLE `think_tank_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `metaName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaDesc` text COLLATE utf8_unicode_ci,
  `metaKey` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '0',
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `email`, `auth_key`, `password_hash`, `token`, `role`, `status`, `created_at`, `updated_at`) VALUES
(1, 'developer@neor.info', 'Jda-l5Tlc6w3_HUYQd0S__xgquoqkZ9v', '$2y$13$4CdvdT.vDNeudoxxnc.TVOWkaxD1wGwu.f1sZT4UBrI9HR1VZQ3t2', NULL, 16, 1, '2020-01-26 18:00:00', '2020-03-20 03:38:57'),
(4, 'manager@a-lux.kz', '5rFeR9fU5nRAkNsssf8-Js8ikHECJRdY', '$2y$13$uP6HzkJvHtgvSbIeHehZ/uX9ok3PIJ6bWOKTzYlHYZ3dgKIOOHgna', 'KHleNzYAesp3pdxsNZ4ruFnzv8ANjQmi_1587102697', 1, 1, '2020-04-17 02:51:36', '2020-04-17 02:51:36');

-- --------------------------------------------------------

--
-- Структура таблицы `values_investments`
--

CREATE TABLE `values_investments` (
  `id` int(11) NOT NULL,
  `classes_id` int(11) DEFAULT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `values_investments`
--

INSERT INTO `values_investments` (`id`, `classes_id`, `name`, `content`, `image`, `created_at`) VALUES
(1, 1, '123123', '[\"123\",\"asd\",\"\"]', '', '2020-05-14 10:58:00');

-- --------------------------------------------------------

--
-- Структура таблицы `values_investments_translation`
--

CREATE TABLE `values_investments_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `values_investments_translation`
--

INSERT INTO `values_investments_translation` (`id`, `language`, `name`, `content`) VALUES
(1, 'en', '', '[\"zxc\",\"zxc\"]'),
(1, 'kk', '', '[\"zxc\",\"zxc\"]'),
(1, 'ru', '123123', '[\"123\",\"asd\",\"\"]');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `about_path`
--
ALTER TABLE `about_path`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `about_path_translation`
--
ALTER TABLE `about_path_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_about_path_translation_language` (`language`);

--
-- Индексы таблицы `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_banner_menu` (`menu_id`);

--
-- Индексы таблицы `banner_translation`
--
ALTER TABLE `banner_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_banner_translation_language` (`language`);

--
-- Индексы таблицы `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `careers_translation`
--
ALTER TABLE `careers_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_careers_translation_language` (`language`);

--
-- Индексы таблицы `classes_investments`
--
ALTER TABLE `classes_investments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `classes_investments_translation`
--
ALTER TABLE `classes_investments_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_classes_investments_translation_language` (`language`);

--
-- Индексы таблицы `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contact_translation`
--
ALTER TABLE `contact_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_contact_translation_language` (`language`);

--
-- Индексы таблицы `core_satellite`
--
ALTER TABLE `core_satellite`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `core_satellite_translation`
--
ALTER TABLE `core_satellite_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_core_satellite_translation_language` (`language`);

--
-- Индексы таблицы `documents_confidential`
--
ALTER TABLE `documents_confidential`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `documents_confidential_translation`
--
ALTER TABLE `documents_confidential_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_documents_confidential_translation_language` (`language`);

--
-- Индексы таблицы `investment_portfolio`
--
ALTER TABLE `investment_portfolio`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `investment_portfolio_translation`
--
ALTER TABLE `investment_portfolio_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_investment_portfolio_translation_language` (`language`);

--
-- Индексы таблицы `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `main_advantages`
--
ALTER TABLE `main_advantages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `main_advantages_translation`
--
ALTER TABLE `main_advantages_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_main_advantages_translation_language` (`language`);

--
-- Индексы таблицы `management_structure`
--
ALTER TABLE `management_structure`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `management_structure_translation`
--
ALTER TABLE `management_structure_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_management_structure_translation_language` (`language`);

--
-- Индексы таблицы `membership`
--
ALTER TABLE `membership`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `membership_translation`
--
ALTER TABLE `membership_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_membership_translation_language` (`language`);

--
-- Индексы таблицы `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menu_translation`
--
ALTER TABLE `menu_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_menu_translation_language` (`language`);

--
-- Индексы таблицы `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_message_language` (`language`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `procurement`
--
ALTER TABLE `procurement`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `procurement_documents`
--
ALTER TABLE `procurement_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_procurement_documents_procurement` (`procurement_id`);

--
-- Индексы таблицы `procurement_documents_translation`
--
ALTER TABLE `procurement_documents_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_procurement_documents_translation_language` (`language`);

--
-- Индексы таблицы `procurement_translation`
--
ALTER TABLE `procurement_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_procurement_translation_language` (`language`);

--
-- Индексы таблицы `procurement_user`
--
ALTER TABLE `procurement_user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `risk`
--
ALTER TABLE `risk`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `risk_system`
--
ALTER TABLE `risk_system`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `risk_system_translation`
--
ALTER TABLE `risk_system_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_risk_system_translation_language` (`language`);

--
-- Индексы таблицы `risk_translation`
--
ALTER TABLE `risk_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_risk_translation_language` (`language`);

--
-- Индексы таблицы `source_message`
--
ALTER TABLE `source_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_source_message_category` (`category`);

--
-- Индексы таблицы `submenu`
--
ALTER TABLE `submenu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_submenu_menu` (`menu_id`);

--
-- Индексы таблицы `submenu_translation`
--
ALTER TABLE `submenu_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_submenu_translation_language` (`language`);

--
-- Индексы таблицы `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_team_management_structure` (`management_id`);

--
-- Индексы таблицы `team_translation`
--
ALTER TABLE `team_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_team_translation_language` (`language`);

--
-- Индексы таблицы `think_tank`
--
ALTER TABLE `think_tank`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `think_tank_translation`
--
ALTER TABLE `think_tank_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_think_tank_translation_language` (`language`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `token` (`token`);

--
-- Индексы таблицы `values_investments`
--
ALTER TABLE `values_investments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_values_investments_classes_investments` (`classes_id`);

--
-- Индексы таблицы `values_investments_translation`
--
ALTER TABLE `values_investments_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_values_investments_translation_language` (`language`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `about_path`
--
ALTER TABLE `about_path`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `careers`
--
ALTER TABLE `careers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `classes_investments`
--
ALTER TABLE `classes_investments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `core_satellite`
--
ALTER TABLE `core_satellite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `documents_confidential`
--
ALTER TABLE `documents_confidential`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `investment_portfolio`
--
ALTER TABLE `investment_portfolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `logo`
--
ALTER TABLE `logo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `main_advantages`
--
ALTER TABLE `main_advantages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `management_structure`
--
ALTER TABLE `management_structure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `membership`
--
ALTER TABLE `membership`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `procurement`
--
ALTER TABLE `procurement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `procurement_documents`
--
ALTER TABLE `procurement_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `procurement_user`
--
ALTER TABLE `procurement_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `risk`
--
ALTER TABLE `risk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `risk_system`
--
ALTER TABLE `risk_system`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `source_message`
--
ALTER TABLE `source_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `submenu`
--
ALTER TABLE `submenu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `think_tank`
--
ALTER TABLE `think_tank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `values_investments`
--
ALTER TABLE `values_investments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `about_path_translation`
--
ALTER TABLE `about_path_translation`
  ADD CONSTRAINT `fk_about_path_translation_about_path` FOREIGN KEY (`id`) REFERENCES `about_path` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `banner`
--
ALTER TABLE `banner`
  ADD CONSTRAINT `fk_banner_menu` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `banner_translation`
--
ALTER TABLE `banner_translation`
  ADD CONSTRAINT `fk_banner_translation_banner` FOREIGN KEY (`id`) REFERENCES `banner` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `careers_translation`
--
ALTER TABLE `careers_translation`
  ADD CONSTRAINT `fk_careers_translation_careers` FOREIGN KEY (`id`) REFERENCES `careers` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `classes_investments_translation`
--
ALTER TABLE `classes_investments_translation`
  ADD CONSTRAINT `fk_classes_investments_translation_classes_investments` FOREIGN KEY (`id`) REFERENCES `classes_investments` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `contact_translation`
--
ALTER TABLE `contact_translation`
  ADD CONSTRAINT `fk_contact_translation_contact` FOREIGN KEY (`id`) REFERENCES `contact` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `core_satellite_translation`
--
ALTER TABLE `core_satellite_translation`
  ADD CONSTRAINT `fk_core_satellite_translation_core_satellite` FOREIGN KEY (`id`) REFERENCES `core_satellite` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `documents_confidential_translation`
--
ALTER TABLE `documents_confidential_translation`
  ADD CONSTRAINT `fk_documents_confidential_translation_documents_confidential` FOREIGN KEY (`id`) REFERENCES `documents_confidential` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `investment_portfolio_translation`
--
ALTER TABLE `investment_portfolio_translation`
  ADD CONSTRAINT `fk_investment_portfolio_translation_investment_portfolio` FOREIGN KEY (`id`) REFERENCES `investment_portfolio` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `main_advantages_translation`
--
ALTER TABLE `main_advantages_translation`
  ADD CONSTRAINT `fk_main_advantages_translation_main_advantages` FOREIGN KEY (`id`) REFERENCES `main_advantages` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `management_structure_translation`
--
ALTER TABLE `management_structure_translation`
  ADD CONSTRAINT `fk_management_structure_translation_management_structure` FOREIGN KEY (`id`) REFERENCES `management_structure` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `membership_translation`
--
ALTER TABLE `membership_translation`
  ADD CONSTRAINT `fk_membership_translation_membership` FOREIGN KEY (`id`) REFERENCES `membership` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `menu_translation`
--
ALTER TABLE `menu_translation`
  ADD CONSTRAINT `fk_menu_translation_menu` FOREIGN KEY (`id`) REFERENCES `menu` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `fk_message_source_message` FOREIGN KEY (`id`) REFERENCES `source_message` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `procurement_documents`
--
ALTER TABLE `procurement_documents`
  ADD CONSTRAINT `fk_procurement_documents_procurement` FOREIGN KEY (`procurement_id`) REFERENCES `procurement` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `procurement_documents_translation`
--
ALTER TABLE `procurement_documents_translation`
  ADD CONSTRAINT `fk_procurement_documents_translation_procurement_documents` FOREIGN KEY (`id`) REFERENCES `procurement_documents` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `procurement_translation`
--
ALTER TABLE `procurement_translation`
  ADD CONSTRAINT `fk_procurement_translation_procurement` FOREIGN KEY (`id`) REFERENCES `procurement` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `risk_system_translation`
--
ALTER TABLE `risk_system_translation`
  ADD CONSTRAINT `fk_risk_system_translation_risk_system` FOREIGN KEY (`id`) REFERENCES `risk_system` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `risk_translation`
--
ALTER TABLE `risk_translation`
  ADD CONSTRAINT `fk_risk_translation_risk` FOREIGN KEY (`id`) REFERENCES `risk` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `submenu`
--
ALTER TABLE `submenu`
  ADD CONSTRAINT `fk_submenu_menu` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `submenu_translation`
--
ALTER TABLE `submenu_translation`
  ADD CONSTRAINT `fk_submenu_translation_submenu` FOREIGN KEY (`id`) REFERENCES `submenu` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `team`
--
ALTER TABLE `team`
  ADD CONSTRAINT `fk_team_management_structure` FOREIGN KEY (`management_id`) REFERENCES `management_structure` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `team_translation`
--
ALTER TABLE `team_translation`
  ADD CONSTRAINT `fk_team_translation_team` FOREIGN KEY (`id`) REFERENCES `team` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `think_tank_translation`
--
ALTER TABLE `think_tank_translation`
  ADD CONSTRAINT `fk_think_tank_translation_think_tank` FOREIGN KEY (`id`) REFERENCES `think_tank` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `values_investments`
--
ALTER TABLE `values_investments`
  ADD CONSTRAINT `fk_values_investments_classes_investments` FOREIGN KEY (`classes_id`) REFERENCES `classes_investments` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `values_investments_translation`
--
ALTER TABLE `values_investments_translation`
  ADD CONSTRAINT `fk_values_investments_translation_values_investments` FOREIGN KEY (`id`) REFERENCES `values_investments` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
