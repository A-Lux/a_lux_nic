<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAssetLanguage extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/font/css/all.css',
        '/css/site.css',
        '/css/bootstrap.min.css',
        '/css/corporation.css',
        '/css/informationcentre.css',
        '/css/investment.css',
        '/css/career.css',
        '/css/fonts.css',
        '/css/slick.css',
        '/css/fm.revealator.jquery.min.css',
        '/css/home.css',
        '/css/map.css',
        '/css/news.css',
        '/css/search.css',
        '/css/registration.css',
        "http://allfont.ru/allfont.css?fonts=yanone-kaffeesatz"
    ];
    public $js = [
        "/js/jquery-2.2.4.min.js",
        "/js/career.js",
        "/js/carrer.js",
        "/js/documents.js",
        "/js/bootstrap.min.js",
        "/js/fm.revealator.jquery.min.js",
        "/js/index-page.js",
        "/js/slick.min.js",
        "/js/corparation.js",
        "/js/jquery.easings.min.js",
        "/js/scrolloverflow.min.js.js",
        "/js/jquery.fullPage.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js",
        "/js/main.js",
    ];
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
//        'yii\web\JqueryAsset',
    ];
}
