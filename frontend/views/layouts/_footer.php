<?php

/* @var $this View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\helpers\ArrayHelper;


$menu       = Yii::$app->view->params['menu'];
$submenu    = Yii::$app->view->params['submenu'];
$contact    = \Yii::$app->view->params['contact'];
$menuKey    = Yii::$app->view->params['menuKey'];

?>

<hr class="footer_line" style="margin-top: 0" />
<div class="container-fluid">
    <div class="footer">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-6 col-xl-2 col-lg-2 footer__col pr-0">
                <div class="footer__logo mb-2">
                   <a href="/"> <img src="<?= Yii::$app->view->params['logoFooter']->getImage(); ?>" alt="" /></a>

                </div>
                <div class="footer__number">
                    <p><?= $contact->phone ?></p>
                </div>
                <div class="footer__email">
                    <?= Yii::t('main-footer', 'Email:'); ?> <?= $contact->email ?>
                </div>
            </div>
            <? foreach ($menu as $item) : ?>
                <div id="footer_<?= $item->id ?>" class="col-12 col-sm-6 col-md-6 <?= !empty($item->submenus) ? 'col-xl-3' : 'col-xl-1' ?> col-lg-2 footer__col">
                    <div class="footer__col_title">
                        <a href="<?= $item->url; ?>"><?= $item->title; ?></a>
                    </div>
                    <? foreach ($submenu as $value) : ?>
                        <? if ($value->menu_id == $item->id) : ?>
                            <div class="footer__col_list sub-list">
                                <a data-slide="1" href="<?= $item->url; ?>#<?= $value->url; ?>"><?= $value->title; ?></a>
                            </div>
                        <? endif; ?>
                    <? endforeach; ?>
                </div>
            <? endforeach; ?>

        </div>
    </div>
</div>
<hr class="footer_line_bottom" />
<div class="container">
    <div class="row end_row">
        <div class="col-sm-4 end_col" style="text-align: left">
            <div class="adress-bottom"><?= $contact->address ?></div>
            <div class="adress-bottom"><?= Yii::t('main-footer', '© 2012-'.date('Y').' NIC NBK.'); ?></div>
        </div>
        <div class="col-sm-4 end_col">
            <div class="footer__col_list text-center link-lux">
                <a href="/site/map">
                    <?= ArrayHelper::getValue($menuKey, 'map')->title; ?>
                </a>
                <!-- <a href="https://www.a-lux.kz/" class="ml-5">
                   Сделано в <img src="/images/a-lux.png" alt="" />
                </a> -->
            </div>

        </div>
        <div class="col-sm-4 end_col col-btn pr-0" style="text-align: right">
            <button type="button" class="end_col_btn btn-invest" style="color: white!important;border:none;" data-toggle="modal" data-target="#modalContact">
                <?= Yii::t('main-footer', 'Контакты'); ?>
            </button>

            <!-- Modal -->
            <div class="modal fade" id="modalContact" tabindex="-1" role="dialog" aria-labelledby="modalContactLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content contact-content">
                        <div class="modal-body text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="row">
                                <div class="col-xl-8">
                                    <div class="text-center col-xl-6 modal-logo">
                                        <img src="<?= Yii::$app->view->params['logoFooter']->getImage(); ?>" alt="" />
                                    </div>
                                    <div class="mt-4 text-left">
                                        <p>
                                            <?= Yii::t('main-footer', 'АО «Национальная инвестиционная корпорация Национального Банка Казахстана»'); ?>

                                        </p>
                                    </div>
                                </div>
                                <div class="col-xl-7 col-md-6 d-flex align-items-start align-items-md-end">
                                    <p><?= $contact->address ?></p>


                                </div>
                                <div class="col-xl-5 col-md-6">
                                    <div class="mod-footer">
                                        <p style="wihte-space: nowrap"><span><?= Yii::t('main', 'тел.:'); ?></span>
                                            <a href="tel:<?= $contact->phone ?>">
                                                <?= $contact->phone ?>
                                            </a></p>
                                        <p><a href="mailto:<?= $contact->email ?>" class="link-nic">
                                                <?= $contact->email ?>
                                            </a></p>
                                        <p><a href="<?= $contact->website ?>" class="link-nic">
                                                <?= $contact->website ?>
                                            </a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>