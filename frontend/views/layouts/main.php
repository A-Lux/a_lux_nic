<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use frontend\assets\AppAssetLanguage;
use common\widgets\ToastrAlert;


AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133405815-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-133405815-1');
    </script>


    <meta charset="<?= Yii::$app->charset ?>">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta
            name="viewport"
            content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
    />
    <meta name="format-detection" content="telephone=no"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link  rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css "/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>

    <?php $this->head() ?>
    <link rel="icon" type="image/png" href="/images/favicon.png">
</head>
<body>
<?php $this->beginBody() ?>

<!-- HEADER -->
<?= $this->render('_header') ?>
<!-- END HEADER -->

<?= \yii2mod\alert\Alert::widget() ?>
<!-- CONTENT -->
    <?= $content ?>
<!-- END CONTENT -->

<!-- FOOTER -->
<?= $this->render('_footer') ?>
<!-- END FOOTER -->

    <!-- ZERO.kz -->
    <span id="_zero_72175" style="display: block; text-align: left;">
          <noscript>
            <a href="http://zero.kz/?s=72175" target="_blank">
              <img src="http://c.zero.kz/z.png?u=72175" width="88" height="31" alt="ZERO.kz" />
            </a>
          </noscript>
        </span>

    <script type="text/javascript"><!--
        var _zero_kz_ = _zero_kz_ || [];
        _zero_kz_.push(["id", 72175]);
        // Цвет кнопки
        _zero_kz_.push(["type", 1]);
        // Проверять url каждые 200 мс, при изменении перегружать код счётчика
        // _zero_kz_.push(["url_watcher", 200]);

        (function () {
            var a = document.getElementsByTagName("script")[0],
                s = document.createElement("script");
            s.type = "text/javascript";
            s.async = true;
            s.src = (document.location.protocol == "https:" ? "https:" : "http:")
                + "//c.zero.kz/z.js";
            a.parentNode.insertBefore(s, a);
        })(); //-->
    </script>
    <!-- End ZERO.kz -->

<!-- Modal -->
<!-- Modal -->

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
        <script  src = "https://unpkg.com/aos@next/dist/aos.js"> </script> 
        <script>
    AOS.init (); 
   
  </script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
