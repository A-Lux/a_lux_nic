<?php

/* @var $this View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;


$menu       = Yii::$app->view->params['menu'];
$submenu    = Yii::$app->view->params['submenu'];
$sliders    = Yii::$app->view->params['sliders'];

?>
<header>
    <div id="header" class="container-fluid nav_container">
        <nav class="navbar navbar-expand-xl navbar-light navbar-inverse">
            <div class="navbar_logo">
                <a href="/"><img src="<?= Yii::$app->view->params['logoHeader']->getImage(); ?>" alt="" class="header_logo" /></a>

                <div class="logo_content" style="line-height: 1.5!important;">
                    <a href="/">
                        <?= Yii::t('main-header', 'Национальная инвестиционная корпорация <br /> Национального Банка <br> Казахстана'); ?>
                    </a>
                </div>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="dropdown" data-target="#collapsingNavbarLg" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsingNavbarLg">
                <ul class="navbar-nav">
                    <? foreach ($menu as $item) : ?>
                        <li class="nav-item nav-dropdown">
                            <a class="nav-link hover-link-1" href="<?= $item->url; ?>">
                                <?= $item->title; ?>
                            </a>

                            <div class="dropdown-mobile">
                                <? if (!empty($item->submenus)) : ?>
                                    <ul>
                                        <? foreach ($submenu as $value) : ?>
                                            <? if ($value->menu->id == $item->id) : ?>
                                                <li>
                                                    <a href="<?= $item->url; ?>#<?= $value->url; ?>">
                                                        <?= $value->title; ?>
                                                    </a>
                                                </li>
                                            <? endif; ?>
                                        <? endforeach; ?>
                                    </ul>
                                <? else : ?>
                                    <ul>
                                        <li></li>
                                        <li></li>
                                    </ul>
                                <? endif; ?>
                            </div>

                            <? if (!empty($item->submenus)) : ?>
                                <div class="dropdown-menu">
                                    <div></div>
                                    <div class="row drop-row" id="hover_id">
                                        <div class="dropdown-right">
                                            <div class="row m-0 dropdown_block">
                                                <div class="col-xl-5 pl-1 header-link-left">
                                                    <div class="drop-right-text pt-2 pb-3 header-link-info">
                                                        <div class="hover_box_el"><?= $item->title; ?></div>
                                                        <p>
                                                            <?= $item->content; ?>
                                                        </p>
                                                    </div>
                                                    <div class="header-link-btn">
                                                        <? if (!(Yii::$app->controller->id == $item->key)) : ?>
                                                            <a href="<?= $item->url; ?>" class="hover_box_btn">
                                                                <?= Yii::t('main', 'Подробнее'); ?>
                                                            </a>
                                                        <? endif; ?>
                                                    </div>
                                                </div>
                                                <div class="col-xl-7 hover_box_bg block_pt_invest">
                                                    <? foreach ($submenu as $value) : ?>
                                                        <? if ($value->menu->id == $item->id) : ?>
                                                            <? if ($value->id == 1) : ?>
                                                                <div class="hover_box_list sub-list">
                                                                    <a href="<?= $item->url; ?>#<?= $value->url; ?>" data-slide="1" class="sub-title">
                                                                        <?= $value->title; ?>
                                                                    </a>
                                                                    <div class="submenu">
                                                                        <div class="submenu-list">
                                                                            <ul>
                                                                                <? if(isset($sliders[0])): ?>
                                                                                    <? foreach ($sliders[0] as $slider): ?>
                                                                                        <li>
                                                                                            <a href="<?= $item->url; ?>#<?= $value->url; ?>" data-slide="<?= $slider->id; ?>">
                                                                                                <?= $slider->title; ?>
                                                                                            </a>
                                                                                        </li>
                                                                                    <? endforeach;?>
                                                                                <? endif; ?>
                                                                            </ul>
                                                                            <ul>
                                                                                <? if(isset($sliders[1])): ?>
                                                                                    <? foreach ($sliders[1] as $slider): ?>
                                                                                        <li>
                                                                                            <a href="<?= $item->url; ?>#<?= $value->url; ?>" data-slide="<?= $slider->id; ?>">
                                                                                                <?= $slider->title; ?>
                                                                                            </a>
                                                                                        </li>
                                                                                    <? endforeach;?>
                                                                                <? endif; ?>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <? else : ?>
                                                                <div class="hover_box_list">
                                                                    <a href="<?= $item->url; ?>#<?= $value->url; ?>">
                                                                        <?= $value->title; ?>
                                                                    </a>
                                                                </div>
                                                            <? endif; ?>
                                                        <? endif; ?>
                                                    <? endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? else : ?>
                                <div class="dropdown-menu"></div>

                                <div class="dropdown-menu drop-menu-4">
                                    <div class="row drop-row" id="hover_id">
                                        <div class="dropdown-right drop-right-text pt-0 pb-3">
                                            <div class="row m-0 dropdown_block">
                                                <div class="col-xl-5 pl-1 header-link-left">
                                                    <div class="drop-right-text pt-2 pb-3 header-link-info">
                                                        <div class="hover_box_el"><?= $item->title; ?></div>
                                                        <p>
                                                            <?= $item->content; ?>
                                                        </p>
                                                    </div>
                                                    <? if (!(Yii::$app->controller->id == $item->key)) : ?>
                                                        <div class="header-link-btn">
                                                            <a href="/career" class="hover_box_btn">
                                                                <?= Yii::t('main', 'Подробнее'); ?>
                                                            </a>
                                                        </div>
                                                    <? endif; ?>
                                                </div>
                                                <div class="col-xl-7 hover_box_bg e">
                                                    <div class="hover_box_list"><a href="#"></a></div>
                                                    <div class="hover_box_list"><a href="#"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endif; ?>
                        </li>
                    <? endforeach; ?>
                </ul>
                <div class="nav__lang nav__lang-mobile">
                    <? $counter = 0; ?>
                    <? foreach (Yii::$app->view->params['language'] as $key => $value): ?>
                        <? $counter++; ?>
                        <a class="nav__lg <?= Yii::$app->language == $value->code ? 'active' : '' ?>"
                           href="<?= Url::to(array_merge(\Yii::$app->request->get(),
                               [\Yii::$app->controller->route, 'language' => $value->code])) ?>">
                            <? if($counter == 1): ?>
                                <?= $value->name; ?> |
                            <? elseif($counter == count(Yii::$app->view->params['language'])): ?>
                                &nbsp;<?= $value->name; ?>
                            <? else: ?>
                                &nbsp;<?= $value->name; ?>  |
                            <? endif; ?>
                        </a>
                    <? endforeach; ?>
                </div>
            </div>
            <div class="nav__lang nav__lang-desk">
                <? $counter = 0; ?>
                <? foreach (Yii::$app->view->params['language'] as $key => $value): ?>
                <? $counter++; ?>
                    <a class="nav__lg <?= Yii::$app->language == $value->code ? 'active' : '' ?>"
                       href="<?= Url::to(array_merge(\Yii::$app->request->get(),
                        [\Yii::$app->controller->route, 'language' => $value->code])) ?>">
                        <? if($counter == 1): ?>
                            <?= $value->name; ?> |
                        <? elseif($counter == count(Yii::$app->view->params['language'])): ?>
                            &nbsp;<?= $value->name; ?>
                        <? else: ?>
                            &nbsp;<?= $value->name; ?>  |
                        <? endif; ?>
                    </a>
                <? endforeach; ?>
            </div>
        </nav>
    </div>
</header>
