<?php

/* @var $this View */
/* @var $publication ThinkTank */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\ThinkTank;

?>

<!-- <div class="test">
    <div class="container-fluid news-top banner-top">
        <div class="banner-title text-center">
            <span id="top__title_text "><?= Yii::t('main-publications', 'Публикации'); ?></span>
        </div>
    </div>
</div> -->
<div class="news__wrapper">
    <div class="container">
        <h2 class="news__title"><?= $publication->title; ?></h2>
        <div class="news__date news-date-info"><?= $publication->date; ?></div>
        <div class="news__content">
            <div class="mb-2">
                <!-- <img class="news__image" src="<?= $publication->getBannerImage(); ?>" alt=""> -->
            </div>
            <?= $publication->content; ?>
        </div>
    </div>
</div>

<div class="buttons">
    <div class="container">
        <a href="/information-centre" class="bottom__button news-back"><?= Yii::t('main-publications', 'Назад'); ?></a>
    </div>
</div>

