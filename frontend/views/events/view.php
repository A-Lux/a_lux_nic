<?php

/* @var $this View */
/* @var $documents DocumentsConfidential */


use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;
use common\models\DocumentsConfidential;

?>

<div class="events">
    <div class="container-fluid">
        <div class="login-info">
            <div class="account">
                <i class="fas fa-user"></i>
                <p>Аккаунт</p>
            </div>
            <div class="logout-info">
                <a href="<?= Url::to(['logout']);?>">Выход</a>
            </div>
        </div>
        <div class="events-content">
            <? foreach ($documents as $document): ?>
                <div class="invest__box procurement-info events-info">
                    <div class="doc-name events-name">
                        <div class="doc-zoom">
                            <img src="/images/doc.svg" alt="">
                        </div>
                        <div>
                            <h1> <?= $document->title; ?> </h1>
                            <p><?= $document->content; ?></p>
                        </div>
                    </div>
                </div>
                <div class="procurement-download procurement-download-btn">
                    <a href="<?= $document->getFile() ?>" target="_blank">
                        <div class="download_box procurements-wrapper">
                            <div class="download_box_title procurements-wrapper-title">
                                <?= $document->name; ?>
                            </div>
                            <div class="download__subtitle">
                                <div class="download_left_col"><?= $document->extension; ?></div>
                                <img class="download_right_col download_procurement" src="/images/download.svg" alt="">
                            </div>
                        </div>
                    </a>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>