<?php

?>
<div class="container-fluid">
    <div class="authorization">
        <h3>Авторизация</h3>
        <form class="reg__form authorization-form" id="authorization-form" name="authorizationForm">
            <input type="hidden"
                   name="<?= Yii::$app->request->csrfParam ?>"
                   value="<?= Yii::$app->request->getCsrfToken() ?>" />

            <label class="form__label rezume-label authorization-label">
                <p class="label__text">Логин <span class="required">*</span></p>
                <input type="text" id="name" name="LoginForm[email]" required>
            </label>
            <label class="form__label rezume-label authorization-label">
                <p class="label__text">Пароль<span class="required">*</span></p>
                <input type="password" id="name" name="LoginForm[password]" required>
            </label>
            <button type="submit" class="button button--reg">Авторизоваться</button>
        </form>
    </div>
</div>