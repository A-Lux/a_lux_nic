<?php

/* @var $this View */

/* @var $portfolio InvestmentPortfolio */
/* @var $coreSatellite CoreSatellite */
/* @var $classesInvestments ClassesInvestments */
/* @var $risk Risk */
/* @var $riskSystem RiskSystem */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;
use common\models\InvestmentPortfolio;
use common\models\CoreSatellite;
use common\models\ClassesInvestments;
use common\models\Risk;
use common\models\RiskSystem;


?>

<div class="container-fluid header_container investments-top banner-top" style="display: none">
    <div class="header__title banner-title">
        <span id="top__title_text "><?= $banner->title; ?></span>

    </div>
</div>

<div class="salutation" >
    <div class="container-fluid">
        <div class="portfel-title title-collapse">
            <h3 class="title" id="portfel"><?= $portfolio->title; ?></h3>
            <img class="pointer-mobile" src="/images/pointer-mobile.png" alt="">
            <span class="line portfel-line  "></span>
        </div>
        <div class="investment-descr">
            <?= $portfolio->content; ?>
            <div class="container invest">
                <div class="row inevst_row m-0">
                    <div class="col-sm-4 col-md-5">
                        <div class="invest__general_cub">
                            <div class="invest__min_cub">
                                <div class="invest__min_cub_cont ">
                                    <?= Yii::t('main-investment', 'CORE'); ?>
                                </div>
                            </div>
                            <div class="invest__polygon ">
                                <?= Yii::t('main-investment', 'SATELLITE'); ?>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-7 col-md-6 invest__col_information">
                        <div class="invest__title">
                            <?= Yii::t('main-investment', 'Подход «core-satellite»'); ?>
                        </div>
                        <? foreach ($coreSatellite as $item) : ?>
                        <div class="invest__box" id="invest-<?= $item->id; ?>">
                            <div class="invest__box_title">
                                <?= $item->title; ?>
                            </div>
                            <div class="invest__box_description">
                                <?= $item->content; ?>
                            </div>
                        </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div>

    </div>


</div>



<? foreach ($classesInvestments as $investment) : ?>
<? if ($investment->id == 1) : ?>
<div class="title-collapse">
    <h3 class="title title-alternative " id="alternative"><?= ArrayHelper::getValue($submenu, 'alternative')->title; ?></h3>
    <span class="line  "></span>
    <h3 class="title p-0"> <?= $investment->title; ?></h3>
    <div class="container">
        <img class="pointer-mobile" src="/images/pointer-mobile.png" alt="">
    </div>
</div>


<div class="container private__description investment-descr">

    <?= $investment->content; ?>
    <? if (!empty($investment->valuesInvestments)) : ?>
    <div class="container">
        <div class="row private__row mt-3">
            <? foreach ($investment->valuesInvestments as $value) : ?>
            <? $content = json_decode($value->content, true); ?>
            <div class="private__col">
                <img src="<?= $value->getImage(); ?>" alt="">
                <div class="private__col_title">
                    <?= $value->name; ?>
                </div>
                <? foreach ($content as $item) : ?>
                <div class="private__col_context">
                    <div class="private__col_cub"></div>
                    <div class="private__col_desc">
                        <?= $item; ?>
                    </div>
                </div>
                <? endforeach; ?>
            </div>
            <? endforeach; ?>
        </div>
    </div>
    <? endif; ?>
</div>


<? elseif ($investment->id == 2) : ?>
<div class="container-fluid realty">

    <div class="container private realty__title title-collapse">
        <h3 class="title "><?= $investment->title; ?></h3>
        <img class="pointer-mobile" src="/images/pointer-mobile.png" alt="">
    </div>
    <div class="container private__description1 investment-descr">
        <div class="container private__description">
            <?= $investment->content; ?>
        </div>
        <? if (!empty($investment->valuesInvestments)) : ?>
        <div class="container">
            <div class="row realty__row m-0">
                <? foreach ($investment->valuesInvestments as $value) : ?>
                <? $content = json_decode($value->content, true); ?>
                <div class="realty__col">
                    <img src="<?= $value->getImage(); ?>" alt="">
                    <div class="private__col_title">
                        <?= $value->name; ?>
                    </div>
                    <? foreach ($content as $item) : ?>
                    <div class="private__col_context">
                        <div class="private__col_cub"></div>
                        <div class="private__col_desc">
                            <?= $item; ?>
                        </div>
                    </div>
                    <? endforeach; ?>
                </div>
                <? endforeach; ?>
            </div>
        </div>
        <? endif; ?>
    </div>

</div>

<? elseif ($investment->id == 3) : ?>
<div class="container-fluid private fund title-collapse">
    <h3 class="title"><?= $investment->title; ?></h3>
    <img class="pointer-mobile" src="/images/pointer-mobile.png" alt="">
</div>
<div class="container private__description private__description-title investment-descr">
    <?= $investment->content; ?>
    <? if (!empty($investment->valuesInvestments)) : ?>
    <div class="container found__container">
        <div class="row realty__row m-0">
            <? foreach ($investment->valuesInvestments as $value) : ?>
            <? $content = json_decode($value->content, true); ?>
            <div class="realty__col ssss">
                <img src="<?= $value->getImage(); ?>" alt="">
                <div class="private__col_title">
                    <?= $value->name; ?>
                </div>
                <? foreach ($content as $item) : ?>
                <div class="private__col_context">
                    <div class="private__col_cub"></div>
                    <div class="private__col_desc">
                        <?= $item; ?>
                    </div>
                </div>
                <? endforeach; ?>
            </div>
            <? endforeach; ?>
        </div>
    </div>
    <? endif; ?>
</div>


<? elseif ($investment->id == 4) : ?>
<div class="container-fluid infrastructure">
    <div class="container private realty__title title-collapse">
        <h3 class="title"> <?= $investment->title; ?></h3>
        <img class="pointer-mobile" src="/images/pointer-mobile.png" alt="">
    </div>
    <div class="container private__description1 investment-descr">
        <div class="container private__description">
            <?= $investment->content; ?>
        </div>
        <? if (!empty($investment->valuesInvestments)) : ?>
        <div class="container">
            <div class="infrastructure__row row m-0">
                <? foreach ($investment->valuesInvestments as $value) : ?>
                <? $content = json_decode($value->content, true); ?>
                <div class="infrastructure__col col-md-4">
                    <img src="<?= $value->getImage(); ?>" alt="">
                    <div class="infrastructure__col_title">
                        <?= $value->name; ?>
                    </div>
                    <? foreach ($content as $item) : ?>
                    <div class="private__col_context">
                        <div class="private__col_cub"></div>
                        <div class="private__col_desc">
                            <?= $item; ?>
                        </div>
                    </div>
                    <? endforeach; ?>
                </div>
                <? endforeach; ?>
            </div>
        </div>
        <? endif; ?>
    </div>

</div>

<? endif; ?>
<? endforeach; ?>


<div id="risk" class="container finish">
    <div class="finish__col_title text-center title-collapse">
        <h3 class="title">
            <?= ArrayHelper::getValue($submenu, 'risk')->title; ?>

        </h3>
        <img class="pointer-mobile" src="/images/pointer-mobile.png" alt="">
        <span class="line   "></span>
    </div>
    <div class="investment-descr text-center">
        <div class="top__subtitle_risk">
            <?= Yii::t('main-investment', 'Система управления рисками'); ?>
        </div>
        <div class="row finish__row ">
            <div class="col-sm-6 finish__col-sm-6 col-xl-5">
                <? foreach ($risk as $item) : ?>
                <div class="finish__col_description finish__box_left">
                    <?= $item->content; ?>
                </div>
                <? endforeach; ?>
            </div>
            <div class="col-md-12 finish__col col-xl-7 col-lg-6">
                <div class="second">

                    <? for ($i = 0; $i < 2; $i++) : ?>
                    <? if ($i == 0) : ?>
                    <div class="second__bottom_row m-0">
                        <? $count = 1; ?>
                        <? foreach ($riskSystem as $item) : ?>
                        <? if ($count == 1 || $count == 2) : ?>
                        <div class="third_box_top third-shadow">
                            <div class="third_box_image">
                                <img src="<?= $item->getImage(); ?>" alt="">
                            </div>
                            <div class="third__descr"><?= $item->name; ?></div>
                        </div>
                        <? else : ?>
                        <? break; ?>
                        <? endif; ?>
                        <? $count++; ?>
                        <? endforeach; ?>
                    </div>
                    <? else : ?>
                    <? $count = 1; ?>
                    <div class="second__top_row">
                        <? foreach ($riskSystem as $item) : ?>
                        <? if ($count !== 1 && $count !== 2) : ?>
                        <div class="third_box_bottom third-shadow ">
                            <div class="third_box_image">
                                <img src="<?= $item->getImage(); ?>" alt="">
                            </div>
                            <h6 class="third__descr"><?= $item->name; ?></h6>
                        </div>
                        <? endif; ?>
                        <? $count++; ?>
                        <? endforeach; ?>
                    </div>
                    <? endif; ?>

                    <? endfor; ?>

                </div>
            </div>

        </div>
    </div>
</div>