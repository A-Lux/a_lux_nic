<?php

/* @var $this View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$counter = 1;

?>

<div class="container-fluid top-career banner-top">
    <div class="banner-title wrapper-title" id="top__title_text">
        <div class="span-fix-1"><?= $banner->title ?></div>
    </div>
</div>

<div class="career-bg">
    <section class="container">
        <? foreach ($career as $item) : ?>
        <? if ($counter == 1) : ?>
        <div class="first-part ">
            <div class="text-part-one col-xl-6 p-0 col-md-6 col-lg-6">
                <h2 class="title-collapse"><?= $item->title; ?> <img class="pointer-mobile" src="/images/pointer-mobile.png" alt=""> </h2>
                <div class="content investment-descr">
                    <?= $item->content; ?>
                </div>
            </div>
            <div class="img-part img-part-bottom col-xl-6 col-md-6 col-lg-6">
                <div class="border-left"></div>
                <img src="<?= $item->getImage(); ?>" class="career-img" alt="">
            </div>
        </div>

        <? elseif ($counter == 2) : ?>
        <div class="sec-part">
            <div class="img-part img-part-bottom p-0 col-xl-6 col-md-6 col-lg-6">
                <img src="<?= $item->getImage(); ?>" class="career-img" alt="">
                <div class="border-right"></div>
            </div>
            <div class="text-part-two col-xl-6 col-md-6 col-lg-6">
                <div>
                    <a href="#" data-toggle="modal" class="career-btn btn" data-target="#exampleModalCenter-career">
                        <?= Yii::t('main-career', 'Присоединяйся к команде корпорации'); ?>
                    </a>
                </div>
                <div class="modal modal-career fade" id="exampleModalCenter-career" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-career" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h3>
                                    <?= Yii::t('main-career', 'Присоединяйся к команде'); ?>
                                </h3>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <form class="reg__form career-form" id="career-form" name="careerForm" enctype="multipart/form-data">
                                    <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                                    <label class="form__label rezume-label">
                                        <p class="label__text">
                                            <?= Yii::t('main-career', 'ФИО'); ?>
                                            <span class="required">*</span></p>
                                        <input type="text" id="name" name="CareerForm[username]" required>
                                    </label>
                                    <label class="form__label rezume-label">
                                        <p class="label__text">
                                            <?= Yii::t('main-career', 'Номер телефона'); ?>
                                            <span class="required">*</span></p>
                                        <input type="text" id="phone" name="CareerForm[phone]" required>
                                    </label>
                                    <label class="form__label rezume-label">
                                        <input type="file" name="CareerForm[file]" id="file" class="input-file">
                                        <div class="btn btn-tertiary js-labelFile">
                                            <span class="js-fileName label__text-span">
                                                <?= Yii::t('main-career', 'Прикрепить резюме'); ?>
                                            </span>
                                        </div>
                                        <div class="file-val">
                                            <img src="/images/clip.png" alt="">
                                            <p id="file-val">
                                            </p>
                                        </div>
                                    </label>
                                    <button type="submit" class="button button--reg">
                                        <?= Yii::t('main-career', 'ОТПРАВИТЬ'); ?>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <script>
                        document.getElementById('file').addEventListener('change', function(e) {
                            let fileVal = document.getElementById('file-val').innerText = e.target.files.item(0).name;
                        })
                    </script>
                </div>
                <h2 class="title-collapse "><?= $item->title; ?>  <img class="pointer-mobile" src="/images/pointer-mobile.png" alt=""></h2>
                <div class="content investment-descr">
                    <?= $item->content; ?>
                </div>
            </div>
        </div>
        <? endif; ?>
        <? $counter++; ?>
        <? endforeach; ?>
    </section>
</div>