<?php

/* @var $this View */
/* @var $procurements Procurement */

/* @var $structures ManagementStructure */

/* @var $sliders Sliders */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;
use common\models\Procurement;
use common\models\ManagementStructure;
use common\models\Sliders;

$counter = 1;

?>
<div class="test">
    <div class="container-fluid about-top banner-top" style="display: none;">
        <div class="text-center banner-title">
            <span id="top__title_text "><?= $banner->title; ?></span>
        </div>
    </div>
    <!--<div class="container top__container">-->
    <div></div>
    
</div>
<div class="top__subtitle" id="corporation" name="corporation">
    <h3 class="title"><?= ArrayHelper::getValue($submenu, 'corporation')->title; ?></h3>
    <span class="line aos-init aos-animate" data-aos-duration="1500" data-aos="zoom-in"></span>
</div>


<div class="container main-content">

    <div class="responsive-slider slider-about" style="opacity: 0;">
        <div>
            <div class="top__subtitle_description">
                <?= ArrayHelper::getValue($sliders, 'management_structure')->title; ?>
            </div>
            <div class="general_row border-box">
                <div class="general_box_left">
                    <a href="https://nationalbank.kz" target="blank">
                        <img src="/images/nac.logo.png" alt="">
                        <div class="box_left_title">
                            <?= Yii::t('main-slider', 'национальный банк казахстана <p> Единственный акционер Корпорации</p>'); ?>
                        </div>

                    </a>
                </div>
            </div>
            <div class="general_information">
                <? foreach ($structures as $structure) : ?>
                    <? if ($counter == 1) : ?>
                        <div class="information_box border-box">
                            <div class="information_title">
                                <?= $structure->name ?>
                            </div>
                            <div class="information_description">
                                <?= $structure->content ?>
                            </div>

                        </div>
                    <? elseif ($counter == 2) : ?>
                        <div class="information_line"></div>
                        <div class="information_box border-box">
                            <div class="information_title">
                                <?= $structure->name ?>
                            </div>
                            <div class="information_description">
                                <?= $structure->content ?>
                            </div>
                        </div>
                    <? elseif ($counter == 3) : ?>
                        <div class="information_line"></div>
                        <div class="information_box border-box">
                            <div class="information_title">
                                <?= $structure->name ?>
                            </div>
                            <div class="information_description">
                                <?= $structure->content ?>
                            </div>
                        </div>
                    <? else : ?>
                    <? endif; ?><? $counter++; ?>

                <? endforeach; ?>
            </div>
        </div>
        <div>
            <div class="top__subtitle_description">
                <?= ArrayHelper::getValue($sliders, 'internal_documents')->title; ?>
                <? $internalDocuments = array_chunk(ArrayHelper::getValue($sliders, 'internal_documents')->sliderDocuments, 4); ?>
            </div>
            <div class="inner-slider inner-slider-1">
                <? $arrayInternal = array_chunk($internalDocuments, 2); ?>
                <? foreach ($arrayInternal as $internal) : ?>
                    <div>
                        <? foreach ($internal as $internalDocument) : ?>
                            <div class="download_row">
                                <? foreach ($internalDocument as $document) : ?>
                                    <div class="download_box border-box download-center">
                                        <div class="download_box_title">
                                            <?= $document->name; ?>
                                        </div>
                                        <div class="download__subtitle">
                                            <? if (count($document->getFile()) == 1) : ?>
                                                <div class="download_left_col">
                                                    <?= $document->getExtensions() ?>
                                                </div>
                                                <a class="download-a" href=" <?= !empty($document->getFile()[0]['file'])
                                                    ? $document->pathToFile . $document->getFile()[0]['file']
                                                    : '#'; ?>" target="_blank">
                                                    <img class="download_right_col" src="/images/download.svg" alt="">
                                                </a>
                                            <? elseif (count($document->getFile()) > 1) : ?>
                                                <div class="download_left_col"><?= $document->getExtensions() ?></div>
                                                <a class="download-a" href="#"
                                                   onclick="getDocuments(<?= $document->id ?>)">
                                                    <img class="download_right_col" src="/images/download.svg" alt="">
                                                </a>

                                            <? else : ?>
                                                <div class="download_left_col">
                                                    <?= $document->getExtensions() ?>
                                                </div>
                                                <a class="download-a" href="#" target="_blank">
                                                    <img class="download_right_col" src="/images/download.svg" alt="">
                                                </a>
                                            <? endif; ?>
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        <? endforeach; ?>
                    </div>
                <? endforeach; ?>
            </div>

        </div>
        <div>
            <div class="top__subtitle_description">
                <?= ArrayHelper::getValue($sliders, 'financial_statements')->title; ?>
                <? $financialStatements = array_chunk(ArrayHelper::getValue($sliders, 'financial_statements')->sliderDocuments, 4); ?>
            </div>
            <div class="inner-slider inner-slider-2">
                <? $arrayFinancial = array_chunk($financialStatements, 2); ?>
                <? foreach ($arrayFinancial as $financial) : ?>
                    <div>
                        <? foreach ($financial as $financialStatement) : ?>
                            <div class="download_row">
                                <? foreach ($financialStatement as $document) : ?>
                                    <div class="download_box border-box download-center">
                                        <div class="download_box_title">
                                            <?= $document->name; ?>
                                        </div>
                                        <div class="download__subtitle">
                                            <? if (count($document->getFile()) == 1) : ?>
                                                <div class="download_left_col">
                                                    <?= $document->getExtensions() ?>
                                                </div>
                                                <a class='download-a' href="
                                                <?= !empty($document->getFile()[0]['file'])
                                                    ? $document->pathToFile . $document->getFile()[0]['file']
                                                    : '#'; ?>" target="_blank">
                                                    <img class="download_right_col" src="/images/download.svg" alt="">
                                                </a>
                                            <? elseif (count($document->getFile()) > 1) : ?>
                                                <div class="download_left_col"><?= $document->getExtensions() ?></div>
                                                <a href="#" class='download-a'
                                                   onclick="getDocuments(<?= $document->id ?>)">
                                                    <img class="download_right_col" src="/images/download.svg" alt="">
                                                </a>
                                            <? else : ?>
                                                <div class="download_left_col">
                                                    <?= $document->getExtensions() ?>
                                                </div>
                                                <a href="#" class='download-a' target="_blank">
                                                    <img class="download_right_col" src="/images/download.svg" alt="">
                                                </a>
                                            <? endif; ?>
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        <? endforeach; ?>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
        <div>
            <div class="top__subtitle_description">
                <?= ArrayHelper::getValue($sliders, 'business_ethics')->title; ?>
                <? $businessEthics = ArrayHelper::getValue($sliders, 'business_ethics')->sliderDocuments; ?>
            </div>
            <div class="codex_top">
                <div class="codex_box">
                    <div class="codex_box_text"><?= Yii::t('main-about', 'Справедливость'); ?></div>
                </div>
                <div class="codex_box" style="background: #d9a158">
                    <img src="/images/logo-white.png" style="max-width: 45px" class="codex_box_logo" alt="">
                    <? foreach ($businessEthics as $document) : ?>
                        <a href="<?= $document->getBusinessEthicsFile(); ?>" class="load_box" target="_blank">
                            <span class="codex_box_text">
                                <?= Yii::t('main-about', 'Скачать PDF'); ?>
                            </span>
                        </a>
                        <? break; ?>
                    <? endforeach; ?>
                </div>

                <div class="codex_box">
                    <div class="codex_box_text">
                        <?= Yii::t('main-about', 'Прозрачность'); ?>
                    </div>
                </div>

            </div>
            <div class="codex_top codex_top_second">
                <div class="codex_box">
                    <div class="codex_box_text">
                        <?= Yii::t('main-about', 'Соблюдение законодательства Республики Казахстан'); ?>
                    </div>
                </div>
                <div class="codex_box">
                    <div class="codex_box_text">
                        <?= Yii::t('main-about', 'Ответственность'); ?>
                    </div>
                </div>


            </div>


        </div>


        <div>
            <div class="top__subtitle_description top-corporation-title">
                <h3><?= ArrayHelper::getValue($sliders, 'procurement')->title; ?></h3>
                <div class="subtitle-text">
                    <?= ArrayHelper::getValue($sliders, 'procurement')->content; ?>
                </div>
            </div>
            <div class="row row_order">
                <? $i = 0; ?>
                <? foreach ($procurements as $procurement) : ?>
                    <? $i++; ?>
                    <div class="download_box col-xl-3 border-box">
                        <? if ($procurement->id == 1) : ?>
                        <a href="<?= Url::to(['/procurement/act', 'id' => $procurement->id]); ?>">
                            <? elseif ($procurement->id == 2) : ?>
                            <a href="<?= Url::to(['/procurement/plan', 'id' => $procurement->id]) ?>">
                                <? else : ?>
                                <a href="<?= empty($procurement->child) ? '#' :
                                    Url::to(['/procurement/index', 'id' => $procurement->id]) ?>">
                                    <? endif; ?>
                                    <div class="download_box_image">
                                        <img src="<?= $procurement->getImage() ?>" alt="">
                                    </div>
                                    <div class="download_box_title">
                                        <?= $procurement->name; ?>
                                    </div>
                                </a>
                    </div>
                <? endforeach; ?>


            </div>
        </div>
        <div>
            <div class="top__subtitle_description">
                <?= ArrayHelper::getValue($sliders, 'compliance_controller')->title; ?>
            </div>
            <div class="row first_row">
                <div class="col-xl-12">
                    <div class="subtitle-text">
                        <?= ArrayHelper::getValue($sliders, 'compliance_controller')->content; ?>
                    </div>
                </div>
                <div class="col-sm-6 first_slide">
                    <div class="first__slide_icon">
                        <img src="/images/E-mail.svg" class="email-icon" alt="">
                    </div>
                    <div class="first__slide_info">
                        <div class="email">
                            <?= Yii::t('main-about', 'E-mail'); ?>
                        </div>
                        <div class="adress">
                            <?= Yii::t('main-about', 'Kozhumov@nicnbk.kz'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 first_slide">
                    <div class="first__slide_icon">
                        <img src="/images/phone.svg" class="phone-icon" alt="">
                        <!--                            <i class="fas fa-phone-volume slide__icon phone-icon"></i>-->
                    </div>
                    <div class="first__slide_info">
                        <div class="email">
                            <?= Yii::t('main-about', 'Телефон'); ?>
                        </div>
                        <div class="adress">
                            <?= Yii::t('main-about', '+7 (7172) 55 71 13'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="about-m" id="comanda">
    <div class="container-fluid team">
        <div class="container">
            <div class="row team__row">
                <div class="team__col col-xl-6">
                    <div class="title-collapse">
                        <h3 class="title"><?= $ourTeam->title ?></h3>
                        <span class="line revealator-zoomin revealator-within"></span>
                    </div>
                    <div class="collapse-content">
                        <?= $ourTeam->content ?>
                    </div>
                </div>
                <div class="team__col team__info">
                    <? foreach ($advantages as $advantage) : ?>
                        <? if ($advantage->id == 1) : ?>
                            <div class="romb_top">
                                <div class="romb__text about_text_inline">
                                    <!-- <span class="romb_txt_span"><?= $advantage->title; ?></span> -->
                                    <?= $advantage->content; ?>
                                </div>
                            </div>
                        <? elseif ($advantage->id == 2) : ?>
                            <div class="romb_left">
                                <div class="romb__text">
                                    <span class="romb_txt_span"><?= $advantage->title; ?></span>
                                    <?= $advantage->content; ?>
                                </div>
                            </div>
                        <? elseif ($advantage->id == 3) : ?>
                            <div class="romb_right">
                                <div class="romb__text">
                                    <span class="romb_txt_span"><img src="/images/male.png" class="gender-icon" alt="">
                                        <?= $advantage->title; ?> <img src="/images/female.png" class="gender-icon"
                                                                       alt=""> </span>
                                    <?= $advantage->content; ?>
                                </div>
                            </div>
                        <? elseif ($advantage->id == 4) : ?>
                            <div class="romb_bottom">
                                <div class="romb__text">
                                    <span class="romb_txt_span"><?= $advantage->title; ?></span>
                                    <?= $advantage->content; ?>
                                </div>
                            </div>
                        <? endif; ?>
                    <? endforeach; ?>

                    <div class="team__general_romb">
                        <img src="/images/logo-white.png" class="general_logo" alt="">
                    </div>

                </div>
            </div>
        </div>


        <div class="container tab-cont px-0">

            <div class="tab-desk">
                <ul class="nav nav-tabs">
                    <? $counter = 1; ?>
                    <? foreach ($structures as $structure) : ?>
                        <li class="nav-item ">
                            <a class="corp-link nav-link-tab <?= $counter == 1 ? 'active' : '' ?>" data-toggle="tab"
                               href="#home-<?= $structure->id ?>">
                                <?= $structure->name; ?>
                            </a>
                        </li>
                        <? $counter++; ?>
                    <? endforeach; ?>

                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <? $count = 1; ?>
                    <? foreach ($structures as $structure) : ?>
                        <div id="home-<?= $structure->id; ?>"
                             class="container tab-pane <?= $count == 1 ? 'active' : '' ?>"><br>
                            <div class="tab-team">
                                <div class="row tab-row <?= $count == 3 ? 'width_tab_3' : '' ?>">
                                    <? foreach ($managements as $management) : ?>
                                        <? if ($management->management_id == $structure->id) : ?>
                                            <? if ($count == 1 || $count == 2) : ?>
                                                <div class="tab-col-image tab-col">
                                                    <div class="tab-img-cont">
                                                        <img src="<?= $management->getImage(); ?>" class="tab-col-img"
                                                             alt="">
                                                    </div>
                                                    <div class="modal fade modal-about"
                                                         id="exampleModalCenter-<?= $management->id; ?>" tabindex="-1"
                                                         role="dialog" aria-labelledby="exampleModalCenterTitle"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body about-team-inner">
                                                                    <div class="about-team-image">
                                                                        <img src="<?= $management->getImage(); ?>"
                                                                             alt="">
                                                                    </div>
                                                                    <div class="about-team-p">
                                                                        <h5 class="modal-title"
                                                                            id="exampleModalCenterTitle">
                                                                            <?= $management->username; ?>
                                                                        </h5>
                                                                        <h2><?= $management->position; ?></h2>
                                                                        <?= $management->content; ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="#" data-toggle="modal" class="title-head"
                                                       data-target="#exampleModalCenter-<?= $management->id; ?>">
                                                        <div class="tab-col_info">
                                                            <div class="tab-col_position">
                                                                <?= $management->position; ?>
                                                            </div>
                                                            <p><?= $management->username; ?></p>
                                                        </div>
                                                    </a>
                                                </div>
                                            <? else : ?>
                                                <div class="tab-col-image_tab_3 col-xl-3 col-md-3 tab-col ">
                                                    <div class="tab-img-cont tab-team-img">
                                                        <img src="<?= $management->getImage(); ?>" class="tab-col-img"
                                                             alt="">
                                                    </div>
                                                    <div class="tab-col_info_3 tab-col-team_3">
                                                        <div class="tab-col_position tab-col_team-title">
                                                            <?= $management->position; ?>
                                                        </div>
                                                        <?= $management->content; ?>
                                                    </div>
                                                </div>
                                            <? endif; ?>
                                        <? endif; ?>

                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <? $count++; ?>
                    <? endforeach; ?>
                </div>
            </div>


            <div class="container tab-cont tab-mobile px-0">
                <div class="tabbedPanels">
                    <ul class="tabs nav nav-tabs mb-3">
                        <? $counter = 1; ?>
                        <? foreach ($structures as $structure) : ?>
                            <li>
                                <a href="#panel<?= $structure->id ?>" data-toggle="tab"
                                   class="corp-link nav-link-tab mobile-link-tab <?= $counter == 1 ? 'active' : '' ?>">
                                    <?= $structure->name; ?>
                                </a>
                            </li>
                            <? $counter++; ?>
                        <? endforeach; ?>
                    </ul>

                    <div class="panelContainer">
                        <? $count = 1; ?>
                        <? foreach ($structures as $structure) : ?>
                            <div id="panel<?= $structure->id ?>" class="panel">
                                <div class="slider">
                                    <? foreach ($managements as $management) : ?>
                                        <? if ($management->management_id == $structure->id) : ?>
                                            <? if ($count == 1 || $count == 2) : ?>
                                                <div class="tab-col-image tab-col">
                                                    <div class="tab-img-cont">
                                                        <img src="<?= $management->getImage(); ?>" class="tab-col-img" alt="">
                                                    </div>
                                                    <a onclick="getUserView(<?= $management->id ?>)">
                                                        <div class="tab-col_info">
                                                            <div class="tab-col_position"><?= $management->position; ?></div>
                                                            <p><?= $management->username; ?></p>
                                                        </div>
                                                    </a>
                                                </div>
                                            <? else : ?>
                                                <div class="tab-col-image tab-col">
                                                    <div class="tab-img-cont">
                                                        <img src="<?= $management->getImage(); ?>" class="tab-col-img" alt="">
                                                    </div>
                                                    <a>
                                                        <div class="tab-col_info">
                                                            <div class="tab-col_position"><?= $management->position; ?></div>
                                                            <?= $management->content; ?>
                                                        </div>
                                                    </a>
                                                </div>
                                            <? endif; ?>
                                        <? endif; ?>
                                    <? endforeach; ?>
                                </div>
                            </div>
                            <? $count++; ?>
                        <? endforeach; ?>

                        <div class="modal-mobile">
                            <div class="modal fade modal-about"
                                 id="mobile-team-modal"
                                 tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalCenterTitle"
                                 aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body about-team-inner">
                                            <div class="about-team-image modal-image">
                                                <img src="" alt="" class="modal-image-mobile">
                                            </div>
                                            <div class="about-team-p">
                                                <h5 class="modal-title modal-username-mobile" id="exampleModalCenterTitle">

                                                </h5>
                                                <h2 class="modal-position-mobile"></h2>
                                                <div class="modal-content-mobile">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<div class="container-fluid bottom_container">
    <div id="inter" class="top__subtitle">
        <div class="h3 title"><?= ArrayHelper::getValue($submenu, 'inter')->title; ?></div>
        <span class="line line-inter aos-init aos-animate" data-aos-duration="1500" data-aos="zoom-in"></span>
    </div>
    <div class="container mb-5 mt-3">
        <div class="row bottom_row">
            <? foreach ($memberships as $membership) : ?>
                <div class="col-xl-3 col-md-4">
                    <div class="bottom_col">
                        <div class="bottom_col_img_zone">
                            <a href="https://<?= $membership->link; ?>" target="_blank">
                                <img src="<?= $membership->getImage(); ?>" class="bottom_col_img logo-companys" alt="">
                            </a>
                        </div>
                        <div class="bottom_col_description">
                            <div class="bottom_col_title"><b>
                                    <?= $membership->title; ?>
                                </b></div>
                            <div class="bottom_col_content">
                                <?= $membership->content; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="documents-slider" tabindex="-1" role="dialog" aria-labelledby="documents-Slider"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="download_box_title documents-title w-100 pt-0">
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body documents-slider">

            </div>
        </div>
    </div>
</div>
