<?php

/* @var $this yii\web\View */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$menus      = Yii::$app->view->params['menu'];

?>
<div class="test">
    <div class="container-fluid map-top banner-top">
        <div class="text-center banner-title">
            <span id="top__title_text "><?= $banner->title; ?></span>
        </div>
    </div>
</div>
<div class="map-wrapper">
    <div class="container map-container">
        <div class="row">
            <? foreach ($menus as $menu) : ?>
                <div class="col-xl-3">
                    <a href="<?= $menu->url ?>">
                        <div class="map_content <?= !empty($menu->submenus) ? 'map-title' : 'map-before' ?>">
                            <img src="<?= $menu->getImage(); ?>" alt="">
                            <a href="<?= $menu->url; ?>" class="map_text">
                                <?= $menu->title; ?>
                            </a>
                        </div>
                    </a>
                    <? if($menu->submenus): ?>
                        <div class="map-list">
                            <ul>
                                <? foreach ($menu->submenus as $submenu): ?>
                                    <? if($submenu->id == 1): ?>
                                        <li class="map-li">
                                            <button class="corp-map-btn">
                                                <?= $submenu->title; ?>
                                            </button>
                                        </li>
                                        <div class="collapse" id="map-collapse">
                                            <ul>
                                                <? foreach ($sliders as $slider): ?>
                                                    <li>
                                                        <a href="<?= $menu->url; ?>#<?= $submenu->url; ?>"
                                                           data-slide="<?= $slider->id; ?>">
                                                            <?= $slider->title; ?>
                                                        </a>
                                                    </li>
                                                <? endforeach; ?>
                                            </ul>
                                        </div>

                                    <? else: ?>
                                        <li class="map-li">
                                            <a href="<?= $menu->url; ?>#<?= $submenu->url; ?>">
                                                <?= $submenu->title; ?>
                                            </a>
                                        </li>
                                    <? endif; ?>


                                <? endforeach; ?>
                            </ul>
                        </div>
                    <? endif; ?>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>
