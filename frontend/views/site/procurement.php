<div class="procurement">
    <div class="container-fluid pl-2 pr-2">
        <div class="back-btn">
            <a href="/about"><img src="/images/arrow_down.svg" alt="">Назад</a>
        </div>
        <div class="procurement-content">
            <div class="procurement-title">
                <h1>Cпособом конкурса</h1>
            </div>
            <div class="procurement-p">
                <p>АО «Национальная инвестиционная корпорация Национального Банка
                     Казахстана» (A25D6H8, г. Алматы, пр. Достык, д. 136) объявляет 
                     о проведении конкурса по закупкам следующих товаров/работ/услуг:</p>
            </div>
           <a href="/site/procurement-inner">
            <div class="invest__box procurement-info">
                <div class="doc-zoom">
                    <img src="/images/doc.svg" alt="">
                </div>
                <div class="doc-name">
                    <h1>Закупки услуг аудита годовой финансовой отчетности АО «НИКНБК» за 2018 год</h1>
                    <p>Период предоставления заявок: 4 июня 2018 г., 9:00 — 25 июня 2018 г., 10:00</p>
                </div>
                <div class="doc-arrow">
                    <img src="/images/arrow-right.svg" alt="">
                </div>
            </div>
            </a>
            <div class="procurement-download">
                <div class="download_box">
                    <div class="download_box_title">
                    Протокол вскрытия   
                    </div>
                    <div class="download__subtitle">
                        <div class="download_left_col">DOCX</div>
                        <img class="download_right_col" src="/images/download.svg" alt="">
                    </div>
                </div>
                <div class="download_box">
                    <div class="download_box_title">
                    Протокол о предварительном допуске к участию в конкурсе
                    </div>
                    <div class="download__subtitle">
                        <div class="download_left_col">DOCX</div>
                        <img class="download_right_col" src="/images/download.svg" alt="">
                    </div>
                </div>
                <div class="download_box">
                    <div class="download_box_title">
                    приложение к протоколу предварит допуска 
                    </div>
                    <div class="download__subtitle">
                        <div class="download_left_col">XLS</div>
                        <img class="download_right_col" src="/images/download.svg" alt="">
                    </div>
                </div>
                <div class="download_box">
                    <div class="download_box_title">
                    приложение 1 к протоколу Допуска 
                    </div>
                    <div class="download__subtitle">
                        <div class="download_left_col">XLS</div>
                        <img class="download_right_col" src="/images/download.svg" alt="">
                    </div>
                </div>
            </div>
           <a href="/site/procurement-inner">
            <div class="invest__box procurement-info">
                <div class="doc-zoom">
                    <img src="/images/doc.svg" alt="">
                </div>
                <div class="doc-name">
                    <h1>Закупки услуг аудита годовой финансовой отчетности АО «НИКНБК» за 2018 год</h1>
                    <p>Период предоставления заявок: 4 июня 2018 г., 9:00 — 25 июня 2018 г., 10:00</p>
                </div>
                <div class="doc-arrow">
                    <img src="/images/arrow-right.svg" alt="">
                </div>
            </div>
            </a>
            <div class="procurement-download procurement-download-btn">
                <div class="download_box">
                    <div class="download_box_title">
                    Протокол вскрытия   
                    </div>
                    <div class="download__subtitle">
                        <div class="download_left_col">DOCX</div>
                        <img class="download_right_col" src="/images/download.svg" alt="">
                    </div>
                </div>
                <div class="download_box">
                    <div class="download_box_title">
                    Протокол о предварительном допуске к участию в конкурсе
                    </div>
                    <div class="download__subtitle">
                        <div class="download_left_col">DOCX</div>
                        <img class="download_right_col" src="/images/download.svg" alt="">
                    </div>
                </div>
                <div class="download_box">
                    <div class="download_box_title">
                    приложение к протоколу предварит допуска 
                    </div>
                    <div class="download__subtitle">
                        <div class="download_left_col">XLS</div>
                        <img class="download_right_col" src="/images/download.svg" alt="">
                    </div>
                </div>
                <div class="download_box">
                    <div class="download_box_title">
                    приложение 1 к протоколу Допуска 
                    </div>
                    <div class="download__subtitle">
                        <div class="download_left_col">XLS</div>
                        <img class="download_right_col" src="/images/download.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>