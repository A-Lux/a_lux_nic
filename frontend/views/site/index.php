<?php

/* @var $this View */
/* @var $advantages MainAdvantages */
/* @var $menu Menu */
/* @var $classesInvestments ClassesInvestments */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\MainAdvantages;
use yii\helpers\ArrayHelper;
use common\models\Menu;
use common\models\ClassesInvestments;

$counter = 1;
$countered = 1;
?>

<div class="banner-main">
    <div class="top-index">
        <!-- <div class="wrapper-video">
    </div> -->
        <!-- <video id="bgvid" width="100%" preload playsinline muted loop playsinline="true" disablePictureInPicture="true">>
            <source src="/images/vid.mp4">
        </video> -->
        <video id="bgvid" width="100%" preload playsinline muted loop playsinline="true" disablePictureInPicture="true" src='/images/videoplayback.mp4' type='video/mp4'></video>
        <script>
            let bgvideo = document.getElementById("bgvid");
            bgvideo.play();
        </script>
        <div class="top__title banner-title" id="top__title_text" style="position: absolute!important;">
            <div class="container-fluid">
                <div class="col-xl-12 px-5"> <?= $banner->title; ?></div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid preview_cont preview_div">
    <div>
        <div class="header_block flip-container" id="view-1">
            <div class="row target-row ml-0 ">
                <? foreach ($advantages as $advantage) : ?>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <div class="target-card">
                        <span class="target-title"><?= $advantage->title; ?></span>
                        <img src="<?= $advantage->getImage(); ?>" class="col-img " alt="" />
                        <div class="target-card-text">
                            <b class="fs18"><?= $advantage->title; ?> </b>
                            <div class="fs18-fix">
                                <?= $advantage->content; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <? endforeach; ?>
            </div>
        </div>
    </div>

</div>

<div class="section-dropdown" id="s1">
    <section class="container-fluid s1 ">
        <div class="container s1-inner">
            <div class="row section-dropdown-row ">
                <div class="col-xl-5 col-md-6 col-lg-6">
                    <div class="s1-left">
                        <div class="title-collapse">
                            <h3 class="title"><?= ArrayHelper::getValue($menu, 'about')->title; ?></h3>
                            <span class="line" data-aos-duration="1500" data-aos="zoom-in"></span>
                        </div>

                        <div class="collapse-content">
                            <p>
                                <?= ArrayHelper::getValue($menu, 'about')->description; ?>
                            </p>
                        </div>
                        <br />
                        <a id="show-1" href="#s1"><span class="s1-left-arrow">
                                <img src="/images/arrow-down.svg" style="max-width: 15px;" alt="" /> </span></a>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-lg-6 s1-right mt-5 mt-md-0" data-aos-duration="1500" data-aos="zoom-in">
                    <div class="romb-rot">
                        <? foreach (ArrayHelper::getValue($menu, 'about')->submenus as $item) : ?>
                        <? if ($counter == 1) : ?>
                        <div class="romb1 romb">
                            <span class="">
                                <a class="romb-link sub-title" href="<?= $item->menu->url . '#' . $item->url ?>" data-slide="1">
                                    <?= $item->title; ?></a>
                            </span>
                        </div>
                        <? elseif ($counter == 2) : ?>
                        <div class="romb2 romb">
                            <span>
                                <a class="romb-link" href="<?= $item->menu->url . '#' . $item->url ?>">
                                    <?= $item->title; ?>
                                </a>
                            </span>
                        </div>
                        <? elseif ($counter == 3) : ?>
                        <div class="romb3 romb"></div>
                        <div class="romb4 romb">
                            <span>
                                <a class="romb-link" href="<?= $item->menu->url . '#' . $item->url ?>">
                                    <?= $item->title; ?>
                                </a>
                            </span>
                        </div>
                        <? else : ?>
                        <? endif; ?>
                        <? $counter++; ?>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container-fluid s2" id="s-2-id">
        <div class="container">
            <div class="s2-inner">
                <h3 class="title"><?= Yii::t('main-index', 'ПУТЬ РАЗВИТИЯ КОРПОРАЦИИ'); ?></h3>
                <span class="line"></span>
                <div class="d-y">
                    <div class="first-years">
                        <? foreach ($wayDevelopment as $key=>$item) : ?>
                            <? if (!(($key % 2 ) == 0)) {?>
                                <div class="year13 year">
                                    <p class="y-text">
                                        <?= strip_tags($item->content); ?>
                                    </p>
                                    <p class="bold-num "><?= $item->title; ?></p>
                                </div>
                            <? } ?>
                        <? endforeach; ?>
                    </div>
                    <div class="first-years-mob">
                        <? foreach ($wayDevelopment as $key=>$item) : ?>
                            <? if (!(($key % 2 ) == 0)) {?>
                                <div class="year13 year ">
                                    <div class="title-collapse">
                                        <p class="bold-num "><?= $item->title; ?></p>
                                        <img class="pointer-mobile" src="/images/pointer-mobile.png" alt="">
                                    </div>
                                    <p class="y-text collapse-content">
                                        <?= strip_tags($item->content); ?>
                                    </p>
                                </div>
                            <? } ?>
                        <? endforeach; ?>
                    </div>
                    <br />
                    <div class="steps">
                        <span class="circle"></span>
                        <img src="/images/ten-dots.png" alt="" />
                        <span class="circle"></span>
                        <img src="/images/ten-dots.png" alt="" />
                        <span class="circle"></span>
                        <img src="/images/ten-dots.png" alt="" />
                        <span class="circle"></span>
                        <img src="/images/ten-dots.png" alt="" />
                        <span class="circle"></span>
                        <img src="/images/ten-dots.png" alt="" />
                        <span class="circle "></span>
                        <img src="/images/ten-dots.png" alt="" />
                        <span class="circle "></span>
                        <img src="/images/ten-dots.png" alt="" />
                        <span class="circle "></span>
                        <img src="/images/ten-dots.png" alt="" />
                        <span class="circle "></span>
                        <img src="/images/ten-dots.png" class="mr" alt="" />
                        <img src="/images/arrow-rigth.svg" class='list-arrow-down' style="max-width: 10px;" alt="" />
                    </div>
                    <br />
                    <div class="second-part-y">
                        <? foreach ($wayDevelopment as $key=>$item) : ?>
                            <? if ((($key % 2 ) == 0)) {?>
                                <div class="year12 year">
                                    <div gitclass="title-collapse">
                                        <p class="bold-num"><?= $item->title; ?></p>
                                        <img class="pointer-mobile" src="/images/pointer-mobile.png" alt="">
                                    </div>
                                    <p class="y-text collapse-content"><?= strip_tags($item->content); ?></p>
                                </div>
                            <? } ?>
                        <? endforeach; ?>
                    </div>
                    <div class="d-y_adaptiv">
                        <div class="first-years-adaptiv">
                            <? foreach ($wayDevelopment as $item) : ?>
                            <div class="year13 year">
                                <p class="bold-num"><?= $item->title; ?></p>
                                <p class="y-text"><?= strip_tags($item->content); ?></p>
                            </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                    <a id="s-1-cl" class="to-top">
                        <span class="s1-left-arrow">
                            <img src="/images/arrow-top.svg" style="max-width: 15px;" alt="" />
                        </span>
                    </a>
                </div>

            </div>
        </div>
    </section>
</div>

<div class="section-dropdown" id="s3">
    <section class="container-fluid s3 ">
        <div class="s3-inner container">
            <div class="col-xl-5 col-md-6  investmentsDiv" data-aos-duration="1500" data-aos="zoom-in">
                <div class="romb-rot2">
                    <? foreach (ArrayHelper::getValue($menu, 'investments')->submenus as $item) : ?>
                    <? if ($countered == 1) : ?>
                    <div class="romb1 romb ">
                        <span>
                            <a href="<?= $item->menu->url . '#' . $item->url ?>">
                                <?= $item->title; ?>
                            </a>
                        </span>
                    </div>
                    <? elseif ($countered == 2) : ?>
                    <div class="romb3 romb "></div>
                    <div class="romb2 romb">
                        <span>
                            <a href="<?= $item->menu->url . '#' . $item->url ?>">
                                <?= $item->title; ?>
                            </a>
                        </span>
                    </div>
                    <? elseif ($countered == 3) : ?>
                    <div class="romb4 romb ">
                        <span>
                            <a href="<?= $item->menu->url . '#' . $item->url ?>">
                                <?= $item->title; ?>
                            </a>
                        </span>
                    </div>
                    <? else : ?>
                    <? endif; ?>
                    <? $countered++; ?>

                    <? endforeach; ?>
                </div>
            </div>
            <div class="col-md-6 col-xl-5 s3-right">
                <div class="title-collapse">
                    <h3 class="title"><?= ArrayHelper::getValue($menu, 'investments')->title; ?></h3>
                    <span class="line" data-aos-duration="1500" data-aos="zoom-in"></span>
                </div>
                <div class="collapse-content">
                    <p>
                        <?= ArrayHelper::getValue($menu, 'investments')->description; ?>
                    </p>
                </div>
                <p></p>
                <br />
                <a class="s3-down-btn" href="#s3" id="s-4-btn"><span class="s1-left-arrow">
                        <img src="/images/arrow-down.svg" style="max-width: 15px;" alt="" /> </span></a>
            </div>
        </div>
    </section>
    <section class="s4 container" id="s-4-id">
        <div class="s4-inner ">
            <h3><?= Yii::t('main-index', 'КЛАССЫ АЛЬТЕРНАТИВНЫХ ИНВЕСТИЦИЙ'); ?></h3>
            <p class="line"></p>
            <br />
            <div class="blockofbox container-fluid">
                <? for ($i = 0; $i < 2; $i++) : ?>
                <? if ($i == 0) : ?>
                <!-- <div class="first-line"> -->
                <? $count = 1; ?>
                <? foreach ($classesInvestments as $investment) : ?>
                <? if ($count == 1 || $count == 2) : ?>
                <div class="box" style="margin:0 20px 0 0;">
                    <p class="f20"><?= $investment->title; ?></p>
                    <p class="f14">
                        <?= $investment->content; ?>
                    </p>
                </div>
                <? else : ?>
                <? break; ?>
                <? endif; ?>
                <? $count++; ?>
                <? endforeach; ?>
                <!-- </div> -->
                <? else : ?>
                <!-- <div class="second-line"> -->
                <? $count = 1; ?>
                <? foreach ($classesInvestments as $investment) : ?>
                <? if ($count !== 1 && $count !== 2) : ?>
                <div class="box" style="margin:0 20px 0 0;">
                    <p class="f20"><?= $investment->title; ?></p>
                    <p class="f14">
                        <?= $investment->content; ?>
                    </p>
                </div>
                <? endif; ?>
                <? $count++; ?>
                <? endforeach; ?>
                <!-- </div> -->
                <? endif; ?>

                <? endfor; ?>
            </div>
            <a id="s-5" class="to-top">
                <span class="s1-left-arrow fix-arrow">
                    <img src="/images/arrow-top.svg" style="max-width: 15px;" alt="" />
                </span>
            </a>
        </div>
    </section>
</div>

<section class="s5 container-fluid">
    <div class="s5-inner">
        <h3><?= ArrayHelper::getValue($menu, 'information-centre')->title; ?></h3>
        <span class="line aos-init aos-animate" data-aos-duration="1500" data-aos="zoom-in"></span>

        <div class="s5-block s5-inform">
            <? foreach ($information as $item) : ?>
            <div class="col-xl-3 col-md-6 col-lg-3 inform-wrapper">
                <a href="<?= $item->type == 1
                                ? Url::to(['/news/view', 'id' => $item->id])
                                : Url::to(['/publications/view', 'id' => $item->id]); ?>" target="">
                    <div class="s5-boxes mr10 w-100">
                        <div class="inf-block-img">
                            <img src="<?= $item->getImage(); ?>" class="col-img " data-aos-duration="1500" data-aos="fade-up" alt="" />
                            <span class="" data-aos-duration="1500" data-aos="fade-up"><?= $item->date; ?></span>
                        </div>

                        <p class="fs18"><?= $item->title; ?></p>
                        <div class="fs18-fix">
                            <?= $item->description; ?>
                        </div>
                    </div>
                </a>
            </div>
            <? endforeach; ?>
            <div class="col-xl-3 col-md-6 col-lg-3">
                <a href="/information-centre/index" class="d-flex justify-content-center" target="">
                    <div class="s5-boxes s-boxes-last inform-wrapper-last" style="background-color: #2D3135;">
                        <div class="color-orange"><?= $thinkTank->title; ?></div>
                        <br />
                        <br />
                        <p>
                            <?= $thinkTank->description; ?>
                        </p>
                    </div>
                </a>
            </div>


        </div>
    </div>
</section>