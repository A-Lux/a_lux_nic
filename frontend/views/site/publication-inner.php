<!-- <div class="test">
    <div class="container-fluid news-top banner-top">
        <div class="banner-title text-center">
            <span id="top__title_text ">Публикации</span>
        </div>
    </div>
</div> -->
<div class="news__wrapper">
    <div class="container">
        <h2 class="news__title">РАБОТНИКИ КОРПОРАЦИИ ПРОВЕЛИ ПРЕЗЕНТАЦИЮ</h2>
        <div class="news__date">2 марта 2020</div>
        <div class="news__content">
            <img class="news__image" src="/images/news-img.png" alt="">
            <div class="news__title--small">
                Аналитический центр Корпорации предоставляет экспертные рекомендации и публикации по основным
                направлениям инвестиционной деятельности, финансовым рынкам и стратегиям
            </div>
            <p class="news__text">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько
                абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык
                публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный
                универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в
                абзаце, что позволяет сделать текст более привлекательным и живым для визуально-слухового
                восприятия.</p>
            <p class="news__text">По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который
                вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от lorem ipsum, текст
                рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских
                времен.</p>
            <div class="news__title--small">Аналитический центр Корпорации:</div>
            <ul class="news__list news-wrapper">
                <li class="news__list__item">По своей сути рыбатекст является альтернативой традиционному lorem ipsum
                </li>
                <li class="news__list__item">По своей сути рыбатекст является альтернативой традиционному lorem ipsum
                </li>
                <li class="news__list__item">По своей сути рыбатекст является альтернативой традиционному lorem ipsum
                </li>
                <li class="news__list__item">По своей сути рыбатекст является альтернативой традиционному lorem ipsum
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="social__block">
    <div class="container">
        <div class="social__inner">
            <div class="social__text">Сохраните статью в социальных сетях,чтобы не потерять её или рассказать о ней.
            </div>
            <div class="social__share">Поделиться:</div>
            <div class="social__icons">
                <a href="#" class="social__icon">
                    <i class="fab fa-twitter"></i>
                    <span class="social__stats">11.6K</span>
                </a>
                <a href="#" class="social__icon">
                    <i class="fab fa-facebook-f"></i>
                    <span class="social__stats">11.6K</span>
                </a>
                <a href="#" class="social__icon">
                    <i class="fab fa-google-plus-g"></i>
                    <span class="social__stats">11.6K</span>
                </a>
                <a href="#" class="social__icon">
                    <i class="fab fa-linkedin-in"></i>
                    <span class="social__stats">11.6K</span>
                </a>
                <a href="#" class="social__icon">
                    <i class="fab fa-pinterest"></i>
                    <span class="social__stats">11.6K</span>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="buttons">
    <div class="container">
        <a href="#" class="button button--reg">Предыдущая публикация</a>
        <a href="#" class="bottom__button news-back">Назад</a>
    </div>
</div>

<section class="s5 container">
    <div class="s5-inner">
        <h3>Похожие статьи</h3>
        <br/>

        <div class="s5-block">
            <div class="first-line2">
                <a href="/site/news">
                    <div class="s5-boxes mr10">
                        <img
                                src="/images/main-col-1.jpg"
                                class="col-img revealator-slideup"
                                alt=""
                        />
                        <span class="revealator-slideup">10 марта 2020 года</span>
                        <p class="fs18">Новые публикации</p>
                        <div class="fs18-fix">
                            Доходность фондов со-инвестиций, занятость населения в США,
                            иностранные инвестиции.
                        </div>
                    </div>
                </a>
                <a href="/site/news">
                    <div class="s5-boxes mr10 ">
                        <img
                                src="/images/main-col-2.jpg"
                                class="col-img revealator-slideup"
                                alt=""
                        />
                        <span class="revealator-slideup">2 марта 2020 года</span>
                        <p class="fs18">Новые публикации</p>
                        <div class="fs18-fix">
                            Аналитический обзор: Коронавирус, рынки недвижимости, мировая
                            цифровая валюта
                        </div>
                    </div>
                </a>
            </div>
            <div class="second-line2">
                <a href="/site/news">
                    <div class="s5-boxes mr10">
                        <img
                                src="/images/main-col-3.jpg"
                                class="col-img revealator-slideup"
                                alt=""
                        />
                        <span class="revealator-slideup">2 марта 2020 года</span>
                        <p class="fs18">Публикации о Корпорации</p>
                        <div class="fs18-fix">
                            Работники Корпорации провели презентацию о деятельности перед
                            студентами Назарбаев Университета.
                        </div>
                    </div>
                </a>
                <a href="/information-centre/index">
                    <div class="s5-boxes s-boxes-last" style="background-color: #2D3135;">
                        <div class="color-orange">ThinkTank</div>
                        <br>
                        <br>
                        <p>
                            Экспертное мнение о рынках, трендах и последних публикациях
                        </p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>