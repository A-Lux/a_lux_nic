<!-- <div class="mainscreen">
      <img src="/images/astana-city-panoramic-view.jpg" alt="" class="mainscreen__img">
      <div class="mainscreen__title">Поиск</div>
    </div> -->
    <div class="test">
    <div class="container-fluid search-top banner-top">
        <div class="top__title text-center banner-title">
            <span id="top__title_text" class="search-top-title">поиск</span>
        </div>
    </div>
    </div>
    <div class="search__wrapper">
      <div class="container">
        <div class="search__title">РЕЗУЛЬТАТЫ ПО ЗАПРОСУ: <span class="text-grey">инвистиционный проект</span></div>
        <div class="group__title">Статьи</div>
        <div class="articles__wrapper">
            <div class="list">
              <div class="search__card">
                <img src="/images/main-col-1.jpg" alt="">
                <div class="card__content">
                  <div class="card__date">2 марта 2020</div>
                  <div class="card__text">Работники Корпорации провели презентацию 
                    о деятельности перед ...</div>
                    <a href="#" class="card__button">Подробнее</a>
                </div>
              </div>
              <div class="search__card">
                <img src="/images/main-col-1.jpg" alt="">
                <div class="card__content">
                  <div class="card__date">2 марта 2020</div>
                  <div class="card__text">Работники Корпорации провели презентацию 
                    о деятельности перед ...</div>
                    <a href="#" class="card__button">Подробнее</a>
                </div>
              </div>
            </div>
        </div>
        <hr/>
        <div class="group__title">Страницы</div>
        <div class="links__wrapper">
          <div class="link__wrapper">
            <a href="" class="link__page"><img src="/images/arrow_down.svg" alt="">Информация по государственным закупкам</a>
          </div>
          <div class="link__wrapper">
            <a href="" class="link__page"><img src="/images/arrow_down.svg" alt="">Нормативно-правовые акты</a>
          </div>
        </div>

      </div>
    </div>