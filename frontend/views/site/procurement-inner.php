<div class="procurement">
    <div class="container-fluid">
        <div class="back-btn">
            <a href="/site/procurement"><img src="/images/arrow_down.svg" alt="">Назад</a>
        </div>
        <div class="procurement-title">
                <h1>Закупки услуг аудита годовой финансовой отчетности АО «НИКНБК» за 2018 год</h1>
            </div>
        <a href="" class="button--reg procurement-btn btn" data-toggle="modal" data-target="#exampleModalCenter"> <i class="fas fa-download"></i>Скачать конкурсную документацию</a>

        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-procurement" role="document">
                    <div class="modal-content procurement-content-modal">
                        <div class="modal-body procurement-body">
                            <h3>Конкурсная документация</h3>
                            <h2>Чтобы получить с сайта конкурсную документацию, необходимо предоставить следующую информацию.</h2>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" class="close-form">×</span>
                            </button>
                            <form class="reg__form">
                                <label class="form__label rezume-label">
                                    <p class="label__text">Наименование лица (юридического или физического)  <span class="required">*</span></p>
                                    <input type="text" id="name">
                                </label>
                                <label class="form__label rezume-label">
                                    <p class="label__text">БИН/ИИН <span class="required">*</span></p>
                                    <input type="text" id="name">
                                </label>
                                <h3>Почтовый адрес</h3>
                                <label class="form__label rezume-label">
                                    <p class="label__text">Почтовый индекс</p>
                                    <input type="text" id="name">
                                </label>
                                <label class="form__label rezume-label">
                                    <p class="label__text">Город </p>
                                    <input type="text" id="name">
                                </label>
                                <label class="form__label rezume-label">
                                    <p class="label__text">Адрес </p>
                                    <input type="text" id="name">
                                </label>
                                <h3>Данные лица</h3>
                                <label class="form__label rezume-label">
                                    <p class="label__text">Имя, фамилия</p>
                                    <input type="text" id="name">
                                </label>
                                <label class="form__label rezume-label">
                                    <p class="label__text">Должность </p>
                                    <input type="text" id="name">
                                </label>
                                <label class="form__label rezume-label">
                                    <p class="label__text">Номер удостоверения личности</p>
                                    <input type="text" id="name">
                                </label>
                                <label class="form__label rezume-label">
                                    <p class="label__text">Кем выдано</p>
                                    <input type="text" id="name">
                                </label>
                                <label class="form__label rezume-label">
                                    <p class="label__text">Дата выдачи</p>
                                    <input type="text" id="name" placeholder="дд.мм.гггг">
                                </label>
                                <label class="form__label rezume-label">
                                    <p class="label__text">Контактный телефон *</p>
                                    <input type="text" id="name" >
                                </label>
                                <label class="form__label rezume-label">
                                    <p class="label__text">Email</p>
                                    <input type="text" id="name">
                                </label>
                            </form>
                                <button type="submit" class="button button--reg">ОТПРАВИТЬ</button>
                            
                        </div>
                    </div>
                </div>
            </div>
        

        <div class="procurement-inner-p">
            <p>АО «Национальная инвестиционная корпорация Национального Банка Казахстана», находящееся  
                по адресу: Республика Казахстан, 050051,  г. Алматы, Медеуский район, пр. Достык 136,
                 интернет-ресурс:  www.nicnbk.kz, электронный адрес: kashaganova@nicnbk.kz , в соответствии 
                 с Правилами приобретения товаров, работ и услуг Национальным Банком Республики Казахстан, 
                 его ведомствами, организациями, входящими в его структуру, и юридическими лицами, пятьдесят
                  и более процентов голосующих акций (долей участия в уставном капитале) которых  принадлежат 
                  Национальному Банку Республики Казахстан или находятся в его доверительном управлении, и 
                  аффилиированными с ними юридическими лицами, утвержденных постановлением Правления Национального 
                  Банка Республики Казахстан 19 декабря 2015 года № 237, объявляет о проведении конкурса по закупкам 
                  услуг аудита годовой финансовой отчетности АО «Национальная инвестиционная корпорация Национального 
                  Банка казахстана» за 2018 год. </p>
            <p>Способ получения конкурсной документации: копия конкурсной документации доступна для получения в электронном
                виде на интернет-ресурсе заказчика www.nicnbk.kz (раздел: Закупки)  либо конкурсную документацию можно 
                получить по адресу: г. Алматы, пр. Достык 136, 5-этаж, тел.+7 (727) 244 94 16,  в срок до 10 часов 00 минут
                «25» июня 2018 года включительно Услуги оказываются в соответствии с условиями договора, полный перечень 
                закупаемых услуг, их количество (объем) и подробная спецификация указаны в конкурсной документации. </p>
            <p>Место оказания услуг: г. Алматы, пр. Достык 136, БЦ «Пионер», 4 и 5 этажи.</p>
            <p>Требуемый срок оказания услуг: в соответствии условиями договора.</p>
            <p>Заявки на участие в конкурсе, запечатанные в конверты, предоставляются (направляются) потенциальными поставщиками 
                в Управление по административным вопросам и коммуникациям АО «Национальная инвестиционная корпорация Национального 
                Банка Казахстана» по адресу: Республика Казахстан, 050051,  г. Алматы, Медеуский район, пр. Достык 136, 5-этаж,  
                с 09 часов 00 минут до 18  часов 30 минут, тел. +7 (727) 244 94 16,  + 7 (727) 244 94 04.Окончательный срок 
                представления заявок на участие в конкурсе: до 10 часов 00 минут «25» июня 2018 года.Конверты с заявками на 
                участие в конкурсе будут вскрываться в 10 часов 30 минут «25» июня 2018 года, по адресу: 050051,  г. Алматы, 
                Медеуский район, пр. Достык 136, 5-этаж.Дополнительную информацию и справку можно получить по телефонам: +7 (727) 244 94 16,
                 + 7 (727) 244 94 04.Уполномоченный представитель организатора закупок, секретарь конкурсной комиссии: Кашаганова А.Н.,
                   Менеджер по закупкам Управления по административным вопросам и коммуникациям АО  «Национальная инвестиционная 
                   корпорация Национального Банка Казахстана», тел.: + 7(727) 244 94 16.</p>
        </div>

    </div>
</div>