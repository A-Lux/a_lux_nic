<?php

/* @var $this View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;


?>

<div class="container-fluid inform-top banner-top">
    <div class="banner-title wrapper-title" id="top__title_text ">
        <div class="span-fix-1">
            <?= $banner->title; ?>
        </div>
    </div>
</div>
<!--<div class="container top__container">-->
<div class="container">
    <div class="row inf-cent_row">
        <div class="col-sm-12 inf-cent__subtitle">
            <p class="orange-title"><?= $thinkTank->title; ?></p>
        </div>
        <div class="col-sm-12 inf-cent__subtitle-descr">
            <?= $thinkTank->content; ?>
        </div>
    </div>
</div>
<div class="container-fluid bottom">
    <div class="row bottom__row m-0">
        <div class="bottom__col col-xl-6">
            <? if (!empty($lastNews)) : ?>
                <div class="bottom__img-block">
                    <a href="<?= Url::to(['/news/view', 'id' => $lastNews->id]) ?>">
                        <div style="background: url(https://nicnbk.kz/backend/web/uploads/images/think-tank/1613627356_2F2XQxTEdC.jpg) no-repeat top; background-size: cover; width: 100%; height: 256px;"></div>
                    </a>
                    <div class="bottom__date">
                        <?= $lastNews->date; ?>
                    </div>
                </div>
                <div class="bottom-box-inner">
                    <div class="bottom__box">
                        <div class="bottom__subtitle">
                            <?= $lastNews->title; ?>
                        </div>
                        <div class="bottom__desc">
                            <?= $lastNews->description; ?>
                        </div>
                    </div>
                    <div class="bottom__button moreNewsBtn">
                        <a href="/news"><?= Yii::t('main-information-centre', 'Все новости'); ?></a>
                    </div>
                    <div class="col-xl-12 mt-5 p-0 moreNews moreNews-mobile moreInfo">
                        <section class="s5 container">
                            <div class="s5-inner pb-4">
                                <br>
                                <div class="row">
                                    <? foreach ($news as $item) : ?>
                                        <div class="col-xl-3 col-lg-4 col-md-6">
                                            <div class="s5-boxes">
                                                <div class="inf-block-img">
                                                    <a href="<?= Url::to(['/news/view', 'id' => $item->id]) ?>" style="display: contents;">
                                                        <img src="<?= $item->getImage() ?>" class="col-img " data-aos="fade-up" data-aos-duration="300" alt="">
                                                    </a>
                                                    <span class="" data-aos="fade-up" data-aos-duration="300">
                                                        <?= $item->date; ?>
                                                    </span>
                                                </div>
                                                <p class="fs18"><?= $item->title; ?></p>
                                                <div class="fs18-fix">
                                                    <?= $item->description; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            <? endif; ?>
        </div>
        <div class="bottom__col col-xl-6">
            <? if (!empty($lastPublications)) : ?>
                <div class="bottom__img-block">
                    <a href="<?= Url::to(['/publication/view', 'id' => $lastPublications->id]) ?>">
                        <div style="background: url(https://nicnbk.kz/backend/web/uploads/images/think-tank/1591334627_TtQ6a14_HN.jpg) no-repeat top; background-size: cover; width: 100%; height: 256px;"></div>
                    </a>
                    <div class="bottom__date">
                        <?= $lastPublications->date; ?>
                    </div>
                </div>
                <div class="bottom-box-inner">
                    <div class="bottom__box">
                        <div class="bottom__subtitle">
                            <?= $lastPublications->title; ?>
                        </div>
                        <div class="bottom__desc">
                            <?= $lastPublications->description; ?>
                        </div>
                    </div>
                    <div class="bottom__button morePublicBtn">
                        <a href="#" role="button"><?= Yii::t('main-information-centre', 'Все публикации'); ?></a>
                    </div>
                </div>
                <div class="col-xl-12 mt-5 p-0 morePublic moreInfo morePublic-mobile">
                    <section class="s5 container">
                        <div class="s5-inner pb-4">
                            <br>
                            <div class="row">
                                <? foreach ($publications as $item) : ?>
                                    <div class="col-xl-3 col-lg-4 col-md-6">
                                        <a href="<?= Url::to(['/publication/view', 'id' => $item->id]) ?>" style="display: contents;">
                                            <div class="s5-boxes">
                                                <div class="inf-block-img">
                                                        <img src="<?= $item->getImage() ?>" class="col-img " data-aos="fade-up" data-aos-duration="300" alt="">
                                                    <span class="" data-aos="fade-up" data-aos-duration="300">
                                                        <?= $item->date; ?>
                                                    </span>
                                                </div>
                                                <p class="fs18"><?= $item->title; ?></p>
                                                <div class="fs18-fix">
                                                    <?= $item->description; ?>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                    </section>
                </div>
            <? endif; ?>

        </div>
    </div>
    <div class="col-xl-12 mt-5 p-0 morePublic morePublic-desktop moreInfo">
        <section class="s5 container" id="publications">
            <div class="s5-inner pb-4">
                <br>
                <div class="row">
                    <? foreach ($publications as $item) : ?>
                        <div class="col-xl-3 col-lg-4 col-md-6">
                            <a href="<?= Url::to(['/publication/view', 'id' => $item->id]) ?>" style="display: contents;">
                                <div class="s5-boxes info-center-block info-center-block-public">
                                    <div class="inf-block-img">
                                        <!-- <img src="<?= $item->getImage() ?>" class="col-img " data-aos="fade-up" data-aos-duration="300" alt=""> -->
                                        <span class="" data-aos="fade-up" data-aos-duration="300"><?= $item->date; ?></span>
                                    </div>
                                    <p class="fs18"><?= $item->title; ?></p>
                                <div class="fs18-fix">
                                    <?= $item->description; ?>
                                </div>
                            </a>
                            
                        </div>
                </div>
            <? endforeach; ?>
            </div>
    </div>
    </section>
</div>
<div class="col-xl-12 mt-5 p-0 moreNews moreNews-desctop moreInfo">
    <section class="s5 container" id="news">
        <div class="s5-inner pb-4">
            <br>
            <div class="row">
                <? foreach ($news as $item) : ?>
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="s5-boxes info-center-block info-center-block-news">
                            <div class="inf-block-img">
                                <a href="<?= Url::to(['/news/view', 'id' => $item->id]) ?>" style="display: contents;">
                                    <img src="<?= $item->getImage() ?>" class="col-img " data-aos="fade-up" data-aos-duration="300" alt="">
                                </a>
                                <span class="" data-aos="fade-up" data-aos-duration="300"><?= $item->date; ?></span>
                            </div>
                            <p class="fs18"><?= $item->title; ?></p>
                            <div class="fs18-fix">
                                <?= $item->description; ?>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </section>
</div>
</div>