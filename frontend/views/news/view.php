<?php

/* @var $this View */
/* @var $news ThinkTank */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\ThinkTank;

?>
<!-- <div class="test">
    <div class="container-fluid news-top banner-top">
        <div class="banner-title text-center">
            <span id="top__title_text "><?= Yii::t('main-news', 'Новости'); ?></span>
        </div>
    </div>
</div> -->
<div class="news__wrapper news_head">
    <div class="container">
        <h2 class="news__title"><?= $news->title; ?></h2>
        <div class="news__date news-date-info"><?= $news->date; ?></div>
        <div class="news__content">
            <!-- <img src="<?= $news->getBannerImage(); ?>" alt=""> -->
            <?= $news->content; ?>
        </div>
    </div>
</div>

<div class="buttons">
    <div class="container">
        <a href="/information-centre" class="bottom__button news-back"><?= Yii::t('main-news', 'Назад'); ?></a>
    </div>
</div>

