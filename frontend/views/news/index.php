<?php


?>

<!-- <div class="test">
    <div class="container-fluid news-top banner-top">
        <div class="top__title text-center banner-title">
            <span id="top__title_text ">Новости</span>
        </div>
    </div>
</div> -->
<section class="s5 container">
    <div class="s5-inner pb-4">
        <br>
        <div class="s5-block">
            <div class="first-line2">
                <div class="s5-boxes mr10">
                    <a href="/site/news" style="display: contents;">
                        <img src="/images/main-col-1.jpg" class="col-img " data-aos="fade-up" data-aos-duration="300" alt="">
                    </a>
                    <span class="" data-aos="fade-up" data-aos-duration="300">10 марта 2020 года</span>
                    <p class="fs18">Новости о Корпорации</p>
                    <div class="fs18-fix">
                        Доходность фондов со-инвестиций, занятость населения в США,
                        иностранные инвестиции.
                    </div>
                </div>
                <div class="s5-boxes mr10 ">
                    <a href="/site/news" style="display: contents;">
                        <img src="/images/main-col-1.jpg" class="col-img " data-aos="fade-up" data-aos-duration="300" alt="">
                    </a>
                    <span class="" data-aos="fade-up" data-aos-duration="300">2 марта 2020 года</span>
                    <p class="fs18">Новости о Корпорации</p>
                    <div class="fs18-fix">
                        Аналитический обзор: Коронавирус, рынки недвижимости, мировая
                        цифровая валюта
                    </div>
                </div>
            </div>
            <div class="second-line2">
                <div class="s5-boxes mr10">
                    <a href="/site/news" style="display: contents;">
                        <img src="/images/main-col-1.jpg" class="col-img " data-aos="fade-up" data-aos-duration="300" alt="">
                    </a>
                    <span class="" data-aos="fade-up" data-aos-duration="300">2 марта 2020 года</span>
                    <p class="fs18">Новости о Корпорации</p>
                    <div class="fs18-fix">
                        Работники Корпорации провели презентацию о деятельности перед
                        студентами Назарбаев Университета.
                    </div>
                </div>
                <div class="s5-boxes mr10">
                    <a href="/site/news" style="display: contents;">
                        <img src="/images/main-col-1.jpg" class="col-img " data-aos="fade-up" data-aos-duration="300" alt="">
                    </a>
                    <span class="" data-aos="fade-up" data-aos-duration="300">2 марта 2020 года</span>
                    <p class="fs18">Новости о Корпорации</p>
                    <div class="fs18-fix">
                        Аналитический обзор: Коронавирус, рынки недвижимости, мировая
                        цифровая валюта
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="s5-inner pb-4">
        <br>
        <div class="s5-block">
            <div class="first-line2">
                <div class="s5-boxes mr10">
                    <a href="/site/news" style="display: contents;">
                        <img src="/images/main-col-1.jpg" class="col-img " data-aos="fade-up" data-aos-duration="300" alt="">
                    </a>
                    <span class="" data-aos="fade-up" data-aos-duration="300">10 марта 2020 года</span>
                    <p class="fs18">Новости о Корпорации</p>
                    <div class="fs18-fix">
                        Доходность фондов со-инвестиций, занятость населения в США,
                        иностранные инвестиции.
                    </div>
                </div>
                <div class="s5-boxes mr10 ">
                    <a href="/site/news" style="display: contents;">
                        <img src="/images/main-col-1.jpg" class="col-img " data-aos="fade-up" data-aos-duration="300" alt="">
                    </a>
                    <span class="" data-aos="fade-up" data-aos-duration="300">2 марта 2020 года</span>
                    <p class="fs18">Новости о Корпорации</p>
                    <div class="fs18-fix">
                        Аналитический обзор: Коронавирус, рынки недвижимости, мировая
                        цифровая валюта
                    </div>
                </div>
            </div>
            <div class="second-line2">
                <div class="s5-boxes mr10">
                    <a href="/site/news" style="display: contents;">
                        <img src="/images/main-col-1.jpg" class="col-img " data-aos="fade-up" data-aos-duration="300" alt="">
                    </a>
                    <span class="" data-aos="fade-up" data-aos-duration="300">2 марта 2020 года</span>
                    <p class="fs18">Новости о Корпорации</p>
                    <div class="fs18-fix">
                        Работники Корпорации провели презентацию о деятельности перед
                        студентами Назарбаев Университета.
                    </div>
                </div>
                <div class="s5-boxes mr10">
                    <a href="/site/news" style="display: contents;">
                        <img src="/images/main-col-1.jpg" class="col-img " data-aos="fade-up" data-aos-duration="300" alt="">
                    </a>
                    <span class="" data-aos="fade-up" data-aos-duration="300">2 марта 2020 года</span>
                    <p class="fs18">Новости о Корпорации</p>
                    <div class="fs18-fix">
                        Аналитический обзор: Коронавирус, рынки недвижимости, мировая
                        цифровая валюта
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>