<?php

/* @var $this View */
/* @var $model Procurement */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\Procurement;

?>

<div class="procurement">
    <div class="container-fluid">
        <div class="back-btn">
            <a href="<?= Url::to(['index', 'id' => $model->parent_id]) ?>"><img src="/images/arrow_down.svg" alt="">
                <?= Yii::t('main-investment', 'Назад'); ?>
            </a>
        </div>
        <div class="procurement-title">
            <h1><?= $model->name; ?></h1>
        </div>
        <a href="" class="button--reg procurement-btn btn" data-toggle="modal" data-target="#procurmentModal"> <i class="fas fa-download"></i>
            <?= Yii::t('main-procurement', 'Скачать конкурсную документацию'); ?>
        </a>

        <div class="modal fade" id="procurmentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-procurement" role="document">
                <div class="modal-content procurement-content-modal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body procurement-body">
                        <h3><?= Yii::t('main-procurement', 'Конкурсная документация'); ?></h3>
                        <h2>
                            <?= Yii::t('main-procurement', 'Чтобы получить с сайта конкурсную документацию, необходимо предоставить следующую информацию.'); ?>
                        </h2>

                        <input class="file-id" type="hidden" value="<?= $model->id; ?>" />
                        <form class="reg__form" id="procurement-documents-form" name="procurementForm">
                            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />

                            <input type="hidden" class="procurement-documents" value="<?= $model->id; ?>">
                            <label class="form__label rezume-label">
                                <p class="label__text">
                                    <?= Yii::t('main-procurement', 'Наименование лица (юридического или физического)'); ?>
                                    <span class="required">*</span></p>
                                <input type="text" name="ProcurementForm[legal_name]" required>
                            </label>
                            <label class="form__label rezume-label">
                                <p class="label__text">
                                    <?= Yii::t('main-procurement', 'БИН/ИИН'); ?>
                                    <span class="required">*</span></p>
                                <input type="text" name="ProcurementForm[bin]" required>
                            </label>
                            <h3><?= Yii::t('main-procurement', 'Почтовый адрес'); ?></h3>
                            <label class="form__label rezume-label">
                                <p class="label__text"><?= Yii::t('main-procurement', 'Почтовый индекс'); ?></p>
                                <input type="text" name="ProcurementForm[postcode]">
                            </label>
                            <label class="form__label rezume-label">
                                <p class="label__text"><?= Yii::t('main-procurement', 'Город'); ?> </p>
                                <input type="text" name="ProcurementForm[city]">
                            </label>
                            <label class="form__label rezume-label">
                                <p class="label__text"><?= Yii::t('main-procurement', 'Адрес'); ?> </p>
                                <input type="text" name="ProcurementForm[address]">
                            </label>
                            <h3>Данные лица</h3>
                            <label class="form__label rezume-label">
                                <p class="label__text"><?= Yii::t('main-procurement', 'Имя, фамилия'); ?></p>
                                <input type="text" name="ProcurementForm[username]">
                            </label>
                            <label class="form__label rezume-label">
                                <p class="label__text"><?= Yii::t('main-procurement', 'Должность'); ?> </p>
                                <input type="text" name="ProcurementForm[position]">
                            </label>
                            <label class="form__label rezume-label">
                                <p class="label__text"><?= Yii::t('main-procurement', 'Номер удостоверения личности'); ?></p>
                                <input type="number" name="ProcurementForm[id_number]">
                            </label>
                            <label class="form__label rezume-label">
                                <p class="label__text"><?= Yii::t('main-procurement', 'Кем выдано'); ?></p>
                                <input type="text" name="ProcurementForm[who_issued]">
                            </label>
                            <label class="form__label rezume-label">
                                <p class="label__text"><?= Yii::t('main-procurement', 'Дата выдачи'); ?></p>
                                <input type="text" placeholder="дд.мм.гггг" id="date" name="ProcurementForm[date_issue]">
                            </label>
                            <label class="form__label rezume-label">
                                <p class="label__text"><?= Yii::t('main-procurement', 'Контактный телефон'); ?> *</p>
                                <input type="text" name="ProcurementForm[phone]" required>

                                <script>
                                    $('input[name="ProcurementForm[phone]"]').inputmask("+7(999) 999-9999");
                                </script>
                            </label>
                            <label class="form__label rezume-label">
                                <p class="label__text"><?= Yii::t('main-procurement', 'Email'); ?></p>
                                <input type="text" name="ProcurementForm[email]">
                            </label>

                            <button type="submit" class="button button--reg procurement-documents-btn">
                                <?= Yii::t('main-procurement', 'ОТПРАВИТЬ'); ?>
                            </button>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalKonkursDoc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" data-backdrop="static" data-keyboard="false" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                        <button type="button" class="close close-procurement" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body modalKonkursBody">

                    </div>

                </div>
            </div>
        </div>


        <div class="procurement-inner-p">
            <?= $model->content; ?>
        </div>

    </div>
</div>