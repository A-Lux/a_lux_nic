<?php

/* @var $this View */
/* @var $procurement Procurement */
/* @var $plan ProcurementDocuments */

use yii\helpers\Url;
use yii\web\View;
use common\models\Procurement;
use common\models\ProcurementDocuments;

?>
<div class="plan">
<div class="container-fluid ">
    <div class="back-btn">
        <a href="/about#corporation" data-slide="5"><img src="/images/arrow_down.svg" alt="">
            <?= Yii::t('main-investment', 'Назад'); ?>
        </a>
    </div>
    <div class="row  pb-5 m-0">
        <div class="col-xl-12 text-center">
            <h3 class="title mb-4"><?= $procurement->name; ?></h3>
            <h3 class="title mb-4">
                <?= $procurement->content; ?>
            </h3>
        </div>
        <div class="responsive-slider">
            <? foreach($plan as $documents): ?>
                <div>
                    <div class="row">
                        <? foreach ($documents as $document) : ?>
                        <div class="col-xl-3 col-md-6 col-lg-4 col-6">
                            <a href="<?= $document->getFile(); ?>" target="_blank">
                                <div class="download_box border-box act-box procurements-box">
                                    <div class="download_box_image">
                                        <img src="/backend/web/uploads/images/procurement/1587711421_about-plan.svg" alt="">
                                    </div>
                                    <div class="download_box_title">
                                        <p><?= $document->name; ?></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <? endforeach; ?>
                    </div>
                </div>
            <? endforeach;?>

        </div>

    </div>
</div>
</div>
