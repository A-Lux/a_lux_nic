<?php

/* @var $this View */
/* @var $procurement Procurement */

/* @var $documents ProcurementDocuments */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\Procurement;
use common\models\ProcurementDocuments;

?>

<div class="procurement">
    <div class="container-fluid">
        <div class="back-btn">
            <a href="/about#corporation" data-slide="5"><img src="/images/arrow_down.svg" alt="">
                <?= Yii::t('main-procurement', 'Назад'); ?>
            </a>
        </div>
        <div class="procurement-content">
            <?= $procurement->content; ?>
            <? foreach ($procurement->child as $value) : ?>
            <a href="<?= Url::to(['view', 'id' => $value->id]) ?>">
                <div class="invest__box procurement-info">

                    <div class="doc-name">
                        <div class="doc-zoom">
                            <img src="/images/doc.svg" alt="">
                        </div>
                        <div>
                            <h1><?= $value->name ?></h1>
                            <p><?= $value->description; ?></p>
                        </div>
                    </div>
                    <div class="doc-arrow">
                        <img src="/images/arrow-right.svg" alt="">
                    </div>  
                </div>
            </a>
            <div class="procurement-download procurement-download-btn">
                <? foreach ($documents as $document) : ?>
                <? if ($document->procurement_id == $value->id) : ?>
                <div class="download_box procurements-wrapper">
                    <a href="<?= $document->getFile(); ?>" target="_blank">

                        <div class="download_box_title procurements-wrapper-title">
                            <?= $document->name; ?>
                        </div>
                        <div class="download__subtitle" onclick="downloadDocuments(<?= $document->id ?>)">
                            <div class="download_left_col">DOCX</div>
                            <img class="download_right_col download_procurement" src="/images/download.svg" alt="">
                        </div>
                    </a>
                </div>
                <? endif; ?>

                <? endforeach; ?>
            </div>
            <? endforeach; ?>

        </div>
    </div>
</div>

<div class="modal fade" id="documentModalCenter" tabindex="-1" role="dialog" aria-labelledby="documentModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-procurement" role="document">
        <div class="modal-content procurement-content-modal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="close-form">×</span>
                </button>
            </div>
            <div class="modal-body procurement-body">
                <h3><?= Yii::t('main-procurement', 'Конкурсная документация'); ?></h3>
                <h2>
                    <?= Yii::t('main-procurement', 'Чтобы получить с сайта конкурсную документацию, необходимо предоставить следующую информацию.'); ?>
                </h2>
                <form class="reg__form" id="download-documents-form" name="downloadForm">
                    <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />

                    <input type="hidden" class="download-documents" id="downloads-documents" value="">
                    <label class="form__label rezume-label">
                        <p class="label__text">
                            <?= Yii::t('main-procurement', 'Наименование лица (юридического или физического)'); ?>
                            <span class="required">*</span>
                        </p>
                        <input type="text" name="ProcurementForm[legal_name]" required>
                    </label>
                    <label class="form__label rezume-label">
                        <p class="label__text">
                            <?= Yii::t('main-procurement', 'БИН/ИИН'); ?>
                            <span class="required">*</span></p>
                        <input type="text" name="ProcurementForm[bin]" required>
                    </label>
                    <h3>
                        <?= Yii::t('main-procurement', 'Почтовый адрес'); ?>
                    </h3>
                    <label class="form__label rezume-label">
                        <p class="label__text"><?= Yii::t('main-procurement', 'Почтовый индекс'); ?></p>
                        <input type="text" name="ProcurementForm[postcode]">
                    </label>
                    <label class="form__label rezume-label">
                        <p class="label__text"><?= Yii::t('main-procurement', 'Город'); ?> </p>
                        <input type="text" name="ProcurementForm[city]">
                    </label>
                    <label class="form__label rezume-label">
                        <p class="label__text"><?= Yii::t('main-procurement', 'Адрес'); ?> </p>
                        <input type="text" name="ProcurementForm[address]">
                    </label>
                    <h3>Данные лица</h3>
                    <label class="form__label rezume-label">
                        <p class="label__text"><?= Yii::t('main-procurement', 'Имя, фамилия'); ?></p>
                        <input type="text" name="ProcurementForm[username]">
                    </label>
                    <label class="form__label rezume-label">
                        <p class="label__text"><?= Yii::t('main-procurement', 'Должность'); ?> </p>
                        <input type="text" name="ProcurementForm[position]">
                    </label>
                    <label class="form__label rezume-label">
                        <p class="label__text"><?= Yii::t('main-procurement', 'Номер удостоверения личности'); ?></p>
                        <input type="number" name="ProcurementForm[id_number]">
                    </label>
                    <label class="form__label rezume-label">
                        <p class="label__text"><?= Yii::t('main-procurement', 'Кем выдано'); ?></p>
                        <input type="text" name="ProcurementForm[who_issued]">
                    </label>
                    <label class="form__label rezume-label">
                        <p class="label__text"><?= Yii::t('main-procurement', 'Дата выдачи'); ?></p>
                        <input type="text" placeholder="дд.мм.гггг" name="ProcurementForm[date_issue]">
                    </label>
                    <label class="form__label rezume-label">
                        <p class="label__text"><?= Yii::t('main-procurement', 'Контактный телефон'); ?> *</p>
                        <input type="text" name="ProcurementForm[phone]" required>

                        <script>
                            $('input[name="ProcurementForm[phone]"]').inputmask("+7(999) 999-9999");
                        </script>
                    </label>
                    <label class="form__label rezume-label">
                        <p class="label__text"><?= Yii::t('main-procurement', 'Email'); ?></p>
                        <input type="text" name="ProcurementForm[email]">
                    </label>

                    <button type="submit" class="button button--reg download-documents-btn">
                        <?= Yii::t('main-procurement', 'ОТПРАВИТЬ'); ?>
                    </button>
                </form>

            </div>
        </div>
    </div>
</div>