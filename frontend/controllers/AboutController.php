<?php
namespace frontend\controllers;

use common\models\Banner;
use common\models\ManagementStructure;
use common\models\Membership;
use common\models\Menu;
use common\models\OurTeam;
use common\models\Procurement;
use common\models\SliderDocuments;
use common\models\Sliders;
use common\models\Submenu;
use common\models\Team;
use common\models\TeamAdvantages;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * About controller
 */
class AboutController extends FrontendController
{
    /**
     * Displays index.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $menu                   = Menu::getAbout();
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $banner             = Banner::getAbout();
        $ourTeam            = OurTeam::getOne();
        $advantages         = TeamAdvantages::getAll();
        $procurements       = Procurement::getAll();
        $structures         = ManagementStructure::getAll();
        $managements        = Team::getAll();
        $memberships        = Membership::getAll();
        $submenu            = Submenu::getKey();
        $sliders            = Sliders::getKey();

        return $this->render('index', [
            'procurements'  => $procurements,
            'structures'    => $structures,
            'managements'   => $managements,
            'memberships'   => $memberships,
            'banner'        => $banner,
            'ourTeam'       => $ourTeam,
            'advantages'    => $advantages,
            'submenu'       => $submenu,
            'sliders'       => $sliders,
        ]);
    }

    public function actionSliderDocuments($id)
    {
        $model  = SliderDocuments::findOne(['id' => $id]);

        $array  = [
            'title' => $model->name,
            'file'  => $model->getFile(),
            'path'  => $model->getPathToFile(),
        ];


        return json_encode($array,JSON_UNESCAPED_UNICODE);
    }

    public function actionViewTeam($id)
    {
        $model  = Team::findOne(['id' => $id]);

        $array  = [
            'username'      => $model->username,
            'position'      => $model->position,
            'content'       => $model->content,
            'image'         => $model->getImage(),
        ];

        return json_encode($array,JSON_UNESCAPED_UNICODE);
    }
}
