<?php
namespace frontend\controllers;

use common\models\Banner;
use common\models\ClassesInvestments;
use common\models\CoreSatellite;
use common\models\InvestmentPortfolio;
use common\models\Menu;
use common\models\Risk;
use common\models\RiskSystem;
use common\models\Submenu;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Investments controller
 */
class InvestmentsController extends FrontendController
{
    /**
     * Displays index.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $menu                   = Menu::getInvestments();
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $banner             = Banner::getInvestment();
        $portfolio          = InvestmentPortfolio::getOne();
        $coreSatellite      = CoreSatellite::getAll();
        $classesInvestments = ClassesInvestments::getAll();
        $risk               = Risk::getAll();
        $riskSystem         = RiskSystem::getAll();
        $submenu            = Submenu::getKey();

        return $this->render('index', [
            'banner'                => $banner,
            'portfolio'             => $portfolio,
            'coreSatellite'         => $coreSatellite,
            'classesInvestments'    => $classesInvestments,
            'risk'                  => $risk,
            'riskSystem'            => $riskSystem,
            'submenu'               => $submenu,

        ]);
    }
}