<?php
namespace frontend\controllers;

use common\models\ThinkTank;
use common\models\ThinkTankContent;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * About controller
 */
class PublicationController extends FrontendController
{
    /**
     * Displays index.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
        ]);
    }

    /**
     * Displays a single model.
     * @param $id
     * @return string
     * @throws
     */
    public function actionView($id)
    {
        $publication   = ThinkTank::findOne(['id' => $id]);
        $this->setMeta($publication->metaName, $publication->metaDesc, $publication->metaKey);

        $recommended        = ThinkTank::recommendedPublications();
        $thinkTank          = ThinkTankContent::getOne();

        return $this->render('view', [
            'publication'   => $publication,
            'recommended'   => $recommended,
            'thinkTank'     => $thinkTank,
        ]);
    }

    /**
     * Finds the Brand model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
//        /* @var $activeRecord MultilingualActiveRecord */
//
//        $model = News::find()->where(['slug' => $slug])->one();
//
//        if (null === $model) {
//            throw new NotFoundHttpException('Указанная страница не найдена.');
//        }
//
//        return $model;
    }
}
