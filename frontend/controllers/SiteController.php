<?php
namespace frontend\controllers;

use common\models\AboutPath;
use common\models\Banner;
use common\models\BannerMobile;
use common\models\BannerMobileProduct;
use common\models\BannerProduct;
use common\models\Brand;
use common\models\Catalog;
use common\models\ClassesInvestments;
use common\models\FavoritesProduct;
use common\models\MainAdvantages;
use common\models\Menu;
use common\models\Product;
use common\models\Sliders;
use common\models\Submenu;
use common\models\ThinkTank;
use common\models\ThinkTankContent;
use frontend\models\Basket;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $menu                   = Menu::getMain();
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $banner                 = Banner::getMain();
        $advantages             = MainAdvantages::all();
        $menu                   = Menu::getAll();
        $classesInvestments     = ClassesInvestments::getAll();
        $thinTank               = ThinkTankContent::getOne();
        $information            = ThinkTank::lastForMain();
        $submenu                = Submenu::getKey();
        $wayDevelopment         = AboutPath::getAll();
        $aboutPath              = AboutPath::arrayAll();

        return $this->render('index', [
            'advantages'                => $advantages,
            'menu'                      => $menu,
            'classesInvestments'        => $classesInvestments,
            'banner'                    => $banner,
            'thinkTank'                 => $thinTank,
            'information'               => $information,
            'submenu'                   => $submenu,
            'wayDevelopment'            => $wayDevelopment,
            'aboutPath'                 => $aboutPath,

        ]);
    }

    public function actionSearch()
    {
    return $this->render('search');
    }
    public function actionMap()
    {
        $menu                   = Menu::getMap();
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $sliders                = Sliders::getAll();
        $banner                 = Banner::getMap();

        return $this->render('map', [
            'sliders'   => $sliders,
            'banner'    => $banner,
        ]);
    }
    public function actionNews()
    {
    return $this->render('news');
    }
    public function actionRegistration()
    {
    return $this->render('registration');
    }

    public function actionPublication()
    {
    return $this->render('publication');
    }

    public function actionPublicationInner()
    {
    return $this->render('publication-inner');
    }

    public function actionProcurement()
    {
    return $this->render('procurement');
    }

    public function actionProcurementInner()
    {
    return $this->render('procurement-inner');
    }

    public function actionDocuments()
    {
    return $this->render('documents');
    }

    public function actionDocuments2()
    {
    return $this->render('documents2');
    }


    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
