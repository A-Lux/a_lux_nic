<?php
namespace frontend\controllers;

use common\models\Banner;
use common\models\Careers;
use common\models\Menu;
use common\models\RequestVacancy;
use frontend\models\CareerForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Career controller
 */
class CareerController extends FrontendController
{
    /**
     * Displays index.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $menu                   = Menu::getCareer();
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $banner             = Banner::getCareer();
        $career             = Careers::getAll();

        return $this->render('index', [
            'career'    => $career,
            'banner'    => $banner,
        ]);
    }

    /**
     * Displays create.
     *
     * @return mixed
     */
    public function actionCreateVacancy()
    {
        $model  = new CareerForm();

        if ($model->load(\Yii::$app->request->post())) {

            if ($request = $model->create()) {
                $response   = ['status' => 1, 'message' => \Yii::t('main-message','Благодарим Вас за предложение!')];
                return json_encode($response);
            } else {
                $errors = $model->firstErrors;
                foreach ($errors as $error) {
                    $message   = \Yii::t('main-message', $error);
                    break;
                }
                $response   = ['status' => 0, 'message' => $message];
                return json_encode($response);
            }
        }else{
            \Yii::$app->response->statusCode = 405;
            $response   = ['status' => 0, 'message' => \Yii::t('main-message','Ошибка сервера, попробуйте позднее!')];
            return json_encode($response);
        }
    }

    public function actionTest()
    {
        $id = 1;
        $request    = RequestVacancy::findOne(['id' => $id]);
        $model  = new CareerForm();

        if($model->sendInformationCareer($request)){
            return 'molodec';
        }else{
            return 'dalbaeb';
        }
    }
}