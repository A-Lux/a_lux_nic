<?php
namespace frontend\controllers;

use common\models\Procurement;
use common\models\ProcurementDocuments;
use common\models\ProcurementFiles;
use frontend\models\ProcurementForm;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * About controller
 */
class ProcurementController extends FrontendController
{
    /**
     * Displays index.
     *
     * @param $id
     * @return mixed
     */
    public function actionIndex($id)
    {
        $procurement    = Procurement::getOne($id);
        $documents      = ProcurementDocuments::getAll();

        return $this->render('index', [
            'procurement'   => $procurement,
            'documents'     => $documents,
        ]);
    }

    /**
     * Displays act.
     *
     * @param $id
     * @return mixed
     */
    public function actionAct($id)
    {
        $procurement    = Procurement::getOne($id);
        $plan           = ProcurementDocuments::getYears($procurement->id);

        return $this->render('act', [
            'procurement'   => $procurement,
            'plan'          => $plan,
        ]);
    }

    /**
     * Displays plan.
     *
     * @param $id
     * @return mixed
     */
    public function actionPlan($id)
    {
        $procurement    = Procurement::getOne($id);
        $plan           = ProcurementDocuments::getYears($procurement->id);

        return $this->render('plan', [
            'procurement'   => $procurement,
            'plan'          => $plan,
        ]);
    }

    /**
     * Displays a single model.
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model      = $this->findModel($id);

        return $this->render('view', [
            'model'         => $model,
        ]);
    }

    public function actionProcurementDocument()
    {
        $model  = new ProcurementForm();

        if ($model->load(\Yii::$app->request->post())) {
            if ($model->createUser()) {
                $response   = ['status' => 1, 'message' => \Yii::t('main-message', 'Благодарим Вас за предложение!')];
                return json_encode($response);
            } else {
                $errors = $model->firstErrors;
                foreach ($errors as $error) {
                    $message   = \Yii::t('main-message', $error);
                    break;
                }
                $response   = ['status' => 0, 'message' => $message];
                return json_encode($response);
            }
        }else{
            \Yii::$app->response->statusCode = 405;
            $response   = ['status' => 0, 'message' => 'Ошибка сервера, попробуйте позднее!'];
            return json_encode($response);
        }
    }

    /**
     * action view document
     * @param $id
     * @return mixed
     * @throws
     */
    public function actionViewDocuments($id)
    {
        $model  = Procurement::getViewFiles($id);

        return json_encode($model, JSON_UNESCAPED_UNICODE);
    }

    /**
     * action download files
     * @param $id
     * @return mixed
     * @throws
     */
    public function actionDownloadProcurementFiles($id)
    {
        $model  = ProcurementFiles::getOne($id);
        $file   = Yii::getAlias('@backend') . '/web/uploads/files/procurement-files/' . $model->file;

        return Yii::$app->response->sendFile($file);
    }

    /**
     * action download document
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDownloadProcurementDocuments($id)
    {
        $model  = $this->findModel($id);

        $file   = Yii::getAlias('@backend') . '/web/uploads/files/procurement/' . $model->file;

        return Yii::$app->response->sendFile($file);
    }

    /**
     * action download document
     * @param $id
     * @return mixed
     */
    public function actionDownloadDocuments($id)
    {
        $model  = ProcurementDocuments::getOne($id);

        $file   = Yii::getAlias('@backend') . '/web/uploads/files/procurement-documents/' . $model->file;

        return Yii::$app->response->sendFile($file);
    }

    /**
     * Finds the Brand model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Procurement::getOne($id);

        if (null === $model) {
            throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}