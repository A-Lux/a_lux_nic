<?php

namespace frontend\controllers;

use common\models\DocumentsConfidential;
use common\models\User;
use frontend\models\LoginForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class EventsController extends FrontendController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        $model  = new LoginForm();

        try {
            if ($model->load(\Yii::$app->request->post()) && $model->login()) {
                $response   = ['status' => 1];
                return json_encode($response);
            }else{
                $errors = $model->firstErrors;
                foreach ($errors as $error) {
                    $message   = \Yii::t('main-message', $error);
                    break;
                }
                $response   = ['status' => 0, 'message' => $message];
                return json_encode($response);
            }
        }catch (\Exception $e){
            \Yii::$app->session->addFlash('error', $e->getMessage());
        }
    }

    public function actionView()
    {
        if(\Yii::$app->user->isGuest){
            return $this->redirect('index');
        }

        $documents  = DocumentsConfidential::getAll();

        return $this->render('view', [
            'documents'     => $documents,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        \Yii::$app->user->logout();

        return $this->goHome();
    }
}