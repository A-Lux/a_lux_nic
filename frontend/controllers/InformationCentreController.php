<?php
namespace frontend\controllers;

use common\models\Banner;
use common\models\InformationCentre;
use common\models\Menu;
use common\models\ThinkTank;
use common\models\ThinkTankContent;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Information Centre controller
 */
class InformationCentreController extends FrontendController
{
    /**
     * Displays index.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $menu                   = Menu::getInformationCentre();
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $banner             = Banner::getInformationCentre();
        $news               = ThinkTank::allNews();
        $publications       = ThinkTank::allPublications();
        $lastNews           = ThinkTank::lastNews();
        $lastPublications   = ThinkTank::lastPublications();
        $thinkTank          = ThinkTankContent::getOne();

        return $this->render('index', [
            'banner'            => $banner,
            'news'              => $news,
            'publications'      => $publications,
            'lastNews'          => $lastNews,
            'lastPublications'  => $lastPublications,
            'thinkTank'         => $thinkTank,

        ]);
    }
}