<?php

namespace frontend\controllers;

use common\models\Banner;
use common\models\Catalog;
use common\models\City;
use common\models\Contact;
use common\models\Documents;
use common\models\Language;
use common\models\Logo;
use common\models\Menu;
use common\models\Sliders;
use common\models\Submenu;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;

class FrontendController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function init()
    {
        $menu           = Menu::getActiveMenu();
        $submenu        = Submenu::getAll();
        $language       = Language::getAll();
        $logoHeader     = Logo::getHeader();
        $logoFooter     = Logo::getFooter();
        $contact        = Contact::getOne();
        $sliders        = Sliders::arrayList();
        $menuKey        = Menu::getAll();

        \Yii::$app->view->params['menu']                = $menu;
        \Yii::$app->view->params['submenu']             = $submenu;
        \Yii::$app->view->params['language']            = $language;
        \Yii::$app->view->params['logoHeader']          = $logoHeader;
        \Yii::$app->view->params['logoFooter']          = $logoFooter;
        \Yii::$app->view->params['contact']             = $contact;
        \Yii::$app->view->params['sliders']             = $sliders;
        \Yii::$app->view->params['menuKey']             = $menuKey;

        parent::init();
    }

    protected function setMeta($title = null, $description = null, $keywords = null)
    {
        $this->view->title      =   $title == null ? 'nicnbk' : $title;
        $this->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
    }
}