<?php
namespace frontend\models;

use common\models\ProcurementUser;
use common\modules\user\models\BaseUser;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Procurement form
 */
class ProcurementForm extends Model
{

    public $legal_name;
    public $bin;
    public $postcode;
    public $city;
    public $address;
    public $username;
    public $position;
    public $id_number;
    public $who_issued;
    public $date_issue;
    public $phone;
    public $email;

    /**
     * Labels for fields
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'legal_name'    => 'Наименование лица',
            'bin'           => 'БИН/ИИН',
            'postcode'      => 'Почтовый индекс',
            'city'          => 'Город',
            'address'       => 'Адрес',
            'username'      => 'Имя, фамилия',
            'position'      => 'Должность',
            'id_number'     => 'Номер удостоверения личности',
            'who_issued'    => 'Кем выдано',
            'date_issue'    => 'Дата выдачи',
            'phone'         => 'Контактный телефон',
            'email'         => 'Email',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['legal_name', 'bin', 'postcode', 'city', 'address', 'username', 'position',
                'id_number', 'who_issued', 'date_issue', 'phone', 'email'
            ], 'string', 'max' => 255],

            [['legal_name', 'bin', 'postcode', 'city', 'address', 'username', 'position',
                'id_number', 'who_issued', 'date_issue', 'phone', 'email'], 'trim'],

            [['email'], 'email'],

            [['legal_name', 'bin', 'city', 'username', 'position', 'id_number', 'who_issued', 'date_issue', 'phone'], 'required'],

        ];
    }

    /**
     * Signs user up.
     *
     * @return bool|\common\models\ProcurementUser
     */
    public function createUser()
    {
        if ($this->validate()) {
            $user                   = new ProcurementUser();
            $user->legal_name       = $this->legal_name;
            $user->bin              = $this->bin;
            $user->postcode         = $this->postcode;
            $user->city             = $this->city;
            $user->address          = $this->address;
            $user->username         = $this->username;
            $user->position         = $this->position;
            $user->id_number        = $this->id_number;
            $user->who_issued       = $this->who_issued;
            $user->date_issue       = $this->date_issue;
            $user->phone            = $this->phone;
            $user->email            = $this->email;

            $user->save();

            return true;
        }

        return false;
    }

}
