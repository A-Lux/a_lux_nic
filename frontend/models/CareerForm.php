<?php

namespace frontend\models;

use common\models\RequestVacancy;
use yii\base\Model;
use yii\web\UploadedFile;

class CareerForm extends Model
{
    public $username;
    public $phone;
    public $file;

    /**
     * Labels for fields
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'username'  => \Yii::t('main-label', 'ФИО'),
            'phone'     => \Yii::t('main-label', 'Телефон'),
            'file'      => \Yii::t('main-label', 'Файл'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'max' => 255],

            ['phone', 'required'],
            ['phone', 'string', 'min' => 8],

            [['file'], 'file', 'extensions' => 'txt,pdf,docx, doc, zip, rar, pptx'],
        ];
    }

    /**
     * Create user up.
     * @param
     *
     * @return bool|\common\models\RequestVacancy
     */
    public function create()
    {
        if ($this->validate()) {
            $user           = new RequestVacancy();
            $user->username     = $this->username;
            $user->phone        = $this->phone;
            $user->created_at   = date("Y-m-d H:i:s", time());
            $this->createFile($user);
            if($user->save()){
                $this->sendInformationCareer($user);
            }

            return $user;
        }

        return false;
    }

    /**
     * create file user up.
     * @param $model RequestVacancy
     * @param
     * @throws
     * @return bool|mixed
     */
    protected function createFile($model)
    {
        $file = UploadedFile::getInstanceByName('file');
        $hash   = \Yii::$app->security->generateRandomString();

        if($file != null){
            $time = time();
            $file->saveAs($model->pathFile() . $time . '_' . $hash . '.' . $file->extension);
            $model->file = $time . '_' . $hash . '.' . $file->extension;
            return true;
        }else{
            return false;
        }
    }

    public function sendInformationCareer($request)
    {
        $host = \Yii::$app->request->hostInfo;

        $emailSend = \Yii::$app->mailer->compose()
            ->setFrom(['info@uww.asia' => '«NICNBK»'])
            ->setTo('hr@nicnbk.kz')
            ->setSubject('Заявка на вакансию')
            ->setHtmlBody("<p>На сайте <a href='$host'>«NICNBK.KZ»</a> поступила новая заявка на вакансию!</br>
                                 </br>
                                 <p>Данные пользователя:</p> </br></br>
                                 <p>ФИО: $request->username</p>
                                 <p>Номер телефона: $request->phone</p> 
                                 </br></br>
                                 <p>---</p></br>
                                 <p>С уважением</p></br>
                                 <p>администрация <a href='$host'>«NICNBK»</a></p>");

        if(!empty($request->getFilePathName())){
            $emailSend->attach($request->getFilePathName());
        }

        if($emailSend->send()){
            return true;
        }

        return false;

    }
}