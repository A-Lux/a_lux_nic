$(function () {
  $(".responsive-slider").slick({
    infinite: true,
    dots: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    cssEase: "linear",
    nextArrow: "<img src='images/arrow-right.png' class='slick-icon-right'>",
    prevArrow: "<img src='images/arrow-left.png' class='slick-icon-left'>",
    // responsive: [
    //     {
    //         breakpoint: 1024,
    //         settings: {
    //             slidesToShow: 1,
    //             slidesToScroll: 1,
    //             infinite: true,
    //         }
    //     },
    //     {
    //         breakpoint: 740,
    //         settings: {
    //             slidesToShow: 1,
    //             slidesToScroll: 1
    //         }
    //     },
    //     {
    //         breakpoint: 480,
    //         settings: {
    //             slidesToShow: 1,
    //             slidesToScroll: 1
    //         }
    //     }
    
    // ]
  });
});
