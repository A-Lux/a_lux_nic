function getUrl() {
    if (document.getElementsByTagName('html')[0].getAttribute('lang') != 'ru') {
        return `${window.location.origin}/${document.getElementsByTagName('html')[0].getAttribute('lang')}`;
    }
    else {
        return `${window.location.origin}`;
    }
}

$('#authorization-form').on('submit', function (e) {
    e.preventDefault();
    let form    = new FormData(document.forms.authorizationForm);

    axios.post(`${getUrl()}/events/login`, form)
        .then(function (response) {
            if(response.data.status === 1) {
                window.location.href    = `${getUrl()}/events/view`;
                document.getElementById('authorization-form').reset();
            } else if(response.data.status === 0){
                Swal.fire({
                    icon: 'warning',
                    text: response.data.message
                });
            }
        })
        .catch(function (error) {
            console.log(error)
            Swal.fire({
                icon: 'error',
                text: error.response.data.message
            });
        });
});