$(document).ready(function() {

    if ($('.plan .slick-slide').length < 2) {
        $('.plan .slick-dots').css('display', 'none')
    } else {
        $('.plan .slick-dots').css('display', 'flex')
    }

    $(".slider").slick({
        infinite: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        swipe: true,
        nextArrow: "<img src='/images/arrow-right.svg' class='slick-icon-right'>",
        prevArrow: "<img src='/images/arrow-left.svg' class='slick-icon-left'>",
    });

    $('.tabs a').click(function() {

        $('.panel').hide();
        $('.tabs a.active').removeClass('active');
        $(this).addClass('active');

        var panel = $(this).attr('href');
        $(panel).fadeIn(500);

        // return false;
    });

    $('.tabs li:first a').click();

    $('a[data-toggle="tab"]').on('click', function(e) {
        $('.slider').slick('setPosition');
        console.log("slider")
    })

    $('.wrapper-video').ContainerPlayer({
        autoplay: true,
        loop: true,
        poster: false,
        muted: true,
        html5: {
            src: '/images/vid.mp4',
        }
    })

    $("#show-1").click(function() {
        $("#s-2-id").addClass("s2-active");
        console.log("fpafkafkpa");
    });
    $("#s-1-cl").click(function() {
        $("#s-2-id").removeClass("s2-active");
    });
    $("#s-4-btn").click(function() {
        $("#s-4-id").addClass("s2-active");
        console.log("fpafkafkpa");
    });
    $("#s-5").click(function() {
        $("#s-4-id").removeClass("s2-active");
    });

    $("#date").mask("99/99/9999");
    $("#phone").mask("8(999) 999-9999");
    $(".nav-link").hover(function() {
        $(this).parent(".nav-dropdown").children(".dropdown-menu").fadeIn(500);
    });
    $(".nav-dropdown").mouseleave(function() {
        $(this).children(".dropdown-menu").fadeOut(300);
    });

    $(".about-menu").click(function() {
        $(".about-collapse").toggleClass("about-collapse-active");
    });
    let cel = document.querySelectorAll(".target-title");
    let previewText = document.querySelectorAll(".preview-text");
    let fixImg = document.querySelectorAll(".fix-img");

    $(cel[0]).click(function() {
        $(fixImg).removeClass("fix-img-active");
        $(fixImg[0]).addClass("fix-img-active");
        $(cel).removeClass("target-active");
        $(this).addClass("target-active");
        $(previewText).removeClass("preview-text-active");
        $(previewText[0]).addClass("preview-text-active");
    });
    $(cel[1]).click(function() {
        $(fixImg).removeClass("fix-img-active");
        $(fixImg[1]).addClass("fix-img-active");
        $(cel).removeClass("target-active");
        $(this).addClass("target-active");
        $(previewText).removeClass("preview-text-active");
        $(previewText[1]).addClass("preview-text-active");
    });
    $(cel[2]).click(function() {
        $(fixImg).removeClass("fix-img-active");
        $(fixImg[2]).addClass("fix-img-active");
        $(cel).removeClass("target-active");
        $(this).addClass("target-active");
        $(previewText).removeClass("preview-text-active");
        $(previewText[2]).addClass("preview-text-active");
    });

    let scheduleBtn = document.querySelectorAll(".top__cub_descr");
    let circleContent = document.querySelectorAll(".svg-background");
    let circleContentSpan = document.querySelectorAll(".circle-content span");
    let targeTitle = document.querySelectorAll(".target-title");

    $(targeTitle).click(function() {
        $(targeTitle).removeClass("target-active");
        $(this).addClass("target-active");
    });
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    console.log(msie);
    if (msie > 0) {} else {}
    $(scheduleBtn[0]).hover(function() {
        $(circleContentSpan).css("color", "#000");
        $(circleContentSpan[0]).css("color", "#fff");
        $(circleContent).css("fill", "#f3f3f2");
        $(circleContent[0]).css("fill", "#d59f57");
    });

    $(scheduleBtn[1]).hover(function() {
        $(circleContentSpan).css("color", "#000");
        $(circleContentSpan[1]).css("color", "#fff");
        $(circleContent).css("fill", "#f3f3f2");
        $(circleContent[1]).css("fill", "#d59f57");
    });

    $(scheduleBtn[2]).hover(function() {
        $(circleContentSpan).css("color", "#000");
        $(circleContentSpan[2]).css("color", "#fff");
        $(circleContent).css("fill", "#f3f3f2");
        $(circleContent[2]).css("fill", "#d59f57");
    });

    $(scheduleBtn).mouseleave(function() {
        $(circleContentSpan).css("color", "#000");
        $(circleContent).css("fill", "#f3f3f2");
    });

    var linkIndex = document.querySelectorAll("a[data-slide]");

    $(linkIndex).click(function() {
        slideIndex = $(this).data("slide");
        localStorage.setItem("index", slideIndex);
        $(".responsive-slider").slick("slickGoTo", slideIndex - 1);
    });
    if (
        localStorage.hasOwnProperty("index") &&
        $("div").is(".responsive-slider") &&
        window.location.hash === "#corporation"
    ) {
        let index = localStorage.getItem("index");
        $(".responsive-slider").slick("slickGoTo", index - 1);
    }

    $(".morePublicBtn").click(function(e) {
        e.preventDefault();
        if ($(window).width() < 576) {
            $(".moreNews-mobile").hide();
            $(".morePublic-mobile").slideToggle();
        } else if ($(window).width() > 577) {
            $(".moreNews-desctop").hide();
            $(".morePublic-desktop").slideToggle();
        }
    });
    $('[href="/information-centre#publications"]').click(function() {
        localStorage.setItem("publicbtn", true);
    });
    $('[href="/information-centre#news"]').click(function() {
        localStorage.setItem("newsbtn", 1);
    });
    $(
        "a:not([href='/information-centre#publications'],[href='/information-centre#news'])"
    ).click(function() {
        if (localStorage.hasOwnProperty("newsbtn")) {
            localStorage.removeItem("newsbtn");
        } else if (localStorage.hasOwnProperty("publicbtn")) {
            localStorage.removeItem("publicbtn");
        }
    });
    $(".moreNewsBtn").click(function(e) {
        e.preventDefault();
        console.log("moreNews");
        if ($(window).width() < 576) {
            $(".morePublic-mobile").hide();
            $(".moreNews-mobile").slideToggle();
        } else if ($(window).width() > 577) {
            $(".morePublic-desktop").hide();
            $(".moreNews-desctop").slideToggle();
        }
    });
    if (
        localStorage.hasOwnProperty("publicbtn") &&
        $("div").is(".morePublic") &&
        window.location.hash === "#publications"
    ) {
        $(".morePublicBtn").click();
    }
    if (
        localStorage.hasOwnProperty("newsbtn") &&
        $("div").is(".moreNews") &&
        window.location.hash === "#news"
    ) {
        $(".moreNewsBtn").click();
    }
    if ($(window).width() < 576) {

        console.log();
        $(".title-collapse").each(function() {
            $(this).click(function() {
                $(this).find('img').addClass('pointer-opacity')
                $(this).next(".collapse-content").slideToggle();
                $(this).next(".investment-descr").slideToggle();
                console.log("title collapse");
            });
        });
    }

    $(".navbar-toggler").click(function() {
        $(".navbar-collapse").slideToggle(0);
    });

    document.addEventListener("touchstart", function(e) {
        if (!$("header").is(e.target) && $("header").has(e.target).length === 0) {
            $(".navbar-collapse").slideUp(0);
        }
    });
    // $(document).click(function (e) {
    //   if (
    //     !$(".navbar-collapse").is(e.target) &&
    //     $(".navbar-collapse").has(e.target).length === 0
    //   ) {
    //     $(".navbar-collapse").slideUp(100);
    //   }
    // });

    $(".inner-slider").slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        swipe: false,
        nextArrow: "<img src='/images/next-inner-slide.png' class='slick-inner-right'>",
        prevArrow: "<img src='/images/prev-inner-slide.png' class='slick-inner-left'>",
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                },
            },
            {
                breakpoint: 740,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    });
});

$(document).ready(function() {
    document.querySelector("body").style.display = "block";
    if (document.querySelector(".about-top.banner-top")) {
        document.querySelector(".about-top.banner-top").style.display = "block";
    } else if (document.querySelector(".investments-top.banner-top")) {
        document.querySelector(".investments-top.banner-top").style.display =
            "block";
    }
    $(".responsive-slider").css("opacity", "1");


});

$(".corp-map-btn").click(function() {
    $("#map-collapse").slideToggle("slow");

    $(".responsive-slider").slick({
        autoplay: false,
        useCSS: false,
    });
});