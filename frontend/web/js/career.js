function getUrl() {
    if (document.getElementsByTagName('html')[0].getAttribute('lang') != 'ru') {
        return `${window.location.origin}/${document.getElementsByTagName('html')[0].getAttribute('lang')}`;
    }
    else {
        return `${window.location.origin}`;
    }
}

$('#career-form').on('submit', function (e) {
    e.preventDefault();
    let form    = new FormData(document.forms.careerForm);
    let file    = document.getElementById('file');
    console.log(1)
    form.append('file', file.files[0]);

    axios.post(`${getUrl()}/career/create-vacancy`, form, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    })
        .then(function (response) {
            if(response.data.status === 1) {
                Swal.fire({
                    icon: 'success',
                    text: response.data.message
                });
                document.getElementById('career-form').reset();
            } else if(response.data.status === 0){
                Swal.fire({
                    icon: 'warning',
                    text: response.data.message
                });
            }
        })
        .catch(function (error) {
            console.log(error)
            Swal.fire({
                icon: 'error',
                text: error.response.data.message
            });
        });
});