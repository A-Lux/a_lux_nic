function getUrl() {
    if (document.getElementsByTagName('html')[0].getAttribute('lang') != 'ru') {
        return `${window.location.origin}/${document.getElementsByTagName('html')[0].getAttribute('lang')}`;
    }
    else {
        return `${window.location.origin}`;
    }
}

function modalDocuments(id)
{
    document.getElementById('downloads-documents').value = id;
    $('#documentModalCenter').modal('show');
}

function downloadDocuments(id)
{
    window.open(`${getUrl()}/procurement/download-documents?id=`  + id, '_blank');
}


$('#procurement-documents-form').on('submit', function (e) {
    e.preventDefault();
    let form    = new FormData(document.forms.procurementForm);

    var id      = $('.procurement-documents').val();
    
    axios.post(`${getUrl()}/procurement/procurement-document`, form)
        .then(function (response) {
            if(response.data.status === 1) {
                // Swal.fire({
                //     icon: 'success',
                //     text: 'Спасибо, Ваши данные отправлены на модерацию!'
                // });
                let docId = document.getElementsByClassName('file-id')[0];
                console.log(docId.value)
                axios.get(`${getUrl()}/procurement/view-documents?id=${docId.value}`)
                    .then (res => {
                        document.getElementById('procurement-documents-form').reset();
                        $('#procurmentModal').modal('hide');
                        $(".modal").on("shown.bs.modal", function () {
                            if ($(".modal-backdrop").length > 1) {
                                $(".modal-backdrop").not(':first').remove();
                            }
                        })
                        $('#modalKonkursDoc').modal('show');
                        $('#modalKonkursDoc').modal({backdrop: 'static', keyboard: false})
                        $('.close-procurement').on('click', function () {
                            $(".modal-backdrop").remove();
                        })
                        let arr = res.data;
                        let modalKonkursBody = document.querySelector('.modalKonkursBody')
                        for(let i = 0; i < arr.length; i++){
                            modalKonkursBody.innerHTML += `
                                <div>
                                    <a class="doc" href="${arr[i].link}" target="_blank">
                                    <div class="doc__type">
                                        <div class="doc__icon">
                                            <i class="fas fa-download down_style"></i>
                                        </div>
                                        <div class="doc__type-title">
                                            DOCX
                                        </div>
                                    </div>
                                    <div class="doc__content w-100">
                                        <div class="doc__title w-100 text-left">
                                            ${arr[i].name}
                                        </div>
                                    </div>
                                    </a>
                                </div>
                            `
                        }
                    })
                
                // window.open('/procurement/download-procurement-documents?id='  + id, '_blank');
                // window.location.href = "/procurement/download-procurement-documents?id=" + id;
            } else if(response.data.status === 0){
                Swal.fire({
                    icon: 'warning',
                    text: response.data.message
                });
            }
        })
        .catch(function (error) {
            console.log(error)
            Swal.fire({
                icon: 'error',
                text: error.response.data.message
            });
        });
});

$('#download-documents-form').on('submit', function (e) {
    e.preventDefault();
    let form    = new FormData(document.forms.downloadForm);

    var document_id = $('#downloads-documents').val();

    axios.post(`${getUrl()}/procurement/procurement-document`, form)
        .then(function (response) {
            if(response.data.status === 1) {
                Swal.fire({
                    icon: 'success',
                    text: 'Спасибо, Ваши данные отправлены на модерацию!'
                });
                window.open(`${getUrl()}/procurement/download-documents?id=`  + document_id, '_blank');
                document.getElementById('download-documents-form').reset();
                // window.location.href = "/procurement/download-documents?id=" + document_id;
            } else if(response.data.status === 0){
                Swal.fire({
                    icon: 'warning',
                    text: response.data.message
                });
            }
        })
        .catch(function (error) {
            // console.log(error)
            Swal.fire({
                icon: 'error',
                text: error.response.data.message
            });
        });
});

