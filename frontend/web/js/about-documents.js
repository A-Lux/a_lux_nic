
function getDocuments(id) {
  axios.get(`${getUrl()}/about/slider-documents?id=${id}`).then((response) => {
    $(".modal").on("shown.bs.modal", function () {
      if ($(".modal-backdrop").length > 1) {
        $(".modal-backdrop").not(":first").remove();
      }
    });
    $("#documents-slider").modal("show");

    let array = response.data.file;
    let path = response.data.path;
    // console.log(response);

    // let title = response.data.title;
    // let documentsTitle = document.querySelector(".documents-title");
    // documentsTitle.innerHTML += `${title}`;

    let documents = document.querySelector(".documents-slider");
    documents.innerHTML = "";
    for (let i = 0; i < array.length; i++) {
      documents.innerHTML += `
                                <div>
                                    <a class="doc" href="${
        path + array[i].file
        }" target="_blank">
                                    <div class="doc__type">
                                        <div class="doc__icon">
                                            <i class="fas fa-download down_style"></i>
                                        </div>
                                        <div class="doc__type-title">
                                            ${array[i].extension}
                                        </div>
                                    </div>
                                    <div class="doc__content w-100">
                                        <div class="doc__title w-100 text-left">
                                            ${array[i].name}
                                        </div>
                                    </div>
                                    </a>
                                </div>
                            `;
    }
  });
}
