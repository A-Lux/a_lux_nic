function getUserView(id) {
    axios.get(`${getUrl()}/about/view-team?id=${id}`).then((response) => {
        $(".modal").on("shown.bs.modal", function () {

            if ($(".modal-backdrop").length > 1) {
                $(".modal-backdrop").not(":first").remove();
            }
        });

        $("#mobile-team-modal").modal("show");


        let json = response.data;

        let username    = document.querySelector(".modal-username-mobile");
        let position    = document.querySelector(".modal-position-mobile");
        let image       = document.querySelector(".modal-image-mobile");
        let content     = document.querySelector(".modal-content-mobile");

        username.innerHTML  = json.username;
        position.innerHTML  = json.position;
        image.src           = json.image;
        content.innerHTML   = json.content;


    });
}
