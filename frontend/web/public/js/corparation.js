!(function (e, t) {
  "object" == typeof module && "object" == typeof module.exports
    ? (module.exports = e.document
        ? t(e, !0)
        : function (e) {
            if (!e.document)
              throw new Error("jQuery requires a window with a document");
            return t(e);
          })
    : t(e);
})("undefined" != typeof window ? window : this, function (e, t) {
  var n = [],
    i = e.document,
    o = n.slice,
    r = n.concat,
    s = n.push,
    a = n.indexOf,
    l = {},
    c = l.toString,
    u = l.hasOwnProperty,
    d = {},
    p = "2.2.4",
    f = function (e, t) {
      return new f.fn.init(e, t);
    },
    h = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
    g = /^-ms-/,
    m = /-([\da-z])/gi,
    v = function (e, t) {
      return t.toUpperCase();
    };
  function y(e) {
    var t = !!e && "length" in e && e.length,
      n = f.type(e);
    return (
      "function" !== n &&
      !f.isWindow(e) &&
      ("array" === n ||
        0 === t ||
        ("number" == typeof t && t > 0 && t - 1 in e))
    );
  }
  (f.fn = f.prototype = {
    jquery: p,
    constructor: f,
    selector: "",
    length: 0,
    toArray: function () {
      return o.call(this);
    },
    get: function (e) {
      return null != e
        ? 0 > e
          ? this[e + this.length]
          : this[e]
        : o.call(this);
    },
    pushStack: function (e) {
      var t = f.merge(this.constructor(), e);
      return (t.prevObject = this), (t.context = this.context), t;
    },
    each: function (e) {
      return f.each(this, e);
    },
    map: function (e) {
      return this.pushStack(
        f.map(this, function (t, n) {
          return e.call(t, n, t);
        })
      );
    },
    slice: function () {
      return this.pushStack(o.apply(this, arguments));
    },
    first: function () {
      return this.eq(0);
    },
    last: function () {
      return this.eq(-1);
    },
    eq: function (e) {
      var t = this.length,
        n = +e + (0 > e ? t : 0);
      return this.pushStack(n >= 0 && t > n ? [this[n]] : []);
    },
    end: function () {
      return this.prevObject || this.constructor();
    },
    push: s,
    sort: n.sort,
    splice: n.splice,
  }),
    (f.extend = f.fn.extend = function () {
      var e,
        t,
        n,
        i,
        o,
        r,
        s = arguments[0] || {},
        a = 1,
        l = arguments.length,
        c = !1;
      for (
        "boolean" == typeof s && ((c = s), (s = arguments[a] || {}), a++),
          "object" == typeof s || f.isFunction(s) || (s = {}),
          a === l && ((s = this), a--);
        l > a;
        a++
      )
        if (null != (e = arguments[a]))
          for (t in e)
            (n = s[t]),
              s !== (i = e[t]) &&
                (c && i && (f.isPlainObject(i) || (o = f.isArray(i)))
                  ? (o
                      ? ((o = !1), (r = n && f.isArray(n) ? n : []))
                      : (r = n && f.isPlainObject(n) ? n : {}),
                    (s[t] = f.extend(c, r, i)))
                  : void 0 !== i && (s[t] = i));
      return s;
    }),
    f.extend({
      expando: "jQuery" + (p + Math.random()).replace(/\D/g, ""),
      isReady: !0,
      error: function (e) {
        throw new Error(e);
      },
      noop: function () {},
      isFunction: function (e) {
        return "function" === f.type(e);
      },
      isArray: Array.isArray,
      isWindow: function (e) {
        return null != e && e === e.window;
      },
      isNumeric: function (e) {
        var t = e && e.toString();
        return !f.isArray(e) && t - parseFloat(t) + 1 >= 0;
      },
      isPlainObject: function (e) {
        var t;
        if ("object" !== f.type(e) || e.nodeType || f.isWindow(e)) return !1;
        if (
          e.constructor &&
          !u.call(e, "constructor") &&
          !u.call(e.constructor.prototype || {}, "isPrototypeOf")
        )
          return !1;
        for (t in e);
        return void 0 === t || u.call(e, t);
      },
      isEmptyObject: function (e) {
        var t;
        for (t in e) return !1;
        return !0;
      },
      type: function (e) {
        return null == e
          ? e + ""
          : "object" == typeof e || "function" == typeof e
          ? l[c.call(e)] || "object"
          : typeof e;
      },
      globalEval: function (e) {
        var t,
          n = eval;
        (e = f.trim(e)) &&
          (1 === e.indexOf("use strict")
            ? (((t = i.createElement("script")).text = e),
              i.head.appendChild(t).parentNode.removeChild(t))
            : n(e));
      },
      camelCase: function (e) {
        return e.replace(g, "ms-").replace(m, v);
      },
      nodeName: function (e, t) {
        return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase();
      },
      each: function (e, t) {
        var n,
          i = 0;
        if (y(e))
          for (n = e.length; n > i && !1 !== t.call(e[i], i, e[i]); i++);
        else for (i in e) if (!1 === t.call(e[i], i, e[i])) break;
        return e;
      },
      trim: function (e) {
        return null == e ? "" : (e + "").replace(h, "");
      },
      makeArray: function (e, t) {
        var n = t || [];
        return (
          null != e &&
            (y(Object(e))
              ? f.merge(n, "string" == typeof e ? [e] : e)
              : s.call(n, e)),
          n
        );
      },
      inArray: function (e, t, n) {
        return null == t ? -1 : a.call(t, e, n);
      },
      merge: function (e, t) {
        for (var n = +t.length, i = 0, o = e.length; n > i; i++) e[o++] = t[i];
        return (e.length = o), e;
      },
      grep: function (e, t, n) {
        for (var i = [], o = 0, r = e.length, s = !n; r > o; o++)
          !t(e[o], o) !== s && i.push(e[o]);
        return i;
      },
      map: function (e, t, n) {
        var i,
          o,
          s = 0,
          a = [];
        if (y(e))
          for (i = e.length; i > s; s++)
            null != (o = t(e[s], s, n)) && a.push(o);
        else for (s in e) null != (o = t(e[s], s, n)) && a.push(o);
        return r.apply([], a);
      },
      guid: 1,
      proxy: function (e, t) {
        var n, i, r;
        return (
          "string" == typeof t && ((n = e[t]), (t = e), (e = n)),
          f.isFunction(e)
            ? ((i = o.call(arguments, 2)),
              ((r = function () {
                return e.apply(t || this, i.concat(o.call(arguments)));
              }).guid = e.guid = e.guid || f.guid++),
              r)
            : void 0
        );
      },
      now: Date.now,
      support: d,
    }),
    "function" == typeof Symbol && (f.fn[Symbol.iterator] = n[Symbol.iterator]),
    f.each(
      "Boolean Number String Function Array Date RegExp Object Error Symbol".split(
        " "
      ),
      function (e, t) {
        l["[object " + t + "]"] = t.toLowerCase();
      }
    );
  var b = (function (e) {
    var t,
      n,
      i,
      o,
      r,
      s,
      a,
      l,
      c,
      u,
      d,
      p,
      f,
      h,
      g,
      m,
      v,
      y,
      b,
      _ = "sizzle" + 1 * new Date(),
      w = e.document,
      T = 0,
      C = 0,
      S = re(),
      k = re(),
      x = re(),
      E = function (e, t) {
        return e === t && (d = !0), 0;
      },
      A = 1 << 31,
      D = {}.hasOwnProperty,
      N = [],
      I = N.pop,
      O = N.push,
      $ = N.push,
      j = N.slice,
      L = function (e, t) {
        for (var n = 0, i = e.length; i > n; n++) if (e[n] === t) return n;
        return -1;
      },
      P =
        "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
      H = "[\\x20\\t\\r\\n\\f]",
      q = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
      R =
        "\\[" +
        H +
        "*(" +
        q +
        ")(?:" +
        H +
        "*([*^$|!~]?=)" +
        H +
        "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" +
        q +
        "))|)" +
        H +
        "*\\]",
      M =
        ":(" +
        q +
        ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" +
        R +
        ")*)|.*)\\)|)",
      F = new RegExp(H + "+", "g"),
      W = new RegExp("^" + H + "+|((?:^|[^\\\\])(?:\\\\.)*)" + H + "+$", "g"),
      z = new RegExp("^" + H + "*," + H + "*"),
      B = new RegExp("^" + H + "*([>+~]|" + H + ")" + H + "*"),
      U = new RegExp("=" + H + "*([^\\]'\"]*?)" + H + "*\\]", "g"),
      X = new RegExp(M),
      Q = new RegExp("^" + q + "$"),
      K = {
        ID: new RegExp("^#(" + q + ")"),
        CLASS: new RegExp("^\\.(" + q + ")"),
        TAG: new RegExp("^(" + q + "|[*])"),
        ATTR: new RegExp("^" + R),
        PSEUDO: new RegExp("^" + M),
        CHILD: new RegExp(
          "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" +
            H +
            "*(even|odd|(([+-]|)(\\d*)n|)" +
            H +
            "*(?:([+-]|)" +
            H +
            "*(\\d+)|))" +
            H +
            "*\\)|)",
          "i"
        ),
        bool: new RegExp("^(?:" + P + ")$", "i"),
        needsContext: new RegExp(
          "^" +
            H +
            "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
            H +
            "*((?:-\\d)?\\d*)" +
            H +
            "*\\)|)(?=[^-]|$)",
          "i"
        ),
      },
      Y = /^(?:input|select|textarea|button)$/i,
      V = /^h\d$/i,
      G = /^[^{]+\{\s*\[native \w/,
      J = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
      Z = /[+~]/,
      ee = /'|\\/g,
      te = new RegExp("\\\\([\\da-f]{1,6}" + H + "?|(" + H + ")|.)", "ig"),
      ne = function (e, t, n) {
        var i = "0x" + t - 65536;
        return i != i || n
          ? t
          : 0 > i
          ? String.fromCharCode(i + 65536)
          : String.fromCharCode((i >> 10) | 55296, (1023 & i) | 56320);
      },
      ie = function () {
        p();
      };
    try {
      $.apply((N = j.call(w.childNodes)), w.childNodes),
        N[w.childNodes.length].nodeType;
    } catch (e) {
      $ = {
        apply: N.length
          ? function (e, t) {
              O.apply(e, j.call(t));
            }
          : function (e, t) {
              for (var n = e.length, i = 0; (e[n++] = t[i++]); );
              e.length = n - 1;
            },
      };
    }
    function oe(e, t, i, o) {
      var r,
        a,
        c,
        u,
        d,
        h,
        v,
        y,
        T = t && t.ownerDocument,
        C = t ? t.nodeType : 9;
      if (
        ((i = i || []),
        "string" != typeof e || !e || (1 !== C && 9 !== C && 11 !== C))
      )
        return i;
      if (
        !o &&
        ((t ? t.ownerDocument || t : w) !== f && p(t), (t = t || f), g)
      ) {
        if (11 !== C && (h = J.exec(e)))
          if ((r = h[1])) {
            if (9 === C) {
              if (!(c = t.getElementById(r))) return i;
              if (c.id === r) return i.push(c), i;
            } else if (T && (c = T.getElementById(r)) && b(t, c) && c.id === r)
              return i.push(c), i;
          } else {
            if (h[2]) return $.apply(i, t.getElementsByTagName(e)), i;
            if (
              (r = h[3]) &&
              n.getElementsByClassName &&
              t.getElementsByClassName
            )
              return $.apply(i, t.getElementsByClassName(r)), i;
          }
        if (n.qsa && !x[e + " "] && (!m || !m.test(e))) {
          if (1 !== C) (T = t), (y = e);
          else if ("object" !== t.nodeName.toLowerCase()) {
            for (
              (u = t.getAttribute("id"))
                ? (u = u.replace(ee, "\\$&"))
                : t.setAttribute("id", (u = _)),
                a = (v = s(e)).length,
                d = Q.test(u) ? "#" + u : "[id='" + u + "']";
              a--;

            )
              v[a] = d + " " + ge(v[a]);
            (y = v.join(",")), (T = (Z.test(e) && fe(t.parentNode)) || t);
          }
          if (y)
            try {
              return $.apply(i, T.querySelectorAll(y)), i;
            } catch (e) {
            } finally {
              u === _ && t.removeAttribute("id");
            }
        }
      }
      return l(e.replace(W, "$1"), t, i, o);
    }
    function re() {
      var e = [];
      return function t(n, o) {
        return (
          e.push(n + " ") > i.cacheLength && delete t[e.shift()],
          (t[n + " "] = o)
        );
      };
    }
    function se(e) {
      return (e[_] = !0), e;
    }
    function ae(e) {
      var t = f.createElement("div");
      try {
        return !!e(t);
      } catch (e) {
        return !1;
      } finally {
        t.parentNode && t.parentNode.removeChild(t), (t = null);
      }
    }
    function le(e, t) {
      for (var n = e.split("|"), o = n.length; o--; ) i.attrHandle[n[o]] = t;
    }
    function ce(e, t) {
      var n = t && e,
        i =
          n &&
          1 === e.nodeType &&
          1 === t.nodeType &&
          (~t.sourceIndex || A) - (~e.sourceIndex || A);
      if (i) return i;
      if (n) for (; (n = n.nextSibling); ) if (n === t) return -1;
      return e ? 1 : -1;
    }
    function ue(e) {
      return function (t) {
        return "input" === t.nodeName.toLowerCase() && t.type === e;
      };
    }
    function de(e) {
      return function (t) {
        var n = t.nodeName.toLowerCase();
        return ("input" === n || "button" === n) && t.type === e;
      };
    }
    function pe(e) {
      return se(function (t) {
        return (
          (t = +t),
          se(function (n, i) {
            for (var o, r = e([], n.length, t), s = r.length; s--; )
              n[(o = r[s])] && (n[o] = !(i[o] = n[o]));
          })
        );
      });
    }
    function fe(e) {
      return e && void 0 !== e.getElementsByTagName && e;
    }
    for (t in ((n = oe.support = {}),
    (r = oe.isXML = function (e) {
      var t = e && (e.ownerDocument || e).documentElement;
      return !!t && "HTML" !== t.nodeName;
    }),
    (p = oe.setDocument = function (e) {
      var t,
        o,
        s = e ? e.ownerDocument || e : w;
      return s !== f && 9 === s.nodeType && s.documentElement
        ? ((h = (f = s).documentElement),
          (g = !r(f)),
          (o = f.defaultView) &&
            o.top !== o &&
            (o.addEventListener
              ? o.addEventListener("unload", ie, !1)
              : o.attachEvent && o.attachEvent("onunload", ie)),
          (n.attributes = ae(function (e) {
            return (e.className = "i"), !e.getAttribute("className");
          })),
          (n.getElementsByTagName = ae(function (e) {
            return (
              e.appendChild(f.createComment("")),
              !e.getElementsByTagName("*").length
            );
          })),
          (n.getElementsByClassName = G.test(f.getElementsByClassName)),
          (n.getById = ae(function (e) {
            return (
              (h.appendChild(e).id = _),
              !f.getElementsByName || !f.getElementsByName(_).length
            );
          })),
          n.getById
            ? ((i.find.ID = function (e, t) {
                if (void 0 !== t.getElementById && g) {
                  var n = t.getElementById(e);
                  return n ? [n] : [];
                }
              }),
              (i.filter.ID = function (e) {
                var t = e.replace(te, ne);
                return function (e) {
                  return e.getAttribute("id") === t;
                };
              }))
            : (delete i.find.ID,
              (i.filter.ID = function (e) {
                var t = e.replace(te, ne);
                return function (e) {
                  var n =
                    void 0 !== e.getAttributeNode && e.getAttributeNode("id");
                  return n && n.value === t;
                };
              })),
          (i.find.TAG = n.getElementsByTagName
            ? function (e, t) {
                return void 0 !== t.getElementsByTagName
                  ? t.getElementsByTagName(e)
                  : n.qsa
                  ? t.querySelectorAll(e)
                  : void 0;
              }
            : function (e, t) {
                var n,
                  i = [],
                  o = 0,
                  r = t.getElementsByTagName(e);
                if ("*" === e) {
                  for (; (n = r[o++]); ) 1 === n.nodeType && i.push(n);
                  return i;
                }
                return r;
              }),
          (i.find.CLASS =
            n.getElementsByClassName &&
            function (e, t) {
              return void 0 !== t.getElementsByClassName && g
                ? t.getElementsByClassName(e)
                : void 0;
            }),
          (v = []),
          (m = []),
          (n.qsa = G.test(f.querySelectorAll)) &&
            (ae(function (e) {
              (h.appendChild(e).innerHTML =
                "<a id='" +
                _ +
                "'></a><select id='" +
                _ +
                "-\r\\' msallowcapture=''><option selected=''></option></select>"),
                e.querySelectorAll("[msallowcapture^='']").length &&
                  m.push("[*^$]=" + H + "*(?:''|\"\")"),
                e.querySelectorAll("[selected]").length ||
                  m.push("\\[" + H + "*(?:value|" + P + ")"),
                e.querySelectorAll("[id~=" + _ + "-]").length || m.push("~="),
                e.querySelectorAll(":checked").length || m.push(":checked"),
                e.querySelectorAll("a#" + _ + "+*").length ||
                  m.push(".#.+[+~]");
            }),
            ae(function (e) {
              var t = f.createElement("input");
              t.setAttribute("type", "hidden"),
                e.appendChild(t).setAttribute("name", "D"),
                e.querySelectorAll("[name=d]").length &&
                  m.push("name" + H + "*[*^$|!~]?="),
                e.querySelectorAll(":enabled").length ||
                  m.push(":enabled", ":disabled"),
                e.querySelectorAll("*,:x"),
                m.push(",.*:");
            })),
          (n.matchesSelector = G.test(
            (y =
              h.matches ||
              h.webkitMatchesSelector ||
              h.mozMatchesSelector ||
              h.oMatchesSelector ||
              h.msMatchesSelector)
          )) &&
            ae(function (e) {
              (n.disconnectedMatch = y.call(e, "div")),
                y.call(e, "[s!='']:x"),
                v.push("!=", M);
            }),
          (m = m.length && new RegExp(m.join("|"))),
          (v = v.length && new RegExp(v.join("|"))),
          (t = G.test(h.compareDocumentPosition)),
          (b =
            t || G.test(h.contains)
              ? function (e, t) {
                  var n = 9 === e.nodeType ? e.documentElement : e,
                    i = t && t.parentNode;
                  return (
                    e === i ||
                    !(
                      !i ||
                      1 !== i.nodeType ||
                      !(n.contains
                        ? n.contains(i)
                        : e.compareDocumentPosition &&
                          16 & e.compareDocumentPosition(i))
                    )
                  );
                }
              : function (e, t) {
                  if (t) for (; (t = t.parentNode); ) if (t === e) return !0;
                  return !1;
                }),
          (E = t
            ? function (e, t) {
                if (e === t) return (d = !0), 0;
                var i = !e.compareDocumentPosition - !t.compareDocumentPosition;
                return (
                  i ||
                  (1 &
                    (i =
                      (e.ownerDocument || e) === (t.ownerDocument || t)
                        ? e.compareDocumentPosition(t)
                        : 1) ||
                  (!n.sortDetached && t.compareDocumentPosition(e) === i)
                    ? e === f || (e.ownerDocument === w && b(w, e))
                      ? -1
                      : t === f || (t.ownerDocument === w && b(w, t))
                      ? 1
                      : u
                      ? L(u, e) - L(u, t)
                      : 0
                    : 4 & i
                    ? -1
                    : 1)
                );
              }
            : function (e, t) {
                if (e === t) return (d = !0), 0;
                var n,
                  i = 0,
                  o = e.parentNode,
                  r = t.parentNode,
                  s = [e],
                  a = [t];
                if (!o || !r)
                  return e === f
                    ? -1
                    : t === f
                    ? 1
                    : o
                    ? -1
                    : r
                    ? 1
                    : u
                    ? L(u, e) - L(u, t)
                    : 0;
                if (o === r) return ce(e, t);
                for (n = e; (n = n.parentNode); ) s.unshift(n);
                for (n = t; (n = n.parentNode); ) a.unshift(n);
                for (; s[i] === a[i]; ) i++;
                return i
                  ? ce(s[i], a[i])
                  : s[i] === w
                  ? -1
                  : a[i] === w
                  ? 1
                  : 0;
              }),
          f)
        : f;
    }),
    (oe.matches = function (e, t) {
      return oe(e, null, null, t);
    }),
    (oe.matchesSelector = function (e, t) {
      if (
        ((e.ownerDocument || e) !== f && p(e),
        (t = t.replace(U, "='$1']")),
        n.matchesSelector &&
          g &&
          !x[t + " "] &&
          (!v || !v.test(t)) &&
          (!m || !m.test(t)))
      )
        try {
          var i = y.call(e, t);
          if (
            i ||
            n.disconnectedMatch ||
            (e.document && 11 !== e.document.nodeType)
          )
            return i;
        } catch (e) {}
      return oe(t, f, null, [e]).length > 0;
    }),
    (oe.contains = function (e, t) {
      return (e.ownerDocument || e) !== f && p(e), b(e, t);
    }),
    (oe.attr = function (e, t) {
      (e.ownerDocument || e) !== f && p(e);
      var o = i.attrHandle[t.toLowerCase()],
        r = o && D.call(i.attrHandle, t.toLowerCase()) ? o(e, t, !g) : void 0;
      return void 0 !== r
        ? r
        : n.attributes || !g
        ? e.getAttribute(t)
        : (r = e.getAttributeNode(t)) && r.specified
        ? r.value
        : null;
    }),
    (oe.error = function (e) {
      throw new Error("Syntax error, unrecognized expression: " + e);
    }),
    (oe.uniqueSort = function (e) {
      var t,
        i = [],
        o = 0,
        r = 0;
      if (
        ((d = !n.detectDuplicates),
        (u = !n.sortStable && e.slice(0)),
        e.sort(E),
        d)
      ) {
        for (; (t = e[r++]); ) t === e[r] && (o = i.push(r));
        for (; o--; ) e.splice(i[o], 1);
      }
      return (u = null), e;
    }),
    (o = oe.getText = function (e) {
      var t,
        n = "",
        i = 0,
        r = e.nodeType;
      if (r) {
        if (1 === r || 9 === r || 11 === r) {
          if ("string" == typeof e.textContent) return e.textContent;
          for (e = e.firstChild; e; e = e.nextSibling) n += o(e);
        } else if (3 === r || 4 === r) return e.nodeValue;
      } else for (; (t = e[i++]); ) n += o(t);
      return n;
    }),
    ((i = oe.selectors = {
      cacheLength: 50,
      createPseudo: se,
      match: K,
      attrHandle: {},
      find: {},
      relative: {
        ">": { dir: "parentNode", first: !0 },
        " ": { dir: "parentNode" },
        "+": { dir: "previousSibling", first: !0 },
        "~": { dir: "previousSibling" },
      },
      preFilter: {
        ATTR: function (e) {
          return (
            (e[1] = e[1].replace(te, ne)),
            (e[3] = (e[3] || e[4] || e[5] || "").replace(te, ne)),
            "~=" === e[2] && (e[3] = " " + e[3] + " "),
            e.slice(0, 4)
          );
        },
        CHILD: function (e) {
          return (
            (e[1] = e[1].toLowerCase()),
            "nth" === e[1].slice(0, 3)
              ? (e[3] || oe.error(e[0]),
                (e[4] = +(e[4]
                  ? e[5] + (e[6] || 1)
                  : 2 * ("even" === e[3] || "odd" === e[3]))),
                (e[5] = +(e[7] + e[8] || "odd" === e[3])))
              : e[3] && oe.error(e[0]),
            e
          );
        },
        PSEUDO: function (e) {
          var t,
            n = !e[6] && e[2];
          return K.CHILD.test(e[0])
            ? null
            : (e[3]
                ? (e[2] = e[4] || e[5] || "")
                : n &&
                  X.test(n) &&
                  (t = s(n, !0)) &&
                  (t = n.indexOf(")", n.length - t) - n.length) &&
                  ((e[0] = e[0].slice(0, t)), (e[2] = n.slice(0, t))),
              e.slice(0, 3));
        },
      },
      filter: {
        TAG: function (e) {
          var t = e.replace(te, ne).toLowerCase();
          return "*" === e
            ? function () {
                return !0;
              }
            : function (e) {
                return e.nodeName && e.nodeName.toLowerCase() === t;
              };
        },
        CLASS: function (e) {
          var t = S[e + " "];
          return (
            t ||
            ((t = new RegExp("(^|" + H + ")" + e + "(" + H + "|$)")) &&
              S(e, function (e) {
                return t.test(
                  ("string" == typeof e.className && e.className) ||
                    (void 0 !== e.getAttribute && e.getAttribute("class")) ||
                    ""
                );
              }))
          );
        },
        ATTR: function (e, t, n) {
          return function (i) {
            var o = oe.attr(i, e);
            return null == o
              ? "!=" === t
              : !t ||
                  ((o += ""),
                  "=" === t
                    ? o === n
                    : "!=" === t
                    ? o !== n
                    : "^=" === t
                    ? n && 0 === o.indexOf(n)
                    : "*=" === t
                    ? n && o.indexOf(n) > -1
                    : "$=" === t
                    ? n && o.slice(-n.length) === n
                    : "~=" === t
                    ? (" " + o.replace(F, " ") + " ").indexOf(n) > -1
                    : "|=" === t &&
                      (o === n || o.slice(0, n.length + 1) === n + "-"));
          };
        },
        CHILD: function (e, t, n, i, o) {
          var r = "nth" !== e.slice(0, 3),
            s = "last" !== e.slice(-4),
            a = "of-type" === t;
          return 1 === i && 0 === o
            ? function (e) {
                return !!e.parentNode;
              }
            : function (t, n, l) {
                var c,
                  u,
                  d,
                  p,
                  f,
                  h,
                  g = r !== s ? "nextSibling" : "previousSibling",
                  m = t.parentNode,
                  v = a && t.nodeName.toLowerCase(),
                  y = !l && !a,
                  b = !1;
                if (m) {
                  if (r) {
                    for (; g; ) {
                      for (p = t; (p = p[g]); )
                        if (
                          a ? p.nodeName.toLowerCase() === v : 1 === p.nodeType
                        )
                          return !1;
                      h = g = "only" === e && !h && "nextSibling";
                    }
                    return !0;
                  }
                  if (((h = [s ? m.firstChild : m.lastChild]), s && y)) {
                    for (
                      b =
                        (f =
                          (c =
                            (u =
                              (d = (p = m)[_] || (p[_] = {}))[p.uniqueID] ||
                              (d[p.uniqueID] = {}))[e] || [])[0] === T &&
                          c[1]) && c[2],
                        p = f && m.childNodes[f];
                      (p = (++f && p && p[g]) || (b = f = 0) || h.pop());

                    )
                      if (1 === p.nodeType && ++b && p === t) {
                        u[e] = [T, f, b];
                        break;
                      }
                  } else if (
                    (y &&
                      (b = f =
                        (c =
                          (u =
                            (d = (p = t)[_] || (p[_] = {}))[p.uniqueID] ||
                            (d[p.uniqueID] = {}))[e] || [])[0] === T && c[1]),
                    !1 === b)
                  )
                    for (
                      ;
                      (p = (++f && p && p[g]) || (b = f = 0) || h.pop()) &&
                      ((a
                        ? p.nodeName.toLowerCase() !== v
                        : 1 !== p.nodeType) ||
                        !++b ||
                        (y &&
                          ((u =
                            (d = p[_] || (p[_] = {}))[p.uniqueID] ||
                            (d[p.uniqueID] = {}))[e] = [T, b]),
                        p !== t));

                    );
                  return (b -= o) === i || (b % i == 0 && b / i >= 0);
                }
              };
        },
        PSEUDO: function (e, t) {
          var n,
            o =
              i.pseudos[e] ||
              i.setFilters[e.toLowerCase()] ||
              oe.error("unsupported pseudo: " + e);
          return o[_]
            ? o(t)
            : o.length > 1
            ? ((n = [e, e, "", t]),
              i.setFilters.hasOwnProperty(e.toLowerCase())
                ? se(function (e, n) {
                    for (var i, r = o(e, t), s = r.length; s--; )
                      e[(i = L(e, r[s]))] = !(n[i] = r[s]);
                  })
                : function (e) {
                    return o(e, 0, n);
                  })
            : o;
        },
      },
      pseudos: {
        not: se(function (e) {
          var t = [],
            n = [],
            i = a(e.replace(W, "$1"));
          return i[_]
            ? se(function (e, t, n, o) {
                for (var r, s = i(e, null, o, []), a = e.length; a--; )
                  (r = s[a]) && (e[a] = !(t[a] = r));
              })
            : function (e, o, r) {
                return (t[0] = e), i(t, null, r, n), (t[0] = null), !n.pop();
              };
        }),
        has: se(function (e) {
          return function (t) {
            return oe(e, t).length > 0;
          };
        }),
        contains: se(function (e) {
          return (
            (e = e.replace(te, ne)),
            function (t) {
              return (t.textContent || t.innerText || o(t)).indexOf(e) > -1;
            }
          );
        }),
        lang: se(function (e) {
          return (
            Q.test(e || "") || oe.error("unsupported lang: " + e),
            (e = e.replace(te, ne).toLowerCase()),
            function (t) {
              var n;
              do {
                if (
                  (n = g
                    ? t.lang
                    : t.getAttribute("xml:lang") || t.getAttribute("lang"))
                )
                  return (
                    (n = n.toLowerCase()) === e || 0 === n.indexOf(e + "-")
                  );
              } while ((t = t.parentNode) && 1 === t.nodeType);
              return !1;
            }
          );
        }),
        target: function (t) {
          var n = e.location && e.location.hash;
          return n && n.slice(1) === t.id;
        },
        root: function (e) {
          return e === h;
        },
        focus: function (e) {
          return (
            e === f.activeElement &&
            (!f.hasFocus || f.hasFocus()) &&
            !!(e.type || e.href || ~e.tabIndex)
          );
        },
        enabled: function (e) {
          return !1 === e.disabled;
        },
        disabled: function (e) {
          return !0 === e.disabled;
        },
        checked: function (e) {
          var t = e.nodeName.toLowerCase();
          return (
            ("input" === t && !!e.checked) || ("option" === t && !!e.selected)
          );
        },
        selected: function (e) {
          return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected;
        },
        empty: function (e) {
          for (e = e.firstChild; e; e = e.nextSibling)
            if (e.nodeType < 6) return !1;
          return !0;
        },
        parent: function (e) {
          return !i.pseudos.empty(e);
        },
        header: function (e) {
          return V.test(e.nodeName);
        },
        input: function (e) {
          return Y.test(e.nodeName);
        },
        button: function (e) {
          var t = e.nodeName.toLowerCase();
          return ("input" === t && "button" === e.type) || "button" === t;
        },
        text: function (e) {
          var t;
          return (
            "input" === e.nodeName.toLowerCase() &&
            "text" === e.type &&
            (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
          );
        },
        first: pe(function () {
          return [0];
        }),
        last: pe(function (e, t) {
          return [t - 1];
        }),
        eq: pe(function (e, t, n) {
          return [0 > n ? n + t : n];
        }),
        even: pe(function (e, t) {
          for (var n = 0; t > n; n += 2) e.push(n);
          return e;
        }),
        odd: pe(function (e, t) {
          for (var n = 1; t > n; n += 2) e.push(n);
          return e;
        }),
        lt: pe(function (e, t, n) {
          for (var i = 0 > n ? n + t : n; --i >= 0; ) e.push(i);
          return e;
        }),
        gt: pe(function (e, t, n) {
          for (var i = 0 > n ? n + t : n; ++i < t; ) e.push(i);
          return e;
        }),
      },
    }).pseudos.nth = i.pseudos.eq),
    { radio: !0, checkbox: !0, file: !0, password: !0, image: !0 }))
      i.pseudos[t] = ue(t);
    for (t in { submit: !0, reset: !0 }) i.pseudos[t] = de(t);
    function he() {}
    function ge(e) {
      for (var t = 0, n = e.length, i = ""; n > t; t++) i += e[t].value;
      return i;
    }
    function me(e, t, n) {
      var i = t.dir,
        o = n && "parentNode" === i,
        r = C++;
      return t.first
        ? function (t, n, r) {
            for (; (t = t[i]); ) if (1 === t.nodeType || o) return e(t, n, r);
          }
        : function (t, n, s) {
            var a,
              l,
              c,
              u = [T, r];
            if (s) {
              for (; (t = t[i]); )
                if ((1 === t.nodeType || o) && e(t, n, s)) return !0;
            } else
              for (; (t = t[i]); )
                if (1 === t.nodeType || o) {
                  if (
                    (a = (l =
                      (c = t[_] || (t[_] = {}))[t.uniqueID] ||
                      (c[t.uniqueID] = {}))[i]) &&
                    a[0] === T &&
                    a[1] === r
                  )
                    return (u[2] = a[2]);
                  if (((l[i] = u), (u[2] = e(t, n, s)))) return !0;
                }
          };
    }
    function ve(e) {
      return e.length > 1
        ? function (t, n, i) {
            for (var o = e.length; o--; ) if (!e[o](t, n, i)) return !1;
            return !0;
          }
        : e[0];
    }
    function ye(e, t, n, i, o) {
      for (var r, s = [], a = 0, l = e.length, c = null != t; l > a; a++)
        (r = e[a]) && ((n && !n(r, i, o)) || (s.push(r), c && t.push(a)));
      return s;
    }
    function be(e, t, n, i, o, r) {
      return (
        i && !i[_] && (i = be(i)),
        o && !o[_] && (o = be(o, r)),
        se(function (r, s, a, l) {
          var c,
            u,
            d,
            p = [],
            f = [],
            h = s.length,
            g =
              r ||
              (function (e, t, n) {
                for (var i = 0, o = t.length; o > i; i++) oe(e, t[i], n);
                return n;
              })(t || "*", a.nodeType ? [a] : a, []),
            m = !e || (!r && t) ? g : ye(g, p, e, a, l),
            v = n ? (o || (r ? e : h || i) ? [] : s) : m;
          if ((n && n(m, v, a, l), i))
            for (c = ye(v, f), i(c, [], a, l), u = c.length; u--; )
              (d = c[u]) && (v[f[u]] = !(m[f[u]] = d));
          if (r) {
            if (o || e) {
              if (o) {
                for (c = [], u = v.length; u--; )
                  (d = v[u]) && c.push((m[u] = d));
                o(null, (v = []), c, l);
              }
              for (u = v.length; u--; )
                (d = v[u]) &&
                  (c = o ? L(r, d) : p[u]) > -1 &&
                  (r[c] = !(s[c] = d));
            }
          } else (v = ye(v === s ? v.splice(h, v.length) : v)), o ? o(null, s, v, l) : $.apply(s, v);
        })
      );
    }
    function _e(e) {
      for (
        var t,
          n,
          o,
          r = e.length,
          s = i.relative[e[0].type],
          a = s || i.relative[" "],
          l = s ? 1 : 0,
          u = me(
            function (e) {
              return e === t;
            },
            a,
            !0
          ),
          d = me(
            function (e) {
              return L(t, e) > -1;
            },
            a,
            !0
          ),
          p = [
            function (e, n, i) {
              var o =
                (!s && (i || n !== c)) ||
                ((t = n).nodeType ? u(e, n, i) : d(e, n, i));
              return (t = null), o;
            },
          ];
        r > l;
        l++
      )
        if ((n = i.relative[e[l].type])) p = [me(ve(p), n)];
        else {
          if ((n = i.filter[e[l].type].apply(null, e[l].matches))[_]) {
            for (o = ++l; r > o && !i.relative[e[o].type]; o++);
            return be(
              l > 1 && ve(p),
              l > 1 &&
                ge(
                  e
                    .slice(0, l - 1)
                    .concat({ value: " " === e[l - 2].type ? "*" : "" })
                ).replace(W, "$1"),
              n,
              o > l && _e(e.slice(l, o)),
              r > o && _e((e = e.slice(o))),
              r > o && ge(e)
            );
          }
          p.push(n);
        }
      return ve(p);
    }
    function we(e, t) {
      var n = t.length > 0,
        o = e.length > 0,
        r = function (r, s, a, l, u) {
          var d,
            h,
            m,
            v = 0,
            y = "0",
            b = r && [],
            _ = [],
            w = c,
            C = r || (o && i.find.TAG("*", u)),
            S = (T += null == w ? 1 : Math.random() || 0.1),
            k = C.length;
          for (
            u && (c = s === f || s || u);
            y !== k && null != (d = C[y]);
            y++
          ) {
            if (o && d) {
              for (
                h = 0, s || d.ownerDocument === f || (p(d), (a = !g));
                (m = e[h++]);

              )
                if (m(d, s || f, a)) {
                  l.push(d);
                  break;
                }
              u && (T = S);
            }
            n && ((d = !m && d) && v--, r && b.push(d));
          }
          if (((v += y), n && y !== v)) {
            for (h = 0; (m = t[h++]); ) m(b, _, s, a);
            if (r) {
              if (v > 0) for (; y--; ) b[y] || _[y] || (_[y] = I.call(l));
              _ = ye(_);
            }
            $.apply(l, _),
              u && !r && _.length > 0 && v + t.length > 1 && oe.uniqueSort(l);
          }
          return u && ((T = S), (c = w)), b;
        };
      return n ? se(r) : r;
    }
    return (
      (he.prototype = i.filters = i.pseudos),
      (i.setFilters = new he()),
      (s = oe.tokenize = function (e, t) {
        var n,
          o,
          r,
          s,
          a,
          l,
          c,
          u = k[e + " "];
        if (u) return t ? 0 : u.slice(0);
        for (a = e, l = [], c = i.preFilter; a; ) {
          for (s in ((n && !(o = z.exec(a))) ||
            (o && (a = a.slice(o[0].length) || a), l.push((r = []))),
          (n = !1),
          (o = B.exec(a)) &&
            ((n = o.shift()),
            r.push({ value: n, type: o[0].replace(W, " ") }),
            (a = a.slice(n.length))),
          i.filter))
            !(o = K[s].exec(a)) ||
              (c[s] && !(o = c[s](o))) ||
              ((n = o.shift()),
              r.push({ value: n, type: s, matches: o }),
              (a = a.slice(n.length)));
          if (!n) break;
        }
        return t ? a.length : a ? oe.error(e) : k(e, l).slice(0);
      }),
      (a = oe.compile = function (e, t) {
        var n,
          i = [],
          o = [],
          r = x[e + " "];
        if (!r) {
          for (t || (t = s(e)), n = t.length; n--; )
            (r = _e(t[n]))[_] ? i.push(r) : o.push(r);
          (r = x(e, we(o, i))).selector = e;
        }
        return r;
      }),
      (l = oe.select = function (e, t, o, r) {
        var l,
          c,
          u,
          d,
          p,
          f = "function" == typeof e && e,
          h = !r && s((e = f.selector || e));
        if (((o = o || []), 1 === h.length)) {
          if (
            (c = h[0] = h[0].slice(0)).length > 2 &&
            "ID" === (u = c[0]).type &&
            n.getById &&
            9 === t.nodeType &&
            g &&
            i.relative[c[1].type]
          ) {
            if (!(t = (i.find.ID(u.matches[0].replace(te, ne), t) || [])[0]))
              return o;
            f && (t = t.parentNode), (e = e.slice(c.shift().value.length));
          }
          for (
            l = K.needsContext.test(e) ? 0 : c.length;
            l-- && ((u = c[l]), !i.relative[(d = u.type)]);

          )
            if (
              (p = i.find[d]) &&
              (r = p(
                u.matches[0].replace(te, ne),
                (Z.test(c[0].type) && fe(t.parentNode)) || t
              ))
            ) {
              if ((c.splice(l, 1), !(e = r.length && ge(c))))
                return $.apply(o, r), o;
              break;
            }
        }
        return (
          (f || a(e, h))(
            r,
            t,
            !g,
            o,
            !t || (Z.test(e) && fe(t.parentNode)) || t
          ),
          o
        );
      }),
      (n.sortStable = _.split("").sort(E).join("") === _),
      (n.detectDuplicates = !!d),
      p(),
      (n.sortDetached = ae(function (e) {
        return 1 & e.compareDocumentPosition(f.createElement("div"));
      })),
      ae(function (e) {
        return (
          (e.innerHTML = "<a href='#'></a>"),
          "#" === e.firstChild.getAttribute("href")
        );
      }) ||
        le("type|href|height|width", function (e, t, n) {
          return n
            ? void 0
            : e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2);
        }),
      (n.attributes &&
        ae(function (e) {
          return (
            (e.innerHTML = "<input/>"),
            e.firstChild.setAttribute("value", ""),
            "" === e.firstChild.getAttribute("value")
          );
        })) ||
        le("value", function (e, t, n) {
          return n || "input" !== e.nodeName.toLowerCase()
            ? void 0
            : e.defaultValue;
        }),
      ae(function (e) {
        return null == e.getAttribute("disabled");
      }) ||
        le(P, function (e, t, n) {
          var i;
          return n
            ? void 0
            : !0 === e[t]
            ? t.toLowerCase()
            : (i = e.getAttributeNode(t)) && i.specified
            ? i.value
            : null;
        }),
      oe
    );
  })(e);
  (f.find = b),
    (f.expr = b.selectors),
    (f.expr[":"] = f.expr.pseudos),
    (f.uniqueSort = f.unique = b.uniqueSort),
    (f.text = b.getText),
    (f.isXMLDoc = b.isXML),
    (f.contains = b.contains);
  var _ = function (e, t, n) {
      for (var i = [], o = void 0 !== n; (e = e[t]) && 9 !== e.nodeType; )
        if (1 === e.nodeType) {
          if (o && f(e).is(n)) break;
          i.push(e);
        }
      return i;
    },
    w = function (e, t) {
      for (var n = []; e; e = e.nextSibling)
        1 === e.nodeType && e !== t && n.push(e);
      return n;
    },
    T = f.expr.match.needsContext,
    C = /^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,
    S = /^.[^:#\[\.,]*$/;
  function k(e, t, n) {
    if (f.isFunction(t))
      return f.grep(e, function (e, i) {
        return !!t.call(e, i, e) !== n;
      });
    if (t.nodeType)
      return f.grep(e, function (e) {
        return (e === t) !== n;
      });
    if ("string" == typeof t) {
      if (S.test(t)) return f.filter(t, e, n);
      t = f.filter(t, e);
    }
    return f.grep(e, function (e) {
      return a.call(t, e) > -1 !== n;
    });
  }
  (f.filter = function (e, t, n) {
    var i = t[0];
    return (
      n && (e = ":not(" + e + ")"),
      1 === t.length && 1 === i.nodeType
        ? f.find.matchesSelector(i, e)
          ? [i]
          : []
        : f.find.matches(
            e,
            f.grep(t, function (e) {
              return 1 === e.nodeType;
            })
          )
    );
  }),
    f.fn.extend({
      find: function (e) {
        var t,
          n = this.length,
          i = [],
          o = this;
        if ("string" != typeof e)
          return this.pushStack(
            f(e).filter(function () {
              for (t = 0; n > t; t++) if (f.contains(o[t], this)) return !0;
            })
          );
        for (t = 0; n > t; t++) f.find(e, o[t], i);
        return (
          ((i = this.pushStack(n > 1 ? f.unique(i) : i)).selector = this
            .selector
            ? this.selector + " " + e
            : e),
          i
        );
      },
      filter: function (e) {
        return this.pushStack(k(this, e || [], !1));
      },
      not: function (e) {
        return this.pushStack(k(this, e || [], !0));
      },
      is: function (e) {
        return !!k(this, "string" == typeof e && T.test(e) ? f(e) : e || [], !1)
          .length;
      },
    });
  var x,
    E = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/;
  ((f.fn.init = function (e, t, n) {
    var o, r;
    if (!e) return this;
    if (((n = n || x), "string" == typeof e)) {
      if (
        !(o =
          "<" === e[0] && ">" === e[e.length - 1] && e.length >= 3
            ? [null, e, null]
            : E.exec(e)) ||
        (!o[1] && t)
      )
        return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
      if (o[1]) {
        if (
          ((t = t instanceof f ? t[0] : t),
          f.merge(
            this,
            f.parseHTML(o[1], t && t.nodeType ? t.ownerDocument || t : i, !0)
          ),
          C.test(o[1]) && f.isPlainObject(t))
        )
          for (o in t)
            f.isFunction(this[o]) ? this[o](t[o]) : this.attr(o, t[o]);
        return this;
      }
      return (
        (r = i.getElementById(o[2])) &&
          r.parentNode &&
          ((this.length = 1), (this[0] = r)),
        (this.context = i),
        (this.selector = e),
        this
      );
    }
    return e.nodeType
      ? ((this.context = this[0] = e), (this.length = 1), this)
      : f.isFunction(e)
      ? void 0 !== n.ready
        ? n.ready(e)
        : e(f)
      : (void 0 !== e.selector &&
          ((this.selector = e.selector), (this.context = e.context)),
        f.makeArray(e, this));
  }).prototype = f.fn),
    (x = f(i));
  var A = /^(?:parents|prev(?:Until|All))/,
    D = { children: !0, contents: !0, next: !0, prev: !0 };
  function N(e, t) {
    for (; (e = e[t]) && 1 !== e.nodeType; );
    return e;
  }
  f.fn.extend({
    has: function (e) {
      var t = f(e, this),
        n = t.length;
      return this.filter(function () {
        for (var e = 0; n > e; e++) if (f.contains(this, t[e])) return !0;
      });
    },
    closest: function (e, t) {
      for (
        var n,
          i = 0,
          o = this.length,
          r = [],
          s = T.test(e) || "string" != typeof e ? f(e, t || this.context) : 0;
        o > i;
        i++
      )
        for (n = this[i]; n && n !== t; n = n.parentNode)
          if (
            n.nodeType < 11 &&
            (s
              ? s.index(n) > -1
              : 1 === n.nodeType && f.find.matchesSelector(n, e))
          ) {
            r.push(n);
            break;
          }
      return this.pushStack(r.length > 1 ? f.uniqueSort(r) : r);
    },
    index: function (e) {
      return e
        ? "string" == typeof e
          ? a.call(f(e), this[0])
          : a.call(this, e.jquery ? e[0] : e)
        : this[0] && this[0].parentNode
        ? this.first().prevAll().length
        : -1;
    },
    add: function (e, t) {
      return this.pushStack(f.uniqueSort(f.merge(this.get(), f(e, t))));
    },
    addBack: function (e) {
      return this.add(null == e ? this.prevObject : this.prevObject.filter(e));
    },
  }),
    f.each(
      {
        parent: function (e) {
          var t = e.parentNode;
          return t && 11 !== t.nodeType ? t : null;
        },
        parents: function (e) {
          return _(e, "parentNode");
        },
        parentsUntil: function (e, t, n) {
          return _(e, "parentNode", n);
        },
        next: function (e) {
          return N(e, "nextSibling");
        },
        prev: function (e) {
          return N(e, "previousSibling");
        },
        nextAll: function (e) {
          return _(e, "nextSibling");
        },
        prevAll: function (e) {
          return _(e, "previousSibling");
        },
        nextUntil: function (e, t, n) {
          return _(e, "nextSibling", n);
        },
        prevUntil: function (e, t, n) {
          return _(e, "previousSibling", n);
        },
        siblings: function (e) {
          return w((e.parentNode || {}).firstChild, e);
        },
        children: function (e) {
          return w(e.firstChild);
        },
        contents: function (e) {
          return e.contentDocument || f.merge([], e.childNodes);
        },
      },
      function (e, t) {
        f.fn[e] = function (n, i) {
          var o = f.map(this, t, n);
          return (
            "Until" !== e.slice(-5) && (i = n),
            i && "string" == typeof i && (o = f.filter(i, o)),
            this.length > 1 &&
              (D[e] || f.uniqueSort(o), A.test(e) && o.reverse()),
            this.pushStack(o)
          );
        };
      }
    );
  var I,
    O = /\S+/g;
  function $() {
    i.removeEventListener("DOMContentLoaded", $),
      e.removeEventListener("load", $),
      f.ready();
  }
  (f.Callbacks = function (e) {
    e =
      "string" == typeof e
        ? (function (e) {
            var t = {};
            return (
              f.each(e.match(O) || [], function (e, n) {
                t[n] = !0;
              }),
              t
            );
          })(e)
        : f.extend({}, e);
    var t,
      n,
      i,
      o,
      r = [],
      s = [],
      a = -1,
      l = function () {
        for (o = e.once, i = t = !0; s.length; a = -1)
          for (n = s.shift(); ++a < r.length; )
            !1 === r[a].apply(n[0], n[1]) &&
              e.stopOnFalse &&
              ((a = r.length), (n = !1));
        e.memory || (n = !1), (t = !1), o && (r = n ? [] : "");
      },
      c = {
        add: function () {
          return (
            r &&
              (n && !t && ((a = r.length - 1), s.push(n)),
              (function t(n) {
                f.each(n, function (n, i) {
                  f.isFunction(i)
                    ? (e.unique && c.has(i)) || r.push(i)
                    : i && i.length && "string" !== f.type(i) && t(i);
                });
              })(arguments),
              n && !t && l()),
            this
          );
        },
        remove: function () {
          return (
            f.each(arguments, function (e, t) {
              for (var n; (n = f.inArray(t, r, n)) > -1; )
                r.splice(n, 1), a >= n && a--;
            }),
            this
          );
        },
        has: function (e) {
          return e ? f.inArray(e, r) > -1 : r.length > 0;
        },
        empty: function () {
          return r && (r = []), this;
        },
        disable: function () {
          return (o = s = []), (r = n = ""), this;
        },
        disabled: function () {
          return !r;
        },
        lock: function () {
          return (o = s = []), n || (r = n = ""), this;
        },
        locked: function () {
          return !!o;
        },
        fireWith: function (e, n) {
          return (
            o ||
              ((n = [e, (n = n || []).slice ? n.slice() : n]),
              s.push(n),
              t || l()),
            this
          );
        },
        fire: function () {
          return c.fireWith(this, arguments), this;
        },
        fired: function () {
          return !!i;
        },
      };
    return c;
  }),
    f.extend({
      Deferred: function (e) {
        var t = [
            ["resolve", "done", f.Callbacks("once memory"), "resolved"],
            ["reject", "fail", f.Callbacks("once memory"), "rejected"],
            ["notify", "progress", f.Callbacks("memory")],
          ],
          n = "pending",
          i = {
            state: function () {
              return n;
            },
            always: function () {
              return o.done(arguments).fail(arguments), this;
            },
            then: function () {
              var e = arguments;
              return f
                .Deferred(function (n) {
                  f.each(t, function (t, r) {
                    var s = f.isFunction(e[t]) && e[t];
                    o[r[1]](function () {
                      var e = s && s.apply(this, arguments);
                      e && f.isFunction(e.promise)
                        ? e
                            .promise()
                            .progress(n.notify)
                            .done(n.resolve)
                            .fail(n.reject)
                        : n[r[0] + "With"](
                            this === i ? n.promise() : this,
                            s ? [e] : arguments
                          );
                    });
                  }),
                    (e = null);
                })
                .promise();
            },
            promise: function (e) {
              return null != e ? f.extend(e, i) : i;
            },
          },
          o = {};
        return (
          (i.pipe = i.then),
          f.each(t, function (e, r) {
            var s = r[2],
              a = r[3];
            (i[r[1]] = s.add),
              a &&
                s.add(
                  function () {
                    n = a;
                  },
                  t[1 ^ e][2].disable,
                  t[2][2].lock
                ),
              (o[r[0]] = function () {
                return o[r[0] + "With"](this === o ? i : this, arguments), this;
              }),
              (o[r[0] + "With"] = s.fireWith);
          }),
          i.promise(o),
          e && e.call(o, o),
          o
        );
      },
      when: function (e) {
        var t,
          n,
          i,
          r = 0,
          s = o.call(arguments),
          a = s.length,
          l = 1 !== a || (e && f.isFunction(e.promise)) ? a : 0,
          c = 1 === l ? e : f.Deferred(),
          u = function (e, n, i) {
            return function (r) {
              (n[e] = this),
                (i[e] = arguments.length > 1 ? o.call(arguments) : r),
                i === t ? c.notifyWith(n, i) : --l || c.resolveWith(n, i);
            };
          };
        if (a > 1)
          for (t = new Array(a), n = new Array(a), i = new Array(a); a > r; r++)
            s[r] && f.isFunction(s[r].promise)
              ? s[r]
                  .promise()
                  .progress(u(r, n, t))
                  .done(u(r, i, s))
                  .fail(c.reject)
              : --l;
        return l || c.resolveWith(i, s), c.promise();
      },
    }),
    (f.fn.ready = function (e) {
      return f.ready.promise().done(e), this;
    }),
    f.extend({
      isReady: !1,
      readyWait: 1,
      holdReady: function (e) {
        e ? f.readyWait++ : f.ready(!0);
      },
      ready: function (e) {
        (!0 === e ? --f.readyWait : f.isReady) ||
          ((f.isReady = !0),
          (!0 !== e && --f.readyWait > 0) ||
            (I.resolveWith(i, [f]),
            f.fn.triggerHandler &&
              (f(i).triggerHandler("ready"), f(i).off("ready"))));
      },
    }),
    (f.ready.promise = function (t) {
      return (
        I ||
          ((I = f.Deferred()),
          "complete" === i.readyState ||
          ("loading" !== i.readyState && !i.documentElement.doScroll)
            ? e.setTimeout(f.ready)
            : (i.addEventListener("DOMContentLoaded", $),
              e.addEventListener("load", $))),
        I.promise(t)
      );
    }),
    f.ready.promise();
  var j = function (e, t, n, i, o, r, s) {
      var a = 0,
        l = e.length,
        c = null == n;
      if ("object" === f.type(n))
        for (a in ((o = !0), n)) j(e, t, a, n[a], !0, r, s);
      else if (
        void 0 !== i &&
        ((o = !0),
        f.isFunction(i) || (s = !0),
        c &&
          (s
            ? (t.call(e, i), (t = null))
            : ((c = t),
              (t = function (e, t, n) {
                return c.call(f(e), n);
              }))),
        t)
      )
        for (; l > a; a++) t(e[a], n, s ? i : i.call(e[a], a, t(e[a], n)));
      return o ? e : c ? t.call(e) : l ? t(e[0], n) : r;
    },
    L = function (e) {
      return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType;
    };
  function P() {
    this.expando = f.expando + P.uid++;
  }
  (P.uid = 1),
    (P.prototype = {
      register: function (e, t) {
        var n = t || {};
        return (
          e.nodeType
            ? (e[this.expando] = n)
            : Object.defineProperty(e, this.expando, {
                value: n,
                writable: !0,
                configurable: !0,
              }),
          e[this.expando]
        );
      },
      cache: function (e) {
        if (!L(e)) return {};
        var t = e[this.expando];
        return (
          t ||
            ((t = {}),
            L(e) &&
              (e.nodeType
                ? (e[this.expando] = t)
                : Object.defineProperty(e, this.expando, {
                    value: t,
                    configurable: !0,
                  }))),
          t
        );
      },
      set: function (e, t, n) {
        var i,
          o = this.cache(e);
        if ("string" == typeof t) o[t] = n;
        else for (i in t) o[i] = t[i];
        return o;
      },
      get: function (e, t) {
        return void 0 === t
          ? this.cache(e)
          : e[this.expando] && e[this.expando][t];
      },
      access: function (e, t, n) {
        var i;
        return void 0 === t || (t && "string" == typeof t && void 0 === n)
          ? void 0 !== (i = this.get(e, t))
            ? i
            : this.get(e, f.camelCase(t))
          : (this.set(e, t, n), void 0 !== n ? n : t);
      },
      remove: function (e, t) {
        var n,
          i,
          o,
          r = e[this.expando];
        if (void 0 !== r) {
          if (void 0 === t) this.register(e);
          else {
            f.isArray(t)
              ? (i = t.concat(t.map(f.camelCase)))
              : ((o = f.camelCase(t)),
                t in r
                  ? (i = [t, o])
                  : (i = (i = o) in r ? [i] : i.match(O) || [])),
              (n = i.length);
            for (; n--; ) delete r[i[n]];
          }
          (void 0 === t || f.isEmptyObject(r)) &&
            (e.nodeType ? (e[this.expando] = void 0) : delete e[this.expando]);
        }
      },
      hasData: function (e) {
        var t = e[this.expando];
        return void 0 !== t && !f.isEmptyObject(t);
      },
    });
  var H = new P(),
    q = new P(),
    R = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
    M = /[A-Z]/g;
  function F(e, t, n) {
    var i;
    if (void 0 === n && 1 === e.nodeType)
      if (
        ((i = "data-" + t.replace(M, "-$&").toLowerCase()),
        "string" == typeof (n = e.getAttribute(i)))
      ) {
        try {
          n =
            "true" === n ||
            ("false" !== n &&
              ("null" === n
                ? null
                : +n + "" === n
                ? +n
                : R.test(n)
                ? f.parseJSON(n)
                : n));
        } catch (e) {}
        q.set(e, t, n);
      } else n = void 0;
    return n;
  }
  f.extend({
    hasData: function (e) {
      return q.hasData(e) || H.hasData(e);
    },
    data: function (e, t, n) {
      return q.access(e, t, n);
    },
    removeData: function (e, t) {
      q.remove(e, t);
    },
    _data: function (e, t, n) {
      return H.access(e, t, n);
    },
    _removeData: function (e, t) {
      H.remove(e, t);
    },
  }),
    f.fn.extend({
      data: function (e, t) {
        var n,
          i,
          o,
          r = this[0],
          s = r && r.attributes;
        if (void 0 === e) {
          if (
            this.length &&
            ((o = q.get(r)), 1 === r.nodeType && !H.get(r, "hasDataAttrs"))
          ) {
            for (n = s.length; n--; )
              s[n] &&
                0 === (i = s[n].name).indexOf("data-") &&
                ((i = f.camelCase(i.slice(5))), F(r, i, o[i]));
            H.set(r, "hasDataAttrs", !0);
          }
          return o;
        }
        return "object" == typeof e
          ? this.each(function () {
              q.set(this, e);
            })
          : j(
              this,
              function (t) {
                var n, i;
                if (r && void 0 === t) {
                  if (
                    void 0 !==
                    (n =
                      q.get(r, e) ||
                      q.get(r, e.replace(M, "-$&").toLowerCase()))
                  )
                    return n;
                  if (((i = f.camelCase(e)), void 0 !== (n = q.get(r, i))))
                    return n;
                  if (void 0 !== (n = F(r, i, void 0))) return n;
                } else
                  (i = f.camelCase(e)),
                    this.each(function () {
                      var n = q.get(this, i);
                      q.set(this, i, t),
                        e.indexOf("-") > -1 &&
                          void 0 !== n &&
                          q.set(this, e, t);
                    });
              },
              null,
              t,
              arguments.length > 1,
              null,
              !0
            );
      },
      removeData: function (e) {
        return this.each(function () {
          q.remove(this, e);
        });
      },
    }),
    f.extend({
      queue: function (e, t, n) {
        var i;
        return e
          ? ((t = (t || "fx") + "queue"),
            (i = H.get(e, t)),
            n &&
              (!i || f.isArray(n)
                ? (i = H.access(e, t, f.makeArray(n)))
                : i.push(n)),
            i || [])
          : void 0;
      },
      dequeue: function (e, t) {
        t = t || "fx";
        var n = f.queue(e, t),
          i = n.length,
          o = n.shift(),
          r = f._queueHooks(e, t);
        "inprogress" === o && ((o = n.shift()), i--),
          o &&
            ("fx" === t && n.unshift("inprogress"),
            delete r.stop,
            o.call(
              e,
              function () {
                f.dequeue(e, t);
              },
              r
            )),
          !i && r && r.empty.fire();
      },
      _queueHooks: function (e, t) {
        var n = t + "queueHooks";
        return (
          H.get(e, n) ||
          H.access(e, n, {
            empty: f.Callbacks("once memory").add(function () {
              H.remove(e, [t + "queue", n]);
            }),
          })
        );
      },
    }),
    f.fn.extend({
      queue: function (e, t) {
        var n = 2;
        return (
          "string" != typeof e && ((t = e), (e = "fx"), n--),
          arguments.length < n
            ? f.queue(this[0], e)
            : void 0 === t
            ? this
            : this.each(function () {
                var n = f.queue(this, e, t);
                f._queueHooks(this, e),
                  "fx" === e && "inprogress" !== n[0] && f.dequeue(this, e);
              })
        );
      },
      dequeue: function (e) {
        return this.each(function () {
          f.dequeue(this, e);
        });
      },
      clearQueue: function (e) {
        return this.queue(e || "fx", []);
      },
      promise: function (e, t) {
        var n,
          i = 1,
          o = f.Deferred(),
          r = this,
          s = this.length,
          a = function () {
            --i || o.resolveWith(r, [r]);
          };
        for (
          "string" != typeof e && ((t = e), (e = void 0)), e = e || "fx";
          s--;

        )
          (n = H.get(r[s], e + "queueHooks")) &&
            n.empty &&
            (i++, n.empty.add(a));
        return a(), o.promise(t);
      },
    });
  var W = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
    z = new RegExp("^(?:([+-])=|)(" + W + ")([a-z%]*)$", "i"),
    B = ["Top", "Right", "Bottom", "Left"],
    U = function (e, t) {
      return (
        (e = t || e),
        "none" === f.css(e, "display") || !f.contains(e.ownerDocument, e)
      );
    };
  function X(e, t, n, i) {
    var o,
      r = 1,
      s = 20,
      a = i
        ? function () {
            return i.cur();
          }
        : function () {
            return f.css(e, t, "");
          },
      l = a(),
      c = (n && n[3]) || (f.cssNumber[t] ? "" : "px"),
      u = (f.cssNumber[t] || ("px" !== c && +l)) && z.exec(f.css(e, t));
    if (u && u[3] !== c) {
      (c = c || u[3]), (n = n || []), (u = +l || 1);
      do {
        (u /= r = r || ".5"), f.style(e, t, u + c);
      } while (r !== (r = a() / l) && 1 !== r && --s);
    }
    return (
      n &&
        ((u = +u || +l || 0),
        (o = n[1] ? u + (n[1] + 1) * n[2] : +n[2]),
        i && ((i.unit = c), (i.start = u), (i.end = o))),
      o
    );
  }
  var Q = /^(?:checkbox|radio)$/i,
    K = /<([\w:-]+)/,
    Y = /^$|\/(?:java|ecma)script/i,
    V = {
      option: [1, "<select multiple='multiple'>", "</select>"],
      thead: [1, "<table>", "</table>"],
      col: [2, "<table><colgroup>", "</colgroup></table>"],
      tr: [2, "<table><tbody>", "</tbody></table>"],
      td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
      _default: [0, "", ""],
    };
  function G(e, t) {
    var n =
      void 0 !== e.getElementsByTagName
        ? e.getElementsByTagName(t || "*")
        : void 0 !== e.querySelectorAll
        ? e.querySelectorAll(t || "*")
        : [];
    return void 0 === t || (t && f.nodeName(e, t)) ? f.merge([e], n) : n;
  }
  function J(e, t) {
    for (var n = 0, i = e.length; i > n; n++)
      H.set(e[n], "globalEval", !t || H.get(t[n], "globalEval"));
  }
  (V.optgroup = V.option),
    (V.tbody = V.tfoot = V.colgroup = V.caption = V.thead),
    (V.th = V.td);
  var Z = /<|&#?\w+;/;
  function ee(e, t, n, i, o) {
    for (
      var r,
        s,
        a,
        l,
        c,
        u,
        d = t.createDocumentFragment(),
        p = [],
        h = 0,
        g = e.length;
      g > h;
      h++
    )
      if ((r = e[h]) || 0 === r)
        if ("object" === f.type(r)) f.merge(p, r.nodeType ? [r] : r);
        else if (Z.test(r)) {
          for (
            s = s || d.appendChild(t.createElement("div")),
              a = (K.exec(r) || ["", ""])[1].toLowerCase(),
              l = V[a] || V._default,
              s.innerHTML = l[1] + f.htmlPrefilter(r) + l[2],
              u = l[0];
            u--;

          )
            s = s.lastChild;
          f.merge(p, s.childNodes), ((s = d.firstChild).textContent = "");
        } else p.push(t.createTextNode(r));
    for (d.textContent = "", h = 0; (r = p[h++]); )
      if (i && f.inArray(r, i) > -1) o && o.push(r);
      else if (
        ((c = f.contains(r.ownerDocument, r)),
        (s = G(d.appendChild(r), "script")),
        c && J(s),
        n)
      )
        for (u = 0; (r = s[u++]); ) Y.test(r.type || "") && n.push(r);
    return d;
  }
  !(function () {
    var e = i.createDocumentFragment().appendChild(i.createElement("div")),
      t = i.createElement("input");
    t.setAttribute("type", "radio"),
      t.setAttribute("checked", "checked"),
      t.setAttribute("name", "t"),
      e.appendChild(t),
      (d.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked),
      (e.innerHTML = "<textarea>x</textarea>"),
      (d.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue);
  })();
  var te = /^key/,
    ne = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
    ie = /^([^.]*)(?:\.(.+)|)/;
  function oe() {
    return !0;
  }
  function re() {
    return !1;
  }
  function se() {
    try {
      return i.activeElement;
    } catch (e) {}
  }
  function ae(e, t, n, i, o, r) {
    var s, a;
    if ("object" == typeof t) {
      for (a in ("string" != typeof n && ((i = i || n), (n = void 0)), t))
        ae(e, a, n, i, t[a], r);
      return e;
    }
    if (
      (null == i && null == o
        ? ((o = n), (i = n = void 0))
        : null == o &&
          ("string" == typeof n
            ? ((o = i), (i = void 0))
            : ((o = i), (i = n), (n = void 0))),
      !1 === o)
    )
      o = re;
    else if (!o) return e;
    return (
      1 === r &&
        ((s = o),
        ((o = function (e) {
          return f().off(e), s.apply(this, arguments);
        }).guid = s.guid || (s.guid = f.guid++))),
      e.each(function () {
        f.event.add(this, t, o, i, n);
      })
    );
  }
  (f.event = {
    global: {},
    add: function (e, t, n, i, o) {
      var r,
        s,
        a,
        l,
        c,
        u,
        d,
        p,
        h,
        g,
        m,
        v = H.get(e);
      if (v)
        for (
          n.handler && ((n = (r = n).handler), (o = r.selector)),
            n.guid || (n.guid = f.guid++),
            (l = v.events) || (l = v.events = {}),
            (s = v.handle) ||
              (s = v.handle = function (t) {
                return void 0 !== f && f.event.triggered !== t.type
                  ? f.event.dispatch.apply(e, arguments)
                  : void 0;
              }),
            c = (t = (t || "").match(O) || [""]).length;
          c--;

        )
          (h = m = (a = ie.exec(t[c]) || [])[1]),
            (g = (a[2] || "").split(".").sort()),
            h &&
              ((d = f.event.special[h] || {}),
              (h = (o ? d.delegateType : d.bindType) || h),
              (d = f.event.special[h] || {}),
              (u = f.extend(
                {
                  type: h,
                  origType: m,
                  data: i,
                  handler: n,
                  guid: n.guid,
                  selector: o,
                  needsContext: o && f.expr.match.needsContext.test(o),
                  namespace: g.join("."),
                },
                r
              )),
              (p = l[h]) ||
                (((p = l[h] = []).delegateCount = 0),
                (d.setup && !1 !== d.setup.call(e, i, g, s)) ||
                  (e.addEventListener && e.addEventListener(h, s))),
              d.add &&
                (d.add.call(e, u), u.handler.guid || (u.handler.guid = n.guid)),
              o ? p.splice(p.delegateCount++, 0, u) : p.push(u),
              (f.event.global[h] = !0));
    },
    remove: function (e, t, n, i, o) {
      var r,
        s,
        a,
        l,
        c,
        u,
        d,
        p,
        h,
        g,
        m,
        v = H.hasData(e) && H.get(e);
      if (v && (l = v.events)) {
        for (c = (t = (t || "").match(O) || [""]).length; c--; )
          if (
            ((h = m = (a = ie.exec(t[c]) || [])[1]),
            (g = (a[2] || "").split(".").sort()),
            h)
          ) {
            for (
              d = f.event.special[h] || {},
                p = l[(h = (i ? d.delegateType : d.bindType) || h)] || [],
                a =
                  a[2] &&
                  new RegExp("(^|\\.)" + g.join("\\.(?:.*\\.|)") + "(\\.|$)"),
                s = r = p.length;
              r--;

            )
              (u = p[r]),
                (!o && m !== u.origType) ||
                  (n && n.guid !== u.guid) ||
                  (a && !a.test(u.namespace)) ||
                  (i && i !== u.selector && ("**" !== i || !u.selector)) ||
                  (p.splice(r, 1),
                  u.selector && p.delegateCount--,
                  d.remove && d.remove.call(e, u));
            s &&
              !p.length &&
              ((d.teardown && !1 !== d.teardown.call(e, g, v.handle)) ||
                f.removeEvent(e, h, v.handle),
              delete l[h]);
          } else for (h in l) f.event.remove(e, h + t[c], n, i, !0);
        f.isEmptyObject(l) && H.remove(e, "handle events");
      }
    },
    dispatch: function (e) {
      e = f.event.fix(e);
      var t,
        n,
        i,
        r,
        s,
        a = [],
        l = o.call(arguments),
        c = (H.get(this, "events") || {})[e.type] || [],
        u = f.event.special[e.type] || {};
      if (
        ((l[0] = e),
        (e.delegateTarget = this),
        !u.preDispatch || !1 !== u.preDispatch.call(this, e))
      ) {
        for (
          a = f.event.handlers.call(this, e, c), t = 0;
          (r = a[t++]) && !e.isPropagationStopped();

        )
          for (
            e.currentTarget = r.elem, n = 0;
            (s = r.handlers[n++]) && !e.isImmediatePropagationStopped();

          )
            (e.rnamespace && !e.rnamespace.test(s.namespace)) ||
              ((e.handleObj = s),
              (e.data = s.data),
              void 0 !==
                (i = (
                  (f.event.special[s.origType] || {}).handle || s.handler
                ).apply(r.elem, l)) &&
                !1 === (e.result = i) &&
                (e.preventDefault(), e.stopPropagation()));
        return u.postDispatch && u.postDispatch.call(this, e), e.result;
      }
    },
    handlers: function (e, t) {
      var n,
        i,
        o,
        r,
        s = [],
        a = t.delegateCount,
        l = e.target;
      if (
        a &&
        l.nodeType &&
        ("click" !== e.type || isNaN(e.button) || e.button < 1)
      )
        for (; l !== this; l = l.parentNode || this)
          if (1 === l.nodeType && (!0 !== l.disabled || "click" !== e.type)) {
            for (i = [], n = 0; a > n; n++)
              void 0 === i[(o = (r = t[n]).selector + " ")] &&
                (i[o] = r.needsContext
                  ? f(o, this).index(l) > -1
                  : f.find(o, this, null, [l]).length),
                i[o] && i.push(r);
            i.length && s.push({ elem: l, handlers: i });
          }
      return a < t.length && s.push({ elem: this, handlers: t.slice(a) }), s;
    },
    props: "altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(
      " "
    ),
    fixHooks: {},
    keyHooks: {
      props: "char charCode key keyCode".split(" "),
      filter: function (e, t) {
        return (
          null == e.which &&
            (e.which = null != t.charCode ? t.charCode : t.keyCode),
          e
        );
      },
    },
    mouseHooks: {
      props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(
        " "
      ),
      filter: function (e, t) {
        var n,
          o,
          r,
          s = t.button;
        return (
          null == e.pageX &&
            null != t.clientX &&
            ((o = (n = e.target.ownerDocument || i).documentElement),
            (r = n.body),
            (e.pageX =
              t.clientX +
              ((o && o.scrollLeft) || (r && r.scrollLeft) || 0) -
              ((o && o.clientLeft) || (r && r.clientLeft) || 0)),
            (e.pageY =
              t.clientY +
              ((o && o.scrollTop) || (r && r.scrollTop) || 0) -
              ((o && o.clientTop) || (r && r.clientTop) || 0))),
          e.which ||
            void 0 === s ||
            (e.which = 1 & s ? 1 : 2 & s ? 3 : 4 & s ? 2 : 0),
          e
        );
      },
    },
    fix: function (e) {
      if (e[f.expando]) return e;
      var t,
        n,
        o,
        r = e.type,
        s = e,
        a = this.fixHooks[r];
      for (
        a ||
          (this.fixHooks[r] = a = ne.test(r)
            ? this.mouseHooks
            : te.test(r)
            ? this.keyHooks
            : {}),
          o = a.props ? this.props.concat(a.props) : this.props,
          e = new f.Event(s),
          t = o.length;
        t--;

      )
        e[(n = o[t])] = s[n];
      return (
        e.target || (e.target = i),
        3 === e.target.nodeType && (e.target = e.target.parentNode),
        a.filter ? a.filter(e, s) : e
      );
    },
    special: {
      load: { noBubble: !0 },
      focus: {
        trigger: function () {
          return this !== se() && this.focus ? (this.focus(), !1) : void 0;
        },
        delegateType: "focusin",
      },
      blur: {
        trigger: function () {
          return this === se() && this.blur ? (this.blur(), !1) : void 0;
        },
        delegateType: "focusout",
      },
      click: {
        trigger: function () {
          return "checkbox" === this.type &&
            this.click &&
            f.nodeName(this, "input")
            ? (this.click(), !1)
            : void 0;
        },
        _default: function (e) {
          return f.nodeName(e.target, "a");
        },
      },
      beforeunload: {
        postDispatch: function (e) {
          void 0 !== e.result &&
            e.originalEvent &&
            (e.originalEvent.returnValue = e.result);
        },
      },
    },
  }),
    (f.removeEvent = function (e, t, n) {
      e.removeEventListener && e.removeEventListener(t, n);
    }),
    (f.Event = function (e, t) {
      return this instanceof f.Event
        ? (e && e.type
            ? ((this.originalEvent = e),
              (this.type = e.type),
              (this.isDefaultPrevented =
                e.defaultPrevented ||
                (void 0 === e.defaultPrevented && !1 === e.returnValue)
                  ? oe
                  : re))
            : (this.type = e),
          t && f.extend(this, t),
          (this.timeStamp = (e && e.timeStamp) || f.now()),
          void (this[f.expando] = !0))
        : new f.Event(e, t);
    }),
    (f.Event.prototype = {
      constructor: f.Event,
      isDefaultPrevented: re,
      isPropagationStopped: re,
      isImmediatePropagationStopped: re,
      isSimulated: !1,
      preventDefault: function () {
        var e = this.originalEvent;
        (this.isDefaultPrevented = oe),
          e && !this.isSimulated && e.preventDefault();
      },
      stopPropagation: function () {
        var e = this.originalEvent;
        (this.isPropagationStopped = oe),
          e && !this.isSimulated && e.stopPropagation();
      },
      stopImmediatePropagation: function () {
        var e = this.originalEvent;
        (this.isImmediatePropagationStopped = oe),
          e && !this.isSimulated && e.stopImmediatePropagation(),
          this.stopPropagation();
      },
    }),
    f.each(
      {
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout",
      },
      function (e, t) {
        f.event.special[e] = {
          delegateType: t,
          bindType: t,
          handle: function (e) {
            var n,
              i = e.relatedTarget,
              o = e.handleObj;
            return (
              (i && (i === this || f.contains(this, i))) ||
                ((e.type = o.origType),
                (n = o.handler.apply(this, arguments)),
                (e.type = t)),
              n
            );
          },
        };
      }
    ),
    f.fn.extend({
      on: function (e, t, n, i) {
        return ae(this, e, t, n, i);
      },
      one: function (e, t, n, i) {
        return ae(this, e, t, n, i, 1);
      },
      off: function (e, t, n) {
        var i, o;
        if (e && e.preventDefault && e.handleObj)
          return (
            (i = e.handleObj),
            f(e.delegateTarget).off(
              i.namespace ? i.origType + "." + i.namespace : i.origType,
              i.selector,
              i.handler
            ),
            this
          );
        if ("object" == typeof e) {
          for (o in e) this.off(o, t, e[o]);
          return this;
        }
        return (
          (!1 !== t && "function" != typeof t) || ((n = t), (t = void 0)),
          !1 === n && (n = re),
          this.each(function () {
            f.event.remove(this, e, n, t);
          })
        );
      },
    });
  var le = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,
    ce = /<script|<style|<link/i,
    ue = /checked\s*(?:[^=]|=\s*.checked.)/i,
    de = /^true\/(.*)/,
    pe = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
  function fe(e, t) {
    return f.nodeName(e, "table") &&
      f.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr")
      ? e.getElementsByTagName("tbody")[0] ||
          e.appendChild(e.ownerDocument.createElement("tbody"))
      : e;
  }
  function he(e) {
    return (e.type = (null !== e.getAttribute("type")) + "/" + e.type), e;
  }
  function ge(e) {
    var t = de.exec(e.type);
    return t ? (e.type = t[1]) : e.removeAttribute("type"), e;
  }
  function me(e, t) {
    var n, i, o, r, s, a, l, c;
    if (1 === t.nodeType) {
      if (
        H.hasData(e) &&
        ((r = H.access(e)), (s = H.set(t, r)), (c = r.events))
      )
        for (o in (delete s.handle, (s.events = {}), c))
          for (n = 0, i = c[o].length; i > n; n++) f.event.add(t, o, c[o][n]);
      q.hasData(e) && ((a = q.access(e)), (l = f.extend({}, a)), q.set(t, l));
    }
  }
  function ve(e, t) {
    var n = t.nodeName.toLowerCase();
    "input" === n && Q.test(e.type)
      ? (t.checked = e.checked)
      : ("input" !== n && "textarea" !== n) ||
        (t.defaultValue = e.defaultValue);
  }
  function ye(e, t, n, i) {
    t = r.apply([], t);
    var o,
      s,
      a,
      l,
      c,
      u,
      p = 0,
      h = e.length,
      g = h - 1,
      m = t[0],
      v = f.isFunction(m);
    if (v || (h > 1 && "string" == typeof m && !d.checkClone && ue.test(m)))
      return e.each(function (o) {
        var r = e.eq(o);
        v && (t[0] = m.call(this, o, r.html())), ye(r, t, n, i);
      });
    if (
      h &&
      ((s = (o = ee(t, e[0].ownerDocument, !1, e, i)).firstChild),
      1 === o.childNodes.length && (o = s),
      s || i)
    ) {
      for (l = (a = f.map(G(o, "script"), he)).length; h > p; p++)
        (c = o),
          p !== g &&
            ((c = f.clone(c, !0, !0)), l && f.merge(a, G(c, "script"))),
          n.call(e[p], c, p);
      if (l)
        for (u = a[a.length - 1].ownerDocument, f.map(a, ge), p = 0; l > p; p++)
          (c = a[p]),
            Y.test(c.type || "") &&
              !H.access(c, "globalEval") &&
              f.contains(u, c) &&
              (c.src
                ? f._evalUrl && f._evalUrl(c.src)
                : f.globalEval(c.textContent.replace(pe, "")));
    }
    return e;
  }
  function be(e, t, n) {
    for (var i, o = t ? f.filter(t, e) : e, r = 0; null != (i = o[r]); r++)
      n || 1 !== i.nodeType || f.cleanData(G(i)),
        i.parentNode &&
          (n && f.contains(i.ownerDocument, i) && J(G(i, "script")),
          i.parentNode.removeChild(i));
    return e;
  }
  f.extend({
    htmlPrefilter: function (e) {
      return e.replace(le, "<$1></$2>");
    },
    clone: function (e, t, n) {
      var i,
        o,
        r,
        s,
        a = e.cloneNode(!0),
        l = f.contains(e.ownerDocument, e);
      if (
        !(
          d.noCloneChecked ||
          (1 !== e.nodeType && 11 !== e.nodeType) ||
          f.isXMLDoc(e)
        )
      )
        for (s = G(a), i = 0, o = (r = G(e)).length; o > i; i++) ve(r[i], s[i]);
      if (t)
        if (n)
          for (r = r || G(e), s = s || G(a), i = 0, o = r.length; o > i; i++)
            me(r[i], s[i]);
        else me(e, a);
      return (s = G(a, "script")).length > 0 && J(s, !l && G(e, "script")), a;
    },
    cleanData: function (e) {
      for (var t, n, i, o = f.event.special, r = 0; void 0 !== (n = e[r]); r++)
        if (L(n)) {
          if ((t = n[H.expando])) {
            if (t.events)
              for (i in t.events)
                o[i] ? f.event.remove(n, i) : f.removeEvent(n, i, t.handle);
            n[H.expando] = void 0;
          }
          n[q.expando] && (n[q.expando] = void 0);
        }
    },
  }),
    f.fn.extend({
      domManip: ye,
      detach: function (e) {
        return be(this, e, !0);
      },
      remove: function (e) {
        return be(this, e);
      },
      text: function (e) {
        return j(
          this,
          function (e) {
            return void 0 === e
              ? f.text(this)
              : this.empty().each(function () {
                  (1 !== this.nodeType &&
                    11 !== this.nodeType &&
                    9 !== this.nodeType) ||
                    (this.textContent = e);
                });
          },
          null,
          e,
          arguments.length
        );
      },
      append: function () {
        return ye(this, arguments, function (e) {
          (1 !== this.nodeType &&
            11 !== this.nodeType &&
            9 !== this.nodeType) ||
            fe(this, e).appendChild(e);
        });
      },
      prepend: function () {
        return ye(this, arguments, function (e) {
          if (
            1 === this.nodeType ||
            11 === this.nodeType ||
            9 === this.nodeType
          ) {
            var t = fe(this, e);
            t.insertBefore(e, t.firstChild);
          }
        });
      },
      before: function () {
        return ye(this, arguments, function (e) {
          this.parentNode && this.parentNode.insertBefore(e, this);
        });
      },
      after: function () {
        return ye(this, arguments, function (e) {
          this.parentNode && this.parentNode.insertBefore(e, this.nextSibling);
        });
      },
      empty: function () {
        for (var e, t = 0; null != (e = this[t]); t++)
          1 === e.nodeType && (f.cleanData(G(e, !1)), (e.textContent = ""));
        return this;
      },
      clone: function (e, t) {
        return (
          (e = null != e && e),
          (t = null == t ? e : t),
          this.map(function () {
            return f.clone(this, e, t);
          })
        );
      },
      html: function (e) {
        return j(
          this,
          function (e) {
            var t = this[0] || {},
              n = 0,
              i = this.length;
            if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
            if (
              "string" == typeof e &&
              !ce.test(e) &&
              !V[(K.exec(e) || ["", ""])[1].toLowerCase()]
            ) {
              e = f.htmlPrefilter(e);
              try {
                for (; i > n; n++)
                  1 === (t = this[n] || {}).nodeType &&
                    (f.cleanData(G(t, !1)), (t.innerHTML = e));
                t = 0;
              } catch (e) {}
            }
            t && this.empty().append(e);
          },
          null,
          e,
          arguments.length
        );
      },
      replaceWith: function () {
        var e = [];
        return ye(
          this,
          arguments,
          function (t) {
            var n = this.parentNode;
            f.inArray(this, e) < 0 &&
              (f.cleanData(G(this)), n && n.replaceChild(t, this));
          },
          e
        );
      },
    }),
    f.each(
      {
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith",
      },
      function (e, t) {
        f.fn[e] = function (e) {
          for (var n, i = [], o = f(e), r = o.length - 1, a = 0; r >= a; a++)
            (n = a === r ? this : this.clone(!0)),
              f(o[a])[t](n),
              s.apply(i, n.get());
          return this.pushStack(i);
        };
      }
    );
  var _e,
    we = { HTML: "block", BODY: "block" };
  function Te(e, t) {
    var n = f(t.createElement(e)).appendTo(t.body),
      i = f.css(n[0], "display");
    return n.detach(), i;
  }
  function Ce(e) {
    var t = i,
      n = we[e];
    return (
      n ||
        (("none" !== (n = Te(e, t)) && n) ||
          ((t = (_e = (
            _e || f("<iframe frameborder='0' width='0' height='0'/>")
          ).appendTo(t.documentElement))[0].contentDocument).write(),
          t.close(),
          (n = Te(e, t)),
          _e.detach()),
        (we[e] = n)),
      n
    );
  }
  var Se = /^margin/,
    ke = new RegExp("^(" + W + ")(?!px)[a-z%]+$", "i"),
    xe = function (t) {
      var n = t.ownerDocument.defaultView;
      return (n && n.opener) || (n = e), n.getComputedStyle(t);
    },
    Ee = function (e, t, n, i) {
      var o,
        r,
        s = {};
      for (r in t) (s[r] = e.style[r]), (e.style[r] = t[r]);
      for (r in ((o = n.apply(e, i || [])), t)) e.style[r] = s[r];
      return o;
    },
    Ae = i.documentElement;
  function De(e, t, n) {
    var i,
      o,
      r,
      s,
      a = e.style;
    return (
      ("" !== (s = (n = n || xe(e)) ? n.getPropertyValue(t) || n[t] : void 0) &&
        void 0 !== s) ||
        f.contains(e.ownerDocument, e) ||
        (s = f.style(e, t)),
      n &&
        !d.pixelMarginRight() &&
        ke.test(s) &&
        Se.test(t) &&
        ((i = a.width),
        (o = a.minWidth),
        (r = a.maxWidth),
        (a.minWidth = a.maxWidth = a.width = s),
        (s = n.width),
        (a.width = i),
        (a.minWidth = o),
        (a.maxWidth = r)),
      void 0 !== s ? s + "" : s
    );
  }
  function Ne(e, t) {
    return {
      get: function () {
        return e()
          ? void delete this.get
          : (this.get = t).apply(this, arguments);
      },
    };
  }
  !(function () {
    var t,
      n,
      o,
      r,
      s = i.createElement("div"),
      a = i.createElement("div");
    if (a.style) {
      function l() {
        (a.style.cssText =
          "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%"),
          (a.innerHTML = ""),
          Ae.appendChild(s);
        var i = e.getComputedStyle(a);
        (t = "1%" !== i.top),
          (r = "2px" === i.marginLeft),
          (n = "4px" === i.width),
          (a.style.marginRight = "50%"),
          (o = "4px" === i.marginRight),
          Ae.removeChild(s);
      }
      (a.style.backgroundClip = "content-box"),
        (a.cloneNode(!0).style.backgroundClip = ""),
        (d.clearCloneStyle = "content-box" === a.style.backgroundClip),
        (s.style.cssText =
          "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute"),
        s.appendChild(a),
        f.extend(d, {
          pixelPosition: function () {
            return l(), t;
          },
          boxSizingReliable: function () {
            return null == n && l(), n;
          },
          pixelMarginRight: function () {
            return null == n && l(), o;
          },
          reliableMarginLeft: function () {
            return null == n && l(), r;
          },
          reliableMarginRight: function () {
            var t,
              n = a.appendChild(i.createElement("div"));
            return (
              (n.style.cssText = a.style.cssText =
                "-webkit-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0"),
              (n.style.marginRight = n.style.width = "0"),
              (a.style.width = "1px"),
              Ae.appendChild(s),
              (t = !parseFloat(e.getComputedStyle(n).marginRight)),
              Ae.removeChild(s),
              a.removeChild(n),
              t
            );
          },
        });
    }
  })();
  var Ie = /^(none|table(?!-c[ea]).+)/,
    Oe = { position: "absolute", visibility: "hidden", display: "block" },
    $e = { letterSpacing: "0", fontWeight: "400" },
    je = ["Webkit", "O", "Moz", "ms"],
    Le = i.createElement("div").style;
  function Pe(e) {
    if (e in Le) return e;
    for (var t = e[0].toUpperCase() + e.slice(1), n = je.length; n--; )
      if ((e = je[n] + t) in Le) return e;
  }
  function He(e, t, n) {
    var i = z.exec(t);
    return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : t;
  }
  function qe(e, t, n, i, o) {
    for (
      var r = n === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0,
        s = 0;
      4 > r;
      r += 2
    )
      "margin" === n && (s += f.css(e, n + B[r], !0, o)),
        i
          ? ("content" === n && (s -= f.css(e, "padding" + B[r], !0, o)),
            "margin" !== n && (s -= f.css(e, "border" + B[r] + "Width", !0, o)))
          : ((s += f.css(e, "padding" + B[r], !0, o)),
            "padding" !== n &&
              (s += f.css(e, "border" + B[r] + "Width", !0, o)));
    return s;
  }
  function Re(e, t, n) {
    var i = !0,
      o = "width" === t ? e.offsetWidth : e.offsetHeight,
      r = xe(e),
      s = "border-box" === f.css(e, "boxSizing", !1, r);
    if (0 >= o || null == o) {
      if (
        ((0 > (o = De(e, t, r)) || null == o) && (o = e.style[t]), ke.test(o))
      )
        return o;
      (i = s && (d.boxSizingReliable() || o === e.style[t])),
        (o = parseFloat(o) || 0);
    }
    return o + qe(e, t, n || (s ? "border" : "content"), i, r) + "px";
  }
  function Me(e, t) {
    for (var n, i, o, r = [], s = 0, a = e.length; a > s; s++)
      (i = e[s]).style &&
        ((r[s] = H.get(i, "olddisplay")),
        (n = i.style.display),
        t
          ? (r[s] || "none" !== n || (i.style.display = ""),
            "" === i.style.display &&
              U(i) &&
              (r[s] = H.access(i, "olddisplay", Ce(i.nodeName))))
          : ((o = U(i)),
            ("none" === n && o) ||
              H.set(i, "olddisplay", o ? n : f.css(i, "display"))));
    for (s = 0; a > s; s++)
      (i = e[s]).style &&
        ((t && "none" !== i.style.display && "" !== i.style.display) ||
          (i.style.display = t ? r[s] || "" : "none"));
    return e;
  }
  function Fe(e, t, n, i, o) {
    return new Fe.prototype.init(e, t, n, i, o);
  }
  f.extend({
    cssHooks: {
      opacity: {
        get: function (e, t) {
          if (t) {
            var n = De(e, "opacity");
            return "" === n ? "1" : n;
          }
        },
      },
    },
    cssNumber: {
      animationIterationCount: !0,
      columnCount: !0,
      fillOpacity: !0,
      flexGrow: !0,
      flexShrink: !0,
      fontWeight: !0,
      lineHeight: !0,
      opacity: !0,
      order: !0,
      orphans: !0,
      widows: !0,
      zIndex: !0,
      zoom: !0,
    },
    cssProps: { float: "cssFloat" },
    style: function (e, t, n, i) {
      if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
        var o,
          r,
          s,
          a = f.camelCase(t),
          l = e.style;
        return (
          (t = f.cssProps[a] || (f.cssProps[a] = Pe(a) || a)),
          (s = f.cssHooks[t] || f.cssHooks[a]),
          void 0 === n
            ? s && "get" in s && void 0 !== (o = s.get(e, !1, i))
              ? o
              : l[t]
            : ("string" === (r = typeof n) &&
                (o = z.exec(n)) &&
                o[1] &&
                ((n = X(e, t, o)), (r = "number")),
              void (
                null != n &&
                n == n &&
                ("number" === r &&
                  (n += (o && o[3]) || (f.cssNumber[a] ? "" : "px")),
                d.clearCloneStyle ||
                  "" !== n ||
                  0 !== t.indexOf("background") ||
                  (l[t] = "inherit"),
                (s && "set" in s && void 0 === (n = s.set(e, n, i))) ||
                  (l[t] = n))
              ))
        );
      }
    },
    css: function (e, t, n, i) {
      var o,
        r,
        s,
        a = f.camelCase(t);
      return (
        (t = f.cssProps[a] || (f.cssProps[a] = Pe(a) || a)),
        (s = f.cssHooks[t] || f.cssHooks[a]) &&
          "get" in s &&
          (o = s.get(e, !0, n)),
        void 0 === o && (o = De(e, t, i)),
        "normal" === o && t in $e && (o = $e[t]),
        "" === n || n
          ? ((r = parseFloat(o)), !0 === n || isFinite(r) ? r || 0 : o)
          : o
      );
    },
  }),
    f.each(["height", "width"], function (e, t) {
      f.cssHooks[t] = {
        get: function (e, n, i) {
          return n
            ? Ie.test(f.css(e, "display")) && 0 === e.offsetWidth
              ? Ee(e, Oe, function () {
                  return Re(e, t, i);
                })
              : Re(e, t, i)
            : void 0;
        },
        set: function (e, n, i) {
          var o,
            r = i && xe(e),
            s =
              i &&
              qe(e, t, i, "border-box" === f.css(e, "boxSizing", !1, r), r);
          return (
            s &&
              (o = z.exec(n)) &&
              "px" !== (o[3] || "px") &&
              ((e.style[t] = n), (n = f.css(e, t))),
            He(0, n, s)
          );
        },
      };
    }),
    (f.cssHooks.marginLeft = Ne(d.reliableMarginLeft, function (e, t) {
      return t
        ? (parseFloat(De(e, "marginLeft")) ||
            e.getBoundingClientRect().left -
              Ee(e, { marginLeft: 0 }, function () {
                return e.getBoundingClientRect().left;
              })) + "px"
        : void 0;
    })),
    (f.cssHooks.marginRight = Ne(d.reliableMarginRight, function (e, t) {
      return t
        ? Ee(e, { display: "inline-block" }, De, [e, "marginRight"])
        : void 0;
    })),
    f.each({ margin: "", padding: "", border: "Width" }, function (e, t) {
      (f.cssHooks[e + t] = {
        expand: function (n) {
          for (
            var i = 0, o = {}, r = "string" == typeof n ? n.split(" ") : [n];
            4 > i;
            i++
          )
            o[e + B[i] + t] = r[i] || r[i - 2] || r[0];
          return o;
        },
      }),
        Se.test(e) || (f.cssHooks[e + t].set = He);
    }),
    f.fn.extend({
      css: function (e, t) {
        return j(
          this,
          function (e, t, n) {
            var i,
              o,
              r = {},
              s = 0;
            if (f.isArray(t)) {
              for (i = xe(e), o = t.length; o > s; s++)
                r[t[s]] = f.css(e, t[s], !1, i);
              return r;
            }
            return void 0 !== n ? f.style(e, t, n) : f.css(e, t);
          },
          e,
          t,
          arguments.length > 1
        );
      },
      show: function () {
        return Me(this, !0);
      },
      hide: function () {
        return Me(this);
      },
      toggle: function (e) {
        return "boolean" == typeof e
          ? e
            ? this.show()
            : this.hide()
          : this.each(function () {
              U(this) ? f(this).show() : f(this).hide();
            });
      },
    }),
    (f.Tween = Fe),
    (Fe.prototype = {
      constructor: Fe,
      init: function (e, t, n, i, o, r) {
        (this.elem = e),
          (this.prop = n),
          (this.easing = o || f.easing._default),
          (this.options = t),
          (this.start = this.now = this.cur()),
          (this.end = i),
          (this.unit = r || (f.cssNumber[n] ? "" : "px"));
      },
      cur: function () {
        var e = Fe.propHooks[this.prop];
        return e && e.get ? e.get(this) : Fe.propHooks._default.get(this);
      },
      run: function (e) {
        var t,
          n = Fe.propHooks[this.prop];
        return (
          this.options.duration
            ? (this.pos = t = f.easing[this.easing](
                e,
                this.options.duration * e,
                0,
                1,
                this.options.duration
              ))
            : (this.pos = t = e),
          (this.now = (this.end - this.start) * t + this.start),
          this.options.step &&
            this.options.step.call(this.elem, this.now, this),
          n && n.set ? n.set(this) : Fe.propHooks._default.set(this),
          this
        );
      },
    }),
    (Fe.prototype.init.prototype = Fe.prototype),
    (Fe.propHooks = {
      _default: {
        get: function (e) {
          var t;
          return 1 !== e.elem.nodeType ||
            (null != e.elem[e.prop] && null == e.elem.style[e.prop])
            ? e.elem[e.prop]
            : (t = f.css(e.elem, e.prop, "")) && "auto" !== t
            ? t
            : 0;
        },
        set: function (e) {
          f.fx.step[e.prop]
            ? f.fx.step[e.prop](e)
            : 1 !== e.elem.nodeType ||
              (null == e.elem.style[f.cssProps[e.prop]] && !f.cssHooks[e.prop])
            ? (e.elem[e.prop] = e.now)
            : f.style(e.elem, e.prop, e.now + e.unit);
        },
      },
    }),
    (Fe.propHooks.scrollTop = Fe.propHooks.scrollLeft = {
      set: function (e) {
        e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now);
      },
    }),
    (f.easing = {
      linear: function (e) {
        return e;
      },
      swing: function (e) {
        return 0.5 - Math.cos(e * Math.PI) / 2;
      },
      _default: "swing",
    }),
    (f.fx = Fe.prototype.init),
    (f.fx.step = {});
  var We,
    ze,
    Be = /^(?:toggle|show|hide)$/,
    Ue = /queueHooks$/;
  function Xe() {
    return (
      e.setTimeout(function () {
        We = void 0;
      }),
      (We = f.now())
    );
  }
  function Qe(e, t) {
    var n,
      i = 0,
      o = { height: e };
    for (t = t ? 1 : 0; 4 > i; i += 2 - t)
      o["margin" + (n = B[i])] = o["padding" + n] = e;
    return t && (o.opacity = o.width = e), o;
  }
  function Ke(e, t, n) {
    for (
      var i,
        o = (Ye.tweeners[t] || []).concat(Ye.tweeners["*"]),
        r = 0,
        s = o.length;
      s > r;
      r++
    )
      if ((i = o[r].call(n, t, e))) return i;
  }
  function Ye(e, t, n) {
    var i,
      o,
      r = 0,
      s = Ye.prefilters.length,
      a = f.Deferred().always(function () {
        delete l.elem;
      }),
      l = function () {
        if (o) return !1;
        for (
          var t = We || Xe(),
            n = Math.max(0, c.startTime + c.duration - t),
            i = 1 - (n / c.duration || 0),
            r = 0,
            s = c.tweens.length;
          s > r;
          r++
        )
          c.tweens[r].run(i);
        return (
          a.notifyWith(e, [c, i, n]),
          1 > i && s ? n : (a.resolveWith(e, [c]), !1)
        );
      },
      c = a.promise({
        elem: e,
        props: f.extend({}, t),
        opts: f.extend(!0, { specialEasing: {}, easing: f.easing._default }, n),
        originalProperties: t,
        originalOptions: n,
        startTime: We || Xe(),
        duration: n.duration,
        tweens: [],
        createTween: function (t, n) {
          var i = f.Tween(
            e,
            c.opts,
            t,
            n,
            c.opts.specialEasing[t] || c.opts.easing
          );
          return c.tweens.push(i), i;
        },
        stop: function (t) {
          var n = 0,
            i = t ? c.tweens.length : 0;
          if (o) return this;
          for (o = !0; i > n; n++) c.tweens[n].run(1);
          return (
            t
              ? (a.notifyWith(e, [c, 1, 0]), a.resolveWith(e, [c, t]))
              : a.rejectWith(e, [c, t]),
            this
          );
        },
      }),
      u = c.props;
    for (
      (function (e, t) {
        var n, i, o, r, s;
        for (n in e)
          if (
            ((o = t[(i = f.camelCase(n))]),
            (r = e[n]),
            f.isArray(r) && ((o = r[1]), (r = e[n] = r[0])),
            n !== i && ((e[i] = r), delete e[n]),
            (s = f.cssHooks[i]) && ("expand" in s))
          )
            for (n in ((r = s.expand(r)), delete e[i], r))
              (n in e) || ((e[n] = r[n]), (t[n] = o));
          else t[i] = o;
      })(u, c.opts.specialEasing);
      s > r;
      r++
    )
      if ((i = Ye.prefilters[r].call(c, e, u, c.opts)))
        return (
          f.isFunction(i.stop) &&
            (f._queueHooks(c.elem, c.opts.queue).stop = f.proxy(i.stop, i)),
          i
        );
    return (
      f.map(u, Ke, c),
      f.isFunction(c.opts.start) && c.opts.start.call(e, c),
      f.fx.timer(f.extend(l, { elem: e, anim: c, queue: c.opts.queue })),
      c
        .progress(c.opts.progress)
        .done(c.opts.done, c.opts.complete)
        .fail(c.opts.fail)
        .always(c.opts.always)
    );
  }
  (f.Animation = f.extend(Ye, {
    tweeners: {
      "*": [
        function (e, t) {
          var n = this.createTween(e, t);
          return X(n.elem, e, z.exec(t), n), n;
        },
      ],
    },
    tweener: function (e, t) {
      f.isFunction(e) ? ((t = e), (e = ["*"])) : (e = e.match(O));
      for (var n, i = 0, o = e.length; o > i; i++)
        (n = e[i]),
          (Ye.tweeners[n] = Ye.tweeners[n] || []),
          Ye.tweeners[n].unshift(t);
    },
    prefilters: [
      function (e, t, n) {
        var i,
          o,
          r,
          s,
          a,
          l,
          c,
          u = this,
          d = {},
          p = e.style,
          h = e.nodeType && U(e),
          g = H.get(e, "fxshow");
        for (i in (n.queue ||
          (null == (a = f._queueHooks(e, "fx")).unqueued &&
            ((a.unqueued = 0),
            (l = a.empty.fire),
            (a.empty.fire = function () {
              a.unqueued || l();
            })),
          a.unqueued++,
          u.always(function () {
            u.always(function () {
              a.unqueued--, f.queue(e, "fx").length || a.empty.fire();
            });
          })),
        1 === e.nodeType &&
          ("height" in t || "width" in t) &&
          ((n.overflow = [p.overflow, p.overflowX, p.overflowY]),
          "inline" ===
            ("none" === (c = f.css(e, "display"))
              ? H.get(e, "olddisplay") || Ce(e.nodeName)
              : c) &&
            "none" === f.css(e, "float") &&
            (p.display = "inline-block")),
        n.overflow &&
          ((p.overflow = "hidden"),
          u.always(function () {
            (p.overflow = n.overflow[0]),
              (p.overflowX = n.overflow[1]),
              (p.overflowY = n.overflow[2]);
          })),
        t))
          if (((o = t[i]), Be.exec(o))) {
            if (
              (delete t[i],
              (r = r || "toggle" === o),
              o === (h ? "hide" : "show"))
            ) {
              if ("show" !== o || !g || void 0 === g[i]) continue;
              h = !0;
            }
            d[i] = (g && g[i]) || f.style(e, i);
          } else c = void 0;
        if (f.isEmptyObject(d))
          "inline" === ("none" === c ? Ce(e.nodeName) : c) && (p.display = c);
        else
          for (i in (g
            ? "hidden" in g && (h = g.hidden)
            : (g = H.access(e, "fxshow", {})),
          r && (g.hidden = !h),
          h
            ? f(e).show()
            : u.done(function () {
                f(e).hide();
              }),
          u.done(function () {
            var t;
            for (t in (H.remove(e, "fxshow"), d)) f.style(e, t, d[t]);
          }),
          d))
            (s = Ke(h ? g[i] : 0, i, u)),
              i in g ||
                ((g[i] = s.start),
                h &&
                  ((s.end = s.start),
                  (s.start = "width" === i || "height" === i ? 1 : 0)));
      },
    ],
    prefilter: function (e, t) {
      t ? Ye.prefilters.unshift(e) : Ye.prefilters.push(e);
    },
  })),
    (f.speed = function (e, t, n) {
      var i =
        e && "object" == typeof e
          ? f.extend({}, e)
          : {
              complete: n || (!n && t) || (f.isFunction(e) && e),
              duration: e,
              easing: (n && t) || (t && !f.isFunction(t) && t),
            };
      return (
        (i.duration = f.fx.off
          ? 0
          : "number" == typeof i.duration
          ? i.duration
          : i.duration in f.fx.speeds
          ? f.fx.speeds[i.duration]
          : f.fx.speeds._default),
        (null != i.queue && !0 !== i.queue) || (i.queue = "fx"),
        (i.old = i.complete),
        (i.complete = function () {
          f.isFunction(i.old) && i.old.call(this),
            i.queue && f.dequeue(this, i.queue);
        }),
        i
      );
    }),
    f.fn.extend({
      fadeTo: function (e, t, n, i) {
        return this.filter(U)
          .css("opacity", 0)
          .show()
          .end()
          .animate({ opacity: t }, e, n, i);
      },
      animate: function (e, t, n, i) {
        var o = f.isEmptyObject(e),
          r = f.speed(t, n, i),
          s = function () {
            var t = Ye(this, f.extend({}, e), r);
            (o || H.get(this, "finish")) && t.stop(!0);
          };
        return (
          (s.finish = s),
          o || !1 === r.queue ? this.each(s) : this.queue(r.queue, s)
        );
      },
      stop: function (e, t, n) {
        var i = function (e) {
          var t = e.stop;
          delete e.stop, t(n);
        };
        return (
          "string" != typeof e && ((n = t), (t = e), (e = void 0)),
          t && !1 !== e && this.queue(e || "fx", []),
          this.each(function () {
            var t = !0,
              o = null != e && e + "queueHooks",
              r = f.timers,
              s = H.get(this);
            if (o) s[o] && s[o].stop && i(s[o]);
            else for (o in s) s[o] && s[o].stop && Ue.test(o) && i(s[o]);
            for (o = r.length; o--; )
              r[o].elem !== this ||
                (null != e && r[o].queue !== e) ||
                (r[o].anim.stop(n), (t = !1), r.splice(o, 1));
            (!t && n) || f.dequeue(this, e);
          })
        );
      },
      finish: function (e) {
        return (
          !1 !== e && (e = e || "fx"),
          this.each(function () {
            var t,
              n = H.get(this),
              i = n[e + "queue"],
              o = n[e + "queueHooks"],
              r = f.timers,
              s = i ? i.length : 0;
            for (
              n.finish = !0,
                f.queue(this, e, []),
                o && o.stop && o.stop.call(this, !0),
                t = r.length;
              t--;

            )
              r[t].elem === this &&
                r[t].queue === e &&
                (r[t].anim.stop(!0), r.splice(t, 1));
            for (t = 0; s > t; t++)
              i[t] && i[t].finish && i[t].finish.call(this);
            delete n.finish;
          })
        );
      },
    }),
    f.each(["toggle", "show", "hide"], function (e, t) {
      var n = f.fn[t];
      f.fn[t] = function (e, i, o) {
        return null == e || "boolean" == typeof e
          ? n.apply(this, arguments)
          : this.animate(Qe(t, !0), e, i, o);
      };
    }),
    f.each(
      {
        slideDown: Qe("show"),
        slideUp: Qe("hide"),
        slideToggle: Qe("toggle"),
        fadeIn: { opacity: "show" },
        fadeOut: { opacity: "hide" },
        fadeToggle: { opacity: "toggle" },
      },
      function (e, t) {
        f.fn[e] = function (e, n, i) {
          return this.animate(t, e, n, i);
        };
      }
    ),
    (f.timers = []),
    (f.fx.tick = function () {
      var e,
        t = 0,
        n = f.timers;
      for (We = f.now(); t < n.length; t++)
        (e = n[t])() || n[t] !== e || n.splice(t--, 1);
      n.length || f.fx.stop(), (We = void 0);
    }),
    (f.fx.timer = function (e) {
      f.timers.push(e), e() ? f.fx.start() : f.timers.pop();
    }),
    (f.fx.interval = 13),
    (f.fx.start = function () {
      ze || (ze = e.setInterval(f.fx.tick, f.fx.interval));
    }),
    (f.fx.stop = function () {
      e.clearInterval(ze), (ze = null);
    }),
    (f.fx.speeds = { slow: 600, fast: 200, _default: 400 }),
    (f.fn.delay = function (t, n) {
      return (
        (t = (f.fx && f.fx.speeds[t]) || t),
        (n = n || "fx"),
        this.queue(n, function (n, i) {
          var o = e.setTimeout(n, t);
          i.stop = function () {
            e.clearTimeout(o);
          };
        })
      );
    }),
    (function () {
      var e = i.createElement("input"),
        t = i.createElement("select"),
        n = t.appendChild(i.createElement("option"));
      (e.type = "checkbox"),
        (d.checkOn = "" !== e.value),
        (d.optSelected = n.selected),
        (t.disabled = !0),
        (d.optDisabled = !n.disabled),
        ((e = i.createElement("input")).value = "t"),
        (e.type = "radio"),
        (d.radioValue = "t" === e.value);
    })();
  var Ve,
    Ge = f.expr.attrHandle;
  f.fn.extend({
    attr: function (e, t) {
      return j(this, f.attr, e, t, arguments.length > 1);
    },
    removeAttr: function (e) {
      return this.each(function () {
        f.removeAttr(this, e);
      });
    },
  }),
    f.extend({
      attr: function (e, t, n) {
        var i,
          o,
          r = e.nodeType;
        if (3 !== r && 8 !== r && 2 !== r)
          return void 0 === e.getAttribute
            ? f.prop(e, t, n)
            : ((1 === r && f.isXMLDoc(e)) ||
                ((t = t.toLowerCase()),
                (o =
                  f.attrHooks[t] || (f.expr.match.bool.test(t) ? Ve : void 0))),
              void 0 !== n
                ? null === n
                  ? void f.removeAttr(e, t)
                  : o && "set" in o && void 0 !== (i = o.set(e, n, t))
                  ? i
                  : (e.setAttribute(t, n + ""), n)
                : o && "get" in o && null !== (i = o.get(e, t))
                ? i
                : null == (i = f.find.attr(e, t))
                ? void 0
                : i);
      },
      attrHooks: {
        type: {
          set: function (e, t) {
            if (!d.radioValue && "radio" === t && f.nodeName(e, "input")) {
              var n = e.value;
              return e.setAttribute("type", t), n && (e.value = n), t;
            }
          },
        },
      },
      removeAttr: function (e, t) {
        var n,
          i,
          o = 0,
          r = t && t.match(O);
        if (r && 1 === e.nodeType)
          for (; (n = r[o++]); )
            (i = f.propFix[n] || n),
              f.expr.match.bool.test(n) && (e[i] = !1),
              e.removeAttribute(n);
      },
    }),
    (Ve = {
      set: function (e, t, n) {
        return !1 === t ? f.removeAttr(e, n) : e.setAttribute(n, n), n;
      },
    }),
    f.each(f.expr.match.bool.source.match(/\w+/g), function (e, t) {
      var n = Ge[t] || f.find.attr;
      Ge[t] = function (e, t, i) {
        var o, r;
        return (
          i ||
            ((r = Ge[t]),
            (Ge[t] = o),
            (o = null != n(e, t, i) ? t.toLowerCase() : null),
            (Ge[t] = r)),
          o
        );
      };
    });
  var Je = /^(?:input|select|textarea|button)$/i,
    Ze = /^(?:a|area)$/i;
  f.fn.extend({
    prop: function (e, t) {
      return j(this, f.prop, e, t, arguments.length > 1);
    },
    removeProp: function (e) {
      return this.each(function () {
        delete this[f.propFix[e] || e];
      });
    },
  }),
    f.extend({
      prop: function (e, t, n) {
        var i,
          o,
          r = e.nodeType;
        if (3 !== r && 8 !== r && 2 !== r)
          return (
            (1 === r && f.isXMLDoc(e)) ||
              ((t = f.propFix[t] || t), (o = f.propHooks[t])),
            void 0 !== n
              ? o && "set" in o && void 0 !== (i = o.set(e, n, t))
                ? i
                : (e[t] = n)
              : o && "get" in o && null !== (i = o.get(e, t))
              ? i
              : e[t]
          );
      },
      propHooks: {
        tabIndex: {
          get: function (e) {
            var t = f.find.attr(e, "tabindex");
            return t
              ? parseInt(t, 10)
              : Je.test(e.nodeName) || (Ze.test(e.nodeName) && e.href)
              ? 0
              : -1;
          },
        },
      },
      propFix: { for: "htmlFor", class: "className" },
    }),
    d.optSelected ||
      (f.propHooks.selected = {
        get: function (e) {
          var t = e.parentNode;
          return t && t.parentNode && t.parentNode.selectedIndex, null;
        },
        set: function (e) {
          var t = e.parentNode;
          t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex);
        },
      }),
    f.each(
      [
        "tabIndex",
        "readOnly",
        "maxLength",
        "cellSpacing",
        "cellPadding",
        "rowSpan",
        "colSpan",
        "useMap",
        "frameBorder",
        "contentEditable",
      ],
      function () {
        f.propFix[this.toLowerCase()] = this;
      }
    );
  var et = /[\t\r\n\f]/g;
  function tt(e) {
    return (e.getAttribute && e.getAttribute("class")) || "";
  }
  f.fn.extend({
    addClass: function (e) {
      var t,
        n,
        i,
        o,
        r,
        s,
        a,
        l = 0;
      if (f.isFunction(e))
        return this.each(function (t) {
          f(this).addClass(e.call(this, t, tt(this)));
        });
      if ("string" == typeof e && e)
        for (t = e.match(O) || []; (n = this[l++]); )
          if (
            ((o = tt(n)),
            (i = 1 === n.nodeType && (" " + o + " ").replace(et, " ")))
          ) {
            for (s = 0; (r = t[s++]); )
              i.indexOf(" " + r + " ") < 0 && (i += r + " ");
            o !== (a = f.trim(i)) && n.setAttribute("class", a);
          }
      return this;
    },
    removeClass: function (e) {
      var t,
        n,
        i,
        o,
        r,
        s,
        a,
        l = 0;
      if (f.isFunction(e))
        return this.each(function (t) {
          f(this).removeClass(e.call(this, t, tt(this)));
        });
      if (!arguments.length) return this.attr("class", "");
      if ("string" == typeof e && e)
        for (t = e.match(O) || []; (n = this[l++]); )
          if (
            ((o = tt(n)),
            (i = 1 === n.nodeType && (" " + o + " ").replace(et, " ")))
          ) {
            for (s = 0; (r = t[s++]); )
              for (; i.indexOf(" " + r + " ") > -1; )
                i = i.replace(" " + r + " ", " ");
            o !== (a = f.trim(i)) && n.setAttribute("class", a);
          }
      return this;
    },
    toggleClass: function (e, t) {
      var n = typeof e;
      return "boolean" == typeof t && "string" === n
        ? t
          ? this.addClass(e)
          : this.removeClass(e)
        : f.isFunction(e)
        ? this.each(function (n) {
            f(this).toggleClass(e.call(this, n, tt(this), t), t);
          })
        : this.each(function () {
            var t, i, o, r;
            if ("string" === n)
              for (i = 0, o = f(this), r = e.match(O) || []; (t = r[i++]); )
                o.hasClass(t) ? o.removeClass(t) : o.addClass(t);
            else
              (void 0 !== e && "boolean" !== n) ||
                ((t = tt(this)) && H.set(this, "__className__", t),
                this.setAttribute &&
                  this.setAttribute(
                    "class",
                    t || !1 === e ? "" : H.get(this, "__className__") || ""
                  ));
          });
    },
    hasClass: function (e) {
      var t,
        n,
        i = 0;
      for (t = " " + e + " "; (n = this[i++]); )
        if (
          1 === n.nodeType &&
          (" " + tt(n) + " ").replace(et, " ").indexOf(t) > -1
        )
          return !0;
      return !1;
    },
  });
  var nt = /\r/g,
    it = /[\x20\t\r\n\f]+/g;
  f.fn.extend({
    val: function (e) {
      var t,
        n,
        i,
        o = this[0];
      return arguments.length
        ? ((i = f.isFunction(e)),
          this.each(function (n) {
            var o;
            1 === this.nodeType &&
              (null == (o = i ? e.call(this, n, f(this).val()) : e)
                ? (o = "")
                : "number" == typeof o
                ? (o += "")
                : f.isArray(o) &&
                  (o = f.map(o, function (e) {
                    return null == e ? "" : e + "";
                  })),
              ((t =
                f.valHooks[this.type] ||
                f.valHooks[this.nodeName.toLowerCase()]) &&
                "set" in t &&
                void 0 !== t.set(this, o, "value")) ||
                (this.value = o));
          }))
        : o
        ? (t = f.valHooks[o.type] || f.valHooks[o.nodeName.toLowerCase()]) &&
          "get" in t &&
          void 0 !== (n = t.get(o, "value"))
          ? n
          : "string" == typeof (n = o.value)
          ? n.replace(nt, "")
          : null == n
          ? ""
          : n
        : void 0;
    },
  }),
    f.extend({
      valHooks: {
        option: {
          get: function (e) {
            var t = f.find.attr(e, "value");
            return null != t ? t : f.trim(f.text(e)).replace(it, " ");
          },
        },
        select: {
          get: function (e) {
            for (
              var t,
                n,
                i = e.options,
                o = e.selectedIndex,
                r = "select-one" === e.type || 0 > o,
                s = r ? null : [],
                a = r ? o + 1 : i.length,
                l = 0 > o ? a : r ? o : 0;
              a > l;
              l++
            )
              if (
                ((n = i[l]).selected || l === o) &&
                (d.optDisabled
                  ? !n.disabled
                  : null === n.getAttribute("disabled")) &&
                (!n.parentNode.disabled ||
                  !f.nodeName(n.parentNode, "optgroup"))
              ) {
                if (((t = f(n).val()), r)) return t;
                s.push(t);
              }
            return s;
          },
          set: function (e, t) {
            for (
              var n, i, o = e.options, r = f.makeArray(t), s = o.length;
              s--;

            )
              ((i = o[s]).selected =
                f.inArray(f.valHooks.option.get(i), r) > -1) && (n = !0);
            return n || (e.selectedIndex = -1), r;
          },
        },
      },
    }),
    f.each(["radio", "checkbox"], function () {
      (f.valHooks[this] = {
        set: function (e, t) {
          return f.isArray(t)
            ? (e.checked = f.inArray(f(e).val(), t) > -1)
            : void 0;
        },
      }),
        d.checkOn ||
          (f.valHooks[this].get = function (e) {
            return null === e.getAttribute("value") ? "on" : e.value;
          });
    });
  var ot = /^(?:focusinfocus|focusoutblur)$/;
  f.extend(f.event, {
    trigger: function (t, n, o, r) {
      var s,
        a,
        l,
        c,
        d,
        p,
        h,
        g = [o || i],
        m = u.call(t, "type") ? t.type : t,
        v = u.call(t, "namespace") ? t.namespace.split(".") : [];
      if (
        ((a = l = o = o || i),
        3 !== o.nodeType &&
          8 !== o.nodeType &&
          !ot.test(m + f.event.triggered) &&
          (m.indexOf(".") > -1 &&
            ((v = m.split(".")), (m = v.shift()), v.sort()),
          (d = m.indexOf(":") < 0 && "on" + m),
          ((t = t[f.expando]
            ? t
            : new f.Event(m, "object" == typeof t && t)).isTrigger = r ? 2 : 3),
          (t.namespace = v.join(".")),
          (t.rnamespace = t.namespace
            ? new RegExp("(^|\\.)" + v.join("\\.(?:.*\\.|)") + "(\\.|$)")
            : null),
          (t.result = void 0),
          t.target || (t.target = o),
          (n = null == n ? [t] : f.makeArray(n, [t])),
          (h = f.event.special[m] || {}),
          r || !h.trigger || !1 !== h.trigger.apply(o, n)))
      ) {
        if (!r && !h.noBubble && !f.isWindow(o)) {
          for (
            c = h.delegateType || m, ot.test(c + m) || (a = a.parentNode);
            a;
            a = a.parentNode
          )
            g.push(a), (l = a);
          l === (o.ownerDocument || i) &&
            g.push(l.defaultView || l.parentWindow || e);
        }
        for (s = 0; (a = g[s++]) && !t.isPropagationStopped(); )
          (t.type = s > 1 ? c : h.bindType || m),
            (p = (H.get(a, "events") || {})[t.type] && H.get(a, "handle")) &&
              p.apply(a, n),
            (p = d && a[d]) &&
              p.apply &&
              L(a) &&
              ((t.result = p.apply(a, n)),
              !1 === t.result && t.preventDefault());
        return (
          (t.type = m),
          r ||
            t.isDefaultPrevented() ||
            (h._default && !1 !== h._default.apply(g.pop(), n)) ||
            !L(o) ||
            (d &&
              f.isFunction(o[m]) &&
              !f.isWindow(o) &&
              ((l = o[d]) && (o[d] = null),
              (f.event.triggered = m),
              o[m](),
              (f.event.triggered = void 0),
              l && (o[d] = l))),
          t.result
        );
      }
    },
    simulate: function (e, t, n) {
      var i = f.extend(new f.Event(), n, { type: e, isSimulated: !0 });
      f.event.trigger(i, null, t);
    },
  }),
    f.fn.extend({
      trigger: function (e, t) {
        return this.each(function () {
          f.event.trigger(e, t, this);
        });
      },
      triggerHandler: function (e, t) {
        var n = this[0];
        return n ? f.event.trigger(e, t, n, !0) : void 0;
      },
    }),
    f.each(
      "blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(
        " "
      ),
      function (e, t) {
        f.fn[t] = function (e, n) {
          return arguments.length > 0
            ? this.on(t, null, e, n)
            : this.trigger(t);
        };
      }
    ),
    f.fn.extend({
      hover: function (e, t) {
        return this.mouseenter(e).mouseleave(t || e);
      },
    }),
    (d.focusin = "onfocusin" in e),
    d.focusin ||
      f.each({ focus: "focusin", blur: "focusout" }, function (e, t) {
        var n = function (e) {
          f.event.simulate(t, e.target, f.event.fix(e));
        };
        f.event.special[t] = {
          setup: function () {
            var i = this.ownerDocument || this,
              o = H.access(i, t);
            o || i.addEventListener(e, n, !0), H.access(i, t, (o || 0) + 1);
          },
          teardown: function () {
            var i = this.ownerDocument || this,
              o = H.access(i, t) - 1;
            o
              ? H.access(i, t, o)
              : (i.removeEventListener(e, n, !0), H.remove(i, t));
          },
        };
      });
  var rt = e.location,
    st = f.now(),
    at = /\?/;
  (f.parseJSON = function (e) {
    return JSON.parse(e + "");
  }),
    (f.parseXML = function (t) {
      var n;
      if (!t || "string" != typeof t) return null;
      try {
        n = new e.DOMParser().parseFromString(t, "text/xml");
      } catch (e) {
        n = void 0;
      }
      return (
        (n && !n.getElementsByTagName("parsererror").length) ||
          f.error("Invalid XML: " + t),
        n
      );
    });
  var lt = /#.*$/,
    ct = /([?&])_=[^&]*/,
    ut = /^(.*?):[ \t]*([^\r\n]*)$/gm,
    dt = /^(?:GET|HEAD)$/,
    pt = /^\/\//,
    ft = {},
    ht = {},
    gt = "*/".concat("*"),
    mt = i.createElement("a");
  function vt(e) {
    return function (t, n) {
      "string" != typeof t && ((n = t), (t = "*"));
      var i,
        o = 0,
        r = t.toLowerCase().match(O) || [];
      if (f.isFunction(n))
        for (; (i = r[o++]); )
          "+" === i[0]
            ? ((i = i.slice(1) || "*"), (e[i] = e[i] || []).unshift(n))
            : (e[i] = e[i] || []).push(n);
    };
  }
  function yt(e, t, n, i) {
    var o = {},
      r = e === ht;
    function s(a) {
      var l;
      return (
        (o[a] = !0),
        f.each(e[a] || [], function (e, a) {
          var c = a(t, n, i);
          return "string" != typeof c || r || o[c]
            ? r
              ? !(l = c)
              : void 0
            : (t.dataTypes.unshift(c), s(c), !1);
        }),
        l
      );
    }
    return s(t.dataTypes[0]) || (!o["*"] && s("*"));
  }
  function bt(e, t) {
    var n,
      i,
      o = f.ajaxSettings.flatOptions || {};
    for (n in t) void 0 !== t[n] && ((o[n] ? e : i || (i = {}))[n] = t[n]);
    return i && f.extend(!0, e, i), e;
  }
  (mt.href = rt.href),
    f.extend({
      active: 0,
      lastModified: {},
      etag: {},
      ajaxSettings: {
        url: rt.href,
        type: "GET",
        isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(
          rt.protocol
        ),
        global: !0,
        processData: !0,
        async: !0,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        accepts: {
          "*": gt,
          text: "text/plain",
          html: "text/html",
          xml: "application/xml, text/xml",
          json: "application/json, text/javascript",
        },
        contents: { xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/ },
        responseFields: {
          xml: "responseXML",
          text: "responseText",
          json: "responseJSON",
        },
        converters: {
          "* text": String,
          "text html": !0,
          "text json": f.parseJSON,
          "text xml": f.parseXML,
        },
        flatOptions: { url: !0, context: !0 },
      },
      ajaxSetup: function (e, t) {
        return t ? bt(bt(e, f.ajaxSettings), t) : bt(f.ajaxSettings, e);
      },
      ajaxPrefilter: vt(ft),
      ajaxTransport: vt(ht),
      ajax: function (t, n) {
        "object" == typeof t && ((n = t), (t = void 0)), (n = n || {});
        var o,
          r,
          s,
          a,
          l,
          c,
          u,
          d,
          p = f.ajaxSetup({}, n),
          h = p.context || p,
          g = p.context && (h.nodeType || h.jquery) ? f(h) : f.event,
          m = f.Deferred(),
          v = f.Callbacks("once memory"),
          y = p.statusCode || {},
          b = {},
          _ = {},
          w = 0,
          T = "canceled",
          C = {
            readyState: 0,
            getResponseHeader: function (e) {
              var t;
              if (2 === w) {
                if (!a)
                  for (a = {}; (t = ut.exec(s)); ) a[t[1].toLowerCase()] = t[2];
                t = a[e.toLowerCase()];
              }
              return null == t ? null : t;
            },
            getAllResponseHeaders: function () {
              return 2 === w ? s : null;
            },
            setRequestHeader: function (e, t) {
              var n = e.toLowerCase();
              return w || ((e = _[n] = _[n] || e), (b[e] = t)), this;
            },
            overrideMimeType: function (e) {
              return w || (p.mimeType = e), this;
            },
            statusCode: function (e) {
              var t;
              if (e)
                if (2 > w) for (t in e) y[t] = [y[t], e[t]];
                else C.always(e[C.status]);
              return this;
            },
            abort: function (e) {
              var t = e || T;
              return o && o.abort(t), S(0, t), this;
            },
          };
        if (
          ((m.promise(C).complete = v.add),
          (C.success = C.done),
          (C.error = C.fail),
          (p.url = ((t || p.url || rt.href) + "")
            .replace(lt, "")
            .replace(pt, rt.protocol + "//")),
          (p.type = n.method || n.type || p.method || p.type),
          (p.dataTypes = f
            .trim(p.dataType || "*")
            .toLowerCase()
            .match(O) || [""]),
          null == p.crossDomain)
        ) {
          c = i.createElement("a");
          try {
            (c.href = p.url),
              (c.href = c.href),
              (p.crossDomain =
                mt.protocol + "//" + mt.host != c.protocol + "//" + c.host);
          } catch (e) {
            p.crossDomain = !0;
          }
        }
        if (
          (p.data &&
            p.processData &&
            "string" != typeof p.data &&
            (p.data = f.param(p.data, p.traditional)),
          yt(ft, p, n, C),
          2 === w)
        )
          return C;
        for (d in ((u = f.event && p.global) &&
          0 == f.active++ &&
          f.event.trigger("ajaxStart"),
        (p.type = p.type.toUpperCase()),
        (p.hasContent = !dt.test(p.type)),
        (r = p.url),
        p.hasContent ||
          (p.data &&
            ((r = p.url += (at.test(r) ? "&" : "?") + p.data), delete p.data),
          !1 === p.cache &&
            (p.url = ct.test(r)
              ? r.replace(ct, "$1_=" + st++)
              : r + (at.test(r) ? "&" : "?") + "_=" + st++)),
        p.ifModified &&
          (f.lastModified[r] &&
            C.setRequestHeader("If-Modified-Since", f.lastModified[r]),
          f.etag[r] && C.setRequestHeader("If-None-Match", f.etag[r])),
        ((p.data && p.hasContent && !1 !== p.contentType) || n.contentType) &&
          C.setRequestHeader("Content-Type", p.contentType),
        C.setRequestHeader(
          "Accept",
          p.dataTypes[0] && p.accepts[p.dataTypes[0]]
            ? p.accepts[p.dataTypes[0]] +
                ("*" !== p.dataTypes[0] ? ", " + gt + "; q=0.01" : "")
            : p.accepts["*"]
        ),
        p.headers))
          C.setRequestHeader(d, p.headers[d]);
        if (p.beforeSend && (!1 === p.beforeSend.call(h, C, p) || 2 === w))
          return C.abort();
        for (d in ((T = "abort"), { success: 1, error: 1, complete: 1 }))
          C[d](p[d]);
        if ((o = yt(ht, p, n, C))) {
          if (((C.readyState = 1), u && g.trigger("ajaxSend", [C, p]), 2 === w))
            return C;
          p.async &&
            p.timeout > 0 &&
            (l = e.setTimeout(function () {
              C.abort("timeout");
            }, p.timeout));
          try {
            (w = 1), o.send(b, S);
          } catch (e) {
            if (!(2 > w)) throw e;
            S(-1, e);
          }
        } else S(-1, "No Transport");
        function S(t, n, i, a) {
          var c,
            d,
            b,
            _,
            T,
            S = n;
          2 !== w &&
            ((w = 2),
            l && e.clearTimeout(l),
            (o = void 0),
            (s = a || ""),
            (C.readyState = t > 0 ? 4 : 0),
            (c = (t >= 200 && 300 > t) || 304 === t),
            i &&
              (_ = (function (e, t, n) {
                for (
                  var i, o, r, s, a = e.contents, l = e.dataTypes;
                  "*" === l[0];

                )
                  l.shift(),
                    void 0 === i &&
                      (i = e.mimeType || t.getResponseHeader("Content-Type"));
                if (i)
                  for (o in a)
                    if (a[o] && a[o].test(i)) {
                      l.unshift(o);
                      break;
                    }
                if (l[0] in n) r = l[0];
                else {
                  for (o in n) {
                    if (!l[0] || e.converters[o + " " + l[0]]) {
                      r = o;
                      break;
                    }
                    s || (s = o);
                  }
                  r = r || s;
                }
                return r ? (r !== l[0] && l.unshift(r), n[r]) : void 0;
              })(p, C, i)),
            (_ = (function (e, t, n, i) {
              var o,
                r,
                s,
                a,
                l,
                c = {},
                u = e.dataTypes.slice();
              if (u[1])
                for (s in e.converters) c[s.toLowerCase()] = e.converters[s];
              for (r = u.shift(); r; )
                if (
                  (e.responseFields[r] && (n[e.responseFields[r]] = t),
                  !l && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)),
                  (l = r),
                  (r = u.shift()))
                )
                  if ("*" === r) r = l;
                  else if ("*" !== l && l !== r) {
                    if (!(s = c[l + " " + r] || c["* " + r]))
                      for (o in c)
                        if (
                          (a = o.split(" "))[1] === r &&
                          (s = c[l + " " + a[0]] || c["* " + a[0]])
                        ) {
                          !0 === s
                            ? (s = c[o])
                            : !0 !== c[o] && ((r = a[0]), u.unshift(a[1]));
                          break;
                        }
                    if (!0 !== s)
                      if (s && e.throws) t = s(t);
                      else
                        try {
                          t = s(t);
                        } catch (e) {
                          return {
                            state: "parsererror",
                            error: s
                              ? e
                              : "No conversion from " + l + " to " + r,
                          };
                        }
                  }
              return { state: "success", data: t };
            })(p, _, C, c)),
            c
              ? (p.ifModified &&
                  ((T = C.getResponseHeader("Last-Modified")) &&
                    (f.lastModified[r] = T),
                  (T = C.getResponseHeader("etag")) && (f.etag[r] = T)),
                204 === t || "HEAD" === p.type
                  ? (S = "nocontent")
                  : 304 === t
                  ? (S = "notmodified")
                  : ((S = _.state), (d = _.data), (c = !(b = _.error))))
              : ((b = S), (!t && S) || ((S = "error"), 0 > t && (t = 0))),
            (C.status = t),
            (C.statusText = (n || S) + ""),
            c ? m.resolveWith(h, [d, S, C]) : m.rejectWith(h, [C, S, b]),
            C.statusCode(y),
            (y = void 0),
            u && g.trigger(c ? "ajaxSuccess" : "ajaxError", [C, p, c ? d : b]),
            v.fireWith(h, [C, S]),
            u &&
              (g.trigger("ajaxComplete", [C, p]),
              --f.active || f.event.trigger("ajaxStop")));
        }
        return C;
      },
      getJSON: function (e, t, n) {
        return f.get(e, t, n, "json");
      },
      getScript: function (e, t) {
        return f.get(e, void 0, t, "script");
      },
    }),
    f.each(["get", "post"], function (e, t) {
      f[t] = function (e, n, i, o) {
        return (
          f.isFunction(n) && ((o = o || i), (i = n), (n = void 0)),
          f.ajax(
            f.extend(
              { url: e, type: t, dataType: o, data: n, success: i },
              f.isPlainObject(e) && e
            )
          )
        );
      };
    }),
    (f._evalUrl = function (e) {
      return f.ajax({
        url: e,
        type: "GET",
        dataType: "script",
        async: !1,
        global: !1,
        throws: !0,
      });
    }),
    f.fn.extend({
      wrapAll: function (e) {
        var t;
        return f.isFunction(e)
          ? this.each(function (t) {
              f(this).wrapAll(e.call(this, t));
            })
          : (this[0] &&
              ((t = f(e, this[0].ownerDocument).eq(0).clone(!0)),
              this[0].parentNode && t.insertBefore(this[0]),
              t
                .map(function () {
                  for (var e = this; e.firstElementChild; )
                    e = e.firstElementChild;
                  return e;
                })
                .append(this)),
            this);
      },
      wrapInner: function (e) {
        return f.isFunction(e)
          ? this.each(function (t) {
              f(this).wrapInner(e.call(this, t));
            })
          : this.each(function () {
              var t = f(this),
                n = t.contents();
              n.length ? n.wrapAll(e) : t.append(e);
            });
      },
      wrap: function (e) {
        var t = f.isFunction(e);
        return this.each(function (n) {
          f(this).wrapAll(t ? e.call(this, n) : e);
        });
      },
      unwrap: function () {
        return this.parent()
          .each(function () {
            f.nodeName(this, "body") || f(this).replaceWith(this.childNodes);
          })
          .end();
      },
    }),
    (f.expr.filters.hidden = function (e) {
      return !f.expr.filters.visible(e);
    }),
    (f.expr.filters.visible = function (e) {
      return (
        e.offsetWidth > 0 || e.offsetHeight > 0 || e.getClientRects().length > 0
      );
    });
  var _t = /%20/g,
    wt = /\[\]$/,
    Tt = /\r?\n/g,
    Ct = /^(?:submit|button|image|reset|file)$/i,
    St = /^(?:input|select|textarea|keygen)/i;
  function kt(e, t, n, i) {
    var o;
    if (f.isArray(t))
      f.each(t, function (t, o) {
        n || wt.test(e)
          ? i(e, o)
          : kt(
              e + "[" + ("object" == typeof o && null != o ? t : "") + "]",
              o,
              n,
              i
            );
      });
    else if (n || "object" !== f.type(t)) i(e, t);
    else for (o in t) kt(e + "[" + o + "]", t[o], n, i);
  }
  (f.param = function (e, t) {
    var n,
      i = [],
      o = function (e, t) {
        (t = f.isFunction(t) ? t() : null == t ? "" : t),
          (i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t));
      };
    if (
      (void 0 === t && (t = f.ajaxSettings && f.ajaxSettings.traditional),
      f.isArray(e) || (e.jquery && !f.isPlainObject(e)))
    )
      f.each(e, function () {
        o(this.name, this.value);
      });
    else for (n in e) kt(n, e[n], t, o);
    return i.join("&").replace(_t, "+");
  }),
    f.fn.extend({
      serialize: function () {
        return f.param(this.serializeArray());
      },
      serializeArray: function () {
        return this.map(function () {
          var e = f.prop(this, "elements");
          return e ? f.makeArray(e) : this;
        })
          .filter(function () {
            var e = this.type;
            return (
              this.name &&
              !f(this).is(":disabled") &&
              St.test(this.nodeName) &&
              !Ct.test(e) &&
              (this.checked || !Q.test(e))
            );
          })
          .map(function (e, t) {
            var n = f(this).val();
            return null == n
              ? null
              : f.isArray(n)
              ? f.map(n, function (e) {
                  return { name: t.name, value: e.replace(Tt, "\r\n") };
                })
              : { name: t.name, value: n.replace(Tt, "\r\n") };
          })
          .get();
      },
    }),
    (f.ajaxSettings.xhr = function () {
      try {
        return new e.XMLHttpRequest();
      } catch (e) {}
    });
  var xt = { 0: 200, 1223: 204 },
    Et = f.ajaxSettings.xhr();
  (d.cors = !!Et && "withCredentials" in Et),
    (d.ajax = Et = !!Et),
    f.ajaxTransport(function (t) {
      var n, i;
      return d.cors || (Et && !t.crossDomain)
        ? {
            send: function (o, r) {
              var s,
                a = t.xhr();
              if (
                (a.open(t.type, t.url, t.async, t.username, t.password),
                t.xhrFields)
              )
                for (s in t.xhrFields) a[s] = t.xhrFields[s];
              for (s in (t.mimeType &&
                a.overrideMimeType &&
                a.overrideMimeType(t.mimeType),
              t.crossDomain ||
                o["X-Requested-With"] ||
                (o["X-Requested-With"] = "XMLHttpRequest"),
              o))
                a.setRequestHeader(s, o[s]);
              (n = function (e) {
                return function () {
                  n &&
                    ((n = i = a.onload = a.onerror = a.onabort = a.onreadystatechange = null),
                    "abort" === e
                      ? a.abort()
                      : "error" === e
                      ? "number" != typeof a.status
                        ? r(0, "error")
                        : r(a.status, a.statusText)
                      : r(
                          xt[a.status] || a.status,
                          a.statusText,
                          "text" !== (a.responseType || "text") ||
                            "string" != typeof a.responseText
                            ? { binary: a.response }
                            : { text: a.responseText },
                          a.getAllResponseHeaders()
                        ));
                };
              }),
                (a.onload = n()),
                (i = a.onerror = n("error")),
                void 0 !== a.onabort
                  ? (a.onabort = i)
                  : (a.onreadystatechange = function () {
                      4 === a.readyState &&
                        e.setTimeout(function () {
                          n && i();
                        });
                    }),
                (n = n("abort"));
              try {
                a.send((t.hasContent && t.data) || null);
              } catch (e) {
                if (n) throw e;
              }
            },
            abort: function () {
              n && n();
            },
          }
        : void 0;
    }),
    f.ajaxSetup({
      accepts: {
        script:
          "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript",
      },
      contents: { script: /\b(?:java|ecma)script\b/ },
      converters: {
        "text script": function (e) {
          return f.globalEval(e), e;
        },
      },
    }),
    f.ajaxPrefilter("script", function (e) {
      void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET");
    }),
    f.ajaxTransport("script", function (e) {
      var t, n;
      if (e.crossDomain)
        return {
          send: function (o, r) {
            (t = f("<script>")
              .prop({ charset: e.scriptCharset, src: e.url })
              .on(
                "load error",
                (n = function (e) {
                  t.remove(),
                    (n = null),
                    e && r("error" === e.type ? 404 : 200, e.type);
                })
              )),
              i.head.appendChild(t[0]);
          },
          abort: function () {
            n && n();
          },
        };
    });
  var At = [],
    Dt = /(=)\?(?=&|$)|\?\?/;
  f.ajaxSetup({
    jsonp: "callback",
    jsonpCallback: function () {
      var e = At.pop() || f.expando + "_" + st++;
      return (this[e] = !0), e;
    },
  }),
    f.ajaxPrefilter("json jsonp", function (t, n, i) {
      var o,
        r,
        s,
        a =
          !1 !== t.jsonp &&
          (Dt.test(t.url)
            ? "url"
            : "string" == typeof t.data &&
              0 ===
                (t.contentType || "").indexOf(
                  "application/x-www-form-urlencoded"
                ) &&
              Dt.test(t.data) &&
              "data");
      return a || "jsonp" === t.dataTypes[0]
        ? ((o = t.jsonpCallback = f.isFunction(t.jsonpCallback)
            ? t.jsonpCallback()
            : t.jsonpCallback),
          a
            ? (t[a] = t[a].replace(Dt, "$1" + o))
            : !1 !== t.jsonp &&
              (t.url += (at.test(t.url) ? "&" : "?") + t.jsonp + "=" + o),
          (t.converters["script json"] = function () {
            return s || f.error(o + " was not called"), s[0];
          }),
          (t.dataTypes[0] = "json"),
          (r = e[o]),
          (e[o] = function () {
            s = arguments;
          }),
          i.always(function () {
            void 0 === r ? f(e).removeProp(o) : (e[o] = r),
              t[o] && ((t.jsonpCallback = n.jsonpCallback), At.push(o)),
              s && f.isFunction(r) && r(s[0]),
              (s = r = void 0);
          }),
          "script")
        : void 0;
    }),
    (f.parseHTML = function (e, t, n) {
      if (!e || "string" != typeof e) return null;
      "boolean" == typeof t && ((n = t), (t = !1)), (t = t || i);
      var o = C.exec(e),
        r = !n && [];
      return o
        ? [t.createElement(o[1])]
        : ((o = ee([e], t, r)),
          r && r.length && f(r).remove(),
          f.merge([], o.childNodes));
    });
  var Nt = f.fn.load;
  function It(e) {
    return f.isWindow(e) ? e : 9 === e.nodeType && e.defaultView;
  }
  (f.fn.load = function (e, t, n) {
    if ("string" != typeof e && Nt) return Nt.apply(this, arguments);
    var i,
      o,
      r,
      s = this,
      a = e.indexOf(" ");
    return (
      a > -1 && ((i = f.trim(e.slice(a))), (e = e.slice(0, a))),
      f.isFunction(t)
        ? ((n = t), (t = void 0))
        : t && "object" == typeof t && (o = "POST"),
      s.length > 0 &&
        f
          .ajax({ url: e, type: o || "GET", dataType: "html", data: t })
          .done(function (e) {
            (r = arguments),
              s.html(i ? f("<div>").append(f.parseHTML(e)).find(i) : e);
          })
          .always(
            n &&
              function (e, t) {
                s.each(function () {
                  n.apply(this, r || [e.responseText, t, e]);
                });
              }
          ),
      this
    );
  }),
    f.each(
      [
        "ajaxStart",
        "ajaxStop",
        "ajaxComplete",
        "ajaxError",
        "ajaxSuccess",
        "ajaxSend",
      ],
      function (e, t) {
        f.fn[t] = function (e) {
          return this.on(t, e);
        };
      }
    ),
    (f.expr.filters.animated = function (e) {
      return f.grep(f.timers, function (t) {
        return e === t.elem;
      }).length;
    }),
    (f.offset = {
      setOffset: function (e, t, n) {
        var i,
          o,
          r,
          s,
          a,
          l,
          c = f.css(e, "position"),
          u = f(e),
          d = {};
        "static" === c && (e.style.position = "relative"),
          (a = u.offset()),
          (r = f.css(e, "top")),
          (l = f.css(e, "left")),
          ("absolute" === c || "fixed" === c) && (r + l).indexOf("auto") > -1
            ? ((s = (i = u.position()).top), (o = i.left))
            : ((s = parseFloat(r) || 0), (o = parseFloat(l) || 0)),
          f.isFunction(t) && (t = t.call(e, n, f.extend({}, a))),
          null != t.top && (d.top = t.top - a.top + s),
          null != t.left && (d.left = t.left - a.left + o),
          "using" in t ? t.using.call(e, d) : u.css(d);
      },
    }),
    f.fn.extend({
      offset: function (e) {
        if (arguments.length)
          return void 0 === e
            ? this
            : this.each(function (t) {
                f.offset.setOffset(this, e, t);
              });
        var t,
          n,
          i = this[0],
          o = { top: 0, left: 0 },
          r = i && i.ownerDocument;
        return r
          ? ((t = r.documentElement),
            f.contains(t, i)
              ? ((o = i.getBoundingClientRect()),
                (n = It(r)),
                {
                  top: o.top + n.pageYOffset - t.clientTop,
                  left: o.left + n.pageXOffset - t.clientLeft,
                })
              : o)
          : void 0;
      },
      position: function () {
        if (this[0]) {
          var e,
            t,
            n = this[0],
            i = { top: 0, left: 0 };
          return (
            "fixed" === f.css(n, "position")
              ? (t = n.getBoundingClientRect())
              : ((e = this.offsetParent()),
                (t = this.offset()),
                f.nodeName(e[0], "html") || (i = e.offset()),
                (i.top += f.css(e[0], "borderTopWidth", !0)),
                (i.left += f.css(e[0], "borderLeftWidth", !0))),
            {
              top: t.top - i.top - f.css(n, "marginTop", !0),
              left: t.left - i.left - f.css(n, "marginLeft", !0),
            }
          );
        }
      },
      offsetParent: function () {
        return this.map(function () {
          for (
            var e = this.offsetParent;
            e && "static" === f.css(e, "position");

          )
            e = e.offsetParent;
          return e || Ae;
        });
      },
    }),
    f.each({ scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function (
      e,
      t
    ) {
      var n = "pageYOffset" === t;
      f.fn[e] = function (i) {
        return j(
          this,
          function (e, i, o) {
            var r = It(e);
            return void 0 === o
              ? r
                ? r[t]
                : e[i]
              : void (r
                  ? r.scrollTo(n ? r.pageXOffset : o, n ? o : r.pageYOffset)
                  : (e[i] = o));
          },
          e,
          i,
          arguments.length
        );
      };
    }),
    f.each(["top", "left"], function (e, t) {
      f.cssHooks[t] = Ne(d.pixelPosition, function (e, n) {
        return n
          ? ((n = De(e, t)), ke.test(n) ? f(e).position()[t] + "px" : n)
          : void 0;
      });
    }),
    f.each({ Height: "height", Width: "width" }, function (e, t) {
      f.each({ padding: "inner" + e, content: t, "": "outer" + e }, function (
        n,
        i
      ) {
        f.fn[i] = function (i, o) {
          var r = arguments.length && (n || "boolean" != typeof i),
            s = n || (!0 === i || !0 === o ? "margin" : "border");
          return j(
            this,
            function (t, n, i) {
              var o;
              return f.isWindow(t)
                ? t.document.documentElement["client" + e]
                : 9 === t.nodeType
                ? ((o = t.documentElement),
                  Math.max(
                    t.body["scroll" + e],
                    o["scroll" + e],
                    t.body["offset" + e],
                    o["offset" + e],
                    o["client" + e]
                  ))
                : void 0 === i
                ? f.css(t, n, s)
                : f.style(t, n, i, s);
            },
            t,
            r ? i : void 0,
            r,
            null
          );
        };
      });
    }),
    f.fn.extend({
      bind: function (e, t, n) {
        return this.on(e, null, t, n);
      },
      unbind: function (e, t) {
        return this.off(e, null, t);
      },
      delegate: function (e, t, n, i) {
        return this.on(t, e, n, i);
      },
      undelegate: function (e, t, n) {
        return 1 === arguments.length
          ? this.off(e, "**")
          : this.off(t, e || "**", n);
      },
      size: function () {
        return this.length;
      },
    }),
    (f.fn.andSelf = f.fn.addBack),
    "function" == typeof define &&
      define.amd &&
      define("jquery", [], function () {
        return f;
      });
  var Ot = e.jQuery,
    $t = e.$;
  return (
    (f.noConflict = function (t) {
      return e.$ === f && (e.$ = $t), t && e.jQuery === f && (e.jQuery = Ot), f;
    }),
    t || (e.jQuery = e.$ = f),
    f
  );
}),
  (function (e, t) {
    "object" == typeof exports && "undefined" != typeof module
      ? t(exports, require("jquery"), require("popper.js"))
      : "function" == typeof define && define.amd
      ? define(["exports", "jquery", "popper.js"], t)
      : t(((e = e || self).bootstrap = {}), e.jQuery, e.Popper);
  })(this, function (e, t, n) {
    "use strict";
    function i(e, t) {
      for (var n = 0; n < t.length; n++) {
        var i = t[n];
        (i.enumerable = i.enumerable || !1),
          (i.configurable = !0),
          "value" in i && (i.writable = !0),
          Object.defineProperty(e, i.key, i);
      }
    }
    function o(e, t, n) {
      return t && i(e.prototype, t), n && i(e, n), e;
    }
    function r(e, t) {
      var n = Object.keys(e);
      if (Object.getOwnPropertySymbols) {
        var i = Object.getOwnPropertySymbols(e);
        t &&
          (i = i.filter(function (t) {
            return Object.getOwnPropertyDescriptor(e, t).enumerable;
          })),
          n.push.apply(n, i);
      }
      return n;
    }
    function s(e) {
      for (var t = 1; t < arguments.length; t++) {
        var n = null != arguments[t] ? arguments[t] : {};
        t % 2
          ? r(Object(n), !0).forEach(function (t) {
              var i, o, r;
              (i = e),
                (r = n[(o = t)]),
                o in i
                  ? Object.defineProperty(i, o, {
                      value: r,
                      enumerable: !0,
                      configurable: !0,
                      writable: !0,
                    })
                  : (i[o] = r);
            })
          : Object.getOwnPropertyDescriptors
          ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n))
          : r(Object(n)).forEach(function (t) {
              Object.defineProperty(
                e,
                t,
                Object.getOwnPropertyDescriptor(n, t)
              );
            });
      }
      return e;
    }
    (t = t && t.hasOwnProperty("default") ? t.default : t),
      (n = n && n.hasOwnProperty("default") ? n.default : n);
    var a = "transitionend";
    var l = {
      TRANSITION_END: "bsTransitionEnd",
      getUID: function (e) {
        for (; (e += ~~(1e6 * Math.random())), document.getElementById(e); );
        return e;
      },
      getSelectorFromElement: function (e) {
        var t = e.getAttribute("data-target");
        if (!t || "#" === t) {
          var n = e.getAttribute("href");
          t = n && "#" !== n ? n.trim() : "";
        }
        try {
          return document.querySelector(t) ? t : null;
        } catch (e) {
          return null;
        }
      },
      getTransitionDurationFromElement: function (e) {
        if (!e) return 0;
        var n = t(e).css("transition-duration"),
          i = t(e).css("transition-delay"),
          o = parseFloat(n),
          r = parseFloat(i);
        return o || r
          ? ((n = n.split(",")[0]),
            (i = i.split(",")[0]),
            1e3 * (parseFloat(n) + parseFloat(i)))
          : 0;
      },
      reflow: function (e) {
        return e.offsetHeight;
      },
      triggerTransitionEnd: function (e) {
        t(e).trigger(a);
      },
      supportsTransitionEnd: function () {
        return Boolean(a);
      },
      isElement: function (e) {
        return (e[0] || e).nodeType;
      },
      typeCheckConfig: function (e, t, n) {
        for (var i in n)
          if (Object.prototype.hasOwnProperty.call(n, i)) {
            var o = n[i],
              r = t[i],
              s =
                r && l.isElement(r)
                  ? "element"
                  : ((a = r),
                    {}.toString
                      .call(a)
                      .match(/\s([a-z]+)/i)[1]
                      .toLowerCase());
            if (!new RegExp(o).test(s))
              throw new Error(
                e.toUpperCase() +
                  ': Option "' +
                  i +
                  '" provided type "' +
                  s +
                  '" but expected type "' +
                  o +
                  '".'
              );
          }
        var a;
      },
      findShadowRoot: function (e) {
        if (!document.documentElement.attachShadow) return null;
        if ("function" != typeof e.getRootNode)
          return e instanceof ShadowRoot
            ? e
            : e.parentNode
            ? l.findShadowRoot(e.parentNode)
            : null;
        var t = e.getRootNode();
        return t instanceof ShadowRoot ? t : null;
      },
      jQueryDetection: function () {
        if (void 0 === t)
          throw new TypeError(
            "Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript."
          );
        var e = t.fn.jquery.split(" ")[0].split(".");
        if (
          (e[0] < 2 && e[1] < 9) ||
          (1 === e[0] && 9 === e[1] && e[2] < 1) ||
          4 <= e[0]
        )
          throw new Error(
            "Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0"
          );
      },
    };
    l.jQueryDetection(),
      (t.fn.emulateTransitionEnd = function (e) {
        var n = this,
          i = !1;
        return (
          t(this).one(l.TRANSITION_END, function () {
            i = !0;
          }),
          setTimeout(function () {
            i || l.triggerTransitionEnd(n);
          }, e),
          this
        );
      }),
      (t.event.special[l.TRANSITION_END] = {
        bindType: a,
        delegateType: a,
        handle: function (e) {
          if (t(e.target).is(this))
            return e.handleObj.handler.apply(this, arguments);
        },
      });
    var c = "alert",
      u = "bs.alert",
      d = "." + u,
      p = t.fn[c],
      f = {
        CLOSE: "close" + d,
        CLOSED: "closed" + d,
        CLICK_DATA_API: "click" + d + ".data-api",
      },
      h = (function () {
        function e(e) {
          this._element = e;
        }
        var n = e.prototype;
        return (
          (n.close = function (e) {
            var t = this._element;
            e && (t = this._getRootElement(e)),
              this._triggerCloseEvent(t).isDefaultPrevented() ||
                this._removeElement(t);
          }),
          (n.dispose = function () {
            t.removeData(this._element, u), (this._element = null);
          }),
          (n._getRootElement = function (e) {
            var n = l.getSelectorFromElement(e),
              i = !1;
            return (
              n && (i = document.querySelector(n)),
              i || t(e).closest(".alert")[0]
            );
          }),
          (n._triggerCloseEvent = function (e) {
            var n = t.Event(f.CLOSE);
            return t(e).trigger(n), n;
          }),
          (n._removeElement = function (e) {
            var n = this;
            if ((t(e).removeClass("show"), t(e).hasClass("fade"))) {
              var i = l.getTransitionDurationFromElement(e);
              t(e)
                .one(l.TRANSITION_END, function (t) {
                  return n._destroyElement(e, t);
                })
                .emulateTransitionEnd(i);
            } else this._destroyElement(e);
          }),
          (n._destroyElement = function (e) {
            t(e).detach().trigger(f.CLOSED).remove();
          }),
          (e._jQueryInterface = function (n) {
            return this.each(function () {
              var i = t(this),
                o = i.data(u);
              o || ((o = new e(this)), i.data(u, o)),
                "close" === n && o[n](this);
            });
          }),
          (e._handleDismiss = function (e) {
            return function (t) {
              t && t.preventDefault(), e.close(this);
            };
          }),
          o(e, null, [
            {
              key: "VERSION",
              get: function () {
                return "4.4.1";
              },
            },
          ]),
          e
        );
      })();
    t(document).on(
      f.CLICK_DATA_API,
      '[data-dismiss="alert"]',
      h._handleDismiss(new h())
    ),
      (t.fn[c] = h._jQueryInterface),
      (t.fn[c].Constructor = h),
      (t.fn[c].noConflict = function () {
        return (t.fn[c] = p), h._jQueryInterface;
      });
    var g = "button",
      m = "bs.button",
      v = "." + m,
      y = ".data-api",
      b = t.fn[g],
      _ = "active",
      w = '[data-toggle^="button"]',
      T = 'input:not([type="hidden"])',
      C = ".btn",
      S = {
        CLICK_DATA_API: "click" + v + y,
        FOCUS_BLUR_DATA_API: "focus" + v + y + " blur" + v + y,
        LOAD_DATA_API: "load" + v + y,
      },
      k = (function () {
        function e(e) {
          this._element = e;
        }
        var n = e.prototype;
        return (
          (n.toggle = function () {
            var e = !0,
              n = !0,
              i = t(this._element).closest('[data-toggle="buttons"]')[0];
            if (i) {
              var o = this._element.querySelector(T);
              if (o) {
                if ("radio" === o.type)
                  if (o.checked && this._element.classList.contains(_)) e = !1;
                  else {
                    var r = i.querySelector(".active");
                    r && t(r).removeClass(_);
                  }
                else
                  "checkbox" === o.type
                    ? "LABEL" === this._element.tagName &&
                      o.checked === this._element.classList.contains(_) &&
                      (e = !1)
                    : (e = !1);
                e &&
                  ((o.checked = !this._element.classList.contains(_)),
                  t(o).trigger("change")),
                  o.focus(),
                  (n = !1);
              }
            }
            this._element.hasAttribute("disabled") ||
              this._element.classList.contains("disabled") ||
              (n &&
                this._element.setAttribute(
                  "aria-pressed",
                  !this._element.classList.contains(_)
                ),
              e && t(this._element).toggleClass(_));
          }),
          (n.dispose = function () {
            t.removeData(this._element, m), (this._element = null);
          }),
          (e._jQueryInterface = function (n) {
            return this.each(function () {
              var i = t(this).data(m);
              i || ((i = new e(this)), t(this).data(m, i)),
                "toggle" === n && i[n]();
            });
          }),
          o(e, null, [
            {
              key: "VERSION",
              get: function () {
                return "4.4.1";
              },
            },
          ]),
          e
        );
      })();
    t(document)
      .on(S.CLICK_DATA_API, w, function (e) {
        var n = e.target;
        if (
          (t(n).hasClass("btn") || (n = t(n).closest(C)[0]),
          !n || n.hasAttribute("disabled") || n.classList.contains("disabled"))
        )
          e.preventDefault();
        else {
          var i = n.querySelector(T);
          if (
            i &&
            (i.hasAttribute("disabled") || i.classList.contains("disabled"))
          )
            return void e.preventDefault();
          k._jQueryInterface.call(t(n), "toggle");
        }
      })
      .on(S.FOCUS_BLUR_DATA_API, w, function (e) {
        var n = t(e.target).closest(C)[0];
        t(n).toggleClass("focus", /^focus(in)?$/.test(e.type));
      }),
      t(window).on(S.LOAD_DATA_API, function () {
        for (
          var e = [].slice.call(
              document.querySelectorAll('[data-toggle="buttons"] .btn')
            ),
            t = 0,
            n = e.length;
          t < n;
          t++
        ) {
          var i = e[t],
            o = i.querySelector(T);
          o.checked || o.hasAttribute("checked")
            ? i.classList.add(_)
            : i.classList.remove(_);
        }
        for (
          var r = 0,
            s = (e = [].slice.call(
              document.querySelectorAll('[data-toggle="button"]')
            )).length;
          r < s;
          r++
        ) {
          var a = e[r];
          "true" === a.getAttribute("aria-pressed")
            ? a.classList.add(_)
            : a.classList.remove(_);
        }
      }),
      (t.fn[g] = k._jQueryInterface),
      (t.fn[g].Constructor = k),
      (t.fn[g].noConflict = function () {
        return (t.fn[g] = b), k._jQueryInterface;
      });
    var x = "carousel",
      E = "bs.carousel",
      A = "." + E,
      D = ".data-api",
      N = t.fn[x],
      I = {
        interval: 5e3,
        keyboard: !0,
        slide: !1,
        pause: "hover",
        wrap: !0,
        touch: !0,
      },
      O = {
        interval: "(number|boolean)",
        keyboard: "boolean",
        slide: "(boolean|string)",
        pause: "(string|boolean)",
        wrap: "boolean",
        touch: "boolean",
      },
      $ = "next",
      j = "prev",
      L = {
        SLIDE: "slide" + A,
        SLID: "slid" + A,
        KEYDOWN: "keydown" + A,
        MOUSEENTER: "mouseenter" + A,
        MOUSELEAVE: "mouseleave" + A,
        TOUCHSTART: "touchstart" + A,
        TOUCHMOVE: "touchmove" + A,
        TOUCHEND: "touchend" + A,
        POINTERDOWN: "pointerdown" + A,
        POINTERUP: "pointerup" + A,
        DRAG_START: "dragstart" + A,
        LOAD_DATA_API: "load" + A + D,
        CLICK_DATA_API: "click" + A + D,
      },
      P = "active",
      H = ".active.carousel-item",
      q = ".carousel-indicators",
      R = { TOUCH: "touch", PEN: "pen" },
      M = (function () {
        function e(e, t) {
          (this._items = null),
            (this._interval = null),
            (this._activeElement = null),
            (this._isPaused = !1),
            (this._isSliding = !1),
            (this.touchTimeout = null),
            (this.touchStartX = 0),
            (this.touchDeltaX = 0),
            (this._config = this._getConfig(t)),
            (this._element = e),
            (this._indicatorsElement = this._element.querySelector(q)),
            (this._touchSupported =
              "ontouchstart" in document.documentElement ||
              0 < navigator.maxTouchPoints),
            (this._pointerEvent = Boolean(
              window.PointerEvent || window.MSPointerEvent
            )),
            this._addEventListeners();
        }
        var n = e.prototype;
        return (
          (n.next = function () {
            this._isSliding || this._slide($);
          }),
          (n.nextWhenVisible = function () {
            !document.hidden &&
              t(this._element).is(":visible") &&
              "hidden" !== t(this._element).css("visibility") &&
              this.next();
          }),
          (n.prev = function () {
            this._isSliding || this._slide(j);
          }),
          (n.pause = function (e) {
            e || (this._isPaused = !0),
              this._element.querySelector(
                ".carousel-item-next, .carousel-item-prev"
              ) && (l.triggerTransitionEnd(this._element), this.cycle(!0)),
              clearInterval(this._interval),
              (this._interval = null);
          }),
          (n.cycle = function (e) {
            e || (this._isPaused = !1),
              this._interval &&
                (clearInterval(this._interval), (this._interval = null)),
              this._config.interval &&
                !this._isPaused &&
                (this._interval = setInterval(
                  (document.visibilityState
                    ? this.nextWhenVisible
                    : this.next
                  ).bind(this),
                  this._config.interval
                ));
          }),
          (n.to = function (e) {
            var n = this;
            this._activeElement = this._element.querySelector(H);
            var i = this._getItemIndex(this._activeElement);
            if (!(e > this._items.length - 1 || e < 0))
              if (this._isSliding)
                t(this._element).one(L.SLID, function () {
                  return n.to(e);
                });
              else {
                if (i === e) return this.pause(), void this.cycle();
                var o = i < e ? $ : j;
                this._slide(o, this._items[e]);
              }
          }),
          (n.dispose = function () {
            t(this._element).off(A),
              t.removeData(this._element, E),
              (this._items = null),
              (this._config = null),
              (this._element = null),
              (this._interval = null),
              (this._isPaused = null),
              (this._isSliding = null),
              (this._activeElement = null),
              (this._indicatorsElement = null);
          }),
          (n._getConfig = function (e) {
            return (e = s({}, I, {}, e)), l.typeCheckConfig(x, e, O), e;
          }),
          (n._handleSwipe = function () {
            var e = Math.abs(this.touchDeltaX);
            if (!(e <= 40)) {
              var t = e / this.touchDeltaX;
              (this.touchDeltaX = 0) < t && this.prev(), t < 0 && this.next();
            }
          }),
          (n._addEventListeners = function () {
            var e = this;
            this._config.keyboard &&
              t(this._element).on(L.KEYDOWN, function (t) {
                return e._keydown(t);
              }),
              "hover" === this._config.pause &&
                t(this._element)
                  .on(L.MOUSEENTER, function (t) {
                    return e.pause(t);
                  })
                  .on(L.MOUSELEAVE, function (t) {
                    return e.cycle(t);
                  }),
              this._config.touch && this._addTouchEventListeners();
          }),
          (n._addTouchEventListeners = function () {
            var e = this;
            if (this._touchSupported) {
              var n = function (t) {
                  e._pointerEvent &&
                  R[t.originalEvent.pointerType.toUpperCase()]
                    ? (e.touchStartX = t.originalEvent.clientX)
                    : e._pointerEvent ||
                      (e.touchStartX = t.originalEvent.touches[0].clientX);
                },
                i = function (t) {
                  e._pointerEvent &&
                    R[t.originalEvent.pointerType.toUpperCase()] &&
                    (e.touchDeltaX = t.originalEvent.clientX - e.touchStartX),
                    e._handleSwipe(),
                    "hover" === e._config.pause &&
                      (e.pause(),
                      e.touchTimeout && clearTimeout(e.touchTimeout),
                      (e.touchTimeout = setTimeout(function (t) {
                        return e.cycle(t);
                      }, 500 + e._config.interval)));
                };
              t(this._element.querySelectorAll(".carousel-item img")).on(
                L.DRAG_START,
                function (e) {
                  return e.preventDefault();
                }
              ),
                this._pointerEvent
                  ? (t(this._element).on(L.POINTERDOWN, function (e) {
                      return n(e);
                    }),
                    t(this._element).on(L.POINTERUP, function (e) {
                      return i(e);
                    }),
                    this._element.classList.add("pointer-event"))
                  : (t(this._element).on(L.TOUCHSTART, function (e) {
                      return n(e);
                    }),
                    t(this._element).on(L.TOUCHMOVE, function (t) {
                      return (function (t) {
                        t.originalEvent.touches &&
                        1 < t.originalEvent.touches.length
                          ? (e.touchDeltaX = 0)
                          : (e.touchDeltaX =
                              t.originalEvent.touches[0].clientX -
                              e.touchStartX);
                      })(t);
                    }),
                    t(this._element).on(L.TOUCHEND, function (e) {
                      return i(e);
                    }));
            }
          }),
          (n._keydown = function (e) {
            if (!/input|textarea/i.test(e.target.tagName))
              switch (e.which) {
                case 37:
                  e.preventDefault(), this.prev();
                  break;
                case 39:
                  e.preventDefault(), this.next();
              }
          }),
          (n._getItemIndex = function (e) {
            return (
              (this._items =
                e && e.parentNode
                  ? [].slice.call(
                      e.parentNode.querySelectorAll(".carousel-item")
                    )
                  : []),
              this._items.indexOf(e)
            );
          }),
          (n._getItemByDirection = function (e, t) {
            var n = e === $,
              i = e === j,
              o = this._getItemIndex(t),
              r = this._items.length - 1;
            if (((i && 0 === o) || (n && o === r)) && !this._config.wrap)
              return t;
            var s = (o + (e === j ? -1 : 1)) % this._items.length;
            return -1 == s
              ? this._items[this._items.length - 1]
              : this._items[s];
          }),
          (n._triggerSlideEvent = function (e, n) {
            var i = this._getItemIndex(e),
              o = this._getItemIndex(this._element.querySelector(H)),
              r = t.Event(L.SLIDE, {
                relatedTarget: e,
                direction: n,
                from: o,
                to: i,
              });
            return t(this._element).trigger(r), r;
          }),
          (n._setActiveIndicatorElement = function (e) {
            if (this._indicatorsElement) {
              var n = [].slice.call(
                this._indicatorsElement.querySelectorAll(".active")
              );
              t(n).removeClass(P);
              var i = this._indicatorsElement.children[this._getItemIndex(e)];
              i && t(i).addClass(P);
            }
          }),
          (n._slide = function (e, n) {
            var i,
              o,
              r,
              s = this,
              a = this._element.querySelector(H),
              c = this._getItemIndex(a),
              u = n || (a && this._getItemByDirection(e, a)),
              d = this._getItemIndex(u),
              p = Boolean(this._interval);
            if (
              ((r =
                e === $
                  ? ((i = "carousel-item-left"),
                    (o = "carousel-item-next"),
                    "left")
                  : ((i = "carousel-item-right"),
                    (o = "carousel-item-prev"),
                    "right")),
              u && t(u).hasClass(P))
            )
              this._isSliding = !1;
            else if (
              !this._triggerSlideEvent(u, r).isDefaultPrevented() &&
              a &&
              u
            ) {
              (this._isSliding = !0),
                p && this.pause(),
                this._setActiveIndicatorElement(u);
              var f = t.Event(L.SLID, {
                relatedTarget: u,
                direction: r,
                from: c,
                to: d,
              });
              if (t(this._element).hasClass("slide")) {
                t(u).addClass(o),
                  l.reflow(u),
                  t(a).addClass(i),
                  t(u).addClass(i);
                var h = parseInt(u.getAttribute("data-interval"), 10);
                h
                  ? ((this._config.defaultInterval =
                      this._config.defaultInterval || this._config.interval),
                    (this._config.interval = h))
                  : (this._config.interval =
                      this._config.defaultInterval || this._config.interval);
                var g = l.getTransitionDurationFromElement(a);
                t(a)
                  .one(l.TRANSITION_END, function () {
                    t(u)
                      .removeClass(i + " " + o)
                      .addClass(P),
                      t(a).removeClass(P + " " + o + " " + i),
                      (s._isSliding = !1),
                      setTimeout(function () {
                        return t(s._element).trigger(f);
                      }, 0);
                  })
                  .emulateTransitionEnd(g);
              } else
                t(a).removeClass(P),
                  t(u).addClass(P),
                  (this._isSliding = !1),
                  t(this._element).trigger(f);
              p && this.cycle();
            }
          }),
          (e._jQueryInterface = function (n) {
            return this.each(function () {
              var i = t(this).data(E),
                o = s({}, I, {}, t(this).data());
              "object" == typeof n && (o = s({}, o, {}, n));
              var r = "string" == typeof n ? n : o.slide;
              if (
                (i || ((i = new e(this, o)), t(this).data(E, i)),
                "number" == typeof n)
              )
                i.to(n);
              else if ("string" == typeof r) {
                if (void 0 === i[r])
                  throw new TypeError('No method named "' + r + '"');
                i[r]();
              } else o.interval && o.ride && (i.pause(), i.cycle());
            });
          }),
          (e._dataApiClickHandler = function (n) {
            var i = l.getSelectorFromElement(this);
            if (i) {
              var o = t(i)[0];
              if (o && t(o).hasClass("carousel")) {
                var r = s({}, t(o).data(), {}, t(this).data()),
                  a = this.getAttribute("data-slide-to");
                a && (r.interval = !1),
                  e._jQueryInterface.call(t(o), r),
                  a && t(o).data(E).to(a),
                  n.preventDefault();
              }
            }
          }),
          o(e, null, [
            {
              key: "VERSION",
              get: function () {
                return "4.4.1";
              },
            },
            {
              key: "Default",
              get: function () {
                return I;
              },
            },
          ]),
          e
        );
      })();
    t(document).on(
      L.CLICK_DATA_API,
      "[data-slide], [data-slide-to]",
      M._dataApiClickHandler
    ),
      t(window).on(L.LOAD_DATA_API, function () {
        for (
          var e = [].slice.call(
              document.querySelectorAll('[data-ride="carousel"]')
            ),
            n = 0,
            i = e.length;
          n < i;
          n++
        ) {
          var o = t(e[n]);
          M._jQueryInterface.call(o, o.data());
        }
      }),
      (t.fn[x] = M._jQueryInterface),
      (t.fn[x].Constructor = M),
      (t.fn[x].noConflict = function () {
        return (t.fn[x] = N), M._jQueryInterface;
      });
    var F = "collapse",
      W = "bs.collapse",
      z = "." + W,
      B = t.fn[F],
      U = { toggle: !0, parent: "" },
      X = { toggle: "boolean", parent: "(string|element)" },
      Q = {
        SHOW: "show" + z,
        SHOWN: "shown" + z,
        HIDE: "hide" + z,
        HIDDEN: "hidden" + z,
        CLICK_DATA_API: "click" + z + ".data-api",
      },
      K = "show",
      Y = "collapse",
      V = "collapsing",
      G = "collapsed",
      J = '[data-toggle="collapse"]',
      Z = (function () {
        function e(e, t) {
          (this._isTransitioning = !1),
            (this._element = e),
            (this._config = this._getConfig(t)),
            (this._triggerArray = [].slice.call(
              document.querySelectorAll(
                '[data-toggle="collapse"][href="#' +
                  e.id +
                  '"],[data-toggle="collapse"][data-target="#' +
                  e.id +
                  '"]'
              )
            ));
          for (
            var n = [].slice.call(document.querySelectorAll(J)),
              i = 0,
              o = n.length;
            i < o;
            i++
          ) {
            var r = n[i],
              s = l.getSelectorFromElement(r),
              a = [].slice
                .call(document.querySelectorAll(s))
                .filter(function (t) {
                  return t === e;
                });
            null !== s &&
              0 < a.length &&
              ((this._selector = s), this._triggerArray.push(r));
          }
          (this._parent = this._config.parent ? this._getParent() : null),
            this._config.parent ||
              this._addAriaAndCollapsedClass(this._element, this._triggerArray),
            this._config.toggle && this.toggle();
        }
        var n = e.prototype;
        return (
          (n.toggle = function () {
            t(this._element).hasClass(K) ? this.hide() : this.show();
          }),
          (n.show = function () {
            var n,
              i,
              o = this;
            if (
              !(
                this._isTransitioning ||
                t(this._element).hasClass(K) ||
                (this._parent &&
                  0 ===
                    (n = [].slice
                      .call(this._parent.querySelectorAll(".show, .collapsing"))
                      .filter(function (e) {
                        return "string" == typeof o._config.parent
                          ? e.getAttribute("data-parent") === o._config.parent
                          : e.classList.contains(Y);
                      })).length &&
                  (n = null),
                n &&
                  (i = t(n).not(this._selector).data(W)) &&
                  i._isTransitioning)
              )
            ) {
              var r = t.Event(Q.SHOW);
              if ((t(this._element).trigger(r), !r.isDefaultPrevented())) {
                n &&
                  (e._jQueryInterface.call(t(n).not(this._selector), "hide"),
                  i || t(n).data(W, null));
                var s = this._getDimension();
                t(this._element).removeClass(Y).addClass(V),
                  (this._element.style[s] = 0),
                  this._triggerArray.length &&
                    t(this._triggerArray)
                      .removeClass(G)
                      .attr("aria-expanded", !0),
                  this.setTransitioning(!0);
                var a = "scroll" + (s[0].toUpperCase() + s.slice(1)),
                  c = l.getTransitionDurationFromElement(this._element);
                t(this._element)
                  .one(l.TRANSITION_END, function () {
                    t(o._element).removeClass(V).addClass(Y).addClass(K),
                      (o._element.style[s] = ""),
                      o.setTransitioning(!1),
                      t(o._element).trigger(Q.SHOWN);
                  })
                  .emulateTransitionEnd(c),
                  (this._element.style[s] = this._element[a] + "px");
              }
            }
          }),
          (n.hide = function () {
            var e = this;
            if (!this._isTransitioning && t(this._element).hasClass(K)) {
              var n = t.Event(Q.HIDE);
              if ((t(this._element).trigger(n), !n.isDefaultPrevented())) {
                var i = this._getDimension();
                (this._element.style[i] =
                  this._element.getBoundingClientRect()[i] + "px"),
                  l.reflow(this._element),
                  t(this._element).addClass(V).removeClass(Y).removeClass(K);
                var o = this._triggerArray.length;
                if (0 < o)
                  for (var r = 0; r < o; r++) {
                    var s = this._triggerArray[r],
                      a = l.getSelectorFromElement(s);
                    null !== a &&
                      (t([].slice.call(document.querySelectorAll(a))).hasClass(
                        K
                      ) ||
                        t(s).addClass(G).attr("aria-expanded", !1));
                  }
                this.setTransitioning(!0), (this._element.style[i] = "");
                var c = l.getTransitionDurationFromElement(this._element);
                t(this._element)
                  .one(l.TRANSITION_END, function () {
                    e.setTransitioning(!1),
                      t(e._element)
                        .removeClass(V)
                        .addClass(Y)
                        .trigger(Q.HIDDEN);
                  })
                  .emulateTransitionEnd(c);
              }
            }
          }),
          (n.setTransitioning = function (e) {
            this._isTransitioning = e;
          }),
          (n.dispose = function () {
            t.removeData(this._element, W),
              (this._config = null),
              (this._parent = null),
              (this._element = null),
              (this._triggerArray = null),
              (this._isTransitioning = null);
          }),
          (n._getConfig = function (e) {
            return (
              ((e = s({}, U, {}, e)).toggle = Boolean(e.toggle)),
              l.typeCheckConfig(F, e, X),
              e
            );
          }),
          (n._getDimension = function () {
            return t(this._element).hasClass("width") ? "width" : "height";
          }),
          (n._getParent = function () {
            var n,
              i = this;
            l.isElement(this._config.parent)
              ? ((n = this._config.parent),
                void 0 !== this._config.parent.jquery &&
                  (n = this._config.parent[0]))
              : (n = document.querySelector(this._config.parent));
            var o =
                '[data-toggle="collapse"][data-parent="' +
                this._config.parent +
                '"]',
              r = [].slice.call(n.querySelectorAll(o));
            return (
              t(r).each(function (t, n) {
                i._addAriaAndCollapsedClass(e._getTargetFromElement(n), [n]);
              }),
              n
            );
          }),
          (n._addAriaAndCollapsedClass = function (e, n) {
            var i = t(e).hasClass(K);
            n.length && t(n).toggleClass(G, !i).attr("aria-expanded", i);
          }),
          (e._getTargetFromElement = function (e) {
            var t = l.getSelectorFromElement(e);
            return t ? document.querySelector(t) : null;
          }),
          (e._jQueryInterface = function (n) {
            return this.each(function () {
              var i = t(this),
                o = i.data(W),
                r = s(
                  {},
                  U,
                  {},
                  i.data(),
                  {},
                  "object" == typeof n && n ? n : {}
                );
              if (
                (!o && r.toggle && /show|hide/.test(n) && (r.toggle = !1),
                o || ((o = new e(this, r)), i.data(W, o)),
                "string" == typeof n)
              ) {
                if (void 0 === o[n])
                  throw new TypeError('No method named "' + n + '"');
                o[n]();
              }
            });
          }),
          o(e, null, [
            {
              key: "VERSION",
              get: function () {
                return "4.4.1";
              },
            },
            {
              key: "Default",
              get: function () {
                return U;
              },
            },
          ]),
          e
        );
      })();
    t(document).on(Q.CLICK_DATA_API, J, function (e) {
      "A" === e.currentTarget.tagName && e.preventDefault();
      var n = t(this),
        i = l.getSelectorFromElement(this),
        o = [].slice.call(document.querySelectorAll(i));
      t(o).each(function () {
        var e = t(this),
          i = e.data(W) ? "toggle" : n.data();
        Z._jQueryInterface.call(e, i);
      });
    }),
      (t.fn[F] = Z._jQueryInterface),
      (t.fn[F].Constructor = Z),
      (t.fn[F].noConflict = function () {
        return (t.fn[F] = B), Z._jQueryInterface;
      });
    var ee = "dropdown",
      te = "bs.dropdown",
      ne = "." + te,
      ie = ".data-api",
      oe = t.fn[ee],
      re = new RegExp("38|40|27"),
      se = {
        HIDE: "hide" + ne,
        HIDDEN: "hidden" + ne,
        SHOW: "show" + ne,
        SHOWN: "shown" + ne,
        CLICK: "click" + ne,
        CLICK_DATA_API: "click" + ne + ie,
        KEYDOWN_DATA_API: "keydown" + ne + ie,
        KEYUP_DATA_API: "keyup" + ne + ie,
      },
      ae = "disabled",
      le = "show",
      ce = "dropdown-menu-right",
      ue = '[data-toggle="dropdown"]',
      de = ".dropdown-menu",
      pe = {
        offset: 0,
        flip: !0,
        boundary: "scrollParent",
        reference: "toggle",
        display: "dynamic",
        popperConfig: null,
      },
      fe = {
        offset: "(number|string|function)",
        flip: "boolean",
        boundary: "(string|element)",
        reference: "(string|element)",
        display: "string",
        popperConfig: "(null|object)",
      },
      he = (function () {
        function e(e, t) {
          (this._element = e),
            (this._popper = null),
            (this._config = this._getConfig(t)),
            (this._menu = this._getMenuElement()),
            (this._inNavbar = this._detectNavbar()),
            this._addEventListeners();
        }
        var i = e.prototype;
        return (
          (i.toggle = function () {
            if (!this._element.disabled && !t(this._element).hasClass(ae)) {
              var n = t(this._menu).hasClass(le);
              e._clearMenus(), n || this.show(!0);
            }
          }),
          (i.show = function (i) {
            if (
              (void 0 === i && (i = !1),
              !(
                this._element.disabled ||
                t(this._element).hasClass(ae) ||
                t(this._menu).hasClass(le)
              ))
            ) {
              var o = { relatedTarget: this._element },
                r = t.Event(se.SHOW, o),
                s = e._getParentFromElement(this._element);
              if ((t(s).trigger(r), !r.isDefaultPrevented())) {
                if (!this._inNavbar && i) {
                  if (void 0 === n)
                    throw new TypeError(
                      "Bootstrap's dropdowns require Popper.js (https://popper.js.org/)"
                    );
                  var a = this._element;
                  "parent" === this._config.reference
                    ? (a = s)
                    : l.isElement(this._config.reference) &&
                      ((a = this._config.reference),
                      void 0 !== this._config.reference.jquery &&
                        (a = this._config.reference[0])),
                    "scrollParent" !== this._config.boundary &&
                      t(s).addClass("position-static"),
                    (this._popper = new n(
                      a,
                      this._menu,
                      this._getPopperConfig()
                    ));
                }
                "ontouchstart" in document.documentElement &&
                  0 === t(s).closest(".navbar-nav").length &&
                  t(document.body).children().on("mouseover", null, t.noop),
                  this._element.focus(),
                  this._element.setAttribute("aria-expanded", !0),
                  t(this._menu).toggleClass(le),
                  t(s).toggleClass(le).trigger(t.Event(se.SHOWN, o));
              }
            }
          }),
          (i.hide = function () {
            if (
              !this._element.disabled &&
              !t(this._element).hasClass(ae) &&
              t(this._menu).hasClass(le)
            ) {
              var n = { relatedTarget: this._element },
                i = t.Event(se.HIDE, n),
                o = e._getParentFromElement(this._element);
              t(o).trigger(i),
                i.isDefaultPrevented() ||
                  (this._popper && this._popper.destroy(),
                  t(this._menu).toggleClass(le),
                  t(o).toggleClass(le).trigger(t.Event(se.HIDDEN, n)));
            }
          }),
          (i.dispose = function () {
            t.removeData(this._element, te),
              t(this._element).off(ne),
              (this._element = null),
              (this._menu = null) !== this._popper &&
                (this._popper.destroy(), (this._popper = null));
          }),
          (i.update = function () {
            (this._inNavbar = this._detectNavbar()),
              null !== this._popper && this._popper.scheduleUpdate();
          }),
          (i._addEventListeners = function () {
            var e = this;
            t(this._element).on(se.CLICK, function (t) {
              t.preventDefault(), t.stopPropagation(), e.toggle();
            });
          }),
          (i._getConfig = function (e) {
            return (
              (e = s(
                {},
                this.constructor.Default,
                {},
                t(this._element).data(),
                {},
                e
              )),
              l.typeCheckConfig(ee, e, this.constructor.DefaultType),
              e
            );
          }),
          (i._getMenuElement = function () {
            if (!this._menu) {
              var t = e._getParentFromElement(this._element);
              t && (this._menu = t.querySelector(de));
            }
            return this._menu;
          }),
          (i._getPlacement = function () {
            var e = t(this._element.parentNode),
              n = "bottom-start";
            return (
              e.hasClass("dropup")
                ? ((n = "top-start"),
                  t(this._menu).hasClass(ce) && (n = "top-end"))
                : e.hasClass("dropright")
                ? (n = "right-start")
                : e.hasClass("dropleft")
                ? (n = "left-start")
                : t(this._menu).hasClass(ce) && (n = "bottom-end"),
              n
            );
          }),
          (i._detectNavbar = function () {
            return 0 < t(this._element).closest(".navbar").length;
          }),
          (i._getOffset = function () {
            var e = this,
              t = {};
            return (
              "function" == typeof this._config.offset
                ? (t.fn = function (t) {
                    return (
                      (t.offsets = s(
                        {},
                        t.offsets,
                        {},
                        e._config.offset(t.offsets, e._element) || {}
                      )),
                      t
                    );
                  })
                : (t.offset = this._config.offset),
              t
            );
          }),
          (i._getPopperConfig = function () {
            var e = {
              placement: this._getPlacement(),
              modifiers: {
                offset: this._getOffset(),
                flip: { enabled: this._config.flip },
                preventOverflow: { boundariesElement: this._config.boundary },
              },
            };
            return (
              "static" === this._config.display &&
                (e.modifiers.applyStyle = { enabled: !1 }),
              s({}, e, {}, this._config.popperConfig)
            );
          }),
          (e._jQueryInterface = function (n) {
            return this.each(function () {
              var i = t(this).data(te);
              if (
                (i ||
                  ((i = new e(this, "object" == typeof n ? n : null)),
                  t(this).data(te, i)),
                "string" == typeof n)
              ) {
                if (void 0 === i[n])
                  throw new TypeError('No method named "' + n + '"');
                i[n]();
              }
            });
          }),
          (e._clearMenus = function (n) {
            if (!n || (3 !== n.which && ("keyup" !== n.type || 9 === n.which)))
              for (
                var i = [].slice.call(document.querySelectorAll(ue)),
                  o = 0,
                  r = i.length;
                o < r;
                o++
              ) {
                var s = e._getParentFromElement(i[o]),
                  a = t(i[o]).data(te),
                  l = { relatedTarget: i[o] };
                if ((n && "click" === n.type && (l.clickEvent = n), a)) {
                  var c = a._menu;
                  if (
                    t(s).hasClass(le) &&
                    !(
                      n &&
                      (("click" === n.type &&
                        /input|textarea/i.test(n.target.tagName)) ||
                        ("keyup" === n.type && 9 === n.which)) &&
                      t.contains(s, n.target)
                    )
                  ) {
                    var u = t.Event(se.HIDE, l);
                    t(s).trigger(u),
                      u.isDefaultPrevented() ||
                        ("ontouchstart" in document.documentElement &&
                          t(document.body)
                            .children()
                            .off("mouseover", null, t.noop),
                        i[o].setAttribute("aria-expanded", "false"),
                        a._popper && a._popper.destroy(),
                        t(c).removeClass(le),
                        t(s).removeClass(le).trigger(t.Event(se.HIDDEN, l)));
                  }
                }
              }
          }),
          (e._getParentFromElement = function (e) {
            var t,
              n = l.getSelectorFromElement(e);
            return n && (t = document.querySelector(n)), t || e.parentNode;
          }),
          (e._dataApiKeydownHandler = function (n) {
            if (
              (/input|textarea/i.test(n.target.tagName)
                ? !(
                    32 === n.which ||
                    (27 !== n.which &&
                      ((40 !== n.which && 38 !== n.which) ||
                        t(n.target).closest(de).length))
                  )
                : re.test(n.which)) &&
              (n.preventDefault(),
              n.stopPropagation(),
              !this.disabled && !t(this).hasClass(ae))
            ) {
              var i = e._getParentFromElement(this),
                o = t(i).hasClass(le);
              if (o || 27 !== n.which)
                if (o && (!o || (27 !== n.which && 32 !== n.which))) {
                  var r = [].slice
                    .call(
                      i.querySelectorAll(
                        ".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)"
                      )
                    )
                    .filter(function (e) {
                      return t(e).is(":visible");
                    });
                  if (0 !== r.length) {
                    var s = r.indexOf(n.target);
                    38 === n.which && 0 < s && s--,
                      40 === n.which && s < r.length - 1 && s++,
                      s < 0 && (s = 0),
                      r[s].focus();
                  }
                } else {
                  if (27 === n.which) {
                    var a = i.querySelector(ue);
                    t(a).trigger("focus");
                  }
                  t(this).trigger("click");
                }
            }
          }),
          o(e, null, [
            {
              key: "VERSION",
              get: function () {
                return "4.4.1";
              },
            },
            {
              key: "Default",
              get: function () {
                return pe;
              },
            },
            {
              key: "DefaultType",
              get: function () {
                return fe;
              },
            },
          ]),
          e
        );
      })();
    t(document)
      .on(se.KEYDOWN_DATA_API, ue, he._dataApiKeydownHandler)
      .on(se.KEYDOWN_DATA_API, de, he._dataApiKeydownHandler)
      .on(se.CLICK_DATA_API + " " + se.KEYUP_DATA_API, he._clearMenus)
      .on(se.CLICK_DATA_API, ue, function (e) {
        e.preventDefault(),
          e.stopPropagation(),
          he._jQueryInterface.call(t(this), "toggle");
      })
      .on(se.CLICK_DATA_API, ".dropdown form", function (e) {
        e.stopPropagation();
      }),
      (t.fn[ee] = he._jQueryInterface),
      (t.fn[ee].Constructor = he),
      (t.fn[ee].noConflict = function () {
        return (t.fn[ee] = oe), he._jQueryInterface;
      });
    var ge = "modal",
      me = "bs.modal",
      ve = "." + me,
      ye = t.fn[ge],
      be = { backdrop: !0, keyboard: !0, focus: !0, show: !0 },
      _e = {
        backdrop: "(boolean|string)",
        keyboard: "boolean",
        focus: "boolean",
        show: "boolean",
      },
      we = {
        HIDE: "hide" + ve,
        HIDE_PREVENTED: "hidePrevented" + ve,
        HIDDEN: "hidden" + ve,
        SHOW: "show" + ve,
        SHOWN: "shown" + ve,
        FOCUSIN: "focusin" + ve,
        RESIZE: "resize" + ve,
        CLICK_DISMISS: "click.dismiss" + ve,
        KEYDOWN_DISMISS: "keydown.dismiss" + ve,
        MOUSEUP_DISMISS: "mouseup.dismiss" + ve,
        MOUSEDOWN_DISMISS: "mousedown.dismiss" + ve,
        CLICK_DATA_API: "click" + ve + ".data-api",
      },
      Te = "modal-open",
      Ce = "fade",
      Se = "show",
      ke = "modal-static",
      xe = ".modal-dialog",
      Ee = ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",
      Ae = ".sticky-top",
      De = (function () {
        function e(e, t) {
          (this._config = this._getConfig(t)),
            (this._element = e),
            (this._dialog = e.querySelector(xe)),
            (this._backdrop = null),
            (this._isShown = !1),
            (this._isBodyOverflowing = !1),
            (this._ignoreBackdropClick = !1),
            (this._isTransitioning = !1),
            (this._scrollbarWidth = 0);
        }
        var n = e.prototype;
        return (
          (n.toggle = function (e) {
            return this._isShown ? this.hide() : this.show(e);
          }),
          (n.show = function (e) {
            var n = this;
            if (!this._isShown && !this._isTransitioning) {
              t(this._element).hasClass(Ce) && (this._isTransitioning = !0);
              var i = t.Event(we.SHOW, { relatedTarget: e });
              t(this._element).trigger(i),
                this._isShown ||
                  i.isDefaultPrevented() ||
                  ((this._isShown = !0),
                  this._checkScrollbar(),
                  this._setScrollbar(),
                  this._adjustDialog(),
                  this._setEscapeEvent(),
                  this._setResizeEvent(),
                  t(this._element).on(
                    we.CLICK_DISMISS,
                    '[data-dismiss="modal"]',
                    function (e) {
                      return n.hide(e);
                    }
                  ),
                  t(this._dialog).on(we.MOUSEDOWN_DISMISS, function () {
                    t(n._element).one(we.MOUSEUP_DISMISS, function (e) {
                      t(e.target).is(n._element) &&
                        (n._ignoreBackdropClick = !0);
                    });
                  }),
                  this._showBackdrop(function () {
                    return n._showElement(e);
                  }));
            }
          }),
          (n.hide = function (e) {
            var n = this;
            if (
              (e && e.preventDefault(), this._isShown && !this._isTransitioning)
            ) {
              var i = t.Event(we.HIDE);
              if (
                (t(this._element).trigger(i),
                this._isShown && !i.isDefaultPrevented())
              ) {
                this._isShown = !1;
                var o = t(this._element).hasClass(Ce);
                if (
                  (o && (this._isTransitioning = !0),
                  this._setEscapeEvent(),
                  this._setResizeEvent(),
                  t(document).off(we.FOCUSIN),
                  t(this._element).removeClass(Se),
                  t(this._element).off(we.CLICK_DISMISS),
                  t(this._dialog).off(we.MOUSEDOWN_DISMISS),
                  o)
                ) {
                  var r = l.getTransitionDurationFromElement(this._element);
                  t(this._element)
                    .one(l.TRANSITION_END, function (e) {
                      return n._hideModal(e);
                    })
                    .emulateTransitionEnd(r);
                } else this._hideModal();
              }
            }
          }),
          (n.dispose = function () {
            [window, this._element, this._dialog].forEach(function (e) {
              return t(e).off(ve);
            }),
              t(document).off(we.FOCUSIN),
              t.removeData(this._element, me),
              (this._config = null),
              (this._element = null),
              (this._dialog = null),
              (this._backdrop = null),
              (this._isShown = null),
              (this._isBodyOverflowing = null),
              (this._ignoreBackdropClick = null),
              (this._isTransitioning = null),
              (this._scrollbarWidth = null);
          }),
          (n.handleUpdate = function () {
            this._adjustDialog();
          }),
          (n._getConfig = function (e) {
            return (e = s({}, be, {}, e)), l.typeCheckConfig(ge, e, _e), e;
          }),
          (n._triggerBackdropTransition = function () {
            var e = this;
            if ("static" === this._config.backdrop) {
              var n = t.Event(we.HIDE_PREVENTED);
              if ((t(this._element).trigger(n), n.defaultPrevented)) return;
              this._element.classList.add(ke);
              var i = l.getTransitionDurationFromElement(this._element);
              t(this._element)
                .one(l.TRANSITION_END, function () {
                  e._element.classList.remove(ke);
                })
                .emulateTransitionEnd(i),
                this._element.focus();
            } else this.hide();
          }),
          (n._showElement = function (e) {
            var n = this,
              i = t(this._element).hasClass(Ce),
              o = this._dialog
                ? this._dialog.querySelector(".modal-body")
                : null;
            function r() {
              n._config.focus && n._element.focus(),
                (n._isTransitioning = !1),
                t(n._element).trigger(s);
            }
            (this._element.parentNode &&
              this._element.parentNode.nodeType === Node.ELEMENT_NODE) ||
              document.body.appendChild(this._element),
              (this._element.style.display = "block"),
              this._element.removeAttribute("aria-hidden"),
              this._element.setAttribute("aria-modal", !0),
              t(this._dialog).hasClass("modal-dialog-scrollable") && o
                ? (o.scrollTop = 0)
                : (this._element.scrollTop = 0),
              i && l.reflow(this._element),
              t(this._element).addClass(Se),
              this._config.focus && this._enforceFocus();
            var s = t.Event(we.SHOWN, { relatedTarget: e });
            if (i) {
              var a = l.getTransitionDurationFromElement(this._dialog);
              t(this._dialog).one(l.TRANSITION_END, r).emulateTransitionEnd(a);
            } else r();
          }),
          (n._enforceFocus = function () {
            var e = this;
            t(document)
              .off(we.FOCUSIN)
              .on(we.FOCUSIN, function (n) {
                document !== n.target &&
                  e._element !== n.target &&
                  0 === t(e._element).has(n.target).length &&
                  e._element.focus();
              });
          }),
          (n._setEscapeEvent = function () {
            var e = this;
            this._isShown && this._config.keyboard
              ? t(this._element).on(we.KEYDOWN_DISMISS, function (t) {
                  27 === t.which && e._triggerBackdropTransition();
                })
              : this._isShown || t(this._element).off(we.KEYDOWN_DISMISS);
          }),
          (n._setResizeEvent = function () {
            var e = this;
            this._isShown
              ? t(window).on(we.RESIZE, function (t) {
                  return e.handleUpdate(t);
                })
              : t(window).off(we.RESIZE);
          }),
          (n._hideModal = function () {
            var e = this;
            (this._element.style.display = "none"),
              this._element.setAttribute("aria-hidden", !0),
              this._element.removeAttribute("aria-modal"),
              (this._isTransitioning = !1),
              this._showBackdrop(function () {
                t(document.body).removeClass(Te),
                  e._resetAdjustments(),
                  e._resetScrollbar(),
                  t(e._element).trigger(we.HIDDEN);
              });
          }),
          (n._removeBackdrop = function () {
            this._backdrop &&
              (t(this._backdrop).remove(), (this._backdrop = null));
          }),
          (n._showBackdrop = function (e) {
            var n = this,
              i = t(this._element).hasClass(Ce) ? Ce : "";
            if (this._isShown && this._config.backdrop) {
              if (
                ((this._backdrop = document.createElement("div")),
                (this._backdrop.className = "modal-backdrop"),
                i && this._backdrop.classList.add(i),
                t(this._backdrop).appendTo(document.body),
                t(this._element).on(we.CLICK_DISMISS, function (e) {
                  n._ignoreBackdropClick
                    ? (n._ignoreBackdropClick = !1)
                    : e.target === e.currentTarget &&
                      n._triggerBackdropTransition();
                }),
                i && l.reflow(this._backdrop),
                t(this._backdrop).addClass(Se),
                !e)
              )
                return;
              if (!i) return void e();
              var o = l.getTransitionDurationFromElement(this._backdrop);
              t(this._backdrop)
                .one(l.TRANSITION_END, e)
                .emulateTransitionEnd(o);
            } else if (!this._isShown && this._backdrop) {
              t(this._backdrop).removeClass(Se);
              var r = function () {
                n._removeBackdrop(), e && e();
              };
              if (t(this._element).hasClass(Ce)) {
                var s = l.getTransitionDurationFromElement(this._backdrop);
                t(this._backdrop)
                  .one(l.TRANSITION_END, r)
                  .emulateTransitionEnd(s);
              } else r();
            } else e && e();
          }),
          (n._adjustDialog = function () {
            var e =
              this._element.scrollHeight >
              document.documentElement.clientHeight;
            !this._isBodyOverflowing &&
              e &&
              (this._element.style.paddingLeft = this._scrollbarWidth + "px"),
              this._isBodyOverflowing &&
                !e &&
                (this._element.style.paddingRight =
                  this._scrollbarWidth + "px");
          }),
          (n._resetAdjustments = function () {
            (this._element.style.paddingLeft = ""),
              (this._element.style.paddingRight = "");
          }),
          (n._checkScrollbar = function () {
            var e = document.body.getBoundingClientRect();
            (this._isBodyOverflowing = e.left + e.right < window.innerWidth),
              (this._scrollbarWidth = this._getScrollbarWidth());
          }),
          (n._setScrollbar = function () {
            var e = this;
            if (this._isBodyOverflowing) {
              var n = [].slice.call(document.querySelectorAll(Ee)),
                i = [].slice.call(document.querySelectorAll(Ae));
              t(n).each(function (n, i) {
                var o = i.style.paddingRight,
                  r = t(i).css("padding-right");
                t(i)
                  .data("padding-right", o)
                  .css(
                    "padding-right",
                    parseFloat(r) + e._scrollbarWidth + "px"
                  );
              }),
                t(i).each(function (n, i) {
                  var o = i.style.marginRight,
                    r = t(i).css("margin-right");
                  t(i)
                    .data("margin-right", o)
                    .css(
                      "margin-right",
                      parseFloat(r) - e._scrollbarWidth + "px"
                    );
                });
              var o = document.body.style.paddingRight,
                r = t(document.body).css("padding-right");
              t(document.body)
                .data("padding-right", o)
                .css(
                  "padding-right",
                  parseFloat(r) + this._scrollbarWidth + "px"
                );
            }
            t(document.body).addClass(Te);
          }),
          (n._resetScrollbar = function () {
            var e = [].slice.call(document.querySelectorAll(Ee));
            t(e).each(function (e, n) {
              var i = t(n).data("padding-right");
              t(n).removeData("padding-right"),
                (n.style.paddingRight = i || "");
            });
            var n = [].slice.call(document.querySelectorAll("" + Ae));
            t(n).each(function (e, n) {
              var i = t(n).data("margin-right");
              void 0 !== i &&
                t(n).css("margin-right", i).removeData("margin-right");
            });
            var i = t(document.body).data("padding-right");
            t(document.body).removeData("padding-right"),
              (document.body.style.paddingRight = i || "");
          }),
          (n._getScrollbarWidth = function () {
            var e = document.createElement("div");
            (e.className = "modal-scrollbar-measure"),
              document.body.appendChild(e);
            var t = e.getBoundingClientRect().width - e.clientWidth;
            return document.body.removeChild(e), t;
          }),
          (e._jQueryInterface = function (n, i) {
            return this.each(function () {
              var o = t(this).data(me),
                r = s(
                  {},
                  be,
                  {},
                  t(this).data(),
                  {},
                  "object" == typeof n && n ? n : {}
                );
              if (
                (o || ((o = new e(this, r)), t(this).data(me, o)),
                "string" == typeof n)
              ) {
                if (void 0 === o[n])
                  throw new TypeError('No method named "' + n + '"');
                o[n](i);
              } else r.show && o.show(i);
            });
          }),
          o(e, null, [
            {
              key: "VERSION",
              get: function () {
                return "4.4.1";
              },
            },
            {
              key: "Default",
              get: function () {
                return be;
              },
            },
          ]),
          e
        );
      })();
    t(document).on(we.CLICK_DATA_API, '[data-toggle="modal"]', function (e) {
      var n,
        i = this,
        o = l.getSelectorFromElement(this);
      o && (n = document.querySelector(o));
      var r = t(n).data(me) ? "toggle" : s({}, t(n).data(), {}, t(this).data());
      ("A" !== this.tagName && "AREA" !== this.tagName) || e.preventDefault();
      var a = t(n).one(we.SHOW, function (e) {
        e.isDefaultPrevented() ||
          a.one(we.HIDDEN, function () {
            t(i).is(":visible") && i.focus();
          });
      });
      De._jQueryInterface.call(t(n), r, this);
    }),
      (t.fn[ge] = De._jQueryInterface),
      (t.fn[ge].Constructor = De),
      (t.fn[ge].noConflict = function () {
        return (t.fn[ge] = ye), De._jQueryInterface;
      });
    var Ne = [
        "background",
        "cite",
        "href",
        "itemtype",
        "longdesc",
        "poster",
        "src",
        "xlink:href",
      ],
      Ie = /^(?:(?:https?|mailto|ftp|tel|file):|[^&:\/?#]*(?:[\/?#]|$))/gi,
      Oe = /^data:(?:image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|video\/(?:mpeg|mp4|ogg|webm)|audio\/(?:mp3|oga|ogg|opus));base64,[a-z0-9+\/]+=*$/i;
    function $e(e, t, n) {
      if (0 === e.length) return e;
      if (n && "function" == typeof n) return n(e);
      for (
        var i = new window.DOMParser().parseFromString(e, "text/html"),
          o = Object.keys(t),
          r = [].slice.call(i.body.querySelectorAll("*")),
          s = function (e) {
            var n = r[e],
              i = n.nodeName.toLowerCase();
            if (-1 === o.indexOf(n.nodeName.toLowerCase()))
              return n.parentNode.removeChild(n), "continue";
            var s = [].slice.call(n.attributes),
              a = [].concat(t["*"] || [], t[i] || []);
            s.forEach(function (e) {
              !(function (e, t) {
                var n = e.nodeName.toLowerCase();
                if (-1 !== t.indexOf(n))
                  return (
                    -1 === Ne.indexOf(n) ||
                    Boolean(e.nodeValue.match(Ie) || e.nodeValue.match(Oe))
                  );
                for (
                  var i = t.filter(function (e) {
                      return e instanceof RegExp;
                    }),
                    o = 0,
                    r = i.length;
                  o < r;
                  o++
                )
                  if (n.match(i[o])) return !0;
                return !1;
              })(e, a) && n.removeAttribute(e.nodeName);
            });
          },
          a = 0,
          l = r.length;
        a < l;
        a++
      )
        s(a);
      return i.body.innerHTML;
    }
    var je = "tooltip",
      Le = "bs.tooltip",
      Pe = "." + Le,
      He = t.fn[je],
      qe = "bs-tooltip",
      Re = new RegExp("(^|\\s)" + qe + "\\S+", "g"),
      Me = ["sanitize", "whiteList", "sanitizeFn"],
      Fe = {
        animation: "boolean",
        template: "string",
        title: "(string|element|function)",
        trigger: "string",
        delay: "(number|object)",
        html: "boolean",
        selector: "(string|boolean)",
        placement: "(string|function)",
        offset: "(number|string|function)",
        container: "(string|element|boolean)",
        fallbackPlacement: "(string|array)",
        boundary: "(string|element)",
        sanitize: "boolean",
        sanitizeFn: "(null|function)",
        whiteList: "object",
        popperConfig: "(null|object)",
      },
      We = {
        AUTO: "auto",
        TOP: "top",
        RIGHT: "right",
        BOTTOM: "bottom",
        LEFT: "left",
      },
      ze = {
        animation: !0,
        template:
          '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: "hover focus",
        title: "",
        delay: 0,
        html: !1,
        selector: !1,
        placement: "top",
        offset: 0,
        container: !1,
        fallbackPlacement: "flip",
        boundary: "scrollParent",
        sanitize: !0,
        sanitizeFn: null,
        whiteList: {
          "*": ["class", "dir", "id", "lang", "role", /^aria-[\w-]*$/i],
          a: ["target", "href", "title", "rel"],
          area: [],
          b: [],
          br: [],
          col: [],
          code: [],
          div: [],
          em: [],
          hr: [],
          h1: [],
          h2: [],
          h3: [],
          h4: [],
          h5: [],
          h6: [],
          i: [],
          img: ["src", "alt", "title", "width", "height"],
          li: [],
          ol: [],
          p: [],
          pre: [],
          s: [],
          small: [],
          span: [],
          sub: [],
          sup: [],
          strong: [],
          u: [],
          ul: [],
        },
        popperConfig: null,
      },
      Be = "show",
      Ue = {
        HIDE: "hide" + Pe,
        HIDDEN: "hidden" + Pe,
        SHOW: "show" + Pe,
        SHOWN: "shown" + Pe,
        INSERTED: "inserted" + Pe,
        CLICK: "click" + Pe,
        FOCUSIN: "focusin" + Pe,
        FOCUSOUT: "focusout" + Pe,
        MOUSEENTER: "mouseenter" + Pe,
        MOUSELEAVE: "mouseleave" + Pe,
      },
      Xe = "fade",
      Qe = "show",
      Ke = "hover",
      Ye = "focus",
      Ve = (function () {
        function e(e, t) {
          if (void 0 === n)
            throw new TypeError(
              "Bootstrap's tooltips require Popper.js (https://popper.js.org/)"
            );
          (this._isEnabled = !0),
            (this._timeout = 0),
            (this._hoverState = ""),
            (this._activeTrigger = {}),
            (this._popper = null),
            (this.element = e),
            (this.config = this._getConfig(t)),
            (this.tip = null),
            this._setListeners();
        }
        var i = e.prototype;
        return (
          (i.enable = function () {
            this._isEnabled = !0;
          }),
          (i.disable = function () {
            this._isEnabled = !1;
          }),
          (i.toggleEnabled = function () {
            this._isEnabled = !this._isEnabled;
          }),
          (i.toggle = function (e) {
            if (this._isEnabled)
              if (e) {
                var n = this.constructor.DATA_KEY,
                  i = t(e.currentTarget).data(n);
                i ||
                  ((i = new this.constructor(
                    e.currentTarget,
                    this._getDelegateConfig()
                  )),
                  t(e.currentTarget).data(n, i)),
                  (i._activeTrigger.click = !i._activeTrigger.click),
                  i._isWithActiveTrigger()
                    ? i._enter(null, i)
                    : i._leave(null, i);
              } else {
                if (t(this.getTipElement()).hasClass(Qe))
                  return void this._leave(null, this);
                this._enter(null, this);
              }
          }),
          (i.dispose = function () {
            clearTimeout(this._timeout),
              t.removeData(this.element, this.constructor.DATA_KEY),
              t(this.element).off(this.constructor.EVENT_KEY),
              t(this.element)
                .closest(".modal")
                .off("hide.bs.modal", this._hideModalHandler),
              this.tip && t(this.tip).remove(),
              (this._isEnabled = null),
              (this._timeout = null),
              (this._hoverState = null),
              (this._activeTrigger = null),
              this._popper && this._popper.destroy(),
              (this._popper = null),
              (this.element = null),
              (this.config = null),
              (this.tip = null);
          }),
          (i.show = function () {
            var e = this;
            if ("none" === t(this.element).css("display"))
              throw new Error("Please use show on visible elements");
            var i = t.Event(this.constructor.Event.SHOW);
            if (this.isWithContent() && this._isEnabled) {
              t(this.element).trigger(i);
              var o = l.findShadowRoot(this.element),
                r = t.contains(
                  null !== o ? o : this.element.ownerDocument.documentElement,
                  this.element
                );
              if (i.isDefaultPrevented() || !r) return;
              var s = this.getTipElement(),
                a = l.getUID(this.constructor.NAME);
              s.setAttribute("id", a),
                this.element.setAttribute("aria-describedby", a),
                this.setContent(),
                this.config.animation && t(s).addClass(Xe);
              var c =
                  "function" == typeof this.config.placement
                    ? this.config.placement.call(this, s, this.element)
                    : this.config.placement,
                u = this._getAttachment(c);
              this.addAttachmentClass(u);
              var d = this._getContainer();
              t(s).data(this.constructor.DATA_KEY, this),
                t.contains(
                  this.element.ownerDocument.documentElement,
                  this.tip
                ) || t(s).appendTo(d),
                t(this.element).trigger(this.constructor.Event.INSERTED),
                (this._popper = new n(
                  this.element,
                  s,
                  this._getPopperConfig(u)
                )),
                t(s).addClass(Qe),
                "ontouchstart" in document.documentElement &&
                  t(document.body).children().on("mouseover", null, t.noop);
              var p = function () {
                e.config.animation && e._fixTransition();
                var n = e._hoverState;
                (e._hoverState = null),
                  t(e.element).trigger(e.constructor.Event.SHOWN),
                  "out" === n && e._leave(null, e);
              };
              if (t(this.tip).hasClass(Xe)) {
                var f = l.getTransitionDurationFromElement(this.tip);
                t(this.tip).one(l.TRANSITION_END, p).emulateTransitionEnd(f);
              } else p();
            }
          }),
          (i.hide = function (e) {
            function n() {
              i._hoverState !== Be &&
                o.parentNode &&
                o.parentNode.removeChild(o),
                i._cleanTipClass(),
                i.element.removeAttribute("aria-describedby"),
                t(i.element).trigger(i.constructor.Event.HIDDEN),
                null !== i._popper && i._popper.destroy(),
                e && e();
            }
            var i = this,
              o = this.getTipElement(),
              r = t.Event(this.constructor.Event.HIDE);
            if ((t(this.element).trigger(r), !r.isDefaultPrevented())) {
              if (
                (t(o).removeClass(Qe),
                "ontouchstart" in document.documentElement &&
                  t(document.body).children().off("mouseover", null, t.noop),
                (this._activeTrigger.click = !1),
                (this._activeTrigger[Ye] = !1),
                (this._activeTrigger[Ke] = !1),
                t(this.tip).hasClass(Xe))
              ) {
                var s = l.getTransitionDurationFromElement(o);
                t(o).one(l.TRANSITION_END, n).emulateTransitionEnd(s);
              } else n();
              this._hoverState = "";
            }
          }),
          (i.update = function () {
            null !== this._popper && this._popper.scheduleUpdate();
          }),
          (i.isWithContent = function () {
            return Boolean(this.getTitle());
          }),
          (i.addAttachmentClass = function (e) {
            t(this.getTipElement()).addClass(qe + "-" + e);
          }),
          (i.getTipElement = function () {
            return (
              (this.tip = this.tip || t(this.config.template)[0]), this.tip
            );
          }),
          (i.setContent = function () {
            var e = this.getTipElement();
            this.setElementContent(
              t(e.querySelectorAll(".tooltip-inner")),
              this.getTitle()
            ),
              t(e).removeClass(Xe + " " + Qe);
          }),
          (i.setElementContent = function (e, n) {
            "object" != typeof n || (!n.nodeType && !n.jquery)
              ? this.config.html
                ? (this.config.sanitize &&
                    (n = $e(n, this.config.whiteList, this.config.sanitizeFn)),
                  e.html(n))
                : e.text(n)
              : this.config.html
              ? t(n).parent().is(e) || e.empty().append(n)
              : e.text(t(n).text());
          }),
          (i.getTitle = function () {
            var e = this.element.getAttribute("data-original-title");
            return (
              e ||
              ("function" == typeof this.config.title
                ? this.config.title.call(this.element)
                : this.config.title)
            );
          }),
          (i._getPopperConfig = function (e) {
            var t = this;
            return s(
              {},
              {
                placement: e,
                modifiers: {
                  offset: this._getOffset(),
                  flip: { behavior: this.config.fallbackPlacement },
                  arrow: { element: ".arrow" },
                  preventOverflow: { boundariesElement: this.config.boundary },
                },
                onCreate: function (e) {
                  e.originalPlacement !== e.placement &&
                    t._handlePopperPlacementChange(e);
                },
                onUpdate: function (e) {
                  return t._handlePopperPlacementChange(e);
                },
              },
              {},
              this.config.popperConfig
            );
          }),
          (i._getOffset = function () {
            var e = this,
              t = {};
            return (
              "function" == typeof this.config.offset
                ? (t.fn = function (t) {
                    return (
                      (t.offsets = s(
                        {},
                        t.offsets,
                        {},
                        e.config.offset(t.offsets, e.element) || {}
                      )),
                      t
                    );
                  })
                : (t.offset = this.config.offset),
              t
            );
          }),
          (i._getContainer = function () {
            return !1 === this.config.container
              ? document.body
              : l.isElement(this.config.container)
              ? t(this.config.container)
              : t(document).find(this.config.container);
          }),
          (i._getAttachment = function (e) {
            return We[e.toUpperCase()];
          }),
          (i._setListeners = function () {
            var e = this;
            this.config.trigger.split(" ").forEach(function (n) {
              if ("click" === n)
                t(e.element).on(
                  e.constructor.Event.CLICK,
                  e.config.selector,
                  function (t) {
                    return e.toggle(t);
                  }
                );
              else if ("manual" !== n) {
                var i =
                    n === Ke
                      ? e.constructor.Event.MOUSEENTER
                      : e.constructor.Event.FOCUSIN,
                  o =
                    n === Ke
                      ? e.constructor.Event.MOUSELEAVE
                      : e.constructor.Event.FOCUSOUT;
                t(e.element)
                  .on(i, e.config.selector, function (t) {
                    return e._enter(t);
                  })
                  .on(o, e.config.selector, function (t) {
                    return e._leave(t);
                  });
              }
            }),
              (this._hideModalHandler = function () {
                e.element && e.hide();
              }),
              t(this.element)
                .closest(".modal")
                .on("hide.bs.modal", this._hideModalHandler),
              this.config.selector
                ? (this.config = s({}, this.config, {
                    trigger: "manual",
                    selector: "",
                  }))
                : this._fixTitle();
          }),
          (i._fixTitle = function () {
            var e = typeof this.element.getAttribute("data-original-title");
            (!this.element.getAttribute("title") && "string" == e) ||
              (this.element.setAttribute(
                "data-original-title",
                this.element.getAttribute("title") || ""
              ),
              this.element.setAttribute("title", ""));
          }),
          (i._enter = function (e, n) {
            var i = this.constructor.DATA_KEY;
            (n = n || t(e.currentTarget).data(i)) ||
              ((n = new this.constructor(
                e.currentTarget,
                this._getDelegateConfig()
              )),
              t(e.currentTarget).data(i, n)),
              e && (n._activeTrigger["focusin" === e.type ? Ye : Ke] = !0),
              t(n.getTipElement()).hasClass(Qe) || n._hoverState === Be
                ? (n._hoverState = Be)
                : (clearTimeout(n._timeout),
                  (n._hoverState = Be),
                  n.config.delay && n.config.delay.show
                    ? (n._timeout = setTimeout(function () {
                        n._hoverState === Be && n.show();
                      }, n.config.delay.show))
                    : n.show());
          }),
          (i._leave = function (e, n) {
            var i = this.constructor.DATA_KEY;
            (n = n || t(e.currentTarget).data(i)) ||
              ((n = new this.constructor(
                e.currentTarget,
                this._getDelegateConfig()
              )),
              t(e.currentTarget).data(i, n)),
              e && (n._activeTrigger["focusout" === e.type ? Ye : Ke] = !1),
              n._isWithActiveTrigger() ||
                (clearTimeout(n._timeout),
                (n._hoverState = "out"),
                n.config.delay && n.config.delay.hide
                  ? (n._timeout = setTimeout(function () {
                      "out" === n._hoverState && n.hide();
                    }, n.config.delay.hide))
                  : n.hide());
          }),
          (i._isWithActiveTrigger = function () {
            for (var e in this._activeTrigger)
              if (this._activeTrigger[e]) return !0;
            return !1;
          }),
          (i._getConfig = function (e) {
            var n = t(this.element).data();
            return (
              Object.keys(n).forEach(function (e) {
                -1 !== Me.indexOf(e) && delete n[e];
              }),
              "number" ==
                typeof (e = s(
                  {},
                  this.constructor.Default,
                  {},
                  n,
                  {},
                  "object" == typeof e && e ? e : {}
                )).delay && (e.delay = { show: e.delay, hide: e.delay }),
              "number" == typeof e.title && (e.title = e.title.toString()),
              "number" == typeof e.content &&
                (e.content = e.content.toString()),
              l.typeCheckConfig(je, e, this.constructor.DefaultType),
              e.sanitize &&
                (e.template = $e(e.template, e.whiteList, e.sanitizeFn)),
              e
            );
          }),
          (i._getDelegateConfig = function () {
            var e = {};
            if (this.config)
              for (var t in this.config)
                this.constructor.Default[t] !== this.config[t] &&
                  (e[t] = this.config[t]);
            return e;
          }),
          (i._cleanTipClass = function () {
            var e = t(this.getTipElement()),
              n = e.attr("class").match(Re);
            null !== n && n.length && e.removeClass(n.join(""));
          }),
          (i._handlePopperPlacementChange = function (e) {
            var t = e.instance;
            (this.tip = t.popper),
              this._cleanTipClass(),
              this.addAttachmentClass(this._getAttachment(e.placement));
          }),
          (i._fixTransition = function () {
            var e = this.getTipElement(),
              n = this.config.animation;
            null === e.getAttribute("x-placement") &&
              (t(e).removeClass(Xe),
              (this.config.animation = !1),
              this.hide(),
              this.show(),
              (this.config.animation = n));
          }),
          (e._jQueryInterface = function (n) {
            return this.each(function () {
              var i = t(this).data(Le),
                o = "object" == typeof n && n;
              if (
                (i || !/dispose|hide/.test(n)) &&
                (i || ((i = new e(this, o)), t(this).data(Le, i)),
                "string" == typeof n)
              ) {
                if (void 0 === i[n])
                  throw new TypeError('No method named "' + n + '"');
                i[n]();
              }
            });
          }),
          o(e, null, [
            {
              key: "VERSION",
              get: function () {
                return "4.4.1";
              },
            },
            {
              key: "Default",
              get: function () {
                return ze;
              },
            },
            {
              key: "NAME",
              get: function () {
                return je;
              },
            },
            {
              key: "DATA_KEY",
              get: function () {
                return Le;
              },
            },
            {
              key: "Event",
              get: function () {
                return Ue;
              },
            },
            {
              key: "EVENT_KEY",
              get: function () {
                return Pe;
              },
            },
            {
              key: "DefaultType",
              get: function () {
                return Fe;
              },
            },
          ]),
          e
        );
      })();
    (t.fn[je] = Ve._jQueryInterface),
      (t.fn[je].Constructor = Ve),
      (t.fn[je].noConflict = function () {
        return (t.fn[je] = He), Ve._jQueryInterface;
      });
    var Ge = "popover",
      Je = "bs.popover",
      Ze = "." + Je,
      et = t.fn[Ge],
      tt = "bs-popover",
      nt = new RegExp("(^|\\s)" + tt + "\\S+", "g"),
      it = s({}, Ve.Default, {
        placement: "right",
        trigger: "click",
        content: "",
        template:
          '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>',
      }),
      ot = s({}, Ve.DefaultType, { content: "(string|element|function)" }),
      rt = {
        HIDE: "hide" + Ze,
        HIDDEN: "hidden" + Ze,
        SHOW: "show" + Ze,
        SHOWN: "shown" + Ze,
        INSERTED: "inserted" + Ze,
        CLICK: "click" + Ze,
        FOCUSIN: "focusin" + Ze,
        FOCUSOUT: "focusout" + Ze,
        MOUSEENTER: "mouseenter" + Ze,
        MOUSELEAVE: "mouseleave" + Ze,
      },
      st = (function (e) {
        function n() {
          return e.apply(this, arguments) || this;
        }
        !(function (e, t) {
          (e.prototype = Object.create(t.prototype)),
            ((e.prototype.constructor = e).__proto__ = t);
        })(n, e);
        var i = n.prototype;
        return (
          (i.isWithContent = function () {
            return this.getTitle() || this._getContent();
          }),
          (i.addAttachmentClass = function (e) {
            t(this.getTipElement()).addClass(tt + "-" + e);
          }),
          (i.getTipElement = function () {
            return (
              (this.tip = this.tip || t(this.config.template)[0]), this.tip
            );
          }),
          (i.setContent = function () {
            var e = t(this.getTipElement());
            this.setElementContent(e.find(".popover-header"), this.getTitle());
            var n = this._getContent();
            "function" == typeof n && (n = n.call(this.element)),
              this.setElementContent(e.find(".popover-body"), n),
              e.removeClass("fade show");
          }),
          (i._getContent = function () {
            return (
              this.element.getAttribute("data-content") || this.config.content
            );
          }),
          (i._cleanTipClass = function () {
            var e = t(this.getTipElement()),
              n = e.attr("class").match(nt);
            null !== n && 0 < n.length && e.removeClass(n.join(""));
          }),
          (n._jQueryInterface = function (e) {
            return this.each(function () {
              var i = t(this).data(Je),
                o = "object" == typeof e ? e : null;
              if (
                (i || !/dispose|hide/.test(e)) &&
                (i || ((i = new n(this, o)), t(this).data(Je, i)),
                "string" == typeof e)
              ) {
                if (void 0 === i[e])
                  throw new TypeError('No method named "' + e + '"');
                i[e]();
              }
            });
          }),
          o(n, null, [
            {
              key: "VERSION",
              get: function () {
                return "4.4.1";
              },
            },
            {
              key: "Default",
              get: function () {
                return it;
              },
            },
            {
              key: "NAME",
              get: function () {
                return Ge;
              },
            },
            {
              key: "DATA_KEY",
              get: function () {
                return Je;
              },
            },
            {
              key: "Event",
              get: function () {
                return rt;
              },
            },
            {
              key: "EVENT_KEY",
              get: function () {
                return Ze;
              },
            },
            {
              key: "DefaultType",
              get: function () {
                return ot;
              },
            },
          ]),
          n
        );
      })(Ve);
    (t.fn[Ge] = st._jQueryInterface),
      (t.fn[Ge].Constructor = st),
      (t.fn[Ge].noConflict = function () {
        return (t.fn[Ge] = et), st._jQueryInterface;
      });
    var at = "scrollspy",
      lt = "bs.scrollspy",
      ct = "." + lt,
      ut = t.fn[at],
      dt = { offset: 10, method: "auto", target: "" },
      pt = { offset: "number", method: "string", target: "(string|element)" },
      ft = {
        ACTIVATE: "activate" + ct,
        SCROLL: "scroll" + ct,
        LOAD_DATA_API: "load" + ct + ".data-api",
      },
      ht = "active",
      gt = ".nav, .list-group",
      mt = ".nav-link",
      vt = ".list-group-item",
      yt = ".dropdown-item",
      bt = "position",
      _t = (function () {
        function e(e, n) {
          var i = this;
          (this._element = e),
            (this._scrollElement = "BODY" === e.tagName ? window : e),
            (this._config = this._getConfig(n)),
            (this._selector =
              this._config.target +
              " " +
              mt +
              "," +
              this._config.target +
              " " +
              vt +
              "," +
              this._config.target +
              " " +
              yt),
            (this._offsets = []),
            (this._targets = []),
            (this._activeTarget = null),
            (this._scrollHeight = 0),
            t(this._scrollElement).on(ft.SCROLL, function (e) {
              return i._process(e);
            }),
            this.refresh(),
            this._process();
        }
        var n = e.prototype;
        return (
          (n.refresh = function () {
            var e = this,
              n =
                this._scrollElement === this._scrollElement.window
                  ? "offset"
                  : bt,
              i = "auto" === this._config.method ? n : this._config.method,
              o = i === bt ? this._getScrollTop() : 0;
            (this._offsets = []),
              (this._targets = []),
              (this._scrollHeight = this._getScrollHeight()),
              [].slice
                .call(document.querySelectorAll(this._selector))
                .map(function (e) {
                  var n,
                    r = l.getSelectorFromElement(e);
                  if ((r && (n = document.querySelector(r)), n)) {
                    var s = n.getBoundingClientRect();
                    if (s.width || s.height) return [t(n)[i]().top + o, r];
                  }
                  return null;
                })
                .filter(function (e) {
                  return e;
                })
                .sort(function (e, t) {
                  return e[0] - t[0];
                })
                .forEach(function (t) {
                  e._offsets.push(t[0]), e._targets.push(t[1]);
                });
          }),
          (n.dispose = function () {
            t.removeData(this._element, lt),
              t(this._scrollElement).off(ct),
              (this._element = null),
              (this._scrollElement = null),
              (this._config = null),
              (this._selector = null),
              (this._offsets = null),
              (this._targets = null),
              (this._activeTarget = null),
              (this._scrollHeight = null);
          }),
          (n._getConfig = function (e) {
            if (
              "string" !=
              typeof (e = s({}, dt, {}, "object" == typeof e && e ? e : {}))
                .target
            ) {
              var n = t(e.target).attr("id");
              n || ((n = l.getUID(at)), t(e.target).attr("id", n)),
                (e.target = "#" + n);
            }
            return l.typeCheckConfig(at, e, pt), e;
          }),
          (n._getScrollTop = function () {
            return this._scrollElement === window
              ? this._scrollElement.pageYOffset
              : this._scrollElement.scrollTop;
          }),
          (n._getScrollHeight = function () {
            return (
              this._scrollElement.scrollHeight ||
              Math.max(
                document.body.scrollHeight,
                document.documentElement.scrollHeight
              )
            );
          }),
          (n._getOffsetHeight = function () {
            return this._scrollElement === window
              ? window.innerHeight
              : this._scrollElement.getBoundingClientRect().height;
          }),
          (n._process = function () {
            var e = this._getScrollTop() + this._config.offset,
              t = this._getScrollHeight(),
              n = this._config.offset + t - this._getOffsetHeight();
            if ((this._scrollHeight !== t && this.refresh(), n <= e)) {
              var i = this._targets[this._targets.length - 1];
              this._activeTarget !== i && this._activate(i);
            } else {
              if (
                this._activeTarget &&
                e < this._offsets[0] &&
                0 < this._offsets[0]
              )
                return (this._activeTarget = null), void this._clear();
              for (var o = this._offsets.length; o--; )
                this._activeTarget !== this._targets[o] &&
                  e >= this._offsets[o] &&
                  (void 0 === this._offsets[o + 1] ||
                    e < this._offsets[o + 1]) &&
                  this._activate(this._targets[o]);
            }
          }),
          (n._activate = function (e) {
            (this._activeTarget = e), this._clear();
            var n = this._selector.split(",").map(function (t) {
                return (
                  t + '[data-target="' + e + '"],' + t + '[href="' + e + '"]'
                );
              }),
              i = t([].slice.call(document.querySelectorAll(n.join(","))));
            i.hasClass("dropdown-item")
              ? (i.closest(".dropdown").find(".dropdown-toggle").addClass(ht),
                i.addClass(ht))
              : (i.addClass(ht),
                i
                  .parents(gt)
                  .prev(mt + ", " + vt)
                  .addClass(ht),
                i.parents(gt).prev(".nav-item").children(mt).addClass(ht)),
              t(this._scrollElement).trigger(ft.ACTIVATE, { relatedTarget: e });
          }),
          (n._clear = function () {
            [].slice
              .call(document.querySelectorAll(this._selector))
              .filter(function (e) {
                return e.classList.contains(ht);
              })
              .forEach(function (e) {
                return e.classList.remove(ht);
              });
          }),
          (e._jQueryInterface = function (n) {
            return this.each(function () {
              var i = t(this).data(lt);
              if (
                (i ||
                  ((i = new e(this, "object" == typeof n && n)),
                  t(this).data(lt, i)),
                "string" == typeof n)
              ) {
                if (void 0 === i[n])
                  throw new TypeError('No method named "' + n + '"');
                i[n]();
              }
            });
          }),
          o(e, null, [
            {
              key: "VERSION",
              get: function () {
                return "4.4.1";
              },
            },
            {
              key: "Default",
              get: function () {
                return dt;
              },
            },
          ]),
          e
        );
      })();
    t(window).on(ft.LOAD_DATA_API, function () {
      for (
        var e = [].slice.call(document.querySelectorAll('[data-spy="scroll"]')),
          n = e.length;
        n--;

      ) {
        var i = t(e[n]);
        _t._jQueryInterface.call(i, i.data());
      }
    }),
      (t.fn[at] = _t._jQueryInterface),
      (t.fn[at].Constructor = _t),
      (t.fn[at].noConflict = function () {
        return (t.fn[at] = ut), _t._jQueryInterface;
      });
    var wt = "bs.tab",
      Tt = "." + wt,
      Ct = t.fn.tab,
      St = {
        HIDE: "hide" + Tt,
        HIDDEN: "hidden" + Tt,
        SHOW: "show" + Tt,
        SHOWN: "shown" + Tt,
        CLICK_DATA_API: "click" + Tt + ".data-api",
      },
      kt = "active",
      xt = ".active",
      Et = "> li > .active",
      At = (function () {
        function e(e) {
          this._element = e;
        }
        var n = e.prototype;
        return (
          (n.show = function () {
            var e = this;
            if (
              !(
                (this._element.parentNode &&
                  this._element.parentNode.nodeType === Node.ELEMENT_NODE &&
                  t(this._element).hasClass(kt)) ||
                t(this._element).hasClass("disabled")
              )
            ) {
              var n,
                i,
                o = t(this._element).closest(".nav, .list-group")[0],
                r = l.getSelectorFromElement(this._element);
              if (o) {
                var s = "UL" === o.nodeName || "OL" === o.nodeName ? Et : xt;
                i = (i = t.makeArray(t(o).find(s)))[i.length - 1];
              }
              var a = t.Event(St.HIDE, { relatedTarget: this._element }),
                c = t.Event(St.SHOW, { relatedTarget: i });
              if (
                (i && t(i).trigger(a),
                t(this._element).trigger(c),
                !c.isDefaultPrevented() && !a.isDefaultPrevented())
              ) {
                r && (n = document.querySelector(r)),
                  this._activate(this._element, o);
                var u = function () {
                  var n = t.Event(St.HIDDEN, { relatedTarget: e._element }),
                    o = t.Event(St.SHOWN, { relatedTarget: i });
                  t(i).trigger(n), t(e._element).trigger(o);
                };
                n ? this._activate(n, n.parentNode, u) : u();
              }
            }
          }),
          (n.dispose = function () {
            t.removeData(this._element, wt), (this._element = null);
          }),
          (n._activate = function (e, n, i) {
            function o() {
              return r._transitionComplete(e, s, i);
            }
            var r = this,
              s = (!n || ("UL" !== n.nodeName && "OL" !== n.nodeName)
                ? t(n).children(xt)
                : t(n).find(Et))[0],
              a = i && s && t(s).hasClass("fade");
            if (s && a) {
              var c = l.getTransitionDurationFromElement(s);
              t(s)
                .removeClass("show")
                .one(l.TRANSITION_END, o)
                .emulateTransitionEnd(c);
            } else o();
          }),
          (n._transitionComplete = function (e, n, i) {
            if (n) {
              t(n).removeClass(kt);
              var o = t(n.parentNode).find("> .dropdown-menu .active")[0];
              o && t(o).removeClass(kt),
                "tab" === n.getAttribute("role") &&
                  n.setAttribute("aria-selected", !1);
            }
            if (
              (t(e).addClass(kt),
              "tab" === e.getAttribute("role") &&
                e.setAttribute("aria-selected", !0),
              l.reflow(e),
              e.classList.contains("fade") && e.classList.add("show"),
              e.parentNode && t(e.parentNode).hasClass("dropdown-menu"))
            ) {
              var r = t(e).closest(".dropdown")[0];
              if (r) {
                var s = [].slice.call(r.querySelectorAll(".dropdown-toggle"));
                t(s).addClass(kt);
              }
              e.setAttribute("aria-expanded", !0);
            }
            i && i();
          }),
          (e._jQueryInterface = function (n) {
            return this.each(function () {
              var i = t(this),
                o = i.data(wt);
              if (
                (o || ((o = new e(this)), i.data(wt, o)), "string" == typeof n)
              ) {
                if (void 0 === o[n])
                  throw new TypeError('No method named "' + n + '"');
                o[n]();
              }
            });
          }),
          o(e, null, [
            {
              key: "VERSION",
              get: function () {
                return "4.4.1";
              },
            },
          ]),
          e
        );
      })();
    t(document).on(
      St.CLICK_DATA_API,
      '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',
      function (e) {
        e.preventDefault(), At._jQueryInterface.call(t(this), "show");
      }
    ),
      (t.fn.tab = At._jQueryInterface),
      (t.fn.tab.Constructor = At),
      (t.fn.tab.noConflict = function () {
        return (t.fn.tab = Ct), At._jQueryInterface;
      });
    var Dt = "toast",
      Nt = "bs.toast",
      It = "." + Nt,
      Ot = t.fn[Dt],
      $t = {
        CLICK_DISMISS: "click.dismiss" + It,
        HIDE: "hide" + It,
        HIDDEN: "hidden" + It,
        SHOW: "show" + It,
        SHOWN: "shown" + It,
      },
      jt = "hide",
      Lt = "show",
      Pt = "showing",
      Ht = { animation: "boolean", autohide: "boolean", delay: "number" },
      qt = { animation: !0, autohide: !0, delay: 500 },
      Rt = (function () {
        function e(e, t) {
          (this._element = e),
            (this._config = this._getConfig(t)),
            (this._timeout = null),
            this._setListeners();
        }
        var n = e.prototype;
        return (
          (n.show = function () {
            var e = this,
              n = t.Event($t.SHOW);
            if ((t(this._element).trigger(n), !n.isDefaultPrevented())) {
              this._config.animation && this._element.classList.add("fade");
              var i = function () {
                e._element.classList.remove(Pt),
                  e._element.classList.add(Lt),
                  t(e._element).trigger($t.SHOWN),
                  e._config.autohide &&
                    (e._timeout = setTimeout(function () {
                      e.hide();
                    }, e._config.delay));
              };
              if (
                (this._element.classList.remove(jt),
                l.reflow(this._element),
                this._element.classList.add(Pt),
                this._config.animation)
              ) {
                var o = l.getTransitionDurationFromElement(this._element);
                t(this._element)
                  .one(l.TRANSITION_END, i)
                  .emulateTransitionEnd(o);
              } else i();
            }
          }),
          (n.hide = function () {
            if (this._element.classList.contains(Lt)) {
              var e = t.Event($t.HIDE);
              t(this._element).trigger(e),
                e.isDefaultPrevented() || this._close();
            }
          }),
          (n.dispose = function () {
            clearTimeout(this._timeout),
              (this._timeout = null),
              this._element.classList.contains(Lt) &&
                this._element.classList.remove(Lt),
              t(this._element).off($t.CLICK_DISMISS),
              t.removeData(this._element, Nt),
              (this._element = null),
              (this._config = null);
          }),
          (n._getConfig = function (e) {
            return (
              (e = s(
                {},
                qt,
                {},
                t(this._element).data(),
                {},
                "object" == typeof e && e ? e : {}
              )),
              l.typeCheckConfig(Dt, e, this.constructor.DefaultType),
              e
            );
          }),
          (n._setListeners = function () {
            var e = this;
            t(this._element).on(
              $t.CLICK_DISMISS,
              '[data-dismiss="toast"]',
              function () {
                return e.hide();
              }
            );
          }),
          (n._close = function () {
            function e() {
              n._element.classList.add(jt), t(n._element).trigger($t.HIDDEN);
            }
            var n = this;
            if ((this._element.classList.remove(Lt), this._config.animation)) {
              var i = l.getTransitionDurationFromElement(this._element);
              t(this._element).one(l.TRANSITION_END, e).emulateTransitionEnd(i);
            } else e();
          }),
          (e._jQueryInterface = function (n) {
            return this.each(function () {
              var i = t(this),
                o = i.data(Nt);
              if (
                (o ||
                  ((o = new e(this, "object" == typeof n && n)), i.data(Nt, o)),
                "string" == typeof n)
              ) {
                if (void 0 === o[n])
                  throw new TypeError('No method named "' + n + '"');
                o[n](this);
              }
            });
          }),
          o(e, null, [
            {
              key: "VERSION",
              get: function () {
                return "4.4.1";
              },
            },
            {
              key: "DefaultType",
              get: function () {
                return Ht;
              },
            },
            {
              key: "Default",
              get: function () {
                return qt;
              },
            },
          ]),
          e
        );
      })();
    (t.fn[Dt] = Rt._jQueryInterface),
      (t.fn[Dt].Constructor = Rt),
      (t.fn[Dt].noConflict = function () {
        return (t.fn[Dt] = Ot), Rt._jQueryInterface;
      }),
      (e.Alert = h),
      (e.Button = k),
      (e.Carousel = M),
      (e.Collapse = Z),
      (e.Dropdown = he),
      (e.Modal = De),
      (e.Popover = st),
      (e.Scrollspy = _t),
      (e.Tab = At),
      (e.Toast = Rt),
      (e.Tooltip = Ve),
      (e.Util = l),
      Object.defineProperty(e, "__esModule", { value: !0 });
  }),
  (function (e) {
    "use strict";
    "function" == typeof define && define.amd
      ? define(["jquery"], e)
      : "undefined" != typeof exports
      ? (module.exports = e(require("jquery")))
      : e(jQuery);
  })(function (e) {
    "use strict";
    var t = window.Slick || {};
    ((t = (function () {
      var t = 0;
      return function (n, i) {
        var o,
          r = this;
        (r.defaults = {
          accessibility: !0,
          adaptiveHeight: !1,
          appendArrows: e(n),
          appendDots: e(n),
          arrows: !0,
          asNavFor: null,
          prevArrow:
            '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
          nextArrow:
            '<button class="slick-next" aria-label="Next" type="button">Next</button>',
          autoplay: !1,
          autoplaySpeed: 3e3,
          centerMode: !1,
          centerPadding: "50px",
          cssEase: "ease",
          customPaging: function (t, n) {
            return e('<button type="button" />').text(n + 1);
          },
          dots: !1,
          dotsClass: "slick-dots",
          draggable: !0,
          easing: "linear",
          edgeFriction: 0.35,
          fade: !1,
          focusOnSelect: !1,
          focusOnChange: !1,
          infinite: !0,
          initialSlide: 0,
          lazyLoad: "ondemand",
          mobileFirst: !1,
          pauseOnHover: !0,
          pauseOnFocus: !0,
          pauseOnDotsHover: !1,
          respondTo: "window",
          responsive: null,
          rows: 1,
          rtl: !1,
          slide: "",
          slidesPerRow: 1,
          slidesToShow: 1,
          slidesToScroll: 1,
          speed: 500,
          swipe: !0,
          swipeToSlide: !1,
          touchMove: !0,
          touchThreshold: 5,
          useCSS: !0,
          useTransform: !0,
          variableWidth: !1,
          vertical: !1,
          verticalSwiping: !1,
          waitForAnimate: !0,
          zIndex: 1e3,
        }),
          (r.initials = {
            animating: !1,
            dragging: !1,
            autoPlayTimer: null,
            currentDirection: 0,
            currentLeft: null,
            currentSlide: 0,
            direction: 1,
            $dots: null,
            listWidth: null,
            listHeight: null,
            loadIndex: 0,
            $nextArrow: null,
            $prevArrow: null,
            scrolling: !1,
            slideCount: null,
            slideWidth: null,
            $slideTrack: null,
            $slides: null,
            sliding: !1,
            slideOffset: 0,
            swipeLeft: null,
            swiping: !1,
            $list: null,
            touchObject: {},
            transformsEnabled: !1,
            unslicked: !1,
          }),
          e.extend(r, r.initials),
          (r.activeBreakpoint = null),
          (r.animType = null),
          (r.animProp = null),
          (r.breakpoints = []),
          (r.breakpointSettings = []),
          (r.cssTransitions = !1),
          (r.focussed = !1),
          (r.interrupted = !1),
          (r.hidden = "hidden"),
          (r.paused = !0),
          (r.positionProp = null),
          (r.respondTo = null),
          (r.rowCount = 1),
          (r.shouldClick = !0),
          (r.$slider = e(n)),
          (r.$slidesCache = null),
          (r.transformType = null),
          (r.transitionType = null),
          (r.visibilityChange = "visibilitychange"),
          (r.windowWidth = 0),
          (r.windowTimer = null),
          (o = e(n).data("slick") || {}),
          (r.options = e.extend({}, r.defaults, i, o)),
          (r.currentSlide = r.options.initialSlide),
          (r.originalSettings = r.options),
          void 0 !== document.mozHidden
            ? ((r.hidden = "mozHidden"),
              (r.visibilityChange = "mozvisibilitychange"))
            : void 0 !== document.webkitHidden &&
              ((r.hidden = "webkitHidden"),
              (r.visibilityChange = "webkitvisibilitychange")),
          (r.autoPlay = e.proxy(r.autoPlay, r)),
          (r.autoPlayClear = e.proxy(r.autoPlayClear, r)),
          (r.autoPlayIterator = e.proxy(r.autoPlayIterator, r)),
          (r.changeSlide = e.proxy(r.changeSlide, r)),
          (r.clickHandler = e.proxy(r.clickHandler, r)),
          (r.selectHandler = e.proxy(r.selectHandler, r)),
          (r.setPosition = e.proxy(r.setPosition, r)),
          (r.swipeHandler = e.proxy(r.swipeHandler, r)),
          (r.dragHandler = e.proxy(r.dragHandler, r)),
          (r.keyHandler = e.proxy(r.keyHandler, r)),
          (r.instanceUid = t++),
          (r.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/),
          r.registerBreakpoints(),
          r.init(!0);
      };
    })()).prototype.activateADA = function () {
      this.$slideTrack
        .find(".slick-active")
        .attr({ "aria-hidden": "false" })
        .find("a, input, button, select")
        .attr({ tabindex: "0" });
    }),
      (t.prototype.addSlide = t.prototype.slickAdd = function (t, n, i) {
        var o = this;
        if ("boolean" == typeof n) (i = n), (n = null);
        else if (n < 0 || n >= o.slideCount) return !1;
        o.unload(),
          "number" == typeof n
            ? 0 === n && 0 === o.$slides.length
              ? e(t).appendTo(o.$slideTrack)
              : i
              ? e(t).insertBefore(o.$slides.eq(n))
              : e(t).insertAfter(o.$slides.eq(n))
            : !0 === i
            ? e(t).prependTo(o.$slideTrack)
            : e(t).appendTo(o.$slideTrack),
          (o.$slides = o.$slideTrack.children(this.options.slide)),
          o.$slideTrack.children(this.options.slide).detach(),
          o.$slideTrack.append(o.$slides),
          o.$slides.each(function (t, n) {
            e(n).attr("data-slick-index", t);
          }),
          (o.$slidesCache = o.$slides),
          o.reinit();
      }),
      (t.prototype.animateHeight = function () {
        var e = this;
        if (
          1 === e.options.slidesToShow &&
          !0 === e.options.adaptiveHeight &&
          !1 === e.options.vertical
        ) {
          var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
          e.$list.animate({ height: t }, e.options.speed);
        }
      }),
      (t.prototype.animateSlide = function (t, n) {
        var i = {},
          o = this;
        o.animateHeight(),
          !0 === o.options.rtl && !1 === o.options.vertical && (t = -t),
          !1 === o.transformsEnabled
            ? !1 === o.options.vertical
              ? o.$slideTrack.animate(
                  { left: t },
                  o.options.speed,
                  o.options.easing,
                  n
                )
              : o.$slideTrack.animate(
                  { top: t },
                  o.options.speed,
                  o.options.easing,
                  n
                )
            : !1 === o.cssTransitions
            ? (!0 === o.options.rtl && (o.currentLeft = -o.currentLeft),
              e({ animStart: o.currentLeft }).animate(
                { animStart: t },
                {
                  duration: o.options.speed,
                  easing: o.options.easing,
                  step: function (e) {
                    (e = Math.ceil(e)),
                      !1 === o.options.vertical
                        ? ((i[o.animType] = "translate(" + e + "px, 0px)"),
                          o.$slideTrack.css(i))
                        : ((i[o.animType] = "translate(0px," + e + "px)"),
                          o.$slideTrack.css(i));
                  },
                  complete: function () {
                    n && n.call();
                  },
                }
              ))
            : (o.applyTransition(),
              (t = Math.ceil(t)),
              !1 === o.options.vertical
                ? (i[o.animType] = "translate3d(" + t + "px, 0px, 0px)")
                : (i[o.animType] = "translate3d(0px," + t + "px, 0px)"),
              o.$slideTrack.css(i),
              n &&
                setTimeout(function () {
                  o.disableTransition(), n.call();
                }, o.options.speed));
      }),
      (t.prototype.getNavTarget = function () {
        var t = this.options.asNavFor;
        return t && null !== t && (t = e(t).not(this.$slider)), t;
      }),
      (t.prototype.asNavFor = function (t) {
        var n = this.getNavTarget();
        null !== n &&
          "object" == typeof n &&
          n.each(function () {
            var n = e(this).slick("getSlick");
            n.unslicked || n.slideHandler(t, !0);
          });
      }),
      (t.prototype.applyTransition = function (e) {
        var t = this,
          n = {};
        !1 === t.options.fade
          ? (n[t.transitionType] =
              t.transformType +
              " " +
              t.options.speed +
              "ms " +
              t.options.cssEase)
          : (n[t.transitionType] =
              "opacity " + t.options.speed + "ms " + t.options.cssEase),
          !1 === t.options.fade ? t.$slideTrack.css(n) : t.$slides.eq(e).css(n);
      }),
      (t.prototype.autoPlay = function () {
        var e = this;
        e.autoPlayClear(),
          e.slideCount > e.options.slidesToShow &&
            (e.autoPlayTimer = setInterval(
              e.autoPlayIterator,
              e.options.autoplaySpeed
            ));
      }),
      (t.prototype.autoPlayClear = function () {
        this.autoPlayTimer && clearInterval(this.autoPlayTimer);
      }),
      (t.prototype.autoPlayIterator = function () {
        var e = this,
          t = e.currentSlide + e.options.slidesToScroll;
        e.paused ||
          e.interrupted ||
          e.focussed ||
          (!1 === e.options.infinite &&
            (1 === e.direction && e.currentSlide + 1 === e.slideCount - 1
              ? (e.direction = 0)
              : 0 === e.direction &&
                ((t = e.currentSlide - e.options.slidesToScroll),
                e.currentSlide - 1 == 0 && (e.direction = 1))),
          e.slideHandler(t));
      }),
      (t.prototype.buildArrows = function () {
        var t = this;
        !0 === t.options.arrows &&
          ((t.$prevArrow = e(t.options.prevArrow).addClass("slick-arrow")),
          (t.$nextArrow = e(t.options.nextArrow).addClass("slick-arrow")),
          t.slideCount > t.options.slidesToShow
            ? (t.$prevArrow
                .removeClass("slick-hidden")
                .removeAttr("aria-hidden tabindex"),
              t.$nextArrow
                .removeClass("slick-hidden")
                .removeAttr("aria-hidden tabindex"),
              t.htmlExpr.test(t.options.prevArrow) &&
                t.$prevArrow.prependTo(t.options.appendArrows),
              t.htmlExpr.test(t.options.nextArrow) &&
                t.$nextArrow.appendTo(t.options.appendArrows),
              !0 !== t.options.infinite &&
                t.$prevArrow
                  .addClass("slick-disabled")
                  .attr("aria-disabled", "true"))
            : t.$prevArrow
                .add(t.$nextArrow)
                .addClass("slick-hidden")
                .attr({ "aria-disabled": "true", tabindex: "-1" }));
      }),
      (t.prototype.buildDots = function () {
        var t,
          n,
          i = this;
        if (!0 === i.options.dots) {
          for (
            i.$slider.addClass("slick-dotted"),
              n = e("<ul />").addClass(i.options.dotsClass),
              t = 0;
            t <= i.getDotCount();
            t += 1
          )
            n.append(
              e("<li />").append(i.options.customPaging.call(this, i, t))
            );
          (i.$dots = n.appendTo(i.options.appendDots)),
            i.$dots.find("li").first().addClass("slick-active");
        }
      }),
      (t.prototype.buildOut = function () {
        var t = this;
        (t.$slides = t.$slider
          .children(t.options.slide + ":not(.slick-cloned)")
          .addClass("slick-slide")),
          (t.slideCount = t.$slides.length),
          t.$slides.each(function (t, n) {
            e(n)
              .attr("data-slick-index", t)
              .data("originalStyling", e(n).attr("style") || "");
          }),
          t.$slider.addClass("slick-slider"),
          (t.$slideTrack =
            0 === t.slideCount
              ? e('<div class="slick-track"/>').appendTo(t.$slider)
              : t.$slides.wrapAll('<div class="slick-track"/>').parent()),
          (t.$list = t.$slideTrack.wrap('<div class="slick-list"/>').parent()),
          t.$slideTrack.css("opacity", 0),
          (!0 !== t.options.centerMode && !0 !== t.options.swipeToSlide) ||
            (t.options.slidesToScroll = 1),
          e("img[data-lazy]", t.$slider).not("[src]").addClass("slick-loading"),
          t.setupInfinite(),
          t.buildArrows(),
          t.buildDots(),
          t.updateDots(),
          t.setSlideClasses(
            "number" == typeof t.currentSlide ? t.currentSlide : 0
          ),
          !0 === t.options.draggable && t.$list.addClass("draggable");
      }),
      (t.prototype.buildRows = function () {
        var e,
          t,
          n,
          i,
          o,
          r,
          s,
          a = this;
        if (
          ((i = document.createDocumentFragment()),
          (r = a.$slider.children()),
          a.options.rows > 1)
        ) {
          for (
            s = a.options.slidesPerRow * a.options.rows,
              o = Math.ceil(r.length / s),
              e = 0;
            e < o;
            e++
          ) {
            var l = document.createElement("div");
            for (t = 0; t < a.options.rows; t++) {
              var c = document.createElement("div");
              for (n = 0; n < a.options.slidesPerRow; n++) {
                var u = e * s + (t * a.options.slidesPerRow + n);
                r.get(u) && c.appendChild(r.get(u));
              }
              l.appendChild(c);
            }
            i.appendChild(l);
          }
          a.$slider.empty().append(i),
            a.$slider
              .children()
              .children()
              .children()
              .css({
                width: 100 / a.options.slidesPerRow + "%",
                display: "inline-block",
              });
        }
      }),
      (t.prototype.checkResponsive = function (t, n) {
        var i,
          o,
          r,
          s = this,
          a = !1,
          l = s.$slider.width(),
          c = window.innerWidth || e(window).width();
        if (
          ("window" === s.respondTo
            ? (r = c)
            : "slider" === s.respondTo
            ? (r = l)
            : "min" === s.respondTo && (r = Math.min(c, l)),
          s.options.responsive &&
            s.options.responsive.length &&
            null !== s.options.responsive)
        ) {
          for (i in ((o = null), s.breakpoints))
            s.breakpoints.hasOwnProperty(i) &&
              (!1 === s.originalSettings.mobileFirst
                ? r < s.breakpoints[i] && (o = s.breakpoints[i])
                : r > s.breakpoints[i] && (o = s.breakpoints[i]));
          null !== o
            ? null !== s.activeBreakpoint
              ? (o !== s.activeBreakpoint || n) &&
                ((s.activeBreakpoint = o),
                "unslick" === s.breakpointSettings[o]
                  ? s.unslick(o)
                  : ((s.options = e.extend(
                      {},
                      s.originalSettings,
                      s.breakpointSettings[o]
                    )),
                    !0 === t && (s.currentSlide = s.options.initialSlide),
                    s.refresh(t)),
                (a = o))
              : ((s.activeBreakpoint = o),
                "unslick" === s.breakpointSettings[o]
                  ? s.unslick(o)
                  : ((s.options = e.extend(
                      {},
                      s.originalSettings,
                      s.breakpointSettings[o]
                    )),
                    !0 === t && (s.currentSlide = s.options.initialSlide),
                    s.refresh(t)),
                (a = o))
            : null !== s.activeBreakpoint &&
              ((s.activeBreakpoint = null),
              (s.options = s.originalSettings),
              !0 === t && (s.currentSlide = s.options.initialSlide),
              s.refresh(t),
              (a = o)),
            t || !1 === a || s.$slider.trigger("breakpoint", [s, a]);
        }
      }),
      (t.prototype.changeSlide = function (t, n) {
        var i,
          o,
          r = this,
          s = e(t.currentTarget);
        switch (
          (s.is("a") && t.preventDefault(),
          s.is("li") || (s = s.closest("li")),
          (i =
            r.slideCount % r.options.slidesToScroll != 0
              ? 0
              : (r.slideCount - r.currentSlide) % r.options.slidesToScroll),
          t.data.message)
        ) {
          case "previous":
            (o =
              0 === i ? r.options.slidesToScroll : r.options.slidesToShow - i),
              r.slideCount > r.options.slidesToShow &&
                r.slideHandler(r.currentSlide - o, !1, n);
            break;
          case "next":
            (o = 0 === i ? r.options.slidesToScroll : i),
              r.slideCount > r.options.slidesToShow &&
                r.slideHandler(r.currentSlide + o, !1, n);
            break;
          case "index":
            var a =
              0 === t.data.index
                ? 0
                : t.data.index || s.index() * r.options.slidesToScroll;
            r.slideHandler(r.checkNavigable(a), !1, n),
              s.children().trigger("focus");
            break;
          default:
            return;
        }
      }),
      (t.prototype.checkNavigable = function (e) {
        var t, n;
        if (((n = 0), e > (t = this.getNavigableIndexes())[t.length - 1]))
          e = t[t.length - 1];
        else
          for (var i in t) {
            if (e < t[i]) {
              e = n;
              break;
            }
            n = t[i];
          }
        return e;
      }),
      (t.prototype.cleanUpEvents = function () {
        var t = this;
        t.options.dots &&
          null !== t.$dots &&
          (e("li", t.$dots)
            .off("click.slick", t.changeSlide)
            .off("mouseenter.slick", e.proxy(t.interrupt, t, !0))
            .off("mouseleave.slick", e.proxy(t.interrupt, t, !1)),
          !0 === t.options.accessibility &&
            t.$dots.off("keydown.slick", t.keyHandler)),
          t.$slider.off("focus.slick blur.slick"),
          !0 === t.options.arrows &&
            t.slideCount > t.options.slidesToShow &&
            (t.$prevArrow && t.$prevArrow.off("click.slick", t.changeSlide),
            t.$nextArrow && t.$nextArrow.off("click.slick", t.changeSlide),
            !0 === t.options.accessibility &&
              (t.$prevArrow && t.$prevArrow.off("keydown.slick", t.keyHandler),
              t.$nextArrow && t.$nextArrow.off("keydown.slick", t.keyHandler))),
          t.$list.off("touchstart.slick mousedown.slick", t.swipeHandler),
          t.$list.off("touchmove.slick mousemove.slick", t.swipeHandler),
          t.$list.off("touchend.slick mouseup.slick", t.swipeHandler),
          t.$list.off("touchcancel.slick mouseleave.slick", t.swipeHandler),
          t.$list.off("click.slick", t.clickHandler),
          e(document).off(t.visibilityChange, t.visibility),
          t.cleanUpSlideEvents(),
          !0 === t.options.accessibility &&
            t.$list.off("keydown.slick", t.keyHandler),
          !0 === t.options.focusOnSelect &&
            e(t.$slideTrack).children().off("click.slick", t.selectHandler),
          e(window).off(
            "orientationchange.slick.slick-" + t.instanceUid,
            t.orientationChange
          ),
          e(window).off("resize.slick.slick-" + t.instanceUid, t.resize),
          e("[draggable!=true]", t.$slideTrack).off(
            "dragstart",
            t.preventDefault
          ),
          e(window).off("load.slick.slick-" + t.instanceUid, t.setPosition);
      }),
      (t.prototype.cleanUpSlideEvents = function () {
        var t = this;
        t.$list.off("mouseenter.slick", e.proxy(t.interrupt, t, !0)),
          t.$list.off("mouseleave.slick", e.proxy(t.interrupt, t, !1));
      }),
      (t.prototype.cleanUpRows = function () {
        var e,
          t = this;
        t.options.rows > 1 &&
          ((e = t.$slides.children().children()).removeAttr("style"),
          t.$slider.empty().append(e));
      }),
      (t.prototype.clickHandler = function (e) {
        !1 === this.shouldClick &&
          (e.stopImmediatePropagation(),
          e.stopPropagation(),
          e.preventDefault());
      }),
      (t.prototype.destroy = function (t) {
        var n = this;
        n.autoPlayClear(),
          (n.touchObject = {}),
          n.cleanUpEvents(),
          e(".slick-cloned", n.$slider).detach(),
          n.$dots && n.$dots.remove(),
          n.$prevArrow &&
            n.$prevArrow.length &&
            (n.$prevArrow
              .removeClass("slick-disabled slick-arrow slick-hidden")
              .removeAttr("aria-hidden aria-disabled tabindex")
              .css("display", ""),
            n.htmlExpr.test(n.options.prevArrow) && n.$prevArrow.remove()),
          n.$nextArrow &&
            n.$nextArrow.length &&
            (n.$nextArrow
              .removeClass("slick-disabled slick-arrow slick-hidden")
              .removeAttr("aria-hidden aria-disabled tabindex")
              .css("display", ""),
            n.htmlExpr.test(n.options.nextArrow) && n.$nextArrow.remove()),
          n.$slides &&
            (n.$slides
              .removeClass(
                "slick-slide slick-active slick-center slick-visible slick-current"
              )
              .removeAttr("aria-hidden")
              .removeAttr("data-slick-index")
              .each(function () {
                e(this).attr("style", e(this).data("originalStyling"));
              }),
            n.$slideTrack.children(this.options.slide).detach(),
            n.$slideTrack.detach(),
            n.$list.detach(),
            n.$slider.append(n.$slides)),
          n.cleanUpRows(),
          n.$slider.removeClass("slick-slider"),
          n.$slider.removeClass("slick-initialized"),
          n.$slider.removeClass("slick-dotted"),
          (n.unslicked = !0),
          t || n.$slider.trigger("destroy", [n]);
      }),
      (t.prototype.disableTransition = function (e) {
        var t = this,
          n = {};
        (n[t.transitionType] = ""),
          !1 === t.options.fade ? t.$slideTrack.css(n) : t.$slides.eq(e).css(n);
      }),
      (t.prototype.fadeSlide = function (e, t) {
        var n = this;
        !1 === n.cssTransitions
          ? (n.$slides.eq(e).css({ zIndex: n.options.zIndex }),
            n.$slides
              .eq(e)
              .animate({ opacity: 1 }, n.options.speed, n.options.easing, t))
          : (n.applyTransition(e),
            n.$slides.eq(e).css({ opacity: 1, zIndex: n.options.zIndex }),
            t &&
              setTimeout(function () {
                n.disableTransition(e), t.call();
              }, n.options.speed));
      }),
      (t.prototype.fadeSlideOut = function (e) {
        var t = this;
        !1 === t.cssTransitions
          ? t.$slides
              .eq(e)
              .animate(
                { opacity: 0, zIndex: t.options.zIndex - 2 },
                t.options.speed,
                t.options.easing
              )
          : (t.applyTransition(e),
            t.$slides.eq(e).css({ opacity: 0, zIndex: t.options.zIndex - 2 }));
      }),
      (t.prototype.filterSlides = t.prototype.slickFilter = function (e) {
        var t = this;
        null !== e &&
          ((t.$slidesCache = t.$slides),
          t.unload(),
          t.$slideTrack.children(this.options.slide).detach(),
          t.$slidesCache.filter(e).appendTo(t.$slideTrack),
          t.reinit());
      }),
      (t.prototype.focusHandler = function () {
        var t = this;
        t.$slider
          .off("focus.slick blur.slick")
          .on("focus.slick blur.slick", "*", function (n) {
            n.stopImmediatePropagation();
            var i = e(this);
            setTimeout(function () {
              t.options.pauseOnFocus &&
                ((t.focussed = i.is(":focus")), t.autoPlay());
            }, 0);
          });
      }),
      (t.prototype.getCurrent = t.prototype.slickCurrentSlide = function () {
        return this.currentSlide;
      }),
      (t.prototype.getDotCount = function () {
        var e = this,
          t = 0,
          n = 0,
          i = 0;
        if (!0 === e.options.infinite)
          if (e.slideCount <= e.options.slidesToShow) ++i;
          else
            for (; t < e.slideCount; )
              ++i,
                (t = n + e.options.slidesToScroll),
                (n +=
                  e.options.slidesToScroll <= e.options.slidesToShow
                    ? e.options.slidesToScroll
                    : e.options.slidesToShow);
        else if (!0 === e.options.centerMode) i = e.slideCount;
        else if (e.options.asNavFor)
          for (; t < e.slideCount; )
            ++i,
              (t = n + e.options.slidesToScroll),
              (n +=
                e.options.slidesToScroll <= e.options.slidesToShow
                  ? e.options.slidesToScroll
                  : e.options.slidesToShow);
        else
          i =
            1 +
            Math.ceil(
              (e.slideCount - e.options.slidesToShow) / e.options.slidesToScroll
            );
        return i - 1;
      }),
      (t.prototype.getLeft = function (e) {
        var t,
          n,
          i,
          o,
          r = this,
          s = 0;
        return (
          (r.slideOffset = 0),
          (n = r.$slides.first().outerHeight(!0)),
          !0 === r.options.infinite
            ? (r.slideCount > r.options.slidesToShow &&
                ((r.slideOffset = r.slideWidth * r.options.slidesToShow * -1),
                (o = -1),
                !0 === r.options.vertical &&
                  !0 === r.options.centerMode &&
                  (2 === r.options.slidesToShow
                    ? (o = -1.5)
                    : 1 === r.options.slidesToShow && (o = -2)),
                (s = n * r.options.slidesToShow * o)),
              r.slideCount % r.options.slidesToScroll != 0 &&
                e + r.options.slidesToScroll > r.slideCount &&
                r.slideCount > r.options.slidesToShow &&
                (e > r.slideCount
                  ? ((r.slideOffset =
                      (r.options.slidesToShow - (e - r.slideCount)) *
                      r.slideWidth *
                      -1),
                    (s =
                      (r.options.slidesToShow - (e - r.slideCount)) * n * -1))
                  : ((r.slideOffset =
                      (r.slideCount % r.options.slidesToScroll) *
                      r.slideWidth *
                      -1),
                    (s = (r.slideCount % r.options.slidesToScroll) * n * -1))))
            : e + r.options.slidesToShow > r.slideCount &&
              ((r.slideOffset =
                (e + r.options.slidesToShow - r.slideCount) * r.slideWidth),
              (s = (e + r.options.slidesToShow - r.slideCount) * n)),
          r.slideCount <= r.options.slidesToShow &&
            ((r.slideOffset = 0), (s = 0)),
          !0 === r.options.centerMode && r.slideCount <= r.options.slidesToShow
            ? (r.slideOffset =
                (r.slideWidth * Math.floor(r.options.slidesToShow)) / 2 -
                (r.slideWidth * r.slideCount) / 2)
            : !0 === r.options.centerMode && !0 === r.options.infinite
            ? (r.slideOffset +=
                r.slideWidth * Math.floor(r.options.slidesToShow / 2) -
                r.slideWidth)
            : !0 === r.options.centerMode &&
              ((r.slideOffset = 0),
              (r.slideOffset +=
                r.slideWidth * Math.floor(r.options.slidesToShow / 2))),
          (t =
            !1 === r.options.vertical
              ? e * r.slideWidth * -1 + r.slideOffset
              : e * n * -1 + s),
          !0 === r.options.variableWidth &&
            ((i =
              r.slideCount <= r.options.slidesToShow ||
              !1 === r.options.infinite
                ? r.$slideTrack.children(".slick-slide").eq(e)
                : r.$slideTrack
                    .children(".slick-slide")
                    .eq(e + r.options.slidesToShow)),
            (t =
              !0 === r.options.rtl
                ? i[0]
                  ? -1 * (r.$slideTrack.width() - i[0].offsetLeft - i.width())
                  : 0
                : i[0]
                ? -1 * i[0].offsetLeft
                : 0),
            !0 === r.options.centerMode &&
              ((i =
                r.slideCount <= r.options.slidesToShow ||
                !1 === r.options.infinite
                  ? r.$slideTrack.children(".slick-slide").eq(e)
                  : r.$slideTrack
                      .children(".slick-slide")
                      .eq(e + r.options.slidesToShow + 1)),
              (t =
                !0 === r.options.rtl
                  ? i[0]
                    ? -1 * (r.$slideTrack.width() - i[0].offsetLeft - i.width())
                    : 0
                  : i[0]
                  ? -1 * i[0].offsetLeft
                  : 0),
              (t += (r.$list.width() - i.outerWidth()) / 2))),
          t
        );
      }),
      (t.prototype.getOption = t.prototype.slickGetOption = function (e) {
        return this.options[e];
      }),
      (t.prototype.getNavigableIndexes = function () {
        var e,
          t = this,
          n = 0,
          i = 0,
          o = [];
        for (
          !1 === t.options.infinite
            ? (e = t.slideCount)
            : ((n = -1 * t.options.slidesToScroll),
              (i = -1 * t.options.slidesToScroll),
              (e = 2 * t.slideCount));
          n < e;

        )
          o.push(n),
            (n = i + t.options.slidesToScroll),
            (i +=
              t.options.slidesToScroll <= t.options.slidesToShow
                ? t.options.slidesToScroll
                : t.options.slidesToShow);
        return o;
      }),
      (t.prototype.getSlick = function () {
        return this;
      }),
      (t.prototype.getSlideCount = function () {
        var t,
          n,
          i = this;
        return (
          (n =
            !0 === i.options.centerMode
              ? i.slideWidth * Math.floor(i.options.slidesToShow / 2)
              : 0),
          !0 === i.options.swipeToSlide
            ? (i.$slideTrack.find(".slick-slide").each(function (o, r) {
                if (r.offsetLeft - n + e(r).outerWidth() / 2 > -1 * i.swipeLeft)
                  return (t = r), !1;
              }),
              Math.abs(e(t).attr("data-slick-index") - i.currentSlide) || 1)
            : i.options.slidesToScroll
        );
      }),
      (t.prototype.goTo = t.prototype.slickGoTo = function (e, t) {
        this.changeSlide({ data: { message: "index", index: parseInt(e) } }, t);
      }),
      (t.prototype.init = function (t) {
        var n = this;
        e(n.$slider).hasClass("slick-initialized") ||
          (e(n.$slider).addClass("slick-initialized"),
          n.buildRows(),
          n.buildOut(),
          n.setProps(),
          n.startLoad(),
          n.loadSlider(),
          n.initializeEvents(),
          n.updateArrows(),
          n.updateDots(),
          n.checkResponsive(!0),
          n.focusHandler()),
          t && n.$slider.trigger("init", [n]),
          !0 === n.options.accessibility && n.initADA(),
          n.options.autoplay && ((n.paused = !1), n.autoPlay());
      }),
      (t.prototype.initADA = function () {
        var t = this,
          n = Math.ceil(t.slideCount / t.options.slidesToShow),
          i = t.getNavigableIndexes().filter(function (e) {
            return e >= 0 && e < t.slideCount;
          });
        t.$slides
          .add(t.$slideTrack.find(".slick-cloned"))
          .attr({ "aria-hidden": "true", tabindex: "-1" })
          .find("a, input, button, select")
          .attr({ tabindex: "-1" }),
          null !== t.$dots &&
            (t.$slides
              .not(t.$slideTrack.find(".slick-cloned"))
              .each(function (n) {
                var o = i.indexOf(n);
                e(this).attr({
                  role: "tabpanel",
                  id: "slick-slide" + t.instanceUid + n,
                  tabindex: -1,
                }),
                  -1 !== o &&
                    e(this).attr({
                      "aria-describedby":
                        "slick-slide-control" + t.instanceUid + o,
                    });
              }),
            t.$dots
              .attr("role", "tablist")
              .find("li")
              .each(function (o) {
                var r = i[o];
                e(this).attr({ role: "presentation" }),
                  e(this)
                    .find("button")
                    .first()
                    .attr({
                      role: "tab",
                      id: "slick-slide-control" + t.instanceUid + o,
                      "aria-controls": "slick-slide" + t.instanceUid + r,
                      "aria-label": o + 1 + " of " + n,
                      "aria-selected": null,
                      tabindex: "-1",
                    });
              })
              .eq(t.currentSlide)
              .find("button")
              .attr({ "aria-selected": "true", tabindex: "0" })
              .end());
        for (var o = t.currentSlide, r = o + t.options.slidesToShow; o < r; o++)
          t.$slides.eq(o).attr("tabindex", 0);
        t.activateADA();
      }),
      (t.prototype.initArrowEvents = function () {
        var e = this;
        !0 === e.options.arrows &&
          e.slideCount > e.options.slidesToShow &&
          (e.$prevArrow
            .off("click.slick")
            .on("click.slick", { message: "previous" }, e.changeSlide),
          e.$nextArrow
            .off("click.slick")
            .on("click.slick", { message: "next" }, e.changeSlide),
          !0 === e.options.accessibility &&
            (e.$prevArrow.on("keydown.slick", e.keyHandler),
            e.$nextArrow.on("keydown.slick", e.keyHandler)));
      }),
      (t.prototype.initDotEvents = function () {
        var t = this;
        !0 === t.options.dots &&
          (e("li", t.$dots).on(
            "click.slick",
            { message: "index" },
            t.changeSlide
          ),
          !0 === t.options.accessibility &&
            t.$dots.on("keydown.slick", t.keyHandler)),
          !0 === t.options.dots &&
            !0 === t.options.pauseOnDotsHover &&
            e("li", t.$dots)
              .on("mouseenter.slick", e.proxy(t.interrupt, t, !0))
              .on("mouseleave.slick", e.proxy(t.interrupt, t, !1));
      }),
      (t.prototype.initSlideEvents = function () {
        var t = this;
        t.options.pauseOnHover &&
          (t.$list.on("mouseenter.slick", e.proxy(t.interrupt, t, !0)),
          t.$list.on("mouseleave.slick", e.proxy(t.interrupt, t, !1)));
      }),
      (t.prototype.initializeEvents = function () {
        var t = this;
        t.initArrowEvents(),
          t.initDotEvents(),
          t.initSlideEvents(),
          t.$list.on(
            "touchstart.slick mousedown.slick",
            { action: "start" },
            t.swipeHandler
          ),
          t.$list.on(
            "touchmove.slick mousemove.slick",
            { action: "move" },
            t.swipeHandler
          ),
          t.$list.on(
            "touchend.slick mouseup.slick",
            { action: "end" },
            t.swipeHandler
          ),
          t.$list.on(
            "touchcancel.slick mouseleave.slick",
            { action: "end" },
            t.swipeHandler
          ),
          t.$list.on("click.slick", t.clickHandler),
          e(document).on(t.visibilityChange, e.proxy(t.visibility, t)),
          !0 === t.options.accessibility &&
            t.$list.on("keydown.slick", t.keyHandler),
          !0 === t.options.focusOnSelect &&
            e(t.$slideTrack).children().on("click.slick", t.selectHandler),
          e(window).on(
            "orientationchange.slick.slick-" + t.instanceUid,
            e.proxy(t.orientationChange, t)
          ),
          e(window).on(
            "resize.slick.slick-" + t.instanceUid,
            e.proxy(t.resize, t)
          ),
          e("[draggable!=true]", t.$slideTrack).on(
            "dragstart",
            t.preventDefault
          ),
          e(window).on("load.slick.slick-" + t.instanceUid, t.setPosition),
          e(t.setPosition);
      }),
      (t.prototype.initUI = function () {
        var e = this;
        !0 === e.options.arrows &&
          e.slideCount > e.options.slidesToShow &&
          (e.$prevArrow.show(), e.$nextArrow.show()),
          !0 === e.options.dots &&
            e.slideCount > e.options.slidesToShow &&
            e.$dots.show();
      }),
      (t.prototype.keyHandler = function (e) {
        var t = this;
        e.target.tagName.match("TEXTAREA|INPUT|SELECT") ||
          (37 === e.keyCode && !0 === t.options.accessibility
            ? t.changeSlide({
                data: { message: !0 === t.options.rtl ? "next" : "previous" },
              })
            : 39 === e.keyCode &&
              !0 === t.options.accessibility &&
              t.changeSlide({
                data: { message: !0 === t.options.rtl ? "previous" : "next" },
              }));
      }),
      (t.prototype.lazyLoad = function () {
        function t(t) {
          e("img[data-lazy]", t).each(function () {
            var t = e(this),
              n = e(this).attr("data-lazy"),
              i = e(this).attr("data-srcset"),
              o = e(this).attr("data-sizes") || r.$slider.attr("data-sizes"),
              s = document.createElement("img");
            (s.onload = function () {
              t.animate({ opacity: 0 }, 100, function () {
                i && (t.attr("srcset", i), o && t.attr("sizes", o)),
                  t.attr("src", n).animate({ opacity: 1 }, 200, function () {
                    t.removeAttr(
                      "data-lazy data-srcset data-sizes"
                    ).removeClass("slick-loading");
                  }),
                  r.$slider.trigger("lazyLoaded", [r, t, n]);
              });
            }),
              (s.onerror = function () {
                t
                  .removeAttr("data-lazy")
                  .removeClass("slick-loading")
                  .addClass("slick-lazyload-error"),
                  r.$slider.trigger("lazyLoadError", [r, t, n]);
              }),
              (s.src = n);
          });
        }
        var n,
          i,
          o,
          r = this;
        if (
          (!0 === r.options.centerMode
            ? !0 === r.options.infinite
              ? (o =
                  (i = r.currentSlide + (r.options.slidesToShow / 2 + 1)) +
                  r.options.slidesToShow +
                  2)
              : ((i = Math.max(
                  0,
                  r.currentSlide - (r.options.slidesToShow / 2 + 1)
                )),
                (o = r.options.slidesToShow / 2 + 1 + 2 + r.currentSlide))
            : ((i = r.options.infinite
                ? r.options.slidesToShow + r.currentSlide
                : r.currentSlide),
              (o = Math.ceil(i + r.options.slidesToShow)),
              !0 === r.options.fade &&
                (i > 0 && i--, o <= r.slideCount && o++)),
          (n = r.$slider.find(".slick-slide").slice(i, o)),
          "anticipated" === r.options.lazyLoad)
        )
          for (
            var s = i - 1, a = o, l = r.$slider.find(".slick-slide"), c = 0;
            c < r.options.slidesToScroll;
            c++
          )
            s < 0 && (s = r.slideCount - 1),
              (n = (n = n.add(l.eq(s))).add(l.eq(a))),
              s--,
              a++;
        t(n),
          r.slideCount <= r.options.slidesToShow
            ? t(r.$slider.find(".slick-slide"))
            : r.currentSlide >= r.slideCount - r.options.slidesToShow
            ? t(
                r.$slider.find(".slick-cloned").slice(0, r.options.slidesToShow)
              )
            : 0 === r.currentSlide &&
              t(
                r.$slider
                  .find(".slick-cloned")
                  .slice(-1 * r.options.slidesToShow)
              );
      }),
      (t.prototype.loadSlider = function () {
        var e = this;
        e.setPosition(),
          e.$slideTrack.css({ opacity: 1 }),
          e.$slider.removeClass("slick-loading"),
          e.initUI(),
          "progressive" === e.options.lazyLoad && e.progressiveLazyLoad();
      }),
      (t.prototype.next = t.prototype.slickNext = function () {
        this.changeSlide({ data: { message: "next" } });
      }),
      (t.prototype.orientationChange = function () {
        this.checkResponsive(), this.setPosition();
      }),
      (t.prototype.pause = t.prototype.slickPause = function () {
        this.autoPlayClear(), (this.paused = !0);
      }),
      (t.prototype.play = t.prototype.slickPlay = function () {
        var e = this;
        e.autoPlay(),
          (e.options.autoplay = !0),
          (e.paused = !1),
          (e.focussed = !1),
          (e.interrupted = !1);
      }),
      (t.prototype.postSlide = function (t) {
        var n = this;
        n.unslicked ||
          (n.$slider.trigger("afterChange", [n, t]),
          (n.animating = !1),
          n.slideCount > n.options.slidesToShow && n.setPosition(),
          (n.swipeLeft = null),
          n.options.autoplay && n.autoPlay(),
          !0 === n.options.accessibility &&
            (n.initADA(),
            n.options.focusOnChange &&
              e(n.$slides.get(n.currentSlide)).attr("tabindex", 0).focus()));
      }),
      (t.prototype.prev = t.prototype.slickPrev = function () {
        this.changeSlide({ data: { message: "previous" } });
      }),
      (t.prototype.preventDefault = function (e) {
        e.preventDefault();
      }),
      (t.prototype.progressiveLazyLoad = function (t) {
        t = t || 1;
        var n,
          i,
          o,
          r,
          s,
          a = this,
          l = e("img[data-lazy]", a.$slider);
        l.length
          ? ((n = l.first()),
            (i = n.attr("data-lazy")),
            (o = n.attr("data-srcset")),
            (r = n.attr("data-sizes") || a.$slider.attr("data-sizes")),
            ((s = document.createElement("img")).onload = function () {
              o && (n.attr("srcset", o), r && n.attr("sizes", r)),
                n
                  .attr("src", i)
                  .removeAttr("data-lazy data-srcset data-sizes")
                  .removeClass("slick-loading"),
                !0 === a.options.adaptiveHeight && a.setPosition(),
                a.$slider.trigger("lazyLoaded", [a, n, i]),
                a.progressiveLazyLoad();
            }),
            (s.onerror = function () {
              t < 3
                ? setTimeout(function () {
                    a.progressiveLazyLoad(t + 1);
                  }, 500)
                : (n
                    .removeAttr("data-lazy")
                    .removeClass("slick-loading")
                    .addClass("slick-lazyload-error"),
                  a.$slider.trigger("lazyLoadError", [a, n, i]),
                  a.progressiveLazyLoad());
            }),
            (s.src = i))
          : a.$slider.trigger("allImagesLoaded", [a]);
      }),
      (t.prototype.refresh = function (t) {
        var n,
          i,
          o = this;
        (i = o.slideCount - o.options.slidesToShow),
          !o.options.infinite && o.currentSlide > i && (o.currentSlide = i),
          o.slideCount <= o.options.slidesToShow && (o.currentSlide = 0),
          (n = o.currentSlide),
          o.destroy(!0),
          e.extend(o, o.initials, { currentSlide: n }),
          o.init(),
          t || o.changeSlide({ data: { message: "index", index: n } }, !1);
      }),
      (t.prototype.registerBreakpoints = function () {
        var t,
          n,
          i,
          o = this,
          r = o.options.responsive || null;
        if ("array" === e.type(r) && r.length) {
          for (t in ((o.respondTo = o.options.respondTo || "window"), r))
            if (((i = o.breakpoints.length - 1), r.hasOwnProperty(t))) {
              for (n = r[t].breakpoint; i >= 0; )
                o.breakpoints[i] &&
                  o.breakpoints[i] === n &&
                  o.breakpoints.splice(i, 1),
                  i--;
              o.breakpoints.push(n), (o.breakpointSettings[n] = r[t].settings);
            }
          o.breakpoints.sort(function (e, t) {
            return o.options.mobileFirst ? e - t : t - e;
          });
        }
      }),
      (t.prototype.reinit = function () {
        var t = this;
        (t.$slides = t.$slideTrack
          .children(t.options.slide)
          .addClass("slick-slide")),
          (t.slideCount = t.$slides.length),
          t.currentSlide >= t.slideCount &&
            0 !== t.currentSlide &&
            (t.currentSlide = t.currentSlide - t.options.slidesToScroll),
          t.slideCount <= t.options.slidesToShow && (t.currentSlide = 0),
          t.registerBreakpoints(),
          t.setProps(),
          t.setupInfinite(),
          t.buildArrows(),
          t.updateArrows(),
          t.initArrowEvents(),
          t.buildDots(),
          t.updateDots(),
          t.initDotEvents(),
          t.cleanUpSlideEvents(),
          t.initSlideEvents(),
          t.checkResponsive(!1, !0),
          !0 === t.options.focusOnSelect &&
            e(t.$slideTrack).children().on("click.slick", t.selectHandler),
          t.setSlideClasses(
            "number" == typeof t.currentSlide ? t.currentSlide : 0
          ),
          t.setPosition(),
          t.focusHandler(),
          (t.paused = !t.options.autoplay),
          t.autoPlay(),
          t.$slider.trigger("reInit", [t]);
      }),
      (t.prototype.resize = function () {
        var t = this;
        e(window).width() !== t.windowWidth &&
          (clearTimeout(t.windowDelay),
          (t.windowDelay = window.setTimeout(function () {
            (t.windowWidth = e(window).width()),
              t.checkResponsive(),
              t.unslicked || t.setPosition();
          }, 50)));
      }),
      (t.prototype.removeSlide = t.prototype.slickRemove = function (e, t, n) {
        var i = this;
        if (
          ((e =
            "boolean" == typeof e
              ? !0 === (t = e)
                ? 0
                : i.slideCount - 1
              : !0 === t
              ? --e
              : e),
          i.slideCount < 1 || e < 0 || e > i.slideCount - 1)
        )
          return !1;
        i.unload(),
          !0 === n
            ? i.$slideTrack.children().remove()
            : i.$slideTrack.children(this.options.slide).eq(e).remove(),
          (i.$slides = i.$slideTrack.children(this.options.slide)),
          i.$slideTrack.children(this.options.slide).detach(),
          i.$slideTrack.append(i.$slides),
          (i.$slidesCache = i.$slides),
          i.reinit();
      }),
      (t.prototype.setCSS = function (e) {
        var t,
          n,
          i = this,
          o = {};
        !0 === i.options.rtl && (e = -e),
          (t = "left" == i.positionProp ? Math.ceil(e) + "px" : "0px"),
          (n = "top" == i.positionProp ? Math.ceil(e) + "px" : "0px"),
          (o[i.positionProp] = e),
          !1 === i.transformsEnabled
            ? i.$slideTrack.css(o)
            : ((o = {}),
              !1 === i.cssTransitions
                ? ((o[i.animType] = "translate(" + t + ", " + n + ")"),
                  i.$slideTrack.css(o))
                : ((o[i.animType] = "translate3d(" + t + ", " + n + ", 0px)"),
                  i.$slideTrack.css(o)));
      }),
      (t.prototype.setDimensions = function () {
        var e = this;
        !1 === e.options.vertical
          ? !0 === e.options.centerMode &&
            e.$list.css({ padding: "0px " + e.options.centerPadding })
          : (e.$list.height(
              e.$slides.first().outerHeight(!0) * e.options.slidesToShow
            ),
            !0 === e.options.centerMode &&
              e.$list.css({ padding: e.options.centerPadding + " 0px" })),
          (e.listWidth = e.$list.width()),
          (e.listHeight = e.$list.height()),
          !1 === e.options.vertical && !1 === e.options.variableWidth
            ? ((e.slideWidth = Math.ceil(e.listWidth / e.options.slidesToShow)),
              e.$slideTrack.width(
                Math.ceil(
                  e.slideWidth * e.$slideTrack.children(".slick-slide").length
                )
              ))
            : !0 === e.options.variableWidth
            ? e.$slideTrack.width(5e3 * e.slideCount)
            : ((e.slideWidth = Math.ceil(e.listWidth)),
              e.$slideTrack.height(
                Math.ceil(
                  e.$slides.first().outerHeight(!0) *
                    e.$slideTrack.children(".slick-slide").length
                )
              ));
        var t = e.$slides.first().outerWidth(!0) - e.$slides.first().width();
        !1 === e.options.variableWidth &&
          e.$slideTrack.children(".slick-slide").width(e.slideWidth - t);
      }),
      (t.prototype.setFade = function () {
        var t,
          n = this;
        n.$slides.each(function (i, o) {
          (t = n.slideWidth * i * -1),
            !0 === n.options.rtl
              ? e(o).css({
                  position: "relative",
                  right: t,
                  top: 0,
                  zIndex: n.options.zIndex - 2,
                  opacity: 0,
                })
              : e(o).css({
                  position: "relative",
                  left: t,
                  top: 0,
                  zIndex: n.options.zIndex - 2,
                  opacity: 0,
                });
        }),
          n.$slides
            .eq(n.currentSlide)
            .css({ zIndex: n.options.zIndex - 1, opacity: 1 });
      }),
      (t.prototype.setHeight = function () {
        var e = this;
        if (
          1 === e.options.slidesToShow &&
          !0 === e.options.adaptiveHeight &&
          !1 === e.options.vertical
        ) {
          var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
          e.$list.css("height", t);
        }
      }),
      (t.prototype.setOption = t.prototype.slickSetOption = function () {
        var t,
          n,
          i,
          o,
          r,
          s = this,
          a = !1;
        if (
          ("object" === e.type(arguments[0])
            ? ((i = arguments[0]), (a = arguments[1]), (r = "multiple"))
            : "string" === e.type(arguments[0]) &&
              ((i = arguments[0]),
              (o = arguments[1]),
              (a = arguments[2]),
              "responsive" === arguments[0] && "array" === e.type(arguments[1])
                ? (r = "responsive")
                : void 0 !== arguments[1] && (r = "single")),
          "single" === r)
        )
          s.options[i] = o;
        else if ("multiple" === r)
          e.each(i, function (e, t) {
            s.options[e] = t;
          });
        else if ("responsive" === r)
          for (n in o)
            if ("array" !== e.type(s.options.responsive))
              s.options.responsive = [o[n]];
            else {
              for (t = s.options.responsive.length - 1; t >= 0; )
                s.options.responsive[t].breakpoint === o[n].breakpoint &&
                  s.options.responsive.splice(t, 1),
                  t--;
              s.options.responsive.push(o[n]);
            }
        a && (s.unload(), s.reinit());
      }),
      (t.prototype.setPosition = function () {
        var e = this;
        e.setDimensions(),
          e.setHeight(),
          !1 === e.options.fade
            ? e.setCSS(e.getLeft(e.currentSlide))
            : e.setFade(),
          e.$slider.trigger("setPosition", [e]);
      }),
      (t.prototype.setProps = function () {
        var e = this,
          t = document.body.style;
        (e.positionProp = !0 === e.options.vertical ? "top" : "left"),
          "top" === e.positionProp
            ? e.$slider.addClass("slick-vertical")
            : e.$slider.removeClass("slick-vertical"),
          (void 0 === t.WebkitTransition &&
            void 0 === t.MozTransition &&
            void 0 === t.msTransition) ||
            (!0 === e.options.useCSS && (e.cssTransitions = !0)),
          e.options.fade &&
            ("number" == typeof e.options.zIndex
              ? e.options.zIndex < 3 && (e.options.zIndex = 3)
              : (e.options.zIndex = e.defaults.zIndex)),
          void 0 !== t.OTransform &&
            ((e.animType = "OTransform"),
            (e.transformType = "-o-transform"),
            (e.transitionType = "OTransition"),
            void 0 === t.perspectiveProperty &&
              void 0 === t.webkitPerspective &&
              (e.animType = !1)),
          void 0 !== t.MozTransform &&
            ((e.animType = "MozTransform"),
            (e.transformType = "-moz-transform"),
            (e.transitionType = "MozTransition"),
            void 0 === t.perspectiveProperty &&
              void 0 === t.MozPerspective &&
              (e.animType = !1)),
          void 0 !== t.webkitTransform &&
            ((e.animType = "webkitTransform"),
            (e.transformType = "-webkit-transform"),
            (e.transitionType = "webkitTransition"),
            void 0 === t.perspectiveProperty &&
              void 0 === t.webkitPerspective &&
              (e.animType = !1)),
          void 0 !== t.msTransform &&
            ((e.animType = "msTransform"),
            (e.transformType = "-ms-transform"),
            (e.transitionType = "msTransition"),
            void 0 === t.msTransform && (e.animType = !1)),
          void 0 !== t.transform &&
            !1 !== e.animType &&
            ((e.animType = "transform"),
            (e.transformType = "transform"),
            (e.transitionType = "transition")),
          (e.transformsEnabled =
            e.options.useTransform && null !== e.animType && !1 !== e.animType);
      }),
      (t.prototype.setSlideClasses = function (e) {
        var t,
          n,
          i,
          o,
          r = this;
        if (
          ((n = r.$slider
            .find(".slick-slide")
            .removeClass("slick-active slick-center slick-current")
            .attr("aria-hidden", "true")),
          r.$slides.eq(e).addClass("slick-current"),
          !0 === r.options.centerMode)
        ) {
          var s = r.options.slidesToShow % 2 == 0 ? 1 : 0;
          (t = Math.floor(r.options.slidesToShow / 2)),
            !0 === r.options.infinite &&
              (e >= t && e <= r.slideCount - 1 - t
                ? r.$slides
                    .slice(e - t + s, e + t + 1)
                    .addClass("slick-active")
                    .attr("aria-hidden", "false")
                : ((i = r.options.slidesToShow + e),
                  n
                    .slice(i - t + 1 + s, i + t + 2)
                    .addClass("slick-active")
                    .attr("aria-hidden", "false")),
              0 === e
                ? n
                    .eq(n.length - 1 - r.options.slidesToShow)
                    .addClass("slick-center")
                : e === r.slideCount - 1 &&
                  n.eq(r.options.slidesToShow).addClass("slick-center")),
            r.$slides.eq(e).addClass("slick-center");
        } else
          e >= 0 && e <= r.slideCount - r.options.slidesToShow
            ? r.$slides
                .slice(e, e + r.options.slidesToShow)
                .addClass("slick-active")
                .attr("aria-hidden", "false")
            : n.length <= r.options.slidesToShow
            ? n.addClass("slick-active").attr("aria-hidden", "false")
            : ((o = r.slideCount % r.options.slidesToShow),
              (i = !0 === r.options.infinite ? r.options.slidesToShow + e : e),
              r.options.slidesToShow == r.options.slidesToScroll &&
              r.slideCount - e < r.options.slidesToShow
                ? n
                    .slice(i - (r.options.slidesToShow - o), i + o)
                    .addClass("slick-active")
                    .attr("aria-hidden", "false")
                : n
                    .slice(i, i + r.options.slidesToShow)
                    .addClass("slick-active")
                    .attr("aria-hidden", "false"));
        ("ondemand" !== r.options.lazyLoad &&
          "anticipated" !== r.options.lazyLoad) ||
          r.lazyLoad();
      }),
      (t.prototype.setupInfinite = function () {
        var t,
          n,
          i,
          o = this;
        if (
          (!0 === o.options.fade && (o.options.centerMode = !1),
          !0 === o.options.infinite &&
            !1 === o.options.fade &&
            ((n = null), o.slideCount > o.options.slidesToShow))
        ) {
          for (
            i =
              !0 === o.options.centerMode
                ? o.options.slidesToShow + 1
                : o.options.slidesToShow,
              t = o.slideCount;
            t > o.slideCount - i;
            t -= 1
          )
            (n = t - 1),
              e(o.$slides[n])
                .clone(!0)
                .attr("id", "")
                .attr("data-slick-index", n - o.slideCount)
                .prependTo(o.$slideTrack)
                .addClass("slick-cloned");
          for (t = 0; t < i + o.slideCount; t += 1)
            (n = t),
              e(o.$slides[n])
                .clone(!0)
                .attr("id", "")
                .attr("data-slick-index", n + o.slideCount)
                .appendTo(o.$slideTrack)
                .addClass("slick-cloned");
          o.$slideTrack
            .find(".slick-cloned")
            .find("[id]")
            .each(function () {
              e(this).attr("id", "");
            });
        }
      }),
      (t.prototype.interrupt = function (e) {
        e || this.autoPlay(), (this.interrupted = e);
      }),
      (t.prototype.selectHandler = function (t) {
        var n = this,
          i = e(t.target).is(".slick-slide")
            ? e(t.target)
            : e(t.target).parents(".slick-slide"),
          o = parseInt(i.attr("data-slick-index"));
        o || (o = 0),
          n.slideCount <= n.options.slidesToShow
            ? n.slideHandler(o, !1, !0)
            : n.slideHandler(o);
      }),
      (t.prototype.slideHandler = function (e, t, n) {
        var i,
          o,
          r,
          s,
          a,
          l = null,
          c = this;
        if (
          ((t = t || !1),
          !(
            (!0 === c.animating && !0 === c.options.waitForAnimate) ||
            (!0 === c.options.fade && c.currentSlide === e)
          ))
        )
          if (
            (!1 === t && c.asNavFor(e),
            (i = e),
            (l = c.getLeft(i)),
            (s = c.getLeft(c.currentSlide)),
            (c.currentLeft = null === c.swipeLeft ? s : c.swipeLeft),
            !1 === c.options.infinite &&
              !1 === c.options.centerMode &&
              (e < 0 || e > c.getDotCount() * c.options.slidesToScroll))
          )
            !1 === c.options.fade &&
              ((i = c.currentSlide),
              !0 !== n
                ? c.animateSlide(s, function () {
                    c.postSlide(i);
                  })
                : c.postSlide(i));
          else if (
            !1 === c.options.infinite &&
            !0 === c.options.centerMode &&
            (e < 0 || e > c.slideCount - c.options.slidesToScroll)
          )
            !1 === c.options.fade &&
              ((i = c.currentSlide),
              !0 !== n
                ? c.animateSlide(s, function () {
                    c.postSlide(i);
                  })
                : c.postSlide(i));
          else {
            if (
              (c.options.autoplay && clearInterval(c.autoPlayTimer),
              (o =
                i < 0
                  ? c.slideCount % c.options.slidesToScroll != 0
                    ? c.slideCount - (c.slideCount % c.options.slidesToScroll)
                    : c.slideCount + i
                  : i >= c.slideCount
                  ? c.slideCount % c.options.slidesToScroll != 0
                    ? 0
                    : i - c.slideCount
                  : i),
              (c.animating = !0),
              c.$slider.trigger("beforeChange", [c, c.currentSlide, o]),
              (r = c.currentSlide),
              (c.currentSlide = o),
              c.setSlideClasses(c.currentSlide),
              c.options.asNavFor &&
                (a = (a = c.getNavTarget()).slick("getSlick")).slideCount <=
                  a.options.slidesToShow &&
                a.setSlideClasses(c.currentSlide),
              c.updateDots(),
              c.updateArrows(),
              !0 === c.options.fade)
            )
              return (
                !0 !== n
                  ? (c.fadeSlideOut(r),
                    c.fadeSlide(o, function () {
                      c.postSlide(o);
                    }))
                  : c.postSlide(o),
                void c.animateHeight()
              );
            !0 !== n
              ? c.animateSlide(l, function () {
                  c.postSlide(o);
                })
              : c.postSlide(o);
          }
      }),
      (t.prototype.startLoad = function () {
        var e = this;
        !0 === e.options.arrows &&
          e.slideCount > e.options.slidesToShow &&
          (e.$prevArrow.hide(), e.$nextArrow.hide()),
          !0 === e.options.dots &&
            e.slideCount > e.options.slidesToShow &&
            e.$dots.hide(),
          e.$slider.addClass("slick-loading");
      }),
      (t.prototype.swipeDirection = function () {
        var e,
          t,
          n,
          i,
          o = this;
        return (
          (e = o.touchObject.startX - o.touchObject.curX),
          (t = o.touchObject.startY - o.touchObject.curY),
          (n = Math.atan2(t, e)),
          (i = Math.round((180 * n) / Math.PI)) < 0 && (i = 360 - Math.abs(i)),
          i <= 45 && i >= 0
            ? !1 === o.options.rtl
              ? "left"
              : "right"
            : i <= 360 && i >= 315
            ? !1 === o.options.rtl
              ? "left"
              : "right"
            : i >= 135 && i <= 225
            ? !1 === o.options.rtl
              ? "right"
              : "left"
            : !0 === o.options.verticalSwiping
            ? i >= 35 && i <= 135
              ? "down"
              : "up"
            : "vertical"
        );
      }),
      (t.prototype.swipeEnd = function (e) {
        var t,
          n,
          i = this;
        if (((i.dragging = !1), (i.swiping = !1), i.scrolling))
          return (i.scrolling = !1), !1;
        if (
          ((i.interrupted = !1),
          (i.shouldClick = !(i.touchObject.swipeLength > 10)),
          void 0 === i.touchObject.curX)
        )
          return !1;
        if (
          (!0 === i.touchObject.edgeHit &&
            i.$slider.trigger("edge", [i, i.swipeDirection()]),
          i.touchObject.swipeLength >= i.touchObject.minSwipe)
        ) {
          switch ((n = i.swipeDirection())) {
            case "left":
            case "down":
              (t = i.options.swipeToSlide
                ? i.checkNavigable(i.currentSlide + i.getSlideCount())
                : i.currentSlide + i.getSlideCount()),
                (i.currentDirection = 0);
              break;
            case "right":
            case "up":
              (t = i.options.swipeToSlide
                ? i.checkNavigable(i.currentSlide - i.getSlideCount())
                : i.currentSlide - i.getSlideCount()),
                (i.currentDirection = 1);
          }
          "vertical" != n &&
            (i.slideHandler(t),
            (i.touchObject = {}),
            i.$slider.trigger("swipe", [i, n]));
        } else
          i.touchObject.startX !== i.touchObject.curX &&
            (i.slideHandler(i.currentSlide), (i.touchObject = {}));
      }),
      (t.prototype.swipeHandler = function (e) {
        var t = this;
        if (
          !(
            !1 === t.options.swipe ||
            ("ontouchend" in document && !1 === t.options.swipe) ||
            (!1 === t.options.draggable && -1 !== e.type.indexOf("mouse"))
          )
        )
          switch (
            ((t.touchObject.fingerCount =
              e.originalEvent && void 0 !== e.originalEvent.touches
                ? e.originalEvent.touches.length
                : 1),
            (t.touchObject.minSwipe = t.listWidth / t.options.touchThreshold),
            !0 === t.options.verticalSwiping &&
              (t.touchObject.minSwipe =
                t.listHeight / t.options.touchThreshold),
            e.data.action)
          ) {
            case "start":
              t.swipeStart(e);
              break;
            case "move":
              t.swipeMove(e);
              break;
            case "end":
              t.swipeEnd(e);
          }
      }),
      (t.prototype.swipeMove = function (e) {
        var t,
          n,
          i,
          o,
          r,
          s,
          a = this;
        return (
          (r = void 0 !== e.originalEvent ? e.originalEvent.touches : null),
          !(!a.dragging || a.scrolling || (r && 1 !== r.length)) &&
            ((t = a.getLeft(a.currentSlide)),
            (a.touchObject.curX = void 0 !== r ? r[0].pageX : e.clientX),
            (a.touchObject.curY = void 0 !== r ? r[0].pageY : e.clientY),
            (a.touchObject.swipeLength = Math.round(
              Math.sqrt(Math.pow(a.touchObject.curX - a.touchObject.startX, 2))
            )),
            (s = Math.round(
              Math.sqrt(Math.pow(a.touchObject.curY - a.touchObject.startY, 2))
            )),
            !a.options.verticalSwiping && !a.swiping && s > 4
              ? ((a.scrolling = !0), !1)
              : (!0 === a.options.verticalSwiping &&
                  (a.touchObject.swipeLength = s),
                (n = a.swipeDirection()),
                void 0 !== e.originalEvent &&
                  a.touchObject.swipeLength > 4 &&
                  ((a.swiping = !0), e.preventDefault()),
                (o =
                  (!1 === a.options.rtl ? 1 : -1) *
                  (a.touchObject.curX > a.touchObject.startX ? 1 : -1)),
                !0 === a.options.verticalSwiping &&
                  (o = a.touchObject.curY > a.touchObject.startY ? 1 : -1),
                (i = a.touchObject.swipeLength),
                (a.touchObject.edgeHit = !1),
                !1 === a.options.infinite &&
                  ((0 === a.currentSlide && "right" === n) ||
                    (a.currentSlide >= a.getDotCount() && "left" === n)) &&
                  ((i = a.touchObject.swipeLength * a.options.edgeFriction),
                  (a.touchObject.edgeHit = !0)),
                !1 === a.options.vertical
                  ? (a.swipeLeft = t + i * o)
                  : (a.swipeLeft =
                      t + i * (a.$list.height() / a.listWidth) * o),
                !0 === a.options.verticalSwiping && (a.swipeLeft = t + i * o),
                !0 !== a.options.fade &&
                  !1 !== a.options.touchMove &&
                  (!0 === a.animating
                    ? ((a.swipeLeft = null), !1)
                    : void a.setCSS(a.swipeLeft))))
        );
      }),
      (t.prototype.swipeStart = function (e) {
        var t,
          n = this;
        if (
          ((n.interrupted = !0),
          1 !== n.touchObject.fingerCount ||
            n.slideCount <= n.options.slidesToShow)
        )
          return (n.touchObject = {}), !1;
        void 0 !== e.originalEvent &&
          void 0 !== e.originalEvent.touches &&
          (t = e.originalEvent.touches[0]),
          (n.touchObject.startX = n.touchObject.curX =
            void 0 !== t ? t.pageX : e.clientX),
          (n.touchObject.startY = n.touchObject.curY =
            void 0 !== t ? t.pageY : e.clientY),
          (n.dragging = !0);
      }),
      (t.prototype.unfilterSlides = t.prototype.slickUnfilter = function () {
        var e = this;
        null !== e.$slidesCache &&
          (e.unload(),
          e.$slideTrack.children(this.options.slide).detach(),
          e.$slidesCache.appendTo(e.$slideTrack),
          e.reinit());
      }),
      (t.prototype.unload = function () {
        var t = this;
        e(".slick-cloned", t.$slider).remove(),
          t.$dots && t.$dots.remove(),
          t.$prevArrow &&
            t.htmlExpr.test(t.options.prevArrow) &&
            t.$prevArrow.remove(),
          t.$nextArrow &&
            t.htmlExpr.test(t.options.nextArrow) &&
            t.$nextArrow.remove(),
          t.$slides
            .removeClass("slick-slide slick-active slick-visible slick-current")
            .attr("aria-hidden", "true")
            .css("width", "");
      }),
      (t.prototype.unslick = function (e) {
        var t = this;
        t.$slider.trigger("unslick", [t, e]), t.destroy();
      }),
      (t.prototype.updateArrows = function () {
        var e = this;
        Math.floor(e.options.slidesToShow / 2),
          !0 === e.options.arrows &&
            e.slideCount > e.options.slidesToShow &&
            !e.options.infinite &&
            (e.$prevArrow
              .removeClass("slick-disabled")
              .attr("aria-disabled", "false"),
            e.$nextArrow
              .removeClass("slick-disabled")
              .attr("aria-disabled", "false"),
            0 === e.currentSlide
              ? (e.$prevArrow
                  .addClass("slick-disabled")
                  .attr("aria-disabled", "true"),
                e.$nextArrow
                  .removeClass("slick-disabled")
                  .attr("aria-disabled", "false"))
              : e.currentSlide >= e.slideCount - e.options.slidesToShow &&
                !1 === e.options.centerMode
              ? (e.$nextArrow
                  .addClass("slick-disabled")
                  .attr("aria-disabled", "true"),
                e.$prevArrow
                  .removeClass("slick-disabled")
                  .attr("aria-disabled", "false"))
              : e.currentSlide >= e.slideCount - 1 &&
                !0 === e.options.centerMode &&
                (e.$nextArrow
                  .addClass("slick-disabled")
                  .attr("aria-disabled", "true"),
                e.$prevArrow
                  .removeClass("slick-disabled")
                  .attr("aria-disabled", "false")));
      }),
      (t.prototype.updateDots = function () {
        var e = this;
        null !== e.$dots &&
          (e.$dots.find("li").removeClass("slick-active").end(),
          e.$dots
            .find("li")
            .eq(Math.floor(e.currentSlide / e.options.slidesToScroll))
            .addClass("slick-active"));
      }),
      (t.prototype.visibility = function () {
        var e = this;
        e.options.autoplay &&
          (document[e.hidden] ? (e.interrupted = !0) : (e.interrupted = !1));
      }),
      (e.fn.slick = function () {
        var e,
          n,
          i = this,
          o = arguments[0],
          r = Array.prototype.slice.call(arguments, 1),
          s = i.length;
        for (e = 0; e < s; e++)
          if (
            ("object" == typeof o || void 0 === o
              ? (i[e].slick = new t(i[e], o))
              : (n = i[e].slick[o].apply(i[e].slick, r)),
            void 0 !== n)
          )
            return n;
        return i;
      });
  }),
  $(function () {
    $(".responsive-slider").slick({
      infinite: !0,
      dots: !0,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: !0,
      fade: !0,
      cssEase: "linear",
      // nextArrow: "<img src='images/arrow-right.svg' class='slick-icon-right'>",
      // prevArrow: "<img src='images/arrow-left.svg' class='slick-icon-left'>",
    });
  });
  
var Revealator = void 0 !== Revealator ? Revealator : {};
$(function () {
  ((Revealator = $.extend(
    {},
    {
      timer: null,
      busy: !1,
      scroll_padding: 0,
      effects_padding: 0,
      refresh: function () {},
    },
    void 0 !== Revealator ? Revealator : {}
  )).refresh = function () {
    var e = $(window),
      t = $(document),
      n = $(document.body),
      i = Revealator.effects_padding,
      o = e.height() - Revealator.effects_padding,
      r = Revealator.scroll_padding,
      s = t.height() - Revealator.scroll_padding;
    0 === e.scrollTop()
      ? n.hasClass("at-top") ||
        n
          .addClass("at-top")
          .removeClass("at-bottom")
          .removeClass("near-top")
          .removeClass("near-bottom")
      : e.scrollTop() + e.height() === t.height()
      ? n.hasClass("at-bottom") ||
        n
          .addClass("at-bottom")
          .removeClass("at-top")
          .removeClass("near-top")
          .removeClass("near-bottom")
      : e.scrollTop() <= r
      ? n.hasClass("near-top") ||
        n
          .addClass("near-top")
          .removeClass("near-bottom")
          .removeClass("at-top")
          .removeClass("at-bottom")
      : e.scrollTop() + e.height() >= s
      ? n.hasClass("near-bottom") ||
        n
          .addClass("near-bottom")
          .removeClass("near-top")
          .removeClass("at-top")
          .removeClass("at-bottom")
      : (n.hasClass("at-top") ||
          n.hasClass("at-bottom") ||
          n.hasClass("near-top") ||
          n.hasClass("near-bottom")) &&
        n
          .removeClass("at-top")
          .removeClass("at-bottom")
          .removeClass("near-top")
          .removeClass("near-bottom"),
      $('*[class*="revealator"]').each(function () {
        0;
        var e,
          t = $(this),
          n = this.getBoundingClientRect();
        (e =
          n.top > o && n.bottom > o
            ? "revealator-below"
            : n.top < o && n.bottom > o
            ? "revealator-partially-below"
            : n.top < i && n.bottom > i
            ? "revealator-partially-above"
            : n.top < i && n.bottom < i
            ? "revealator-above"
            : "revealator-within"),
          t.hasClass("revealator-load") &&
            !t.hasClass("revealator-within") &&
            (t.removeClass(
              "revealator-below revealator-partially-below revealator-within revealator-partially-above revealator-above"
            ),
            t.addClass("revealator-within")),
          t.hasClass(e) ||
            t.hasClass("revealator-load") ||
            (t.hasClass("revealator-once")
              ? (t.hasClass("revealator-within") ||
                  (t.removeClass(
                    "revealator-below revealator-partially-below revealator-within revealator-partially-above revealator-above"
                  ),
                  t.addClass(e)),
                (t.hasClass("revealator-partially-above") ||
                  t.hasClass("revealator-above")) &&
                  t.addClass("revealator-within"))
              : (t.removeClass(
                  "revealator-below revealator-partially-below revealator-within revealator-partially-above revealator-above"
                ),
                t.addClass(e)));
      });
  }),
    $(window).bind("scroll resize load ready", function () {
      Revealator.busy ||
        ((Revealator.busy = !0),
        setTimeout(function () {
          (Revealator.busy = !1), Revealator.refresh();
        }, 150));
    });
});

console.log("corp");
console.log($(".nav-dropdown"));
$(".nav-dropdown").hover(function () {
  $(this).children(".dropdown-menu").css("display", "block");
});
$(".nav-dropdown").mouseleave(function () {
  $(this).children(".dropdown-menu").css("display", "none");
});
