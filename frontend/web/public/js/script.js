$(function () {
  $(".responsive-slider").slick({
    infinite: true,
    dots: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    cssEase: "linear",
    arrows: true,
    // nextArrow: "<img src='images/arrow-right.svg' class='slick-icon-right'>",
    // prevArrow: "<img src='images/arrow-left.svg' class='slick-icon-left'>",
    // responsive: [
    //     {
    //         breakpoint: 1024,
    //         settings: {
    //             slidesToShow: 3,
    //             slidesToScroll: 1,
    //             infinite: true,
    //         }
    //     },
    //     {
    //         breakpoint: 740,
    //         settings: {
    //             slidesToShow: 1,
    //             slidesToScroll: 1
    //         }
    //     },
    //     {
    //         breakpoint: 480,
    //         settings: {
    //             slidesToShow: 1,
    //             slidesToScroll: 1
    //         }
    //     }
    //
    // ]
  });
});
