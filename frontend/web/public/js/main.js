$(document).ready(function () {
  $(".nav-dropdown").on("mouseover", function () {
    event.stopPropagation();
    $(".active_nav .dropdown-menu").hide();
    $(".nav-dropdown").removeClass("active_nav");
    $(this).addClass("active_nav");
    $(".active_nav .dropdown-menu").show();
  });

  $(".dropdown-menu").on("mouseout", function () {
    $(".active_nav .dropdown-menu").hide();
    $(".nav-dropdown").removeClass("active_nav");
    $(".bg-banner").css({ backgroundImage: "none", opacity: "0" });
  });

  $(".nav_container").on("mouseover", function () {
    $(".active_nav .dropdown-menu").hide();
    $(".nav-dropdown").removeClass("active_nav");
  });

  $(".about-menu").click(function () {
    $(".about-collapse").toggleClass("about-collapse-active");
  });
  let cel = document.querySelectorAll(".target-title");
  let previewText = document.querySelectorAll(".preview-text");
  let fixImg = document.querySelectorAll(".fix-img");

  console.log(cel);
  $(cel[0]).click(function () {
    $(fixImg).removeClass("fix-img-active");
    $(fixImg[0]).addClass("fix-img-active");
    $(cel).removeClass("target-active");
    $(this).addClass("target-active");
    $(previewText).removeClass("preview-text-active");
    $(previewText[0]).addClass("preview-text-active");
  });
  $(cel[1]).click(function () {
    $(fixImg).removeClass("fix-img-active");
    $(fixImg[1]).addClass("fix-img-active");
    $(cel).removeClass("target-active");
    $(this).addClass("target-active");
    $(previewText).removeClass("preview-text-active");
    $(previewText[1]).addClass("preview-text-active");
  });
  $(cel[2]).click(function () {
    $(fixImg).removeClass("fix-img-active");
    $(fixImg[2]).addClass("fix-img-active");
    $(cel).removeClass("target-active");
    $(this).addClass("target-active");
    $(previewText).removeClass("preview-text-active");
    $(previewText[2]).addClass("preview-text-active");
  });

  let scheduleBtn = document.querySelectorAll(".top__cub_descr");
  let circleContent = document.querySelectorAll(".circle-content");
  let circleContentSpan = document.querySelectorAll(".circle-content span");
  let targeTitle = document.querySelectorAll(".target-title");

  $(targeTitle).click(function () {
    $(targeTitle).removeClass("target-active");
    $(this).addClass("target-active");
  });

  $(scheduleBtn[0]).hover(function () {
    $(circleContentSpan).css("color", "#000");
    $(circleContentSpan[0]).css("color", "#fff");
    $(circleContent).css("backgroundColor", "#f3f3f2");
    $(circleContent[0]).css("backgroundColor", "#d59f57");
  });

  $(scheduleBtn[1]).hover(function () {
    $(circleContentSpan).css("color", "#000");
    $(circleContentSpan[1]).css("color", "#fff");
    $(circleContent).css("backgroundColor", "#f3f3f2");
    $(circleContent[1]).css("backgroundColor", "#d59f57");
  });

  $(scheduleBtn[2]).hover(function () {
    $(circleContentSpan).css("color", "#000");
    $(circleContentSpan[2]).css("color", "#fff");
    $(circleContent).css("backgroundColor", "#f3f3f2");
    $(circleContent[2]).css("backgroundColor", "#d59f57");
  });

  $(scheduleBtn).mouseleave(function () {
    $(circleContentSpan).css("color", "#000");
    $(circleContent).css("backgroundColor", "#f3f3f2");
  });



});
