<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id'                => $this->primaryKey(),
            'email'             => $this->string()->notNull()->unique(),
            'auth_key'          => $this->string(32)->notNull(),
            'password_hash'     => $this->string()->notNull(),
            'token'             => $this->string(128)->unique()->defaultValue(null),
            'role'              => $this->boolean()->notNull()->defaultValue(0),
            'status'            => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at'        => $this->timestamp()->defaultValue(null),
            'updated_at'        => $this->timestamp()->defaultValue(null),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
