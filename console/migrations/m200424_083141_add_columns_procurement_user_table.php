<?php

use yii\db\Migration;

/**
 * Class m200424_083141_add_columns_procurement_user_table
 */
class m200424_083141_add_columns_procurement_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('procurement_user', 'phone', $this->string(255)->null());
        $this->addColumn('procurement_user', 'email', $this->string(255)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('procurement_user', 'phone');
        $this->dropColumn('procurement_user', 'email');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200424_083141_add_columns_procurement_user_table cannot be reverted.\n";

        return false;
    }
    */
}
