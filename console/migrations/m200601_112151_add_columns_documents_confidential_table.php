<?php

use yii\db\Migration;

/**
 * Class m200601_112151_add_columns_documents_confidential_table
 */
class m200601_112151_add_columns_documents_confidential_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('documents_confidential', 'title', $this->string(255)->null());
        $this->addColumn('documents_confidential', 'content', $this->text()->null());

        $this->addColumn('documents_confidential', 'extension', $this->text()->null());

        $this->addColumn('documents_confidential_translation', 'title', $this->string(255)->null());
        $this->addColumn('documents_confidential_translation', 'content', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('documents_confidential', 'title');
        $this->dropColumn('documents_confidential', 'content');
        $this->dropColumn('documents_confidential', 'extension');

        $this->dropColumn('documents_confidential_translation', 'title');
        $this->dropColumn('documents_confidential_translation', 'content');
    }
}
