<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%procurement_documents}}`.
 */
class m200414_072915_create_procurement_documents_table extends Migration
{
    public $table               = 'procurement_documents';
    public $translationTable    = 'procurement_documents_translation';
    public $procurementTable    = 'procurement';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'procurement_id'    => $this->integer()->null(),
            'name'              => $this->string(255)->notNull(),
            'file'              => $this->string(255)->null(),
            'date'              => $this->date()->null(),
            'create_at'         => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

        $this->createTable("{{{$this->translationTable}}}", [
            'id'            => $this->integer()->notNull(),
            'language'      => $this->string(16)->notNull(),
            'name'          => $this->string(255)->null(),
            'file'          => $this->string(255)->null(),
        ], $tableOptions);

        $this->addPrimaryKey("pk_{$this->translationTable}_id_language", "{{{$this->translationTable}}}", ['id', 'language']);
        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->translationTable}_{$this->table}", "{{{$this->translationTable}}}", 'id', "{{{$this->table}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->createIndex("idx_{$this->translationTable}_language", "{{{$this->translationTable}}}", 'language');

        $this->addForeignKey("fk_{$this->table}_{$this->procurementTable}", "{{{$this->table}}}", 'procurement_id', "{{{$this->procurementTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->procurementTable}", "{{{$this->procurementTable}}}");
        $this->dropForeignKey("fk_{$this->translationTable}_{$this->table}", "{{{$this->translationTable}}}");
        $this->dropTable("{{{$this->translationTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
