<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%request_vacancy}}`.
 */
class m200520_124249_create_request_vacancy_table extends Migration
{
    public $table               = 'request_vacancy';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                        => $this->primaryKey(),
            'status'                    => $this->integer()->defaultValue(1)->notNull(),
            'username'                  => $this->string(255)->null(),
            'phone'                     => $this->string(255)->null(),
            'email'                     => $this->string(255)->null(),
            'file'                      => $this->string(255)->null(),
            'created_at'                => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
