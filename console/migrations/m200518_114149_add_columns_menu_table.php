<?php

use yii\db\Migration;

/**
 * Class m200518_114149_add_columns_menu_table
 */
class m200518_114149_add_columns_menu_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('menu', 'description', $this->text()->null());
        $this->addColumn('menu', 'image', $this->string(255)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('menu', 'description');
        $this->dropColumn('menu', 'image');
    }
}
