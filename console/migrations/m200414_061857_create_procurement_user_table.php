<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%procurement_user}}`.
 */
class m200414_061857_create_procurement_user_table extends Migration
{
    public $table               = 'procurement_user';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                    => $this->primaryKey(),
            'legal_name'            => $this->string(255)->null(),
            'bin'                   => $this->string(255)->null(),

            'postcode'              => $this->string(255)->null(),
            'city'                  => $this->string(255)->null(),
            'address'               => $this->string(255)->null(),


            'username'              => $this->string(255)->null(),
            'position'              => $this->string(255)->null(),
            'id_number'             => $this->string(255)->null(),
            'who_issued'            => $this->string(255)->null(),
            'date_issue'            => $this->string(255)->null(),
            'created_at'            => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
