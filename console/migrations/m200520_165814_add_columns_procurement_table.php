<?php

use yii\db\Migration;

/**
 * Class m200520_165814_add_columns_procurement_table
 */
class m200520_165814_add_columns_procurement_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('procurement', 'date', $this->timestamp()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('procurement', 'date');
    }
}
