<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%team}}`.
 */
class m200513_040759_create_team_table extends Migration
{
    public $table               = 'team';
    public $translationTable    = 'team_translation';
    public $managementTable     = 'management_structure';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'management_id'     => $this->integer()->null(),
            'username'          => $this->string(255)->null(),
            'position'          => $this->string(255)->null(),
            'content'           => $this->text()->null(),
            'image'             => $this->string(255)->null(),
            'status'            => $this->integer()->defaultValue(1),
            'created_at'        => $this->timestamp()->null(),
        ], $tableOptions);

        $this->createTable("{{{$this->translationTable}}}", [
            'id'            => $this->integer()->notNull(),
            'language'      => $this->string(16)->notNull(),
            'username'      => $this->string(255)->null(),
            'position'      => $this->string(255)->null(),
            'content'       => $this->text()->null(),
        ], $tableOptions);

        $this->addPrimaryKey("pk_{$this->translationTable}_id_language", "{{{$this->translationTable}}}", ['id', 'language']);
        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->translationTable}_{$this->table}", "{{{$this->translationTable}}}", 'id', "{{{$this->table}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->createIndex("idx_{$this->translationTable}_language", "{{{$this->translationTable}}}", 'language');

        $this->addForeignKey("fk_{$this->table}_{$this->managementTable}", "{{{$this->table}}}", 'management_id', "{{{$this->managementTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->managementTable}", "{{{$this->managementTable}}}");
        $this->dropForeignKey("fk_{$this->translationTable}_{$this->table}", "{{{$this->translationTable}}}");
        $this->dropTable("{{{$this->translationTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
