<?php

use yii\db\Migration;

/**
 * Class m200520_054123_add_columns_sliders_table
 */
class m200520_054123_add_columns_sliders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sliders', 'key', $this->string(255)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('sliders', 'key');
    }
}
