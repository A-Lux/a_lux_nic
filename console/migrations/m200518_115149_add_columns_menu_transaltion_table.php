<?php

use yii\db\Migration;

/**
 * Class m200518_115149_add_columns_menu_transaltion_table
 */
class m200518_115149_add_columns_menu_transaltion_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('menu_translation', 'description', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('menu_translation', 'description');
    }
}
