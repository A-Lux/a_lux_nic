<?php

use yii\db\Migration;

/**
 * Class m200424_064131_add_image_column_procurement_table
 */
class m200424_064131_add_image_column_procurement_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('procurement', 'image', $this->string(255)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('procurement', 'image');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200424_064131_add_image_column_procurement_table cannot be reverted.\n";

        return false;
    }
    */
}
