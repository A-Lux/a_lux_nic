<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "information_centre".
 *
 * @property int $id
 * @property int $status
 * @property string $title
 * @property string|null $content
 * @property string $image
 * @property string|null $date
 *
 * @property InformationCentreTranslation[] $informationCentreTranslations
 */
class InformationCentre extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'information_centre';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['title', 'image'], 'required'],
            [['content'], 'string'],
            [['date'], 'safe'],
            [['title'], 'string', 'max' => 128],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id' => 'ID',
            'status' => 'Status',
            'title' => 'Title',
            'content' => 'Content',
            'image' => 'Image',
            'date' => 'Date',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => InformationCentreTranslation::className(),
                'tableName'     => InformationCentreTranslation::tableName(),
                'attributes'    => ['title', 'content'],
            ],
        ]);
    }

    /**
     * Gets query for [[InformationCentreTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInformationCentreTranslations()
    {
        return $this->hasMany(InformationCentreTranslation::className(), ['id' => 'id']);
    }
}
