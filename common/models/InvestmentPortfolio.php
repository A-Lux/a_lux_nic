<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "investment_portfolio".
 *
 * @property int $id
 * @property string $title
 * @property string|null $content
 * @property string|null $created_at
 *
 * @property InvestmentPortfolioTranslation[] $investmentPortfolioTranslations
 */
class InvestmentPortfolio extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'investment_portfolio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['content'], 'string'],
            [['created_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'title'         => 'Заголовок',
            'content'       => 'Контент',
            'created_at'    => 'Дата создания',
        ]);
    }

    public function beforeSave($insert)
    {
        $this->created_at    = Yii::$app->formatter->asDate(time(), 'Y-MM-dd H:i');

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => InvestmentPortfolioTranslation::className(),
                'tableName'     => InvestmentPortfolioTranslation::tableName(),
                'attributes'    => ['title', 'content'],
            ],
        ]);
    }

    /**
     * Gets query for [[InvestmentPortfolioTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvestmentPortfolioTranslations()
    {
        return $this->hasMany(InvestmentPortfolioTranslation::className(), ['id' => 'id']);
    }

    public static function getOne()
    {
        return self::find()->orderBy(['id' => SORT_ASC])->one();
    }
}
