<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "documents_confidential_translation".
 *
 * @property int $id
 * @property string $language
 * @property string|null $name
 * @property string|null $title
 * @property string|null $content
 * @property string|null $file
 *
 * @property DocumentsConfidential $id0
 */
class DocumentsConfidentialTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'documents_confidential_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['content'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['title', 'name', 'file'], 'string', 'max' => 255],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentsConfidential::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'name' => 'Name',
            'file' => 'File',
        ];
    }

    /**
     * Gets query for [[Id0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(DocumentsConfidential::className(), ['id' => 'id']);
    }
}
