<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "language".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string|null $image
 */
class Language extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'language';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['name', 'image'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'    => 'ID',
            'name'  => 'Название',
            'code'  => 'Код',
            'image' => 'Изображение',
        ];
    }

    public static function languages()
    {
        return ArrayHelper::map(self::find()->all(), 'code', 'name');
    }

    public function path()
    {
        return Yii::getAlias('@backend') . '/web/uploads/images/language/';
    }

    public function getImage()
    {
        return isset($this->image) ? '/backend/web/uploads/images/language/' . $this->image : '';
    }

    public static function getAll()
    {
        return self::find()->all();
    }
}
