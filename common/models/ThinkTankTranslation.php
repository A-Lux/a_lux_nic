<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "think_tank_translation".
 *
 * @property int $id
 * @property string $language
 * @property string|null $title
 * @property string|null $description
 * @property string|null $content
 * @property string|null $metaName
 * @property string|null $metaDesc
 * @property string|null $metaKey
 *
 * @property ThinkTank $id0
 */
class ThinkTankTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'think_tank_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['description', 'content', 'metaDesc', 'metaKey'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['title', 'metaName'], 'string', 'max' => 255],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ThinkTank::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'title' => 'Title',
            'description' => 'Description',
            'content' => 'Content',
            'metaName' => 'Meta Name',
            'metaDesc' => 'Meta Desc',
            'metaKey' => 'Meta Key',
        ];
    }

    /**
     * Gets query for [[Id0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ThinkTank::className(), ['id' => 'id']);
    }
}
