<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "procurement_translation".
 *
 * @property int $id
 * @property string $language
 * @property string|null $name
 * @property string|null $description
 * @property string|null $content
 * @property string|null $file
 *
 * @property Procurement $id0
 */
class ProcurementTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'procurement_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['description', 'content'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['name', 'file'], 'string', 'max' => 255],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Procurement::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'name' => 'Name',
            'description' => 'Description',
            'content' => 'Content',
            'file' => 'File',
        ];
    }

    /**
     * Gets query for [[Id0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Procurement::className(), ['id' => 'id']);
    }
}
