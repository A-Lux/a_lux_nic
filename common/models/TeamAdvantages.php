<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "team_advantages".
 *
 * @property int $id
 * @property string $title
 * @property string|null $content
 * @property string|null $created_at
 *
 * @property TeamAdvantagesTranslation[] $teamAdvantagesTranslations
 */
class TeamAdvantages extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team_advantages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['content'], 'string'],
            [['created_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id' => 'ID',
            'title' => 'Title',
            'content' => 'Content',
            'created_at' => 'Created At',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => TeamAdvantagesTranslation::className(),
                'tableName'     => TeamAdvantagesTranslation::tableName(),
                'attributes'    => ['title', 'content'],
            ],
        ]);
    }

    /**
     * Gets query for [[TeamAdvantagesTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTeamAdvantagesTranslations()
    {
        return $this->hasMany(TeamAdvantagesTranslation::className(), ['id' => 'id']);
    }

    public static function getAll()
    {
        return self::find()->all();
    }
}
