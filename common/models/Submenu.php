<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "submenu".
 *
 * @property int $id
 * @property int|null $menu_id
 * @property string $title
 * @property string|null $content
 * @property string $url
 *
 * @property Menu $menu
 * @property SubmenuTranslation[] $submenuTranslations
 */
class Submenu extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'submenu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menu_id'], 'integer'],
            [['title', 'url'], 'required'],
            [['content'], 'string'],
            [['title', 'url'], 'string', 'max' => 128],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'title' => 'Title',
            'content' => 'Content',
            'url' => 'Url',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => SubmenuTranslation::className(),
                'tableName'     => SubmenuTranslation::tableName(),
                'attributes'    => ['title', 'content'],
            ],
        ]);
    }

    /**
     * Gets query for [[Menu]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    /**
     * Gets query for [[SubmenuTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubmenuTranslations()
    {
        return $this->hasMany(SubmenuTranslation::className(), ['id' => 'id']);
    }

    public static function getAll()
    {
        return self::find()->all();
    }

    public static function getKey()
    {
        $array = [];

        $submenus  = self::find()->all();
        foreach ($submenus as $menu){
            $array[$menu->url] = $menu;
        }

        return $array;
    }
}
