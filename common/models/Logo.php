<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "logo".
 *
 * @property int $id
 * @property int $position
 * @property string|null $image
 */
class Logo extends \yii\db\ActiveRecord
{
    const POSITION_HEADER       = 1;
    const POSITION_FOOTER       = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['position'], 'integer'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'position'  => 'Позиция',
            'image'     => 'Изображение',
        ];
    }

    /**
     * @return array
     */
    public static function positions()
    {
        return [
            self::POSITION_HEADER       => 'header',
            self::POSITION_FOOTER       => 'footer',
        ];
    }

    /**
     * @return array
     */
    public static function positionDescription()
    {
        return [
            self::POSITION_HEADER       => 'Шапка',
            self::POSITION_FOOTER       => 'Футер',
        ];
    }

    public function path()
    {
        return Yii::getAlias('@backend') . '/web/uploads/images/logo/';
    }

    public function getImage()
    {
        return isset($this->image) ? '/backend/web/uploads/images/logo/' . $this->image : '';
    }

    public static function getHeader()
    {
        return self::find()->where(['position' => self::POSITION_HEADER])->orderBy(['id' => SORT_ASC])->one();
    }

    public static function getFooter()
    {
        return self::find()->where(['position' => self::POSITION_FOOTER])->orderBy(['id' => SORT_ASC])->one();
    }
}
