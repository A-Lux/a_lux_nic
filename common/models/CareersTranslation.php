<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "careers_translation".
 *
 * @property int $id
 * @property string $language
 * @property string|null $title
 * @property string|null $content
 *
 * @property Careers $id0
 */
class CareersTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'careers_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['content'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['title'], 'string', 'max' => 255],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Careers::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'title' => 'Title',
            'content' => 'Content',
        ];
    }

    /**
     * Gets query for [[Id0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Careers::className(), ['id' => 'id']);
    }
}
