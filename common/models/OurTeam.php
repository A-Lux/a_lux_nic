<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "our_team".
 *
 * @property int $id
 * @property string $title
 * @property string|null $content
 * @property string|null $created_at
 *
 * @property OurTeamTranslation[] $ourTeamTranslations
 */
class OurTeam extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'our_team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['content'], 'string'],
            [['created_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id' => 'ID',
            'title' => 'Title',
            'content' => 'Content',
            'created_at' => 'Created At',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => OurTeamTranslation::className(),
                'tableName'     => OurTeamTranslation::tableName(),
                'attributes'    => ['title', 'content'],
            ],
        ]);
    }

    /**
     * Gets query for [[OurTeamTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOurTeamTranslations()
    {
        return $this->hasMany(OurTeamTranslation::className(), ['id' => 'id']);
    }

    public static function getOne()
    {
        return self::find()->orderBy(['id' => SORT_ASC])->one();
    }
}
