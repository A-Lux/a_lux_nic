<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "management_structure_translation".
 *
 * @property int $id
 * @property string $language
 * @property string|null $name
 * @property string|null $content
 *
 * @property ManagementStructure $id0
 */
class ManagementStructureTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'management_structure_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['content'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['name'], 'string', 'max' => 128],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ManagementStructure::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'name' => 'Name',
            'content' => 'Content',
        ];
    }

    /**
     * Gets query for [[Id0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ManagementStructure::className(), ['id' => 'id']);
    }
}
