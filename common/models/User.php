<?php
namespace common\models;

use common\modules\user\models\BaseUser;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string  $email
 * @property string  $auth_key
 * @property string  $password_hash
 * @property string  $token
 * @property integer $status
 * @property integer $role
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends \common\modules\user\models\User
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

//    public function beforeSave($insert)
//    {
//        if (parent::beforeSave($insert)) {
//            if ($insert) {
//                if(isset($this->password)){
//                    $this->setPassword($this->password);
//                }elseif(empty($this->password_hash)) {
//                    $this->setPassword(\Yii::$app->security->generateRandomString());
//                }
//                $this->generateAuthKey();
//            }
//            return true;
//        }
//
//        return false;
//    }

    public function getPassword()
    {
        return '';
    }

    public static function getOne()
    {
        $model  = self::findOne(['id' => Yii::$app->user->identity->id, 'status' => self::STATUS_ACTIVE]);

        return $model;
    }

}
