<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "procurement_user".
 *
 * @property int $id
 * @property string|null $legal_name
 * @property string|null $bin
 * @property string|null $postcode
 * @property string|null $city
 * @property string|null $address
 * @property string|null $username
 * @property string|null $position
 * @property string|null $id_number
 * @property string|null $who_issued
 * @property string|null $date_issue
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $created_at
 */
class ProcurementUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'procurement_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['legal_name', 'bin', 'postcode', 'city', 'address', 'username', 'position', 'id_number', 'who_issued', 'date_issue', 'phone', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'legal_name'    => 'Наименование лица',
            'bin'           => 'БИН/ИИН',
            'postcode'      => 'Почтовый индекс',
            'city'          => 'Город',
            'address'       => 'Адрес',
            'username'      => 'Имя, фамилия',
            'position'      => 'Должность',
            'id_number'     => 'Номер удостоверения личности',
            'who_issued'    => 'Кем выдано',
            'date_issue'    => 'Дата выдачи',
            'phone'         => 'Контактный телефон',
            'email'         => 'Email',
            'created_at'    => 'Дата создания',
        ];
    }
}
