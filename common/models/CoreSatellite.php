<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "core_satellite".
 *
 * @property int $id
 * @property string $title
 * @property string|null $content
 * @property string|null $created_at
 *
 * @property CoreSatelliteTranslation[] $coreSatelliteTranslations
 */
class CoreSatellite extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_satellite';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['content'], 'string'],
            [['created_at'], 'safe'],
            [['title'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'title'         => 'Заголовок',
            'content'       => 'Контент',
            'created_at'    => 'Дата создания',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => CoreSatelliteTranslation::className(),
                'tableName'     => CoreSatelliteTranslation::tableName(),
                'attributes'    => ['title', 'content'],
            ],
        ]);
    }

    public function beforeSave($insert)
    {
        $this->created_at    = Yii::$app->formatter->asDate(time(), 'Y-MM-dd H:i');

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * Gets query for [[CoreSatelliteTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCoreSatelliteTranslations()
    {
        return $this->hasMany(CoreSatelliteTranslation::className(), ['id' => 'id']);
    }

    public static function getAll()
    {
        return self::find()->all();
    }
}
